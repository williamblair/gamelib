#include <GameTimer.h>

namespace GameLib
{
namespace PS3
{

GameTimer::GameTimer()
{}

GameTimer::~GameTimer()
{}

static inline float diffSeconds(timeval* cur, timeval* prev)
{
    return (float)(cur->tv_sec - prev->tv_sec) +
        ((float)(cur->tv_usec - prev->tv_usec)) / 1000000.0f;
}

float GameTimer::Update()
{
    struct timeval curTime;
    gettimeofday(&curTime, 0);
    
    
    float dt = diffSeconds(&curTime, &mLastTime);
    
    mLastTime.tv_sec = curTime.tv_sec;
    mLastTime.tv_usec = curTime.tv_usec;
    
    return dt;
}

} // namespace PS3
} // namespace GameLib

