#include <Texture.h>
#include <stdio.h>

#include <fstream>
#include <vector>

namespace GameLib
{
namespace PS3
{

namespace Targa
{

// imageDesc bitmasks,
// bit 4 is left-to-right ordering,
// bit 5 is top-to-bottom ordering
enum IMAGE_ORIENTATIONS
{
    BOTTOM_LEFT  = 0x00,    // first px is bottom left
    BOTTOM_RIGHT = 0x10,    // first px is bottom right
    TOP_LEFT     = 0x20,    // first px is top left
    TOP_RIGHT    = 0x30     // first px is top right
};

enum FILE_TYPES
{
    TFT_NO_DATA = 0,
    TFT_INDEXED = 1,
    TFT_RGB = 2,
    TFT_GRAYSCALE = 3,
    TFT_RLE_INDEXED = 9,
    TFT_RLE_RGB = 10,
    TFT_RLE_GRAYSCALE = 11
};

struct Header
{
    uint8_t idLength;
    uint8_t colorMapType;
    uint8_t imageTypeCode;
    uint8_t colorMapSpec[5];
    uint16_t xOrigin;
    uint16_t yOrigin;
    uint16_t width;
    uint16_t height;
    uint8_t bpp;        // bits per pixel
    uint8_t imageDesc;
};

// little endian to big endian
static inline uint16_t lend2bend16(uint16_t lend)
{
    char* ptr = (char*)&lend;
    return (ptr[1] << 8) | 
           (ptr[0]);
}

bool LoadUncompressed(std::ifstream& inFile,
    std::vector<uint8_t>& imageData,
    unsigned int bytesPerPixel)
{
    unsigned int imageSize = imageData.size();
    inFile.read((char*)imageData.data(), imageSize);

    // swap R and B components (BGR --> RGB)
    for (unsigned int swap = 0; swap < imageSize; swap += bytesPerPixel)
    {
        uint8_t cswap = imageData[swap];
        imageData[swap] = imageData[swap+2];
        imageData[swap+2] = cswap;
    }

    return true;
}

bool LoadCompressed(std::ifstream& inFile,
    std::vector<uint8_t>& imageData,
    unsigned int bytesPerPixel,
    unsigned int width, unsigned int height)
{
    unsigned int pixelCount = height * width;
    unsigned int curPixel = 0;
    unsigned int curByte = 0;

    std::vector<uint8_t> colorBuffer(bytesPerPixel);

    do
    {
        uint8_t chunkHeader = 0;
        inFile.read((char*)&chunkHeader, sizeof(uint8_t)); // read in a single byte

        if (chunkHeader < 128)
        {
            chunkHeader++;

            for (short counter = 0; counter < chunkHeader; counter++)
            {
                // read a color chunk
                inFile.read((char*)colorBuffer.data(), bytesPerPixel);

                imageData[curByte+0] = colorBuffer[2];
                imageData[curByte+1] = colorBuffer[1];
                imageData[curByte+2] = colorBuffer[0];

                if (bytesPerPixel == 4) {
                    imageData[curByte+3] = colorBuffer[3];
                }

                curByte += bytesPerPixel;
                curPixel++;

                // mismatch between sizes
                if (curPixel > pixelCount) {
                    return false;
                }
            }
        }
        // chunkHeader >= 128
        else
        {
            chunkHeader -= 127;

            // read a color chunk
            inFile.read((char*)colorBuffer.data(), bytesPerPixel);

            for (short counter = 0; counter < chunkHeader; counter++)
            {
                imageData[curByte+0] = colorBuffer[2];
                imageData[curByte+1] = colorBuffer[1];
                imageData[curByte+2] = colorBuffer[0];
                
                if (bytesPerPixel == 4) {
                    imageData[curByte+3] = colorBuffer[3];
                }

                curByte += bytesPerPixel;
                curPixel++;

                // size mismatch
                if (curPixel > pixelCount) {
                    return false;
                }
            }
        }

    } while (curPixel < pixelCount);

    return true;
}

bool IsImageTypeSupported(const Header& header)
{
    // only support color images currently
    return ((header.imageTypeCode == TFT_RGB ||
             header.imageTypeCode == TFT_RLE_RGB) &&
             header.colorMapType == 0);
}

bool IsCompressed(const Header& header)
{
    return (header.imageTypeCode == TFT_RLE_RGB ||
            header.imageTypeCode == TFT_RLE_GRAYSCALE);
}

bool IsUncompressed(const Header& header)
{
    return (header.imageTypeCode == TFT_RGB ||
            header.imageTypeCode == TFT_GRAYSCALE);
}

void FlipImageVertically(std::vector<uint8_t>& imageData,
    u32 width, u32 height,
    u32 bytesPerPixel)
{
    std::vector<uint8_t> flippedData(imageData.size());
    size_t flippedIndex = 0;
    int step = bytesPerPixel;
    for (int row = height - 1; row >= 0; --row) {
        for (int col = 0; col < int(width) * step; ++col) {
            uint8_t* rowData = &imageData[row * width * step];

            for (unsigned int i = 0; i < width * step; ++i) {
                flippedData[flippedIndex] = *rowData;
                flippedIndex++;
                rowData++;
            } 
        }
    }

    imageData.assign(flippedData.begin(), flippedData.end());
}

bool Load(const char* fileName,
    std::vector<uint8_t>& imageData,
    u32& width,
    u32& height,
    u32& bytesPerPixel)
{
    Header header;
    
    std::ifstream inFile(fileName, std::ios::binary);
    if (!inFile.is_open()) {
        printf("failed to open targa image file: %s\n", 
            fileName);
        return false;
    }

    inFile.read((char*)&header, sizeof(Header));
    if (!IsImageTypeSupported(header)) {
        printf("unsupported targa type\n");
        return false;
    }

    //uint16_t width = lend2bend16(header.width);
    //uint16_t height = lend2bend16(header.height);
    width = (u32)lend2bend16(header.width);
    height = (u32)lend2bend16(header.height);

    //uint8_t bitsPerPixel = header.bpp;
    //uint8_t bytesPerPixel = header.bpp / 8;
    bytesPerPixel = (u32)header.bpp / 8;

    if (bytesPerPixel < 3) {
        printf("unsupported bytesPerPixel: %u\n",  
            bytesPerPixel);
        return false;
    }

    printf("Targa::Image bytes per pixel: %u\n", bytesPerPixel);
    printf("Targa::Image image size: %u, %u\n", width, height);
    unsigned int imageSize = width * height * bytesPerPixel;
    imageData.resize(imageSize);

    // skip past the id if there is one
    if (header.idLength > 0) {
        inFile.ignore(header.idLength);
    }

    bool result = false;

    if (IsUncompressed(header)) {
        result = LoadUncompressed(inFile, imageData, bytesPerPixel);
    }
    else {
        result = LoadCompressed(inFile, imageData, bytesPerPixel, width, height);
    }
    if (!result) {
        return false;
    }

    if ((header.imageDesc & TOP_LEFT) == TOP_LEFT) {
        FlipImageVertically(imageData, width, height, bytesPerPixel);
    }

    /*return Texture::Init( context,
                          imageData.data(),
                          width,
                          height,
                          bytesPerPixel );*/
    printf("Targa::Load return true\n");
    return true;
}

} // end namespace TGA

Texture::Texture() :
    mRsxMem(nullptr),
    mRsxTexOffset(0),
    mWidth(0),
    mHeight(0)
{}

Texture::~Texture()
{}

bool Texture::Init(const char* data, int width, int height, int bytesPerPixel)
{
    mWidth  = (u32)width;
    mHeight = (u32)height;

    // force 4 bytes per pixel
    mRsxMem = tiny3d_AllocTexture(width*height*4);
    if (!mRsxMem) {
        printf("Failed to alloc rsx texture mem\n");
        return false;
    }
    mRsxTexOffset = tiny3d_TextureOffset(mRsxMem);
    
    printf("Texture::init width, height, bpp: %u, %u, %u\n", width, height, bytesPerPixel);
    
    u8* buffer = (u8*)mRsxMem;
    for (int i = 0; i < width * height * 4; i += 4)
    {
        buffer[i+1] = *data++; // r
        buffer[i+2] = *data++; // g
        buffer[i+3] = *data++; // b
        if (bytesPerPixel == 4) {
            buffer[i+0] = *data++; // a
        }
        else {
            buffer[i+0] = 255;
        }
    }
    
#if 0
    // force 4 bytes per pixel
    mBuffer = (u32*)rsxMemalign(128, width*height*4);
    if (!mBuffer) {
        printf("Failed to alloc tex buffer\n");
        return false;
    }
    rsxAddressToOffset(mBuffer, &mOffset);
#endif
    
    return true;
}

bool Texture::LoadFromTGA(const char* fileName)
{
    std::vector<uint8_t> imageData;
    u32 bytesPerPixel;
    if (!Targa::Load(
        fileName,
        imageData,
        mWidth, mHeight,
        bytesPerPixel))
    {
        printf("Failed to load from TGA\n");
        return false;
    }
    return Init((const char*)imageData.data(), (int)mWidth, (int)mHeight, (int)bytesPerPixel);
}

} // namespace PS3
} // namespace GameLib

