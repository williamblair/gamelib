#ifndef GAMELIB_PS3_TEXTURE_H_INCLUDED
#define GAMELIB_PS3_TEXTURE_H_INCLUDED

#include <tiny3d.h>

namespace GameLib
{
namespace PS3
{

// forward declaration
class Renderer;

class Texture
{
friend class Renderer;
    
public:
    
    Texture();
    ~Texture();
    
    bool Init(const char* data, int width, int height, int bytesPerPixel);
    
    bool LoadFromTGA(const char* fileName);
    
private:
    void* mRsxMem;
    u32 mRsxTexOffset;
    u32 mWidth;
    u32 mHeight;
};

} // namespace PS3
} // namespace GameLib

#endif // GAMELIB_PS3_TEXTURE_H_INCLUDED

