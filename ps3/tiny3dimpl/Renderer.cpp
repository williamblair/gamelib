#include <Renderer.h>
#include <stdio.h>

namespace GameLib
{
namespace PS3
{

Renderer::Renderer()
{}

Renderer::~Renderer()
{}

bool Renderer::Init(int width, int height, const char* title)
{
    (void)width;
    (void)height;
    (void)title;

    // 4MB vertex data
    if (tiny3d_Init(TINY3D_Z16 | 4*1024*1024) < 0) {
        printf("Error init tiny3D\n");
        return false;
    }

    mProjMat = GameMath::Perspective(
        GameMath::Deg2Rad(60.0f), // fov radians
        640.0f/480.0f, // aspect
        /*1.0f*/0.00125f, // near
        /*500.0f*/300.0f // far
    );
    // Test
    //mProjMat.r2c3 = 1.0f / mProjMat.r2c3;
    //mProjMat = GameMath::Transpose(mProjMat);
    /*mProjMat = GameMath::Perspective(
        GameMath::Deg2Rad(60.0f), // fov radians
        640.0f/480.0f, // aspect
        0.00125f, // near
        300.0f // far
    );*/
    mProjMatTiny3d = gameMath2TinyProjMat(mProjMat);

    return true;
}

void Renderer::Clear()
{
    // alpha is first in color (argb)
    // TODO - set bg color
    tiny3d_Clear(0xFF008080, TINY3D_CLEAR_ALL);

    // enable alpha test and alpha blending
    blend_src_func srcFun = (blend_src_func)(TINY3D_BLEND_FUNC_SRC_RGB_SRC_ALPHA | TINY3D_BLEND_FUNC_SRC_ALPHA_SRC_ALPHA);
    blend_dst_func dstFun = (blend_dst_func)(TINY3D_BLEND_FUNC_DST_RGB_ONE_MINUS_SRC_ALPHA | TINY3D_BLEND_FUNC_DST_ALPHA_ZERO);
    blend_func blndFun = (blend_func)(TINY3D_BLEND_RGB_FUNC_ADD | TINY3D_BLEND_ALPHA_FUNC_ADD);
    tiny3d_AlphaTest(1, 0x10, TINY3D_ALPHA_FUNC_GEQUAL);
    tiny3d_BlendFunc(1, srcFun, dstFun, blndFun);

    // Set 3d mode
    tiny3d_Project3D();
}

void Renderer::Update()
{
    tiny3d_Flip();
}

void Renderer::DrawVertexBuffer(
    GameMath::Mat4& modelMat,
    GameMath::Mat4& viewMat,
    VertexBuffer& vb)
{
    // Set model,view,projection matrix
    //GameMath::Mat4 modelView = GameMath::Transpose(viewMat * modelMat);
    GameMath::Mat4 modelView = viewMat * modelMat;
    GameMath::Mat4 mvpMat = mProjMat * modelView;
    //tiny3d_SetProjectionMatrix((MATRIX*)&mProjMatTiny3d);
    //tiny3d_SetMatrixModelView((MATRIX*)&modelViewTiny3d);
    tiny3d_SetProjectionMatrix((MATRIX*)&mProjMat);
    tiny3d_SetMatrixModelView((MATRIX*)&modelView);

    // Set polygon type
    tiny3d_SetPolygon(TINY3D_TRIANGLES);

    float vertWInv;

    // Draw vertices
    if (vb.mIndicesSize > 0)
    {
        switch (vb.mType)
        {
        case VertexBuffer::POS_COLOR:
        {
            const size_t floatsPerVertex = 6; // xyz rgb
            for (size_t i=0; i<vb.mIndicesSize; ++i)
            {
                const size_t floatIndex = vb.mIndices[i]*floatsPerVertex;
                float* p = &vb.mVertices[floatIndex];
                // the tiny3d vertex shader multiplies z by the result vertex w; this should invert that...
                vertWInv = 1.0f / (p[2]*mvpMat.r3c2 + 1.0f*mvpMat.r3c3);
                // Draw vertices (position must be first)
                tiny3d_VertexPos(p[0], p[1], p[2] * vertWInv);
                tiny3d_VertexFcolor(p[3], p[4], p[5], 1.0f);
            }
            break;
        }
        case VertexBuffer::POS_TEXCOORD:
        {
            const size_t floatsPerVertex = 5; // xyz uv
            for (size_t i=0; i<vb.mIndicesSize; ++i)
            {
                const size_t floatIndex = vb.mIndices[i]*floatsPerVertex;
                float* p = &vb.mVertices[floatIndex];
                // the tiny3d vertex shader multiplies z by the result vertex w; this should invert that...
                vertWInv = 1.0f / (p[2]*mvpMat.r3c2 + 1.0f*mvpMat.r3c3);
                // Draw vertices (position must be first)
                tiny3d_VertexPos(p[0], p[1], p[2] * vertWInv);
                tiny3d_VertexTexture(p[3], p[4]);
            }
            break;
        }
        default:
            printf("ERROR - unhandled vertexbuffer type\n");
            break;
        }
    }
    else
    {
        switch (vb.mType)
        {
        case VertexBuffer::POS_COLOR:
        {
            const size_t floatsPerVertex = 6; // xyz rgb
            for (size_t i=0; i<vb.mVerticesSize; i+=floatsPerVertex)
            {
                float* p = &vb.mVertices[i];
                // the tiny3d vertex shader multiplies z by the result vertex w; this should invert that...
                vertWInv = 1.0f / (p[2]*mvpMat.r3c2 + 1.0f*mvpMat.r3c3);
                // Draw vertices (position must be first)
                tiny3d_VertexPos(p[0], p[1], p[2] * vertWInv);
                tiny3d_VertexFcolor(p[3], p[4], p[5], 1.0f);
            }
            break;
        }
        case VertexBuffer::POS_TEXCOORD:
        {
            const size_t floatsPerVertex = 5; // xyz uv
            for (size_t i=0; i<vb.mVerticesSize; i+=floatsPerVertex)
            {
                float* p = &vb.mVertices[i];
                // the tiny3d vertex shader multiplies z by the result vertex w; this should invert that...
                vertWInv = 1.0f / (p[2]*mvpMat.r3c2 + 1.0f*mvpMat.r3c3);
                // Draw vertices (position must be first)
                tiny3d_VertexPos(p[0], p[1], p[2] * vertWInv);
                tiny3d_VertexTexture(p[3], p[4]);
            }
            break;
        }
        default:
            printf("ERROR - unhandled vertexbuffer type\n");
            break;
        }
    }

    tiny3d_End();

    // here projection, modelview, texture can be changed and tiny3d_setpolygon can
    // be called again to start drawing more vertices
}

void Renderer::DrawAabb(AABB& aabb, GameMath::Mat4& viewMat)
{
    // TODO
    (void)aabb;
    (void)viewMat;
}

void Renderer::SetTexture(Texture& tex)
{
    tiny3d_SetTexture(
        0, // Unit = 0 for TEX0, 1 for TEX1, ...
        tex.mRsxTexOffset, // rsx texture offset
        tex.mWidth, tex.mHeight, // image width, height
        tex.mWidth*4, // stride = width*4 for 32bit color
        TINY3D_TEX_FORMAT_A8R8G8B8, // argb color
        0 // smooth = 1 or 0
    );
}

} // namespace PS3
} // namespace GameLib

