#ifndef GAMELIB_PS3_VERTEX_BUFFER_H_INCLUDED
#define GAMELIB_PS3_VERTEX_BUFFER_H_INCLUDED

#include <stddef.h>
#include <string.h>

namespace GameLib
{
namespace PS3
{

class VertexBuffer
{

friend class Renderer;

public:
    
    enum Type
    {
        POS_COLOR,
        POS_TEXCOORD,
        UNINITIALIZED
    };

    VertexBuffer();
    ~VertexBuffer();

    
    bool Init(
        Type type,
        float* vertices,
        size_t verticesSize,
        int* indices,
        size_t indicesSize
    );
    
    bool UpdateVertices(float* vertices, size_t verticesSize);

private:
    int* mIndices;
    size_t mIndicesSize;
    
    float* mVertices;
    size_t mVerticesSize;
    
    Type mType;
};

} // namespace PS3
} // namespace GameLib

#endif // VERTEX_BUFFER_H_INCLUDED

