#ifndef GAMELIB_PS3_TILE_FLOOR_H_INCLUDED
#define GAMELIB_PS3_TILE_FLOOR_H_INCLUDED

#include <Texture.h>
#include <VertexBuffer.h>
#include <Renderer.h>

namespace GameLib
{
namespace PS3
{

class TileFloor
{
public:
    TileFloor();
    ~TileFloor();

    bool Init();

    void SetTexture(Texture* tex) { mTexture = tex; }
    void SetPosition(GameMath::Vec3 pos) { mPosition = pos; }
    void SetScale(float scale) { mScale = GameMath::Vec3(scale, scale, scale); }

    bool Draw(GameMath::Mat4& viewMat, Renderer& render);

private:
    VertexBuffer mVertBuf;
    static float sVertices[6*5]; // 2 triangles (3*2=6) * 5 floats per vertex
    GameMath::Vec3 mPosition; // top left coords
    GameMath::Vec3 mScale;
    Texture* mTexture;
};

} // namespace PS3
} // namespace GameLib

#endif // GAMELIB_PS3_TILE_FLOOR_H_INCLUDED

