#ifndef GAMELIB_PS3_RENDERER_H_INCLUDED
#define GAMELIB_PS3_RENDERER_H_INCLUDED

#include <tiny3d.h>

#include <GameMath/GameMath.h>
#include <VertexBuffer.h>
#include <Texture.h>
#include <AABB.h>

namespace GameLib
{
namespace PS3
{

class Renderer
{
public:

    Renderer();
    ~Renderer();
    
    bool Init(int width, int height, const char* title);
    
    void Clear();
    void Update();
    
    void DrawVertexBuffer(
        GameMath::Mat4& modelMat,
        GameMath::Mat4& viewMat,
        VertexBuffer& vb
    );
    void DrawAabb(AABB& aabb, GameMath::Mat4& viewMat);
    
    void SetTexture(Texture& tex);

    GameMath::Mat4& GetProjMat() { return mProjMat; }
    
private:
    
    GameMath::Mat4 mProjMat;
    GameMath::Mat4 mProjMatTiny3d;

    // tiny3d uses left-handed (+x=right, +y=up, z=away from player)
    // GameMath uses right-handed (+x=right, +y=up, z=towards player)
    inline GameMath::Mat4 gameMath2TinyProjMat(GameMath::Mat4& mat) {
        /*GameMath::Mat4 result = Transpose(mat);
        result.v[1][1] = -result.v[1][1];
        result.v[2][2] = -result.v[2][2];
        result.v[3][2] = -result.v[3][2];
        result.v[2][3] = -result.v[2][3] * 0.5f;
        return result;*/
        
        GameMath::Mat4 res(mat);
        // assumes near=1.0f, far=500.0f
        const float near = /*1.0f*/0.00125f, far = /*500.0f*/300.0f;
        const float cotan = res.v[1][1];
        res.v[1][1] = -cotan;
        res.v[2][2] = 1.0f;
        res.v[2][3] = (near * far) / (far - near);
        res.v[3][2] = 1.0f;
        return res;
    }
    inline GameMath::Mat4 gameMath2TinyModelViewMat(GameMath::Mat4& mat) {
        /*GameMath::Mat4 result = mat;
        result.v[3][2] = -result.v[3][2];
        return result;*/
        /*GameMath::Mat4 res(mat);
        res.v[0][0] = -res.v[0][0];
        res.v[3][0] = -res.v[3][0];
        res.v[2][1] = -res.v[2][1];
        res.v[2][2] = -res.v[2][2];
        return res;*/
        GameMath::Mat4 result(mat);
        //result.v[2][0] = -result.v[2][0];
        //result.v[2][1] = -result.v[2][1];
        result.v[2][2] = -result.v[2][2];
        //result.v[3][0] = -result.v[3][0];
        //result.v[3][1] = -result.v[3][1];
        result.v[3][2] = -result.v[3][2];
        return result;
    }
};

} // namespace PS3
} // namespace GameLib

#endif // GAMELIB_PS3_RENDERER_H_INCLUDED

