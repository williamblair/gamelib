#ifndef GAMELIB_PS3_THIRD_PERSON_PLAYER_H_INCLUDED
#define GAMELIB_PS3_THIRD_PERSON_PLAYER_H_INCLUDED

#include <stdio.h>

#include <Renderer.h>
#include <Camera.h>
#include <Texture.h>
#include <IqmMesh.h>
#include <AABB.h>

namespace GameLib
{
namespace PS3
{

class ThirdPersonPlayer
{
public:
    ThirdPersonPlayer();
    ~ThirdPersonPlayer();

    void SetModel(IqmMesh* model) {
        mModel = model;
        GameMath::Vec3 targetOffs(0.0f, 0.0f, 0.0f);
        targetOffs.y = 5.0f;//model->GetFrameRadius(0);
        mCamera.SetTargetOffset(targetOffs);
        AABB& meshAabb = model->GetAabb();
        mAabb.min = meshAabb.min;
        mAabb.max = meshAabb.max;
        mAabb.pos = meshAabb.pos;
    }
    void SetModelScale(const GameMath::Vec3& scale) { mModelScale = scale; }
    // Model rotation offset (for if it's facing by default a different direction)
    void SetDrawYawOffset(const float yawDegrees) { mDrawYawOffs = yawDegrees; }
    IqmMesh* GetModel() { return mModel; }
    
    void SetTexture(Texture* tex) { mTexture = tex; }
    Texture* GetTexture() { return mTexture; }

    GameMath::Mat4& GetViewMat() { return mCamera.GetViewMat(); }
    GameMath::Mat4& GetModelMat() { return mModelMat; }

    OrbitCamera& GetCamera() { return mCamera; }

    GameMath::Vec3& GetPosition() { return mPosition; }
    AABB& GetAabb() { return mAabb; }

    void Update(const float dt) {
        if (mModel == nullptr) {
            printf("ThirdPersonPlayer update: mModel null\n");
            return;
        }
        // 32 fps anim
        mModel->Update(dt * 32.0f);
        mCamera.Update();
        // assumes scale is uniform (x == y == z)
        mAabb.min = mModelScale.x * mModel->GetAabb().min;
        mAabb.max = mModelScale.x * mModel->GetAabb().max;
        mAabb.pos = mMoveComp.position;
    }

    // Move forward based on x/y values of analog stick
    void Move(const float x, const float y, const float amount);

    void Draw(Renderer& render)
    {
        if (mModel == nullptr ||
            mTexture == nullptr)
        {
            printf("ThirdPersonPlayer Draw: mModel/mTexture null\n");
            return;
        }
        render.SetTexture(*mTexture);
        mModel->DrawVertexBuffer(mModelMat, GetViewMat(), render, 0);
        //render.DrawAabb(mAabb, GetViewMat());
    }

private:
    IqmMesh* mModel;
    Texture* mTexture;
    GameMath::Mat4 mModelMat;
    GameMath::Vec3 mPosition;
    GameMath::Vec3 mModelScale;
    AABB mAabb;
    MoveComponent mMoveComp;
    OrbitCamera mCamera;
    float mDrawYawOffs;
    float mDrawPitchOffs;
};

} // namespace GameLib
} // namespace PS3

#endif // GAMELIB_PS3_THIRD_PERSON_PLAYER_H_INCLUDED

