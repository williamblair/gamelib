#ifndef GAMELIB_PS3_GAME_TIMER_H_INCLUDED
#define GAMELIB_PS3_GAME_TIMER_H_INCLUDED

#include <sys/time.h>

namespace GameLib
{
namespace PS3
{

class GameTimer
{
public:

    GameTimer();
    ~GameTimer();
    
    // returns delta time in seconds
    float Update();

private:
    struct timeval mLastTime = {0,0};
};

} // namespace PS3
} // namespace GameLib

#endif // GAMELIB_WII_GAME_TIMER_H_INCLUDED

