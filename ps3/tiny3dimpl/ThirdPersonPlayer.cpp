#include <ThirdPersonPlayer.h>

namespace GameLib
{
namespace PS3
{

ThirdPersonPlayer::ThirdPersonPlayer() :
    mModel(nullptr),
    mTexture(nullptr),
    mDrawYawOffs(180.0f),
    mDrawPitchOffs(-90.0f)
{
    mCamera.SetMoveComponent(&mMoveComp);
    mCamera.SetDistance(15.0f);
    mPosition = GameMath::Vec3(0.0f, 0.0f, 0.0f);
    mModelScale = GameMath::Vec3(0.1f, 0.1f, 0.1f);
    
    mModelMat =
        GameMath::Translate(
            mMoveComp.position.x,
            mMoveComp.position.y,
            mMoveComp.position.z
        ) *
        GameMath::Rotate(GameMath::Deg2Rad(mMoveComp.yaw + mDrawYawOffs),
            GameMath::Vec3(0.0f, 1.0f, 0.0f)) *
        GameMath::Rotate(GameMath::Deg2Rad(mDrawPitchOffs),
            GameMath::Vec3(1.0f, 0.0f, 0.0f)) *
        GameMath::Scale(mModelScale.x, mModelScale.y, mModelScale.z);
}

ThirdPersonPlayer::~ThirdPersonPlayer()
{}


void ThirdPersonPlayer::Move(
    const float x,
    const float y,
    const float amount)
{
    // Put the model animation to idle
    if (fabsf(x) < 0.300f && fabsf(y) < 0.300f) {
        mModel->SetAnim("idle");
        return;
    }
    
    const float xyLen = sqrtf(x*x + y*y);
    const float normX = x / xyLen;
    const float normY = y / xyLen;
    
    mMoveComp.yaw = GameMath::Rad2Deg(atan2f(normY, normX));
    mMoveComp.yaw += mCamera.GetYaw();
    // by default we want to face forward down the z axis (-z), not the X axis
    mMoveComp.yaw += 90.0f;
    while (mMoveComp.yaw >= 360.0f) { mMoveComp.yaw -= 360.0f; }
    while (mMoveComp.yaw <= 0.0f) { mMoveComp.yaw += 360.0f; }
    
    mMoveComp.pitch = 0.0f; // no pitch allowed
    mMoveComp.Update();
    mMoveComp.MoveForward(amount);
    mModelMat =
        GameMath::Translate(
            mMoveComp.position.x,
            mMoveComp.position.y,
            mMoveComp.position.z
        ) *
        GameMath::Rotate(GameMath::Deg2Rad(mMoveComp.yaw + mDrawYawOffs),
            GameMath::Vec3(0.0f, 1.0f, 0.0f)) *
        GameMath::Rotate(GameMath::Deg2Rad(mDrawPitchOffs),
            GameMath::Vec3(1.0f, 0.0f, 0.0f)) *
        GameMath::Scale(mModelScale.x, mModelScale.y, mModelScale.z);
        

    // TODO - not necessary...
    mCamera.SetMoveComponent(&mMoveComp);

    // Put the model animation to move; will do nothing if already set to move
    mModel->SetAnim("walk");

    // save position
    mPosition = mMoveComp.position;
}

} // namespace PS3
} // namespace GameLib

