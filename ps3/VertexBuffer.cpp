#include <VertexBuffer.h>
#include <stdio.h>

namespace GameLib
{
namespace PS3
{

VertexBuffer::VertexBuffer() :
    mIndices(nullptr),
    mIndicesSize(0),
    mVertices(nullptr),
    mVerticesSize(0),
    mType(UNINITIALIZED)
{}

VertexBuffer::~VertexBuffer()
{
    //delete[] mVertices; mVertices = nullptr;
    //delete[] mIndices; mIndices = nullptr;
    free(mVertices); mVertices = nullptr;
    free(mIndices); mIndices = nullptr;
}

bool VertexBuffer::Init(
    Type type,
    float* vertices,
    size_t verticesSize,
    int* indices,
    size_t indicesSize)
{
    // from Renderer.cpp
    /*const size_t maxIndices = 200;
    const size_t maxVertexFloats = 200;
    if (indicesSize > maxIndices) {
        printf("indicesSize > maxIndices\n");
        return false;
    }
    if (verticesSize > maxVertexFloats) {
        printf("verticesSize > maxVertexFloats\n");
        return false;
    }*/

    mType = type;
    //mVertices = new float[verticesSize];
    mVertices = (f32*)rsxMemalign(128, verticesSize*sizeof(f32));
    if (!mVertices) { printf("Failed to alloc vertices mem\n"); return false; }
    memcpy(mVertices, vertices, verticesSize * sizeof(float));
    mVerticesSize = verticesSize;
    
    if (indicesSize > 0) {
        //mIndices = new int[indicesSize];
        mIndices = (u32*)rsxMemalign(128, indicesSize*sizeof(u32));
        if (!mIndices) { printf("Failed to alloc indices mem\n"); return false; }
        memcpy(mIndices, indices, sizeof(int) * indicesSize);
        mIndicesSize = indicesSize;
    }
    
    return true;
}

bool VertexBuffer::UpdateVertices(
    float* vertices,
    size_t verticesSize)
{
    if (verticesSize != mVerticesSize) {
        printf("ERROR - update vertices size != mVerticesSize\n");
        return false;
    }
    memcpy(mVertices, vertices, verticesSize * sizeof(float));
    return true;
}

} // namespace PS3
} // namespace GameLib

