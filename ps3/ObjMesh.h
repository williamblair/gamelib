#ifndef GAMELIB_PS3_OBJ_MESH_H_INCLUDED
#define GAMELIB_PS3_OBJ_MESH_H_INCLUDED

#include <GameMath/GameMath.h>
#include <Renderer.h>
#include <VertexBuffer.h>

namespace GameLib
{
namespace PS3
{

class ObjMesh
{
public:

    ObjMesh();
    ~ObjMesh();
    
    bool Init(const char* fileName);

    void Draw(
        GameMath::Mat4& modelMat,
        GameMath::Mat4& viewMat,
        Renderer& render)
    {
        render.DrawVertexBuffer(modelMat, viewMat, mVertBuf);
    }

private:
    VertexBuffer mVertBuf;
};

} // namespace PS3
} // namespace GameLib

#endif // GAMELIB_PS3_OBJ_MESH_H_INCLUDED

