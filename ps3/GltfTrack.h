#ifndef GAMELIB_PS3_GLTF_TRACK_H_INCLUDED
#define GAMELIB_PS3_GLTF_TRACK_H_INCLUDED

#include <vector>

#include <GameMath/GameMath.h>

#include <GltfFrame.h>
#include <GltfInterpolation.h>

namespace GameLib
{
namespace PS3
{
namespace Gltf
{

// a track is a collection of frames which can be interpolated between
template<typename T, int N>
class Track
{
public:
    
    Track();

    void Resize(unsigned int size);
    unsigned int GetSize() const;
    Interpolation GetInterpolation() const;
    void SetInterpolation(Interpolation interp);
    float GetStartTime() const;
    float GetEndTime() const;

    T Sample(float time, bool looping);
    Frame<N>& operator[](unsigned int index);

    T Hermite(float time, const T& p1, const T& s1, const T& p2, const T& s2);

    // returns the last frame before the requested time
    int FrameIndex(float time, bool looping);

    // maps time within input range
    float AdjustTimeToFitTrack(float t, bool loop);

    // will be specialized based on template implementation
    T Cast(float* value);

private:
    std::vector<Frame<N>> frames;
    Interpolation interpolation;

    T SampleConstant(float time, bool looping);
    T SampleLinear(float time, bool looping);
    T SampleCubic(float time, bool looping);
};

typedef Track<float,1> ScalarTrack;
typedef Track<GameMath::Vec3,3> VectorTrack;
typedef Track<GameMath::Quat,4> QuaternionTrack;

} // namespace Gltf
} // namespace PS3
} // namespace GameLib

#endif // GAMELIB_PS3_GLTF_TRACK_H_INCLUDED

