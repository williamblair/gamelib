#include <TileFloor.h>
#include <stdio.h>

namespace GameLib
{
namespace PS3
{

#define TILESIZE 1.0f

float TileFloor::sVertices[6*5] = {
    // position                     texcoord
    -TILESIZE, 0.0f, TILESIZE,      0.0f, 0.0f,  // bottom left
    TILESIZE, 0.0f, TILESIZE,       1.0f, 0.0f,  // bottom right
    TILESIZE, 0.0f,-TILESIZE,       1.0f, 1.0f,  // top right
    
    -TILESIZE, 0.0f, TILESIZE,      0.0f, 0.0f, // bottom left
    TILESIZE, 0.0f,-TILESIZE,       1.0f, 1.0f, // top right
    -TILESIZE, 0.0f, -TILESIZE,     0.0f, 1.0f  // top left
};
/*float TileFloor::sVertices[6*6] = {
    // position                     color
    -TILESIZE, 0.0f, TILESIZE,      1.0f, 0.0f, 1.0f, // bottom left
    TILESIZE, 0.0f, TILESIZE,       0.0f, 1.0f, 0.0f,  // bottom right
    TILESIZE, 0.0f,-TILESIZE,       0.0f, 0.0f, 1.0f, // top right
    
    -TILESIZE, 0.0f, TILESIZE,      1.0f, 0.0f, 1.0f, // bottom left
    TILESIZE, 0.0f,-TILESIZE,       0.0f, 1.0f, 0.0f, // top right
    -TILESIZE, 0.0f, -TILESIZE,     0.0f, 0.0f, 1.0f  // top left
};*/

TileFloor::TileFloor() :
    mPosition(0.0f, 0.0f, 0.0f),
    mScale(1.0f, 1.0f, 1.0f),
    mTexture(nullptr)
{
    //void Init(
    //    float* vertices,
    //    size_t verticesSize,
    //    int* indices,
    //    size_t indicesSize,
    //    size_t floatsPerVertex
    //);
    
    // verts, vertssize, indices, indicessize, floats per vertex
    /*mVertBuf.Init(
        sVertices, // vertices
        6*5, // vertices size
        nullptr, // indices
        0, // indicesSize
        5 // floats per vertex
    );*/
}

TileFloor::~TileFloor()
{
}

bool TileFloor::Init()
{
    return mVertBuf.Init(
        VertexBuffer::POS_TEXCOORD, // type
        sVertices, // vertices
        6*5, // vertices size
        nullptr, // indices
        0 // indicesSize
    );
    /*return mVertBuf.Init(
        VertexBuffer::POS_COLOR, // type
        sVertices, // vertices
        6*6, // vertices size
        nullptr, // indices
        0 // indicesSize
    );*/
#if 0
    float vertices[4*4*36];
    float* vp = vertices;
    float x = -32.0f;
    for (int i=0; i<4; ++i)
    {
        float z = 0.0f;
        for (int j=0; j<4; ++j)
        {
            // position
            *vp++ = x;
            *vp++ = 0.0f;
            *vp++ = z+16.0f-1.0f;
            // color
            *vp++ = 1.0f;
            *vp++ = 0.0f;
            *vp++ = 0.0f;

            // position
            *vp++ = x+16.0f-1.0f;
            *vp++ = 0.0f;
            *vp++ = z+16.0f-1.0f;
            // color
            *vp++ = 0.0f;
            *vp++ = 1.0f;
            *vp++ = 0.0f;

            // position
            *vp++ = x;
            *vp++ = 0.0f;
            *vp++ = z;
            // color
            *vp++ = 0.0f;
            *vp++ = 0.0f;
            *vp++ = 1.0f;



            // position
            *vp++ = x+16.0f-1.0f;
            *vp++ = 0.0f;
            *vp++ = z+16.0f-1.0f;
            // color
            *vp++ = 1.0f;
            *vp++ = 0.0f;
            *vp++ = 0.0f;

            // position
            *vp++ = x+16.0f-1.0f;
            *vp++ = 0.0f;
            *vp++ = z;
            // color
            *vp++ = 0.0f;
            *vp++ = 1.0f;
            *vp++ = 0.0f;

            // position
            *vp++ = x;
            *vp++ = 0.0f;
            *vp++ = z;
            // color
            *vp++ = 0.0f;
            *vp++ = 0.0f;
            *vp++ = 1.0f;

            z += 16.0f;
        }
        x += 16.0f;
    }

    return mVertBuf.Init(
        VertexBuffer::POS_COLOR, // type
        vertices, // vertices
        4*4*36, // vertices size
        nullptr, // indices
        0 // indicesSize
    );
#endif
}

bool TileFloor::Draw(
    GameMath::Mat4& viewMat,
    Renderer& render)
{
    if (mTexture == nullptr) {
        printf("TileFloor draw tex is null\n");
        return false;
    }
    // assumes all scale components are equal
    const float tileSize = 2.0f * mScale.x;
    //const float tileSize = TILESIZE;
    float x = mPosition.x;
    float z = mPosition.z;
    const size_t numTilesX = 8;
    const size_t numTilesZ = 8;
    GameMath::Mat4 modelMat;
    
    render.SetTexture(*mTexture);
    for (size_t i = 0; i < numTilesX; ++i)
    {
        z = mPosition.z;
        for (size_t j = 0; j <numTilesZ; ++j)
        {
            modelMat = 
                GameMath::Translate(x, mPosition.y, z) *
                GameMath::Scale(mScale.x, 1.0f, mScale.z);
            //if (!render.DrawVertexBuffer(modelMat, viewMat, mVertBuf))
            //    return false;
            render.DrawVertexBuffer(modelMat, viewMat, mVertBuf);
            
            z += tileSize;
        }
        x += tileSize;
    }
    return true;
}

} // namespace PS3
} // namespace GameLib


