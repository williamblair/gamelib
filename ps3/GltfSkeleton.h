#ifndef GAMELIB_PS3_GLTF_SKELETON_H_INCLUDED
#define GAMELIB_PS3_GLTF_SKELETON_H_INCLUDED

#include <vector>
#include <string>

#include <GameMath/GameMath.h>

#include <GltfPose.h>
#include <GltfTransform.h>

namespace GameLib
{
namespace PS3
{
namespace Gltf
{

class Skeleton
{
public:

    Skeleton();
    Skeleton(const Pose& rest, const Pose& bind, const std::vector<std::string>& names);
    ~Skeleton();
    
    void Set(const Pose& rest, const Pose& bind, const std::vector<std::string>& names);
    
    Pose& GetBindPose() { return bindPose; }
    Pose& GetRestPose() { return restPose; }
    
    std::vector<GameMath::Mat4>& GetInvBindPose() { return invBindPose; }
    std::vector<std::string>& GetJointNames() { return jointNames; }
    std::string& GetJointName(unsigned int index) { return jointNames[index]; }

private:

    Pose restPose;
    Pose bindPose;
    
    std::vector<GameMath::Mat4> invBindPose;
    void UpdateInverseBindPose();
    
    std::vector<std::string> jointNames;
};


} // namespace Gltf
} // namespace PS3
} // namespace GameLib

#endif // GAMELIB_PS3_GLTF_SKELETON_H_INCLUDED

