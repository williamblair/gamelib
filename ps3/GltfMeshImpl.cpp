#include <cstdio>
#include <GltfMeshImpl.h>
#include <GltfTransform.h>

namespace GameLib
{
namespace PS3
{
namespace Gltf
{

#define Vec2 GameMath::Vec2
#define Vec3 GameMath::Vec3
#define Vec4 GameMath::Vec4
#define Mat4 GameMath::Mat4

Mesh::Mesh() :
    vertBufInitted(false)
{}

#if 1
void Mesh::CPUSkin(Skeleton& skeleton, Pose& pose)
{
    unsigned int numVerts = positions.size();
    if (numVerts == 0) {
        // TODO error
        printf("Mesh::CPUSkin numverts == 0\n");
        return;
    }

    skinnedPositions.resize(numVerts);
    skinnedNormals.resize(numVerts);
    combinedVertices.resize(numVerts * 5); // xyz uv

    // stores pose transform matrices in posePalette
    pose.GetMatrixPalette(posePalette);
    std::vector<Mat4> invPosePalette = skeleton.GetInvBindPose();

    for (unsigned int i = 0; i < numVerts; ++i) {
        iVec4& joint = influences[i];
        Vec4& weight = weights[i];
        Vec3* resultPos = (Vec3*)&combinedVertices[i*5];
        Vec2* resultTexcoord = (Vec2*)&combinedVertices[i*5 + 3];

        // calculate skin matrix
        // up to 4 different joint influences (not all necessarily used)
        Mat4 m0 = (posePalette[joint.x] * invPosePalette[joint.x]) * weight.x; // first joint influence
        Mat4 m1 = (posePalette[joint.y] * invPosePalette[joint.y]) * weight.y; // first joint influence
        Mat4 m2 = (posePalette[joint.z] * invPosePalette[joint.z]) * weight.z; // first joint influence
        Mat4 m3 = (posePalette[joint.w] * invPosePalette[joint.w]) * weight.w; // first joint influence
        Mat4 skin = m0 + m1 + m2 + m3;

        // combine transforms
        *resultPos = GameMath::Transform(skin, positions[i]);
        skinnedNormals[i] = GameMath::Transform(skin, normals[i], 0.0f);
        resultTexcoord->u = texCoords[i].u;
        resultTexcoord->v = 1.0f - texCoords[i].v;
    }

    UpdateOpenGLBuffers();
}
#endif
#if 0
void Mesh::CPUSkin(Skeleton& skeleton, Pose& pose)
{
    unsigned int numVerts = positions.size();
    if (numVerts == 0) {
        // TODO error
        return;
    }

    skinnedPositions.resize(numVerts);
    skinnedNormals.resize(numVerts);
    combinedVertices.resize(numVerts * 5); // xyz uv
    Pose& bindPose = skeleton.GetBindPose();

    for (unsigned int i = 0; i < numVerts; ++i)
    {
        iVec4& joint = influences[i];
        Vec4& weight = weights[i];
        Vec3* resultPos = (Vec3*)&combinedVertices[i*5];
        Vec2* resultTexcoord = (Vec2*)&combinedVertices[i*5 + 3];

        // calculate skin transform
        Transform skin0 = Combine(pose[joint.x], Inverse(bindPose[joint.x]));
        Vec3 p0 = TransformPoint(skin0, positions[i]);
        Vec3 n0 = TransformVector(skin0, normals[i]);

        Transform skin1 = Combine(pose[joint.y], Inverse(bindPose[joint.y]));
        Vec3 p1 = TransformPoint(skin1, positions[i]);
        Vec3 n1 = TransformVector(skin1, normals[i]);

        Transform skin2 = Combine(pose[joint.z], Inverse(bindPose[joint.z]));
        Vec3 p2 = TransformPoint(skin2, positions[i]);
        Vec3 n2 = TransformVector(skin2, normals[i]);

        Transform skin3 = Combine(pose[joint.w], Inverse(bindPose[joint.w]));
        Vec3 p3 = TransformPoint(skin3, positions[i]);
        Vec3 n3 = TransformVector(skin3, normals[i]);

        // combine transforms
        *resultPos =
            p0 * weight.x +
            p1 * weight.y +
            p2 * weight.z + 
            p3 * weight.w;
        skinnedNormals[i] =
            n0 * weight.x +
            n1 * weight.y +
            n2 * weight.z + 
            n3 * weight.w;

        resultTexcoord->u = texCoords[i].u;
        resultTexcoord->v = 1.0f - texCoords[i].v;
    }

    UpdateOpenGLBuffers();
}
#endif

void Mesh::UpdateOpenGLBuffers()
{
    const unsigned int numVerts = skinnedPositions.size();
    if (skinnedNormals.size() != numVerts ||
        texCoords.size() != numVerts)
    {
        printf("Mesh::UpdateOpenGLBuffers mismatch noramls/texcoords size\n");
        return;
    }

    if (!vertBufInitted) {
        vertBuf.Init(
            VertexBuffer::POS_TEXCOORD,
            combinedVertices.data(),
            combinedVertices.size(),
            (indices.size() > 0) ? indices.data() : nullptr,
            indices.size()
        );
        vertBufInitted = true;
    } else {
        vertBuf.UpdateVertices(
            combinedVertices.data(),
            combinedVertices.size()
        );
    }
}

#undef Vec2
#undef Vec3
#undef Vec4
#undef Mat4

} // namespace Gltf
} // namespace PS3
} // namespace GameLib

