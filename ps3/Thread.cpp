#include <Thread.h>
#include <stdio.h>

namespace GameLib
{
namespace PS3
{

Thread::Thread() :
    mStarted(false),
    mJoined(false)
{}

Thread::~Thread()
{
    if (!mJoined && mStarted) {
        u64 retVal;
        sysThreadJoin(mId, &retVal);
        mJoined = true;
    }
}

bool Thread::Start(ThreadFcn fcn, void* arg, const char* name)
{
    mStarted = false;
    mJoined = false;
    
    int ret = sysThreadCreate(
        &mId,
        fcn,
        arg,
        sPriority,
        sStackSize,
        THREAD_JOINABLE,
        (char*)name
    );
    if (ret != 0) {
        printf("Failed to start thread!\n");
        return false;
    }

    mStarted = true;
    return true;
}

void Thread::Join(u64* retVal)
{
    int ret = sysThreadJoin(mId, retVal);
    if (ret != 0) {
        printf("Failed to join thread!\n");
    }
    mJoined = true;
}

u64 Thread::GetThreadId()
{
    sys_ppu_thread_t id;
    sysThreadGetId(&id);
    return (u64)id;
}

void Thread::Yield()
{
    sysThreadYield();
}

void Thread::Exit(u64 retVal)
{
    sysThreadExit(retVal);
}

} // namespace PS3
} // namespace GameLib

