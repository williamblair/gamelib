#ifndef GAMELIB_PS3_VERTEX_BUFFER_H_INCLUDED
#define GAMELIB_PS3_VERTEX_BUFFER_H_INCLUDED

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <ppu-types.h>

#include <sys/process.h>

#include <io/pad.h>
#include <rsx/rsx.h>
#include <sysutil/sysutil.h>

namespace GameLib
{
namespace PS3
{

class VertexBuffer
{

friend class Renderer;

public:
    
    enum Type
    {
        POS_COLOR,
        POS_TEXCOORD,
        UNINITIALIZED
    };

    VertexBuffer();
    ~VertexBuffer();

    
    bool Init(
        Type type,
        float* vertices,
        size_t verticesSize,
        int* indices,
        size_t indicesSize
    );
    
    bool UpdateVertices(float* vertices, size_t verticesSize);

private:
    // mIndices and mVertices are rsx memory
    u32* mIndices;
    size_t mIndicesSize;
    f32* mVertices;
    size_t mVerticesSize;
    
    Type mType;
};

} // namespace PS3
} // namespace GameLib

#endif // VERTEX_BUFFER_H_INCLUDED

