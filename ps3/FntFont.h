#ifndef GAMELIB_PS3_FNT_FONT_H_INCLUDED
#define GAMELIB_PS3_FNT_FONT_H_INCLUDED

#include <map>

#include <Texture.h>
#include <Sprite.h>
#include <Renderer.h>

namespace GameLib
{
namespace PS3
{

/**
 * @brief Font drawing from fnt format from 
 *  <a>https://angelcode.com/products/bmfont/</a>
 */
class FntFont
{
public:
    
    FntFont();
    ~FntFont();
    
    /**
     * @brief Load font file and initialize
     * @param fileName the location of the .fnt file describing texture
     * @param texFileName texture file to use
     * @param r font color red
     * @param g font color green
     * @param b font color blue
     * @return true on success, false on failure
     */
    bool Init(
        const char* fileName,
        const char* texFileName,
        uint8_t r, uint8_t g, uint8_t b
    );
    
    /**
     * @brief how much to scale the font size when drawing; default = 1
     */
    void SetDrawScale(float scale) { mDrawScale = scale; }
    
    /**
     * @brief Draw string to the screen with the top left at x,y
     * @param x normalized screen position x (-1..1)
     * @param y normalized screen position y (-1..1)
     */
    void Draw(const char* msg, float x, float y, Renderer& render);
    
    /** @brief returns how many pixels tall each drawn line is */
    int GetLineHeight() const { return mLineHeight; }
    
private:
    Texture mTexture;
    Sprite mSprite;
    int mLineHeight;
    float mDrawScale;
    struct CharInfo
    {
        char id;
        int x, y;
        int w, h;
        int xoffset, yoffset;
        int xadvance;
        int page;
        int channel;
        // normalized position and size within texture for uv coords
        float u, v;
        float uWidth, vHeight;
    };
    std::map<int,CharInfo> mCharInfo;
    
    bool LoadFntFile(const char* fileName);
    bool LoadTexture(const char* texFileName,
        uint8_t r, uint8_t g, uint8_t b);
    bool LoadFromTga(const char* texFileName,
        uint8_t r, uint8_t g, uint8_t b);
    bool LoadFromBmp(const char* texFileName,
        uint8_t r, uint8_t g, uint8_t b);
};

} // namespace PS3
} // namespace GameLib

#endif // GAMELIB_PS3FNT_FONT_H_INCLUDED
