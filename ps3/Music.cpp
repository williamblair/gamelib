#include <Music.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>

namespace GameLib
{
namespace PS3
{

void Music::sThreadFcn(void* arg)
{
    Music* mus = (Music*)arg;
    WAVStreamData& wavStream = mus->mWavStream;

    u64 id = Thread::GetThreadId();
    u32 audioBlockIndex = 1;
    u64* readIndex = (u64*)(u64)mus->mConfig.readIndex;
    float* audioDataStart = (float*)(u64)mus->mConfig.audioDataStart;

    printf("Music sThreadFcn start with id: %lu\n", id);

    bool runThread = true;
    while (runThread)
    {
        if (wavStream.ended && !wavStream.looping) {
            break;
        }

        if (mus->mPaused) {
            Thread::Yield();
            continue;
        }

        // get position of the hardware
        u64 currentBlock = *readIndex;
        if (audioBlockIndex == currentBlock) {
            Thread::Yield();
            continue;
        }

        // get position of the block to write
        // 2 = 2 channels (left, right)
        float* buf = audioDataStart + (2*AUDIO_BLOCK_SAMPLES*audioBlockIndex);
        
        // fill buffer
        // samples are floating point with range -1..1
        const float sampScale = 1.0f/32768.0f;
        for (unsigned int i = 0; (i < AUDIO_BLOCK_SAMPLES) && runThread; ++i) {
            const int16_t leftSamp = ((int16_t*)wavStream.bufPtr)[0];
            const int16_t rightSamp = ((int16_t*)wavStream.bufPtr)[1];
            buf[i*2 + 0] = ((float)lend2bend16(leftSamp)) * sampScale;
            buf[i*2 + 1] = ((float)lend2bend16(rightSamp)) * sampScale;

            wavStream.bufPtr += sizeof(int16_t)*2;
            wavStream.bufSizeRemain -= sizeof(int16_t)*2;
            if (wavStream.bufSizeRemain == 0) {

                wavStream.bufPtr = wavStream.buffer;
                wavStream.bufSizeRemain = sizeof(wavStream.buffer);

                int numFileReadRemain = sizeof(wavStream.buffer);
                unsigned char* fileReadPtr = wavStream.buffer;

                while (numFileReadRemain > 0) {
                    int numToRead =
                        (wavStream.fileBytesRemain >= numFileReadRemain) ?
                        numFileReadRemain :
                        wavStream.fileBytesRemain;
                    int numRead = read(wavStream.wavFd, fileReadPtr, numToRead);
                    if (numRead <= 0) {
                        if (wavStream.looping) {
                            // go back to beginning of WAV file past header
                            lseek(wavStream.wavFd, sizeof(WAVHeader), SEEK_SET);
                            wavStream.fileBytesRemain = wavStream.fileSizeBytes;

                            // re-read
                            numRead = read(wavStream.wavFd, fileReadPtr, numToRead);
                        } else {
                            wavStream.ended = true;
                            runThread = false;
                            break;
                        }
                    }

                    fileReadPtr += numRead;
                    numFileReadRemain -= numRead;
                    wavStream.fileBytesRemain -= numRead;

                    // If we reached the end of the file...
                    if (wavStream.fileBytesRemain == 0) {
                        if (wavStream.looping) {
                            // go back to beginning of WAV file past header
                            lseek(wavStream.wavFd, sizeof(WAVHeader), SEEK_SET);
                            wavStream.fileBytesRemain = wavStream.fileSizeBytes;
                        } else {
                            wavStream.ended = true;
                            runThread = false;
                            break;
                        }
                    }
                }
            }
        }

        audioBlockIndex = (audioBlockIndex+1) % AUDIO_BLOCK_8;
    }

    Thread::Exit(0);
}

bool Music::sAudioInitted = false;

Music::Music() :
    mPortNum(0),
    mRunning(false),
    mLooping(false),
    mPaused(false),
    mThreadJoined(false)
{}

Music::~Music()
{
    // make thread close if it hasn't been already
    mWavStream.ended = true;
    mWavStream.looping = false;
    if (mRunning && !mThreadJoined) {
        u64 val;
        mThread.Join(&val);
        mThreadJoined = true;
    }
}

bool Music::Init(const char* fileName)
{
    WAVHeader& hdr = mWavHdr;
    WAVStreamData& strm = mWavStream;

    // open the WAV file
    strm.wavFd = open(fileName, O_RDONLY);
    if (strm.wavFd < 0) {
        printf("Failed to open %s\n", fileName);
        return false;
    }

    // read the WAV header
    int numRead = read(strm.wavFd, (char*)&hdr, sizeof(WAVHeader));
    if (numRead != sizeof(WAVHeader)) {
        printf("Failed to read WAV header\n");
        return false;
    }
    // convert endianness
    hdr.fileSize = lend2bend32(hdr.fileSize);
    hdr.fmtLen = lend2bend32(hdr.fmtLen);
    hdr.fmtType = lend2bend16(hdr.fmtType);
    hdr.numChannels = lend2bend16(hdr.numChannels);
    hdr.sampleRate = lend2bend32(hdr.sampleRate);
    hdr.dataRateBytes = lend2bend32(hdr.dataRateBytes);
    hdr.sampleSizeBytes = lend2bend16(hdr.sampleSizeBytes);
    hdr.bitsPerSample = lend2bend16(hdr.bitsPerSample);
    hdr.dataSize = lend2bend32(hdr.bitsPerSample);

    // Verify expected header contents
    printf("hdr RIFF: %c,%c,%c,%c\n",
        hdr.riff[0],
        hdr.riff[1],
        hdr.riff[2],
        hdr.riff[3]);
    printf("hdr fileSize: %d\n", hdr.fileSize);
    printf("hdr WAVE: %c,%c,%c,%c\n",
        hdr.wave[0],
        hdr.wave[1],
        hdr.wave[2],
        hdr.wave[3]);
    printf("hdr fmt: %c,%c,%c,%c\n",
        hdr.fmt[0],
        hdr.fmt[1],
        hdr.fmt[2],
        hdr.fmt[3]);
    printf("hdr fmtLen: %d\n", hdr.fmtLen);
    printf("hdr fmtType: %d\n", hdr.fmtType);
    printf("hdr numChannels: %d\n", hdr.numChannels);
    printf("hdr sampleRate: %d\n", hdr.sampleRate);
    printf("hdr dataRateBytes: %d\n", hdr.dataRateBytes);
    printf("hdr sampleSizeBytes: %d\n", hdr.sampleSizeBytes);
    printf("hdr bitsPerSample: %d\n", hdr.bitsPerSample);
    printf("hdr DATA: %c,%c,%c,%c\n",
        hdr.data[0],
        hdr.data[1],
        hdr.data[2],
        hdr.data[3]);
    printf("hdr dataSize: %d\n", hdr.dataSize);

    if (hdr.sampleRate != 48000) {
        printf("Unexpected WAV sampleRate\n");
        return false;
    }
    if (hdr.sampleSizeBytes != 4) {
        printf("Unexpected sampleSizeBytes\n");
        return false;
    }
    if (hdr.bitsPerSample != 16) {
        printf("Unexpected bitsPerSample\n");
        return false;
    }

    // initialize streaming data
    strm.fileSizeBytes = hdr.dataSize; // bytes of the data section
    strm.fileBytesRemain = hdr.dataSize; // how many bytes left to be read from the file
    strm.bufPtr = strm.buffer; // beginning of the samples buffer
    strm.bufSizeRemain = sizeof(strm.buffer); // sample data left in buffer in bytes
    strm.looping = false;
    strm.ended = false;

    // read initial samples into the buffer
    numRead = read(strm.wavFd, strm.bufPtr, sizeof(strm.buffer));
    if (numRead != sizeof(strm.buffer)) {
        printf("Failed to read initial samples\n");
        return false;
    }
    
    // Init PS3 audio
    int ret;
    if (!sAudioInitted) {
        ret = audioInit();
        printf("audioInit: %d\n", ret);
        sAudioInitted = true;
    }
    
    mParams.numChannels = AUDIO_PORT_2CH;
    mParams.numBlocks = AUDIO_BLOCK_8;
    mParams.attrib = 0;
    mParams.level = 1;

    ret = audioPortOpen(&mParams, &mPortNum);
    printf("audioPortOpen: %d\n", ret);
    printf("  portNum: %d\n", mPortNum);

    ret = audioGetPortConfig(mPortNum, &mConfig);
    printf("audioGetPortConfig: %d\n", ret);
    printf("  readIndex: 0x%8X\n",mConfig.readIndex);
    printf("  status: %d\n",mConfig.status);
    printf("  channelCount: %ld\n",mConfig.channelCount);
    printf("  numBlocks: %ld\n",mConfig.numBlocks);
    printf("  portSize: %d\n",mConfig.portSize);
    printf("  audioDataStart: 0x%8X\n",mConfig.audioDataStart);

    return true;
}

void Music::Play(const bool loop)
{
    mWavStream.looping = loop;
    int ret = audioPortStart(mPortNum);
    printf("audioPortStart: %d\n", ret);

    if (!mThread.Start(sThreadFcn, (void*)this, "Music Thread")) {
        printf("Failed to start audio thread!\n");
    } else {
        mRunning = true;
        mThreadJoined = false;
    }
}

void Music::Stop()
{
    int ret = audioPortStop(mPortNum);
    printf("audioPortStop: %d\n", ret);
    ret = audioPortClose(mPortNum);
    printf("audioPortClose: %d\n", ret);

    u64 retVal;
    mRunning = false;
    mThread.Join(&retVal);
    mThreadJoined = true;
}

} // namespace PS3
} // namespace GameLib

