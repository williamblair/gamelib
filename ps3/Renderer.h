#ifndef GAMELIB_PS3_RENDERER_H_INCLUDED
#define GAMELIB_PS3_RENDERER_H_INCLUDED

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <ppu-types.h>

#include <sys/process.h>

#include <io/pad.h>
#include <rsx/rsx.h>
#include <sysutil/sysutil.h>

#include <VertexBuffer.h>
#include <Shader.h>
#include <Texture.h>
#include <AABB.h>
#include <Sprite.h>

// 512 sprites, 6 vertices per sprite
#define SPRITE_NVERTICES (512*6)

namespace GameLib
{
namespace PS3
{

class Renderer
{
public:

    Renderer();
    ~Renderer();

    bool Init(int width, int height, bool fullScreen, const char* title);

    void SetBGColor(uint8_t r, uint8_t g, uint8_t b);

    void Clear();
    void Update();

    void DrawVertexBuffer(
        GameMath::Mat4& modelMat,
        GameMath::Mat4& viewMat,
        const VertexBuffer& vb
    );
    void DrawAabb(AABB& aabb, GameMath::Mat4& viewMat);
    void DrawSprite(Sprite& sprite);

    void SetTexture(Texture& tex);

    int GetWidth() const { return mWidth; }
    int GetHeight() const { return mHeight; }

private:

    int mWidth;
    int mHeight;
    u32 mBgColor;

    void* mHostAddr;
    gcmContextData* mContext;

    GameMath::Mat4 mProjMat;

    static VertexBuffer sAabbVertBuf;

    // array of vertices/UVs for sprite drawing
    static f32* sSpriteVerts;
    int mCurSpriteVert;

    Shader mPosColorShader;
    Shader mPosTexCoordShader;

    Texture* mCurTex;

    bool InitAabbVertBuf();
    bool InitSpriteVertBuf();
};

} // namespace PS3
} // namespace GameLib

#endif // GAMELIB_PS3_RENDERER_H_INCLUDED

