#ifndef GAMEMATH_PS3_GLTF_MESH_H_INCLUDED
#define GAMEMATH_PS3_GLTF_MESH_H_INCLUDED

#include <GameMath/GameMath.h>
#include <GltfLoader.h>
#include <GltfMeshImpl.h>
#include <Renderer.h>

namespace GameLib
{
namespace PS3
{

class GltfMesh
{
public:
    
    GltfMesh();
    
    bool Init(const char* fileName);
    
    void Update(const float dt);
    
    void Draw(
        GameMath::Mat4& modelMat,
        GameMath::Mat4& viewMat,
        Renderer& render
    );
    
    void SetAnim(std::string animName, bool loop = true)
    {
        unsigned int i = 0;
        for (Gltf::Clip& gltfClip : clips)
        {
            if (gltfClip.GetName() == animName)
            {
                clip = i;
                mCurAnimName = animName;
                clips[i].SetLooping(loop);
                // TODO - maybe not reset this?
                playback = 0.0f;
                break;
            }
            ++i;
        }
        // TODO - error if not found
    }
    std::string& GetAnim() { return mCurAnimName; }
    bool AnimIsFinished() const { return playback >= clips[clip].GetDuration(); }
    
private:
    Gltf::Pose animatedPose;
    std::vector<GameMath::Mat4> posePalette;
    unsigned int clip;
    float playback;
    std::string mCurAnimName;
    std::vector<Gltf::Mesh> meshes;
    Gltf::Skeleton skeleton;
    std::vector<Gltf::Clip> clips;
};

} // namespace PS3
} // namespace GameLib

#endif // GAMEMATH_PS3_GLTF_MESH_H_INCLUDED

