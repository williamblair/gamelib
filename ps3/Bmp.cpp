#include <Bmp.h>
#include <stdio.h>

namespace GameLib
{
namespace PS3
{
namespace BMP
{

// little endian to big endian
static inline uint16_t lend2bend16(uint16_t lend)
{
    char* ptr = (char*)&lend;
    return (ptr[1] << 8) | 
           (ptr[0]);
}
static inline uint32_t lend2bend32(uint32_t lend)
{
    char* ptr = (char*)&lend;
    return ((uint32_t)ptr[3] << 24) |
           ((uint32_t)ptr[2] << 16) |
           ((uint32_t)ptr[1] << 8) | 
           ((uint32_t)ptr[0]);    
}

bool Load(
    const char* fileName,
    std::vector<uint8_t>& buffer,
    int& width, int& height)
{
    uint8_t* bmpPxData = nullptr;
    Header hdr;
    BITMAPINFOHEADER infoHdr;

    FILE* fp = fopen(fileName, "rb");
    if (!fp) {
        printf("Failed to open BMP File: %s\n", fileName);
        return false;
    }

    // Read in the BMP header
    if (fread((void*)&hdr, 1, sizeof(hdr), fp) != sizeof(hdr)) {
        printf("Failed to read BMP header\n");
        return false;
    }
    if (((char*)&hdr.header)[0] != 'B' ||
        ((char*)&hdr.header)[1] != 'M')
    {
        printf("File is not a windows BMP file: %c, %c\n",
           ((char*)&hdr.header)[0],
           ((char*)&hdr.header)[1]
        );
        return false;
    }
    if (fread((void*)&infoHdr, 1, sizeof(infoHdr), fp) != sizeof(infoHdr)) {
        printf("Failed to read BMP info header\n");
        return false;
    }
    
    infoHdr.size = lend2bend32(infoHdr.size);
    infoHdr.width = lend2bend32(infoHdr.width);
    infoHdr.height = lend2bend32(infoHdr.height);
    infoHdr.planes = lend2bend16(infoHdr.planes);
    infoHdr.bitCount = lend2bend16(infoHdr.bitCount);
    infoHdr.compression = lend2bend32(infoHdr.compression);
    infoHdr.sizeImage = lend2bend32(infoHdr.sizeImage);
    infoHdr.xPelsPerMeter = lend2bend32(infoHdr.xPelsPerMeter);
    infoHdr.yPelsPerMeter = lend2bend32(infoHdr.yPelsPerMeter);
    infoHdr.clrUsed = lend2bend32(infoHdr.clrUsed);
    infoHdr.clrImportant = lend2bend32(infoHdr.clrImportant);
    
    if (infoHdr.size != 40) {
        printf("Unexpected infoHdr size: %u\n", infoHdr.size);
        return false;
    }
    if (infoHdr.planes != 1) {
        printf("Unexpected num planes: %d\n", (int)infoHdr.planes);
        return false;
    }
    if (infoHdr.bitCount != 24) {
        printf("Unhandled bits per pixel: %d\n", (int)infoHdr.bitCount);
        return false;
    }
    // 0 == BI_RGB == no compression
    if (infoHdr.compression != 0) {
        printf("Unhandled BMP compression: %d\n", (int)infoHdr.compression);
        return false;
    }
    printf("infoHdr size: %u\n", (unsigned int)infoHdr.size);
    printf("infoHdr width, height: %d, %d\n", (int)infoHdr.width, (int)infoHdr.height);

    width = infoHdr.width;
    height = infoHdr.height;

    bmpPxData = new uint8_t[infoHdr.sizeImage];
    if (!bmpPxData) {
        printf("Failed to alloc bmp px buffer\n");
        return false;
    }

    fseek(fp, lend2bend32(hdr.pixOffset), SEEK_SET);
    if (fread((void*)bmpPxData, 1, infoHdr.sizeImage, fp) != infoHdr.sizeImage) {
        printf("Failed to read bmp px buffer data\n");
        return false;
    }
    fclose(fp);

    // assumes 24 bits per pixel
    int unpaddedRowWidthBytes = width * 3;
    int paddedRowWidthBytes = unpaddedRowWidthBytes;
    if (paddedRowWidthBytes % 4 != 0) {
        paddedRowWidthBytes += 4 - (paddedRowWidthBytes % 4);
    }
    // force 4 bytes per pixel
    buffer.resize(width * height * 4);
    uint8_t* outPx = buffer.data();
    for (int row = 0; row < height; ++row)
    {
        uint8_t* inPx = &bmpPxData[row * paddedRowWidthBytes];
        for (int col = 0; col < width; ++col)
        {
            uint8_t r = *inPx++;
            uint8_t g = *inPx++;
            uint8_t b = *inPx++;

            *outPx++ = b;
            *outPx++ = g;
            *outPx++ = r;
            *outPx++ = 255;
        }
    }

    delete[] bmpPxData;

    return true;
}

} // namespace BMP
} // namespace PS3
} // namespace GameLib

