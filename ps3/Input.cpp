#include <Input.h>

namespace GameLib
{
namespace PS3
{

bool Input::sSysPadInitted = false;

Input::Input() :
    mIsActive(false),
    mPadnum(0),
    mQuit(false),
    mLeft(false),
    mRight(false),
    mUp(false),
    mDown(false),
    mMouseMoveX(0),
    mMouseMoveY(0)
{
    mPadnum = 0;
    mIsActive = false;
    memset((void*)&mPaddata, 0, sizeof(mPaddata));
    memset((void*)&mPrevPadData, 0, sizeof(mPrevPadData));
    memset((void*)&mActparam, 0, sizeof(mActparam));
    mPaddata.ANA_L_H = 127; // halfway between 0..255
    mPaddata.ANA_L_V = 127;
    mPaddata.ANA_R_H = 127;
    mPaddata.ANA_R_V = 127;
}

Input::~Input()
{
}

void Input::Update()
{
    ioPadGetInfo(&mPadInfo);

    mUp = false;
    mDown = false;
    mLeft = false;
    mRight = false;

    mMouseMoveX = 0;
    mMouseMoveY = 0;

    if (mPadInfo.status[mPadnum])
    {
        mIsActive = true;
        memcpy((void*)&mPrevPadData, (void*)&mPaddata, sizeof(mPrevPadData));
        ioPadGetData(mPadnum, &mPaddata);

        mUp = (bool)mPaddata.BTN_UP;
        mDown = (bool)mPaddata.BTN_DOWN;
        mLeft = (bool)mPaddata.BTN_LEFT;
        mRight = (bool)mPaddata.BTN_RIGHT;

        // TODO - let user select quit buttons
        mQuit = (bool)mPaddata.BTN_SELECT;

        // default is 0..255, map to -127..127
        int curLeftAnalogX = ((int)mPaddata.ANA_L_H) - 127;
        int curLeftAnalogY = ((int)mPaddata.ANA_L_V) - 127;
        if ((curLeftAnalogX < -50 || curLeftAnalogX > 50) &&
            (mLastLeftAnalogX < -50 || mLastLeftAnalogX > 50))
        {
            mMouseMoveX = curLeftAnalogX - mLastLeftAnalogX;
        }
        if ((curLeftAnalogY < -50 || curLeftAnalogY > 50) &&
            (mLastLeftAnalogY < -50 || mLastLeftAnalogY > 50))
        {
            mMouseMoveY = curLeftAnalogY - mLastLeftAnalogY;
        }
        mLastLeftAnalogX = curLeftAnalogX;
        mLastLeftAnalogX = curLeftAnalogY;
    }
    else
    {
        mIsActive = false;
    }
}

bool Input::IsHeld(Button b) const
{
    if (!mIsActive) {
        return false;
    }
    bool held = false;
    switch (b)
    {
    case LEFT:     held = (bool)mPaddata.BTN_LEFT;     break;
    case DOWN:     held = (bool)mPaddata.BTN_DOWN;     break;
    case RIGHT:    held = (bool)mPaddata.BTN_RIGHT;    break;
    case UP:       held = (bool)mPaddata.BTN_UP;       break;
    case SQUARE:   held = (bool)mPaddata.BTN_SQUARE;   break;
    case CROSS:    held = (bool)mPaddata.BTN_CROSS;    break;
    case CIRCLE:   held = (bool)mPaddata.BTN_CIRCLE;   break;
    case TRIANGLE: held = (bool)mPaddata.BTN_TRIANGLE; break;
    case L1: held = (bool)mPaddata.BTN_L1; break;
    case L2: held = (bool)mPaddata.BTN_L2; break;
    case R1: held = (bool)mPaddata.BTN_R1; break;
    case R2: held = (bool)mPaddata.BTN_R2; break;
    default:
        break;
    }

    return held;
}

bool Input::IsClicked(Button b) const
{
    if (!mIsActive) {
        return false;
    }
    bool clicked = false;
    switch (b)
    {
    case LEFT:     clicked = (!mPaddata.BTN_LEFT && mPrevPadData.BTN_LEFT);          break;
    case DOWN:     clicked = (!mPaddata.BTN_DOWN && mPrevPadData.BTN_DOWN);          break;
    case RIGHT:    clicked = (!mPaddata.BTN_RIGHT && mPrevPadData.BTN_RIGHT);        break;
    case UP:       clicked = (!mPaddata.BTN_UP && mPrevPadData.BTN_UP);              break;
    case SQUARE:   clicked = (!mPaddata.BTN_SQUARE && mPrevPadData.BTN_SQUARE);      break;
    case CROSS:    clicked = (!mPaddata.BTN_CROSS && mPrevPadData.BTN_CROSS);        break;
    case CIRCLE:   clicked = (!mPaddata.BTN_CIRCLE && mPrevPadData.BTN_CIRCLE);      break;
    case TRIANGLE: clicked = (!mPaddata.BTN_TRIANGLE && mPrevPadData.BTN_TRIANGLE);  break;
    default:
        break;
    }

    return clicked;
}

bool Input::SetSmallActuator(bool isOn)
{
    if (mIsActive)
    {
        mActparam.small_motor = (int)isOn;
        ioPadSetActDirect(mPadnum, &mActparam);
        return true;
    }
    return false;
}

bool Input::SetLargeActuator(unsigned char val)
{
    if (mIsActive)
    {
        mActparam.large_motor = val;
        ioPadSetActDirect(mPadnum, &mActparam);
        return true;
    }
    return false;
}

} // namespace PS3
} // namespace GameLib

