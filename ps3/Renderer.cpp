#include <Renderer.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>

#include <ppu-types.h>
#include <rsx/rsx.h>
#include <sysutil/video.h>

// Position color shader
#include "pos_color_shader_vpo.h"
#include "pos_color_shader_fpo.h"
// Position texcoord shader
#include "pos_texcoord_shader_vpo.h"
#include "pos_texcoord_shader_fpo.h"

namespace GameLib
{
namespace PS3
{

// from rsxutil.h
#define DEFUALT_CB_SIZE						0x80000		// 512Kb default command buffer size
#define HOST_STATE_CB_SIZE					0x10000		// 64Kb state command buffer size (used for resetting certain default states)
#define HOST_ADDR_ALIGNMENT					(1024*1024)
#define HOSTBUFFER_SIZE				        (128*1024*1024)

#define FRAME_BUFFER_COUNT					2


// from rsxutil.cpp
#define GCM_LABEL_INDEX		255

videoResolution vResolution;
gcmContextData *context = NULL;

u32 curr_fb = 0;
u32 first_fb = 1;

u32 display_width;
u32 display_height;

u32 depth_pitch;
u32 depth_offset;
u32 *depth_buffer;

u32 color_pitch;
u32 color_offset[FRAME_BUFFER_COUNT];
u32 *color_buffer[FRAME_BUFFER_COUNT];

f32 aspect_ratio;

// for pos_color shader
namespace PosColorShdr
{
rsxProgramConst* mvpMatUniform;
rsxProgramAttrib* posAttrib;
rsxProgramAttrib* colorAttrib;
}
// for pos texcoord shader
namespace PosTexCoordShdr
{
rsxProgramConst* mvpMatUniform;
rsxProgramAttrib* posAttrib;
rsxProgramAttrib* texcoordAttrib;
}

static u32 sResolutionIds[] = {
    VIDEO_RESOLUTION_960x1080,
    VIDEO_RESOLUTION_720,
    VIDEO_RESOLUTION_480,
    VIDEO_RESOLUTION_576
};
static size_t RESOLUTION_ID_COUNT = sizeof(sResolutionIds)/sizeof(u32);

static u32 sLabelVal = 1;

//static RSXDebugFontRenderer *debugFontRenderer;

static void waitFinish()
{
	rsxSetWriteBackendLabel(context,GCM_LABEL_INDEX,sLabelVal);

	rsxFlushBuffer(context);

	while(*(vu32*)gcmGetLabelAddress(GCM_LABEL_INDEX)!=sLabelVal)
		usleep(30);

	++sLabelVal;
}

static void waitRSXIdle()
{
	rsxSetWriteBackendLabel(context,GCM_LABEL_INDEX,sLabelVal);
	rsxSetWaitLabel(context,GCM_LABEL_INDEX,sLabelVal);

	++sLabelVal;

	waitFinish();
}

void initVideoConfiguration()
{
    s32 rval = 0;
    s32 resId = 0;

    for (size_t i=0;i < RESOLUTION_ID_COUNT;i++) {
        rval = videoGetResolutionAvailability(VIDEO_PRIMARY, sResolutionIds[i], VIDEO_ASPECT_AUTO, 0);
        if (rval != 1) continue;

        resId = sResolutionIds[i];
        rval = videoGetResolution(resId, &vResolution);
        if(!rval) break;
    }

    if(rval) {
        printf("Error: videoGetResolutionAvailability failed. No usable resolution.\n");
        exit(1);
    }

    videoConfiguration config = {
        (u8)resId,
        VIDEO_BUFFER_FORMAT_XRGB,
        VIDEO_ASPECT_AUTO,
        {0,0,0,0,0,0,0,0,0},
        (u32)vResolution.width*4
    };

    rval = videoConfigure(VIDEO_PRIMARY, &config, NULL, 0);
    if(rval) {
        printf("Error: videoConfigure failed.\n");
        exit(1);
    }

    videoState state;

    rval = videoGetState(VIDEO_PRIMARY, 0, &state);
    switch(state.displayMode.aspect) {
        case VIDEO_ASPECT_4_3:
            aspect_ratio = 4.0f/3.0f;
            break;
        case VIDEO_ASPECT_16_9:
            aspect_ratio = 16.0f/9.0f;
            break;
        default:
            printf("unknown aspect ratio %x\n", state.displayMode.aspect);
            aspect_ratio = 16.0f/9.0f;
            break;
    }

    display_height = vResolution.height;
    display_width = vResolution.width;
}

void setRenderTarget(u32 index)
{
	gcmSurface sf;

	sf.colorFormat		= GCM_SURFACE_X8R8G8B8;
	sf.colorTarget		= GCM_SURFACE_TARGET_0;
	sf.colorLocation[0]	= GCM_LOCATION_RSX;
	sf.colorOffset[0]	= color_offset[index];
	sf.colorPitch[0]	= color_pitch;

	sf.colorLocation[1]	= GCM_LOCATION_RSX;
	sf.colorLocation[2]	= GCM_LOCATION_RSX;
	sf.colorLocation[3]	= GCM_LOCATION_RSX;
	sf.colorOffset[1]	= 0;
	sf.colorOffset[2]	= 0;
	sf.colorOffset[3]	= 0;
	sf.colorPitch[1]	= 64;
	sf.colorPitch[2]	= 64;
	sf.colorPitch[3]	= 64;

	sf.depthFormat		= GCM_SURFACE_ZETA_Z24S8;
	sf.depthLocation	= GCM_LOCATION_RSX;
	sf.depthOffset		= depth_offset;
	sf.depthPitch		= depth_pitch;

	sf.type				= GCM_SURFACE_TYPE_LINEAR;
	sf.antiAlias		= GCM_SURFACE_CENTER_1;

	sf.width			= display_width;
	sf.height			= display_height;
	sf.x				= 0;
	sf.y				= 0;

	rsxSetSurface(context,&sf);
}

void init_screen(void *host_addr,u32 size)
{
    u32 zs_depth = 4;
    u32 color_depth = 4;

	rsxInit(&context,DEFUALT_CB_SIZE,size,host_addr);

	initVideoConfiguration();

	waitRSXIdle();

	gcmSetFlipMode(GCM_FLIP_VSYNC);

	color_pitch = display_width*color_depth;
	depth_pitch = display_width*zs_depth;

	for (u32 i=0;i < FRAME_BUFFER_COUNT;i++) {
		color_buffer[i] = (u32*)rsxMemalign(64,(display_height*color_pitch));
		rsxAddressToOffset(color_buffer[i],&color_offset[i]);
		gcmSetDisplayBuffer(i,color_offset[i],color_pitch,display_width,display_height);
	}

	depth_buffer = (u32*)rsxMemalign(64, display_height*depth_pitch);
	rsxAddressToOffset(depth_buffer,&depth_offset);

	//debugFontRenderer = new RSXDebugFontRenderer(context);
}

void waitflip()
{
	while(gcmGetFlipStatus()!=0)
		usleep(200);
	gcmResetFlipStatus();
}

static inline void flip()
{
	if(!first_fb) waitflip();
	else gcmResetFlipStatus();

	gcmSetFlip(context,curr_fb);
	rsxFlushBuffer(context);

	gcmSetWaitFlip(context);

	curr_fb ^= 1;
	setRenderTarget(curr_fb);

	first_fb = 0;
}

void SetDrawEnv()
{
    rsxSetColorMask(
        context,
        GCM_COLOR_MASK_B |
            GCM_COLOR_MASK_G |
            GCM_COLOR_MASK_R |
            GCM_COLOR_MASK_A
    );

    rsxSetColorMaskMrt(context, 0);

    u16 x,y,w,h;
    f32 min, max;
    f32 scale[4],offset[4];

    x = 0;
    y = 0;
    w = display_width;
    h = display_height;
    min = 0.0f;
    max = 1.0f;
    scale[0] = w*0.5f;
    scale[1] = h*-0.5f;
    scale[2] = (max - min)*0.5f;
    scale[3] = 0.0f;
    offset[0] = x + w*0.5f;
    offset[1] = y + h*0.5f;
    offset[2] = (max + min)*0.5f;
    offset[3] = 0.0f;

    rsxSetViewport(context, x, y, w, h, min, max, scale, offset);
    rsxSetScissor(context, x, y, w, h);

    rsxSetDepthTestEnable(context, GCM_TRUE);
    rsxSetDepthFunc(context, GCM_LESS);
    rsxSetShadeModel(context, GCM_SHADE_MODEL_SMOOTH);
    rsxSetDepthWriteEnable(context, 1);
    rsxSetFrontFace(context, GCM_FRONTFACE_CCW);
    rsxSetBlendEnable(context, GCM_TRUE); // enable blending
    rsxSetBlendEquation(context, GCM_FUNC_ADD, GCM_FUNC_ADD); // default blend equation...
    rsxSetBlendFunc(
        context,
        GCM_SRC_ALPHA, // sfcolor (source)
        GCM_ONE_MINUS_SRC_ALPHA, // dfcolor (destination)
        GCM_SRC_ALPHA, // sfalpha
        GCM_ONE_MINUS_SRC_ALPHA // dfalpha
    );
}

VertexBuffer Renderer::sAabbVertBuf;
f32* Renderer::sSpriteVerts = nullptr;

Renderer::Renderer() :
    mWidth(0),
    mHeight(0),
    mBgColor(0xFF008080),
    mHostAddr(nullptr),
    mContext(nullptr),
    mCurSpriteVert(0),
    mCurTex(nullptr)
{}

Renderer::~Renderer()
{}

bool Renderer::Init(int width, int height, bool fullScreen, const char* title)
{
    (void)width;
    (void)height;
    (void)fullScreen;
    (void)title;

    mHostAddr = memalign(HOST_ADDR_ALIGNMENT, HOSTBUFFER_SIZE);
    if (!mHostAddr) {
        printf("Renderer failed to alloc hostaddr\n");
        return false;
    }
    init_screen(mHostAddr, HOSTBUFFER_SIZE);
    mContext = context; // rsxutil.h
    SetDrawEnv();
    setRenderTarget(curr_fb);

    if (!mPosColorShader.InitFromMemory(
            (void*)pos_color_shader_vpo,
            (void*)pos_color_shader_fpo))
    {
        printf("Renderer failed to init pos color shader\n");
        return false;
    }
    PosColorShdr::mvpMatUniform = rsxVertexProgramGetConst(mPosColorShader.mVpo, "uMvpMatrix");
    PosColorShdr::posAttrib = rsxVertexProgramGetAttrib(mPosColorShader.mVpo, "aPos");
    PosColorShdr::colorAttrib = rsxVertexProgramGetAttrib(mPosColorShader.mVpo, "aColor");
    if (!PosColorShdr::mvpMatUniform) {
        printf("Failed to get mvpMatUniform from shader\n");
        return false;
    }
    if (!PosColorShdr::posAttrib) {
        printf("Failed to get posAttrib from shader\n");
        return false;
    }
    if (!PosColorShdr::colorAttrib) {
        printf("Failed to get colorAttrib from shader\n");
        return false;
    }

    if (!mPosTexCoordShader.InitFromMemory(
            (void*)pos_texcoord_shader_vpo,
            (void*)pos_texcoord_shader_fpo))
    {
        printf("Failed to init pos texcoord shader\n");
        return false;
    }
    PosTexCoordShdr::mvpMatUniform = rsxVertexProgramGetConst(mPosTexCoordShader.mVpo, "uMvpMatrix");
    PosTexCoordShdr::posAttrib = rsxVertexProgramGetAttrib(mPosTexCoordShader.mVpo, "aPos");
    PosTexCoordShdr::texcoordAttrib = rsxVertexProgramGetAttrib(mPosTexCoordShader.mVpo, "aTexCoord");
    if (!PosTexCoordShdr::mvpMatUniform) {
        printf("Failed to get mvpMatUniform from tex coord shader\n");
        return false;
    }
    if (!PosTexCoordShdr::posAttrib) {
        printf("Failed to get posAttrib from tex coord shader\n");
        return false;
    }
    if (!PosTexCoordShdr::texcoordAttrib) {
        printf("Failed to get texcoordAttrib from tex coord shader\n");
        return false;
    }

    mProjMat = GameMath::Perspective(
        GameMath::Deg2Rad(60.0f),
        /*640.0f/480.0f*/aspect_ratio,
        1.0f/*0.00125f*//*2.0f*/,
        500.0f
    );

    if (sAabbVertBuf.mVerticesSize == 0) {
        if (!InitAabbVertBuf()) {
            printf("Failed to init AABB debug draw vertex buffer\n");
            return false;
        }
    }

    if (sSpriteVerts == nullptr) {
        if (!InitSpriteVertBuf()) {
            printf("Failed to init Sprite vertbuf\n");
            return false;
        }
    }

    mWidth = display_width;
    mHeight = display_height;

    return true;
}

void Renderer::SetBGColor(uint8_t r, uint8_t g, uint8_t b)
{
    mBgColor = 0xFF000000 |
        (((u32)r) << 16) |
        (((u32)g) << 8) |
        (((u32)b) << 0);
}

void Renderer::Clear()
{
    u32 i;

    mCurSpriteVert = 0;

    SetDrawEnv();
    rsxSetClearColor(context, mBgColor);
    rsxSetClearDepthStencil(context, 0xffffff00);
    rsxClearSurface(
        context,
        GCM_CLEAR_R |
            GCM_CLEAR_G |
            GCM_CLEAR_B |
            GCM_CLEAR_A |
            GCM_CLEAR_S |
            GCM_CLEAR_Z
    );

    // these seemed to cause weird vertices freakout issues on real hardware
    // args - context, cullNearFar, zClampEnable, cullIgnoreW
    //rsxSetZMinMaxControl(context, 0, 1, 1);
    //rsxSetZMinMaxControl(context, 0, 0, 0);
    //rsxSetZMinMaxControl(context, 0, 0, 1);
    //rsxSetZMinMaxControl(context, 0, 1, 0);
    //rsxSetZMinMaxControl(context, 0, 1, 1);
    //rsxSetZMinMaxControl(context, 1, 0, 0);
    //rsxSetZMinMaxControl(context, 1, 0, 1);
    //rsxSetZMinMaxControl(context, 1, 1, 0);
    //rsxSetZMinMaxControl(context, 1, 1, 1);

    for (i=0; i<8; i++) {
        rsxSetViewportClip(context, i, display_width, display_height);
    }
}

void Renderer::Update()
{
    flip();
}

void Renderer::DrawVertexBuffer(
    GameMath::Mat4& modelMat,
    GameMath::Mat4& viewMat,
    const VertexBuffer& vb)
{
    u32 offset;

    GameMath::Mat4 mvpMat = mProjMat * viewMat * modelMat;
    // TODO - make this mCurShader or something
    Shader* shader = nullptr;

    size_t floatsPerVertex = 1;
    switch (vb.mType)
    {
    case VertexBuffer::POS_COLOR:
    {
        shader = &mPosColorShader;
        floatsPerVertex = 6; // xyz rgb
        // position
        rsxAddressToOffset(&vb.mVertices[0], &offset);
        rsxBindVertexArrayAttrib(
            context,
            PosColorShdr::posAttrib->index,
            0,
            offset,
            floatsPerVertex*sizeof(f32),
            3,
            GCM_VERTEX_DATA_TYPE_F32,
            GCM_LOCATION_RSX
        );
        // color
        rsxAddressToOffset(&vb.mVertices[3], &offset); // skip xyz
        rsxBindVertexArrayAttrib(
            context,
            PosColorShdr::colorAttrib->index,
            0,
            offset,
            floatsPerVertex*sizeof(f32),
            3,
            GCM_VERTEX_DATA_TYPE_F32,
            GCM_LOCATION_RSX
        );
        
        break;
    }
    case VertexBuffer::POS_TEXCOORD:
    {
        shader = &mPosTexCoordShader;
        floatsPerVertex = 5; // xyz uv
        // position
        rsxAddressToOffset(&vb.mVertices[0], &offset);
        rsxBindVertexArrayAttrib(
            context,
            PosTexCoordShdr::posAttrib->index,
            0,
            offset,
            floatsPerVertex*sizeof(f32),
            3,
            GCM_VERTEX_DATA_TYPE_F32,
            GCM_LOCATION_RSX
        );
        // tex coord
        rsxAddressToOffset(&vb.mVertices[3], &offset); // skip xyz
        rsxBindVertexArrayAttrib(
            context,
            PosTexCoordShdr::texcoordAttrib->index,
            0,
            offset,
            floatsPerVertex*sizeof(f32),
            2,
            GCM_VERTEX_DATA_TYPE_F32,
            GCM_LOCATION_RSX
        );

        break;
    }
    default:
        printf("DrawVertexBuffer unhandled type\n");
        return;
    }

    // TODO - other shaders
    //Shader& shader = mPosColorShader;

    // Load vertex shader
    rsxLoadVertexProgram(context, shader->mVpo, shader->mVpUcode);
    // Set uniforms
    // TODO - not if-else this
    if (vb.mType == VertexBuffer::POS_COLOR) {
        rsxSetVertexProgramParameter(context, shader->mVpo, PosColorShdr::mvpMatUniform, (float*)&mvpMat.r0c0);
    } else {
        rsxSetVertexProgramParameter(context, shader->mVpo, PosTexCoordShdr::mvpMatUniform, (float*)&mvpMat.r0c0);
    }

    // specify fragment shader
    rsxLoadFragmentProgramLocation(context, shader->mFpo, shader->mFpOffset, GCM_LOCATION_RSX);

    // Load texture if necessary
    if (vb.mType == VertexBuffer::POS_TEXCOORD && mCurTex != nullptr) {
        u8 shaderTexUnit = 0;

        // fragment shader sampler
        rsxProgramAttrib* texSmplrAttr = rsxFragmentProgramGetAttrib(shader->mFpo, "uTexture");
        if (!texSmplrAttr) {
            printf("Failed to get uTexture sampler from fragment shader\n");
            return;
        }
        shaderTexUnit = texSmplrAttr->index;

        u32 width = mCurTex->mWidth;
        u32 height = mCurTex->mHeight;
        u32 pitch = width * 4; // assumes 4 bytes per pixel

        rsxInvalidateTextureCache(context, GCM_INVALIDATE_TEXTURE);

        // TODO - can probably be done in Texture::Init()
        gcmTexture& texture = mCurTex->mTexture;
        texture.format = (GCM_TEXTURE_FORMAT_A8R8G8B8 | GCM_TEXTURE_FORMAT_LIN);
        texture.mipmap = 1;
        texture.dimension = GCM_TEXTURE_DIMS_2D;
        texture.cubemap = GCM_FALSE;
        texture.remap = 
            ((GCM_TEXTURE_REMAP_TYPE_REMAP << GCM_TEXTURE_REMAP_TYPE_B_SHIFT) |
            (GCM_TEXTURE_REMAP_TYPE_REMAP << GCM_TEXTURE_REMAP_TYPE_G_SHIFT) |
            (GCM_TEXTURE_REMAP_TYPE_REMAP << GCM_TEXTURE_REMAP_TYPE_R_SHIFT) |
            (GCM_TEXTURE_REMAP_TYPE_REMAP << GCM_TEXTURE_REMAP_TYPE_A_SHIFT) |
            (GCM_TEXTURE_REMAP_COLOR_B << GCM_TEXTURE_REMAP_COLOR_B_SHIFT) |
            (GCM_TEXTURE_REMAP_COLOR_G << GCM_TEXTURE_REMAP_COLOR_G_SHIFT) |
            (GCM_TEXTURE_REMAP_COLOR_R << GCM_TEXTURE_REMAP_COLOR_R_SHIFT) |
            (GCM_TEXTURE_REMAP_COLOR_A << GCM_TEXTURE_REMAP_COLOR_A_SHIFT));
        texture.width = width;
        texture.height = height;
        texture.depth = 1;
        texture.location = GCM_LOCATION_RSX;
        texture.pitch = pitch;
        texture.offset = mCurTex->mOffset;

        rsxLoadTexture(context, shaderTexUnit, &texture);
        rsxTextureControl(context, shaderTexUnit, GCM_TRUE, 0<<8, 12<<8, GCM_TEXTURE_MAX_ANISO_1);
        rsxTextureFilter(context, shaderTexUnit, 0, GCM_TEXTURE_LINEAR, GCM_TEXTURE_LINEAR, GCM_TEXTURE_CONVOLUTION_QUINCUNX);
        rsxTextureWrapMode(context, shaderTexUnit, GCM_TEXTURE_REPEAT, GCM_TEXTURE_REPEAT, GCM_TEXTURE_REPEAT, 0, GCM_TEXTURE_ZFUNC_LESS, 0);
    }

    // Not sure what this does...
    /*rsxSetUserClipPlaneControl(
        context,
        GCM_USER_CLIP_PLANE_DISABLE,
        GCM_USER_CLIP_PLANE_DISABLE,
        GCM_USER_CLIP_PLANE_DISABLE,
        GCM_USER_CLIP_PLANE_DISABLE,
        GCM_USER_CLIP_PLANE_DISABLE,
        GCM_USER_CLIP_PLANE_DISABLE
    );*/

    if (vb.mIndicesSize > 0) {
        rsxAddressToOffset(&vb.mIndices[0], &offset);
        rsxDrawIndexArray(
            context,
            GCM_TYPE_TRIANGLES,
            offset,
            vb.mIndicesSize,
            GCM_INDEX_TYPE_32B,
            GCM_LOCATION_RSX
        );
    }
    else {
        //printf("TODO - non-indexed vert buf drawing not added\n");
        rsxDrawVertexArray(
            context,
            GCM_TYPE_TRIANGLES,
            0,
            vb.mVerticesSize / floatsPerVertex
        );
    }
}

void Renderer::DrawAabb(AABB& aabb, GameMath::Mat4& viewMat)
{
    u32 offset;

    GameMath::Mat4 modelMat =
        GameMath::Translate(aabb.pos) *
        GameMath::Scale(
            aabb.max.x - aabb.min.x,
            aabb.max.y - aabb.min.y,
            aabb.max.z - aabb.min.z
        );
    GameMath::Mat4 mvpMat = mProjMat * viewMat * modelMat;

    Shader* shader = &mPosColorShader;

    size_t floatsPerVertex = 6; // xyz rgb
    // position
    rsxAddressToOffset(&sAabbVertBuf.mVertices[0], &offset);
    rsxBindVertexArrayAttrib(
        context,
        PosColorShdr::posAttrib->index,
        0,
        offset,
        floatsPerVertex*sizeof(f32),
        3,
        GCM_VERTEX_DATA_TYPE_F32,
        GCM_LOCATION_RSX
    );
    // color
    rsxAddressToOffset(&sAabbVertBuf.mVertices[3], &offset); // skip xyz
    rsxBindVertexArrayAttrib(
        context,
        PosColorShdr::colorAttrib->index,
        0,
        offset,
        floatsPerVertex*sizeof(f32),
        3,
        GCM_VERTEX_DATA_TYPE_F32,
        GCM_LOCATION_RSX
    );

    // Load vertex shader
    rsxLoadVertexProgram(context, shader->mVpo, shader->mVpUcode);
    // Set uniforms
    rsxSetVertexProgramParameter(context, shader->mVpo, PosColorShdr::mvpMatUniform, (float*)&mvpMat.r0c0);
    // specify fragment shader
    rsxLoadFragmentProgramLocation(context, shader->mFpo, shader->mFpOffset, GCM_LOCATION_RSX);

    rsxAddressToOffset(&sAabbVertBuf.mIndices[0], &offset);
    rsxDrawIndexArray(
        context,
        GCM_TYPE_LINES,
        offset,
        sAabbVertBuf.mIndicesSize,
        GCM_INDEX_TYPE_32B,
        GCM_LOCATION_RSX
    );
}

void Renderer::DrawSprite(Sprite& sprite)
{
    u32 offset;

    // disable depth test
    rsxSetDepthTestEnable(context, GCM_FALSE);

    // 5 floats per vertex
    f32* vertBuf = &sSpriteVerts[mCurSpriteVert*5];
    mCurSpriteVert += 6; // 6 vertices per sprite draw (2 triangles)
    if (mCurSpriteVert >= SPRITE_NVERTICES) {
        printf("ERROR - exceeded max sprite draws per frame\n");
        return;
    }

    // updated tex coords
    float leftU = sprite.mU;
    float rightU = leftU + sprite.mW;
    float topV = 1.0f - sprite.mV;
    float bottomV = 1.0f - (sprite.mV + sprite.mH);
    
    // pos       texcoord
    // 0  1  2   3 4
    // 5  6  7   8 9
    // 10 11 12  13 14
    // 15 16 17  18 19
    // 20 21 22  23 24
    // 25 26 27  28 29
    vertBuf[3] = leftU; vertBuf[4] = bottomV; // bottom left
    vertBuf[8] = rightU; vertBuf[9] = bottomV; // bottom right
    vertBuf[13] = rightU; vertBuf[14] = topV; // top right

    vertBuf[18] = leftU; vertBuf[19] = bottomV; // bottom left
    vertBuf[23] = rightU; vertBuf[24] = topV; // top right
    vertBuf[28] = leftU; vertBuf[29] = topV; // top left

    SetTexture(*sprite.mTexture);
    
    // scale size from 0..1 to -1..1
    float normW = sprite.mWidth * 2.0f;
    float normH = sprite.mHeight * 2.0f;
    GameMath::Mat4 mvpMat =
        GameMath::Translate(sprite.mX, sprite.mY, 0.0f) *
        
        // need to offset by w/2, h/2 because we draw relative to top left corner,
        // as opposed to the center of the image
        GameMath::Translate(normW/2,-normH/2,0.0f) *
        GameMath::Rotate(GameMath::Deg2Rad(sprite.mRotDegrees),
            GameMath::Vec3(0.0f, 0.0f, 1.0f)) *
        GameMath::Translate(-normW/2,normH/2,0.0f) *
        
        GameMath::Scale(normW, normH, 1.0f);


    Shader* shader = &mPosTexCoordShader;
    size_t floatsPerVertex = 5; // xyz uv
    // position
    rsxAddressToOffset(&vertBuf[0], &offset);
    rsxBindVertexArrayAttrib(
        context,
        PosTexCoordShdr::posAttrib->index,
        0,
        offset,
        floatsPerVertex*sizeof(f32),
        3,
        GCM_VERTEX_DATA_TYPE_F32,
        GCM_LOCATION_RSX
    );
    // tex coord
    rsxAddressToOffset(&vertBuf[3], &offset); // skip xyz
    rsxBindVertexArrayAttrib(
        context,
        PosTexCoordShdr::texcoordAttrib->index,
        0,
        offset,
        floatsPerVertex*sizeof(f32),
        2,
        GCM_VERTEX_DATA_TYPE_F32,
        GCM_LOCATION_RSX
    );

    // Load vertex shader
    rsxLoadVertexProgram(context, shader->mVpo, shader->mVpUcode);
    // Set uniforms
    rsxSetVertexProgramParameter(context, shader->mVpo, PosTexCoordShdr::mvpMatUniform, (float*)&mvpMat.r0c0);

    // specify fragment shader
    rsxLoadFragmentProgramLocation(context, shader->mFpo, shader->mFpOffset, GCM_LOCATION_RSX);

    // Load texture
    if (mCurTex != nullptr) {
        u8 shaderTexUnit = 0;

        // fragment shader sampler
        rsxProgramAttrib* texSmplrAttr = rsxFragmentProgramGetAttrib(shader->mFpo, "uTexture");
        if (!texSmplrAttr) {
            printf("Failed to get uTexture sampler from fragment shader\n");
            return;
        }
        shaderTexUnit = texSmplrAttr->index;

        u32 width = mCurTex->mWidth;
        u32 height = mCurTex->mHeight;
        u32 pitch = width * 4; // assumes 4 bytes per pixel

        rsxInvalidateTextureCache(context, GCM_INVALIDATE_TEXTURE);

        // TODO - can probably be done in Texture::Init()
        gcmTexture& texture = mCurTex->mTexture;
        texture.format = (GCM_TEXTURE_FORMAT_A8R8G8B8 | GCM_TEXTURE_FORMAT_LIN);
        texture.mipmap = 1;
        texture.dimension = GCM_TEXTURE_DIMS_2D;
        texture.cubemap = GCM_FALSE;
        texture.remap = 
            ((GCM_TEXTURE_REMAP_TYPE_REMAP << GCM_TEXTURE_REMAP_TYPE_B_SHIFT) |
            (GCM_TEXTURE_REMAP_TYPE_REMAP << GCM_TEXTURE_REMAP_TYPE_G_SHIFT) |
            (GCM_TEXTURE_REMAP_TYPE_REMAP << GCM_TEXTURE_REMAP_TYPE_R_SHIFT) |
            (GCM_TEXTURE_REMAP_TYPE_REMAP << GCM_TEXTURE_REMAP_TYPE_A_SHIFT) |
            (GCM_TEXTURE_REMAP_COLOR_B << GCM_TEXTURE_REMAP_COLOR_B_SHIFT) |
            (GCM_TEXTURE_REMAP_COLOR_G << GCM_TEXTURE_REMAP_COLOR_G_SHIFT) |
            (GCM_TEXTURE_REMAP_COLOR_R << GCM_TEXTURE_REMAP_COLOR_R_SHIFT) |
            (GCM_TEXTURE_REMAP_COLOR_A << GCM_TEXTURE_REMAP_COLOR_A_SHIFT));
        texture.width = width;
        texture.height = height;
        texture.depth = 1;
        texture.location = GCM_LOCATION_RSX;
        texture.pitch = pitch;
        texture.offset = mCurTex->mOffset;

        rsxLoadTexture(context, shaderTexUnit, &texture);
        rsxTextureControl(context, shaderTexUnit, GCM_TRUE, 0<<8, 12<<8, GCM_TEXTURE_MAX_ANISO_1);
        rsxTextureFilter(context, shaderTexUnit, 0, GCM_TEXTURE_LINEAR, GCM_TEXTURE_LINEAR, GCM_TEXTURE_CONVOLUTION_QUINCUNX);
        rsxTextureWrapMode(context, shaderTexUnit, GCM_TEXTURE_REPEAT, GCM_TEXTURE_REPEAT, GCM_TEXTURE_REPEAT, 0, GCM_TEXTURE_ZFUNC_LESS, 0);
    }

    rsxDrawVertexArray(
        context,
        GCM_TYPE_TRIANGLES,
        0,
        6 // 6 vertices == 2 triangles
    );

    // reenable depth test
    rsxSetDepthTestEnable(context, GCM_TRUE);
}

bool Renderer::InitAabbVertBuf()
{
    float vertices[6*8] = {
        // pos                  color
        -0.5f,  0.0f, -0.5f,    0.0f, 0.0f, 1.0f, // 0 bottom back left
        -0.5f,  0.0f,  0.5f,    0.0f, 0.0f, 1.0f, // 1 bottom front left
         0.5f,  0.0f, -0.5f,    0.0f, 0.0f, 1.0f, // 2 bottom back right
         0.5f,  0.0f,  0.5f,    0.0f, 0.0f, 1.0f, // 3 bottom front right
        -0.5f,  1.0f, -0.5f,    0.0f, 0.0f, 1.0f, // 4 top back left
        -0.5f,  1.0f,  0.5f,    0.0f, 0.0f, 1.0f, // 5 top front left
         0.5f,  1.0f, -0.5f,    0.0f, 0.0f, 1.0f, // 6 top back right
         0.5f,  1.0f,  0.5f,    0.0f, 0.0f, 1.0f, // 7 top front right
    };
    int indices[2*12] = {
        0, 1,
        0, 2,
        0, 4,
        1, 3,
        2, 3,
        2, 6,
        3, 7,
        1, 5,
        4, 5,
        5, 7,
        4, 6,
        6, 7
    };
    return sAabbVertBuf.Init(
        VertexBuffer::POS_COLOR,
        vertices,
        6*8,
        indices,
        2*12
    );
}

bool Renderer::InitSpriteVertBuf()
{
    // 5 floats per vertex
    sSpriteVerts = (f32*)rsxMemalign(
        128,
        SPRITE_NVERTICES*5*sizeof(f32)
    );
    if (!sSpriteVerts) {
        printf("ERROR - failed to init sSpriteVertBuf\n");
        return false;
    }

    float vertices[5*6] = {
        // position         texcoord
        0.0f, -1.0f, 0.0f,   0.0f, 0.0f, // bottom left
        1.0f, -1.0f, 0.0f,   1.0f, 0.0f, // bottom right
        1.0f, 0.0f, 0.0f,   1.0f, 1.0f, // top right

        0.0f, -1.0f, 0.0f,   0.0f, 0.0f, // bottom left
        1.0f, 0.0f, 0.0f,   1.0f, 1.0f, // top right
        0.0f, 0.0f, 0.0f,   0.0f, 1.0f, // top left
    };

    int i = 0;
    while (i<SPRITE_NVERTICES) {
        for (int j=0; j<6; ++j, ++i) {
            float* in = &vertices[j*5];
            float* out = &sSpriteVerts[i*5];
            out[0] = in[0];
            out[1] = in[1];
            out[2] = in[2];
            out[3] = in[3];
            out[4] = in[4];
        }
    }

    return true;
}

void Renderer::SetTexture(Texture& tex)
{
    mCurTex = &tex;
}

} // namespace PS3
} // namespace GameLib

