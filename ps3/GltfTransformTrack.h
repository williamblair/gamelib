#ifndef GAMELIB_PS3_GLTF_TRANSFORM_TRACK_H_INCLUDED
#define GAMELIB_PS3_GLTF_TRANSFORM_TRACK_H_INCLUDED

#include <GltfTrack.h>
#include <GltfTransform.h>

namespace GameLib
{
namespace PS3
{
namespace Gltf
{

class TransformTrack
{
public:

    TransformTrack();

    unsigned int GetId() const { return id; }
    void SetId(unsigned int id) { this->id = id; }
    
    VectorTrack& GetPositionTrack() { return position; }
    QuaternionTrack& GetRotationTrack() { return rotation; }
    VectorTrack& GetScaleTrack() { return scale; }

    float GetStartTime() const;
    float GetEndTime() const;
    bool IsValid() const; // true if any of the three tracks are valid

    Transform Sample(const Transform& ref, float time, bool looping);

private:
    unsigned int id; // bone/joint id this track is used for
    
    VectorTrack position;
    QuaternionTrack rotation;
    VectorTrack scale;
};

} // namespace Gltf
} // namespace PS3
} // namespace GameLib

#endif // GAMELIB_PS3_GLTF_TRANSFORM_TRACK_H_INCLUDED

