#ifndef GAMELIB_PS3_GLTF_INTERPOLATION_H_INCLUDED
#define GAMELIB_PS3_GLTF_INTERPOLATION_H_INCLUDED

namespace GameLib
{
namespace PS3
{
namespace Gltf
{

enum class Interpolation
{
    Constant,
    Linear,
    Cubic
};

} // namespace Gltf
} // namespace PS3
} // namespace GameLib

#endif // GAMELIB_PS3_GLTF_INTERPOLATION_H_INCLUDED

