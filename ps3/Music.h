#ifndef GAMELIB_PS3_MUSIC_H_INCLUDED
#define GAMELIB_PS3_MUSIC_H_INCLUDED

#include <audio/audio.h>

#include <Thread.h>

namespace GameLib
{
namespace PS3
{

class Music
{

friend class SoundEffect;

public:
    struct WAVHeader {
        char riff[4]; // should equal "RIFF"
        int32_t fileSize;
        char wave[4]; // should equal "WAVE"
        char fmt[4]; // should have trailing null, "fmt "
        int32_t fmtLen;
        int16_t fmtType; // 1 == PCM
        int16_t numChannels;
        int32_t sampleRate; // we want 48000
        int32_t dataRateBytes; // (sample rate * bitsPerSample * channels) / 8
        int16_t sampleSizeBytes; // (bitsPerSample * channels) / 8
        int16_t bitsPerSample;
        char data[4]; // "data" chunk header
        int32_t dataSize; // size of the data section after this header
    };

    struct WAVStreamData {
        int wavFd;
        int fileSizeBytes; // size of the data portion of the WAV file
        int fileBytesRemain; // remaining bytes of the file data portion
        unsigned char* bufPtr; // pointer into below buffer
        unsigned int bufSizeRemain; // end of buffer - bufPtr
        unsigned char buffer[2048*4]; // save 2048 samples at once
        bool looping; // wether or not to loop back to beginning when reach end of file
        bool ended; // true if reached the end of the file and not looping
    };

    Music();
    ~Music();

    // Must be a WAV file, 48000 sample rate
    bool Init(const char* fileName);

    void Play(const bool loop);
    void Stop();

private:
    audioPortParam mParams;
    audioPortConfig mConfig;
    u32 mPortNum;

    WAVHeader mWavHdr;
    WAVStreamData mWavStream;

    static bool sAudioInitted;

    bool mRunning;
    bool mLooping;
    bool mPaused;

    // Thread to read more audio samples from WAV file and feed them
    // to PS3 audio buffer
    Thread mThread;
    bool mThreadJoined;
    static void sThreadFcn(void* arg);

    // WAV file is little endian; PS3 is big endian
    static inline int32_t lend2bend32(int32_t val)
    {
        int32_t res;
        char* out = (char*)&res;
        char* in = (char*)&val;
        out[0] = in[3];
        out[1] = in[2];
        out[2] = in[1];
        out[3] = in[0];
        return res;
    }
    static inline int16_t lend2bend16(int16_t val)
    {
        int16_t res;
        char* out = (char*)&res;
        char* in = (char*)&val;
        out[0] = in[1];
        out[1] = in[0];
        return res;
    }
};

} // namespace PS3
} // namespace GameLib

#endif // GAMELIB_PS3_MUSIC_H_INCLUDED

