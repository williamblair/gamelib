#ifndef GAMELIB_PS3_SOUNDEFFECT_H_INCLUDED
#define GAMELIB_PS3_SOUNDEFFECT_H_INCLUDED

#include <Music.h>

namespace GameLib
{
namespace PS3
{

class SoundEffect
{
public:

    SoundEffect();
    ~SoundEffect();

    // Must be a .wav file, 16bit stereo, 48000 sample rate
    bool Init(const char* fileName);

    void Play(int channel = -1, bool loop = false);
    void Stop(int channel = -1);

private:
    audioPortParam mParams;
    audioPortConfig mConfig;
    u32 mPortNum;

    Music::WAVHeader mWavHdr;
    Music::WAVStreamData mWavStream;
    
    bool mRunning;
    bool mLooping;
    bool mPaused;

    // Thread to read more audio samples from the WAV file and feed them
    // to PS3 audio buffer
    Thread mThread;
    bool mThreadJoined;
    static void sThreadFcn(void* arg);
};

} // namespace PS3
} // namespace GameLib

#endif // GAMELIB_PS3_SOUNDEFFECT_H_INCLUDED

