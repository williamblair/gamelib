#ifndef GAMELIB_PS3_AABB_H_INCLUDED
#define GAMELIB_PS3_AABB_H_INCLUDED

#include <GameMath/GameMath.h>

namespace GameLib
{
namespace PS3
{

struct AABB
{
    GameMath::Vec3 min;
    GameMath::Vec3 max;
    GameMath::Vec3 pos;

    inline bool Intersects(const AABB& other) {
        if (min.x + pos.x > other.max.x + other.pos.x ||
            max.x + pos.x < other.min.x + other.pos.x ||
            min.y + pos.y > other.max.y + other.pos.y ||
            max.y + pos.y < other.min.y + other.pos.y ||
            min.z + pos.z > other.max.z + other.pos.z ||
            max.z + pos.z < other.min.z + other.pos.z)
        {
            return false;
        }
        return true;
    }
};

} // namespace PS3
} // namespace GameLib

#endif // GAMELIB_AABB_H_INCLUDED

