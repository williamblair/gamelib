#include <Shader.h>

namespace GameLib
{
namespace PS3
{

Shader::Shader() :
    mVpo(nullptr),
    mFpo(nullptr),
    mVpUcode(nullptr),
    mFpUcode(nullptr),
    mVpSize(0),
    mFpSize(0),
    mFpOffset(0),
    mFpBuffer(nullptr)
{}

Shader::~Shader()
{}

bool Shader::InitFromMemory(void* vertexProg, void* fragmentProg)
{
    mVpo = (rsxVertexProgram*)vertexProg;
    mFpo = (rsxFragmentProgram*)fragmentProg;

    rsxVertexProgramGetUCode(mVpo, &mVpUcode, &mVpSize);
    printf("VP size: %u\n", mVpSize);

    rsxFragmentProgramGetUCode(mFpo, &mFpUcode, &mFpSize);
    printf("FP size: %u\n", mFpSize);

    mFpBuffer = (u32*)rsxMemalign(64, mFpSize);
    if (!mFpBuffer) {
        printf("Shader failed to alloc fp buffer\n");
        return false;
    }

    memcpy((void*)mFpBuffer, (void*)mFpUcode, mFpSize);
    rsxAddressToOffset(mFpBuffer, &mFpOffset);

    // POSSIBLY TODO - get attribs/const members here

    return true;
}

} // namespace PS3
} // namespace GameLib

