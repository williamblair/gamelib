#include <cstdio>
#include <cstring>
#include <GltfTrack.h>

namespace GameLib
{
namespace PS3
{
namespace Gltf
{

template class Track<float,1>;
template class Track<GameMath::Vec3,3>;
template class Track<GameMath::Quat,4>;

#define Vec3 GameMath::Vec3
#define Quat GameMath::Quat

namespace TrackHelpers
{
    // linear interpolation
    inline float Interpolate(float a, float b, float t) {
        return a + (b-a) * t;
    }
    inline Vec3 Interpolate(const Vec3& a, const Vec3& b, float t) {
        return GameMath::Lerp(a, b, t);
    }
    inline Quat Interpolate(const Quat& a, const Quat& b, float t) {
        Quat result = /*Mix(a, b, t)*/GameMath::Lerp(a, b, t);
        // perform neighborhood check
        if (GameMath::Dot(a,b) < 0) {
            result = /*Mix(a, -b, t)*/GameMath::Lerp(a, -b, t);
        }
        return GameMath::Normalize(result);
    }

    // normalization if necessary for each type spec
    inline float AdjustHermiteResult(float f) {
        return f;
    }
    inline Vec3 AdjustHermiteResult(const Vec3& v) {
        return v;
    }
    inline Quat AdjustHermiteResult(const Quat& q) {
        return GameMath::Normalize(q);
    }

    // neighborhooding if necessary for each type
    inline void Neighborhood(const float& a, const float& b) {}
    inline void Neighborhood(const Vec3& a, const Vec3& b) {}
    inline void Neighborhood(const Quat& a, Quat& b) {
        if (GameMath::Dot(a,b) < 0) {
            b = -b;
        }
    }
} // namespace TrackHelpers

template<typename T, int N>
Track<T,N>::Track() :
    interpolation(Interpolation::Linear)
{}

template<typename T, int N>
void Track<T,N>::Resize(unsigned int size) {
   frames.resize(size); 
}
template<typename T, int N>
unsigned int Track<T,N>::GetSize() const {
    return frames.size();
}

template<typename T, int N>
Interpolation Track<T,N>::GetInterpolation() const {
    return interpolation;
}
template<typename T, int N>
void Track<T,N>::SetInterpolation(Interpolation interp) {
    interpolation = interp;
}

template<typename T, int N>
float Track<T,N>::GetStartTime() const {
    return frames[0].time;
}
template<typename T, int N>
float Track<T,N>::GetEndTime() const {
    return frames[frames.size()-1].time;
}

template<typename T, int N>
T Track<T,N>::Sample(float time, bool looping)
{
    switch (interpolation)
    {
    case Interpolation::Constant: return SampleConstant(time, looping);
    case Interpolation::Linear: return SampleLinear(time, looping);
    case Interpolation::Cubic: return SampleCubic(time, looping);
    default: break;
    }
    // should never happen
    return SampleConstant(time, looping);
}

template<typename T, int N>
Frame<N>& Track<T,N>::operator[](unsigned int index) {
    return frames[index];
}

template<typename T, int N>
T Track<T,N>::Hermite(float time, const T& p1, const T& s1, const T& p2, const T& s2)
{
    float tt = time * time;
    float ttt = tt * time;
    T tmpP2 = p2;
    TrackHelpers::Neighborhood(p1, tmpP2);

    float h1 = 2.0f*ttt - 3.0f*tt + 1.0f;
    float h2 = -2.0f*ttt + 3.0f*tt;
    float h3 = ttt - 2.0f*tt + time;
    float h4 = ttt - tt;

    T result = p1*h1 + tmpP2*h2 + s1*h3 + s2*h4;
    return TrackHelpers::AdjustHermiteResult(result);
}

template<typename T, int N>
int Track<T,N>::FrameIndex(float time, bool looping)
{
    // if the track has 1 frame or less it's invalid
    unsigned int size = (unsigned int)frames.size();
    if (size <= 1) { return -1; }

    // if looping, we need to map between the start and end time
    if (looping)
    {
        float startTime = frames[0].time;
        float endTime = frames[size - 1].time;
        float duration = endTime - startTime;

        time = fmodf(time - startTime, duration);
        if (time < 0.0f) {
            time += duration;
        }
        time = time + startTime;
    }
    // otherwise clamp between start/end time
    else
    {
        if (time <= frames[0].time) {
            return 0;
        }
        if (time >= frames[size - 2].time) {
            return (int)size - 2;
        }
    }

    // find the frame less than and closest to the desired time
    for (int i = (int)size - 1; i >= 0; --i) {
        if (time >= frames[i].time) {
            return i;
        }
    }
    // should never reach here
    return -1;
}

template<typename T, int N>
float Track<T,N>::AdjustTimeToFitTrack(float t, bool loop)
{
    unsigned int size = (unsigned int)frames.size();
    if (size <= 1) { return 0.0f; }

    float startTime = frames[0].time;
    float endTime = frames[size - 1].time;
    float duration = endTime - startTime;
    if (duration <= 0.0f) { return 0.0f; }

    // wrap t if looping
    if (loop) {
        t = fmodf(t - startTime, duration);
        if (t < 0.0f) {
            t += duration;
        }
        t = t + startTime;
    }
    // otherwise clamp between start/end
    else {
        if (t <= startTime) {
            t = startTime;
        }
        if (t >= endTime) {
            t = endTime;
        }
    }

    return t;
}

template<>
float Track<float,1>::Cast(float* value) {
    return value[0];
}

template<>
Vec3 Track<Vec3,3>::Cast(float* value) {
    return Vec3(value[0], value[1], value[2]);
}

template<>
Quat Track<Quat,4>::Cast(float* value) {
    return GameMath::Normalize(
        Quat(value[0], value[1], value[2], value[3])
    );
}

// no interpolation; results in immediate change in value (e.g. turn on/off)
template<typename T, int N>
T Track<T,N>::SampleConstant(float t, bool looping)
{
    int frame = FrameIndex(t, looping);
    if (frame < 0 || frame >= (int)frames.size()) {
        // TODO error
        return T();
    }

    return Cast(&frames[frame].value[0]);
}

template<typename T, int N>
T Track<T,N>::SampleLinear(float time, bool looping)
{
    int thisFrame = FrameIndex(time, looping);
    if (thisFrame < 0 || thisFrame >= (int)(frames.size()-1)) {
        printf("Sample linear this frame error, returning default\n");
        return T();
    }

    int nextFrame = thisFrame + 1;
    float trackTime = AdjustTimeToFitTrack(time, looping);
    float thisTime = frames[thisFrame].time;
    float frameDelta = frames[nextFrame].time - thisTime;
    if (frameDelta <= 0.0f) {
        printf("Frame delta < 0, returning default\n");
        return T();
    }

    float t = (trackTime - thisTime) / frameDelta;
    T start = Cast(&frames[thisFrame].value[0]);
    T end = Cast(&frames[nextFrame].value[0]);
    return TrackHelpers::Interpolate(start, end, t);
}

template<typename T, int N>
T Track<T,N>::SampleCubic(float time, bool looping)
{
    int thisFrame = FrameIndex(time, looping);
    if (thisFrame < 0 || thisFrame >= (int)(frames.size()-1)) {
        printf("Sample cubic this frame error, returning default\n");
        return T();
    }
    
    int nextFrame = thisFrame + 1;
    float trackTime = AdjustTimeToFitTrack(time, looping);
    float thisTime = frames[thisFrame].time;
    float frameDelta = frames[nextFrame].time - thisTime;
    if (frameDelta <= 0.0f) {
        printf("Frame delta < 0, returning default\n");
        return T();
    }
    
    float t = (trackTime - thisTime) / frameDelta;

    // memcpy instead of Cast so we don't normalize
    size_t floatSize = sizeof(float);
    T point1 = Cast(&frames[thisFrame].value[0]);
    T slope1;
    memcpy((void*)&slope1, (void*)frames[thisFrame].out, N * floatSize);
    slope1 = slope1 * frameDelta;

    T point2 = Cast(&frames[nextFrame].value[0]);
    T slope2;
    memcpy((void*)&slope2, (void*)frames[nextFrame].in, N * floatSize);
    slope2 = slope2 * frameDelta;

    return Hermite(t, point1, slope1, point2, slope2);
}

#undef Vec3
#undef Quat

} // namespace Gltf
} // namespace PS3
} // namespace GameLib

