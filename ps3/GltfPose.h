#ifndef GAMELIB_PS3_GLTF_POSE_H_INCLUDED
#define GAMELIB_PS3_GLTF_POSE_H_INCLUDED

#include <vector>
#include <GameMath/GameMath.h>
#include <GltfTransform.h>

namespace GameLib
{
namespace PS3
{
namespace Gltf
{

class Pose
{
public:

    Pose();
    Pose(const Pose& p);
    Pose& operator=(const Pose& p);
    Pose(unsigned int numJoints);
    
    void Resize(unsigned int size) {
        joints.resize(size);
        parents.resize(size);
    }
    unsigned int GetSize() const { return joints.size(); }
    
    int GetParent(unsigned int index) const { return parents[index]; }
    void SetParent(unsigned int index, int parent) {
        parents[index] = parent;
    }
    
    Transform GetLocalTransform(unsigned int index) const { return joints[index]; }
    void SetLocalTransform(unsigned int index, const Transform& transform) {
        joints[index] = transform;
    }
    
    // combines transforms from the root up until the desired local joint index
    Transform GetGlobalTransform(unsigned int index);
    Transform operator[](unsigned int index);
    
    // fills out with a linear array of matrices as the global transform
    // matrix of each joint in the pose
    void GetMatrixPalette(std::vector<GameMath::Mat4>& out);
    
    bool operator==(const Pose& other);
    bool operator!=(const Pose& other);

private:
    
    std::vector<Transform> joints;
    std::vector<int> parents;
};
    
} // namespace Gltf
} // namespace PS3
} // namespace GameLib

#endif // GAMELIB_PS3_GLTF_POSE_H_INCLUDED
