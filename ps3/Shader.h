#ifndef GAMELIB_PS3_SHADER_H_INCLUDED
#define GAMELIB_PS3_SHADER_H_INCLUDED

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <ppu-types.h>
#include <sys/process.h>
#include <io/pad.h>
#include <rsx/rsx.h>
#include <sysutil/sysutil.h>

namespace GameLib
{
namespace PS3
{

class Shader
{

friend class Renderer;

public:

    Shader();
    ~Shader();

    bool InitFromMemory(void* vertexProg, void* fragmentProg);

private:
    rsxVertexProgram* mVpo;
    rsxFragmentProgram* mFpo;
    void* mVpUcode;
    void* mFpUcode;
    u32 mVpSize;
    u32 mFpSize;
    u32 mFpOffset;
    u32* mFpBuffer;
};

} // namespace PS3
} // namespace GameLib

#endif // GAMELIB_PS3_SHADER_H_INCLUDED

