#include <GltfTransformTrack.h>

namespace GameLib
{
namespace PS3
{
namespace Gltf
{

TransformTrack::TransformTrack() :
    id(0)
{}

float TransformTrack::GetStartTime() const
{
    float result = 0.0f;
    bool isSet = false;

    if (position.GetSize() > 1) {
        result = position.GetStartTime();
        isSet = true;
    }

    if (rotation.GetSize() > 1) {
        float rotStart = rotation.GetStartTime();
        if (rotStart < result || !isSet) {
            result = rotStart;
            isSet = true;
        }
    }

    if (scale.GetSize() > 1) {
        float scaleStart = scale.GetStartTime();
        if (scaleStart < result || !isSet) {
            result = scaleStart;
            isSet = true;
        }
    }

    return result;
}

float TransformTrack::GetEndTime() const
{
    float result = 0.0f;
    bool isSet = false;

    if (position.GetSize() > 1) {
        result = position.GetEndTime();
        isSet = true;
    }

    if (rotation.GetSize() > 1) {
        float rotEnd = rotation.GetEndTime();
        if (rotEnd > result || !isSet) {
            result = rotEnd;
            isSet = true;
        }
    }

    if (scale.GetSize() > 1) {
        float scaleEnd = scale.GetEndTime();
        if (scaleEnd > result || !isSet) {
            result = scaleEnd;
            isSet = true;
        }
    }

    return result;
}

bool TransformTrack::IsValid() const
{
    return (position.GetSize() > 1) ||
        (rotation.GetSize() > 1) ||
        (scale.GetSize() > 1);
}

Transform TransformTrack::Sample(const Transform& ref, float time, bool looping)
{
    Transform result = ref; // default values
    if (position.GetSize() > 1) {
        result.position = position.Sample(time, looping);
    }
    if (rotation.GetSize() > 1) {
        result.rotation = rotation.Sample(time, looping);
    }
    if (scale.GetSize() > 1) {
        result.scale = scale.Sample(time, looping);
    }
    return result;
}

} // namespace Gltf
} // namespace PS3
} // namespace GameLib


