#ifndef GAMELIB_PS3_INPUT_H_INCLUDED
#define GAMELIB_PS3_INPUT_H_INCLUDED

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <ppu-types.h>

#include <sys/process.h>

#include <io/pad.h>
#include <rsx/rsx.h>
#include <sysutil/sysutil.h>

namespace GameLib
{
namespace PS3
{

class Input
{
public:

    Input();
    ~Input();

    enum Button
    {
        LEFT = 0,
        DOWN,
        RIGHT,
        UP,
        SQUARE,
        CROSS,
        CIRCLE,
        TRIANGLE,
        L1,
        L2,
        R1,
        R2
    };

    bool Init(int padnum = 0)
    {
        if (!sSysPadInitted) {
            // TODO - look into this function
            ioPadInit(7);
            sSysPadInitted = true;
        }
        mPadnum = padnum;
        return true;
    }

    void Update();
    bool IsHeld(Button b) const;
    bool IsClicked(Button b) const;
    // -127..127
    int GetLeftAnalogX() { return ((int)mPaddata.ANA_L_H) - 127; }
    int GetLeftAnalogY() { return ((int)mPaddata.ANA_L_V) - 127; }
    int GetRightAnalogX() { return ((int)mPaddata.ANA_R_H) - 127; }
    int GetRightAnalogY() { return ((int)mPaddata.ANA_R_V) - 127; }

    bool SetSmallActuator(bool isOn); // either on or off
    bool SetLargeActuator(unsigned char val); // 0-255

    bool Quit() const { return mQuit; }
    bool Up() const { return mUp; }
    bool Down() const { return mDown; }
    bool Left() const { return mLeft; }
    bool Right() const { return mRight; }

    bool UpClicked() const { return IsClicked(UP); }
    bool DownClicked() const { return IsClicked(DOWN); }
    bool LeftClicked() const { return IsClicked(LEFT); }
    bool RightClicked() const { return IsClicked(RIGHT); }

    // for PC compatibility
    int MouseMoveX() { return mMouseMoveX; }
    int MouseMoveY() { return mMouseMoveY; }

private:
    static bool sSysPadInitted;
    padInfo mPadInfo;
    padData mPaddata;
    padData mPrevPadData;
    bool mIsActive;
    padActParam mActparam;
    int mPadnum;

    bool mQuit;
    bool mLeft;
    bool mRight;
    bool mUp;
    bool mDown;
    int mMouseMoveX;
    int mMouseMoveY;
    int mLastLeftAnalogX; // mapped to -127..127 instead of 0..255
    int mLastLeftAnalogY;
};

} // namespace PS3
} // namespace GameLib

#endif // GAMELIB_PS3_INPUT_H_INCLUDED


