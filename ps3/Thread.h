#ifndef GAMELIB_PS3_THREAD_H_INCLUDED
#define GAMELIB_PS3_THREAD_H_INCLUDED

#include <sys/thread.h>

namespace GameLib
{
namespace PS3
{

// Thread function should be of form
// void myFcn(void* arg)
typedef void (*ThreadFcn)(void*);

class Thread
{
public:
    Thread();
    ~Thread();

    bool Start(ThreadFcn fcn, void* arg, const char* name);
    void Join(u64* retVal);

    // call from within the ThreadFcn to get the current thread ID
    static u64 GetThreadId();
    // call from within the ThreadFcn to yield
    static void Yield();
    // must call this from within the ThreadFcn upon exit
    static void Exit(u64 retVal);

private:
    sys_ppu_thread_t mId;
    bool mStarted;
    bool mJoined;

    static const u64 sPriority = 1500;
    static const size_t sStackSize = 0x1000;
};

} // namespace PS3
} // namespace GameLib

#endif // GAMELIB_PS3_THREAD_H_INCLUDED

