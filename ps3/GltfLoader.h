#ifndef GAMELIB_PS3_GLTF_LOADER_H_INCLUDED
#define GAMELIB_PS3_GLTF_LOADER_H_INCLUDED

#include <vector>
#include <string>
#include <cgltf.h>
#include <GltfPose.h>
#include <GltfClip.h>
#include <GltfSkeleton.h>
#include <GltfMeshImpl.h>

namespace GameLib
{
namespace PS3
{
namespace Gltf
{

cgltf_data* LoadGLTFFile(const char* path);
void FreeGLTFFile(cgltf_data* data);
Pose LoadRestPose(cgltf_data* data);
Pose LoadBindPose(cgltf_data* data);
Skeleton LoadSkeleton(cgltf_data* data);

std::vector<std::string> LoadJointNames(cgltf_data* data);
void LoadMeshes(cgltf_data* data, std::vector<Mesh>& meshes);
void LoadAnimationClips(cgltf_data* data, std::vector<Clip>& clips);

} // namespace Gltf
} // namespace PS3
} // namespace GameLib

#endif // GAMELIB_PS3_GLTF_LOADER_H_INCLUDED

