#include <cstdio>

#include <GltfMesh.h>

namespace GameLib
{
namespace PS3
{

GltfMesh::GltfMesh() :
    clip(0),
    playback(0.0f),
    mCurAnimName("")
{}

bool GltfMesh::Init(const char* fileName)
{
    cgltf_data* gltf = Gltf::LoadGLTFFile(fileName);
    if (!gltf) {
        printf("Failed to load gltf file: %s\n", fileName);
        return false;
    }
    
    Gltf::LoadMeshes(gltf, meshes);
    skeleton = Gltf::LoadSkeleton(gltf);
    Gltf::LoadAnimationClips(gltf, clips);
    
    animatedPose = skeleton.GetRestPose();
    posePalette.resize(skeleton.GetRestPose().GetSize());
    
    unsigned int numUIClips = clips.size();
    clip = 0;
    mCurAnimName = clips[clip].GetName();
    
    // fill in buffer data
    Update(0.0f);

    Gltf::FreeGLTFFile(gltf);
    
    return true;
}

void GltfMesh::Update(const float dt)
{
    playback = clips[clip].Sample(animatedPose, playback + dt);
    
    for (unsigned int i = 0,
        size = (unsigned int)meshes.size();
        i < size;
        ++i)
    {
        meshes[i].CPUSkin(skeleton, animatedPose);
    }
}

void GltfMesh::Draw(
    GameMath::Mat4& modelMat,
    GameMath::Mat4& viewMat,
    Renderer& render)
{
    for (unsigned int i = 0; i < meshes.size(); ++i) {
        render.DrawVertexBuffer(
            modelMat,
            viewMat,
            meshes[i].GetVertexBuffer()
        );
    }
}
    
} // namespace PS3
} // namespace GameLib

