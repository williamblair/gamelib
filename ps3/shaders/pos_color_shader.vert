#version 130

in vec3 aPos;
in vec3 aColor;

out vec4 fragColor;

uniform mat4 uMvpMatrix;


void main(void)
{
    gl_Position = uMvpMatrix * vec4(aPos, 1.0);
    fragColor = vec4(aColor, 1.0);
}

