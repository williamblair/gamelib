#ifndef GAMELIB_PS3_TEXTURE_H_INCLUDED
#define GAMELIB_PS3_TEXTURE_H_INCLUDED

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <ppu-types.h>

#include <sys/process.h>

#include <io/pad.h>
#include <rsx/rsx.h>
#include <sysutil/sysutil.h>

namespace GameLib
{
namespace PS3
{

class Texture
{
friend class Renderer;
friend class Sprite;
    
public:
    
    Texture();
    ~Texture();
    
    bool Init(const char* data, int width, int height, int bytesPerPixel);
    
    bool LoadFromTGA(const char* fileName);
    bool LoadFromBMP(const char* fileName);

    int GetWidth() const { return (int)mWidth; }
    int GetHeight() const { return (int)mHeight; }

    /**
     * @brief set new texture data. must be the same size as the original data.
     * @param data the new pixel data to use for this texture, format RGBA32
     * @param width the width of the pixel data (must be same as original)
     * @param height the height of the pixel data (must be same as original)
     * @param bytesPerPixel bytes per pixel of the data (must be same as original)
     * @return true on success, false on failure
     */
    bool Update(const char* data, int width, int height, int bytesPerPixel);

    /**
     * @brief get the internal RSX texture buffer so you can write to it directly
     * Its format is ARGB, 32bits per pixel. Must be called after Init().
     */
    uint8_t* GetTexBuf() { return (uint8_t*)mBuffer; }
    
private:
    u32* mBuffer;
    u32 mOffset;
    u32 mWidth;
    u32 mHeight;
    gcmTexture mTexture;
};

} // namespace PS3
} // namespace GameLib

#endif // GAMELIB_PS3_TEXTURE_H_INCLUDED

