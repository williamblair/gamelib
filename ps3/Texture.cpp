#include <Texture.h>
#include <Targa.h>
#include <Bmp.h>
#include <stdio.h>
#include <iostream>

#include <fstream>
#include <vector>

namespace GameLib
{
namespace PS3
{
    
// little endian to big endian
static inline uint16_t lend2bend16(uint16_t lend)
{
    char* ptr = (char*)&lend;
    return (ptr[1] << 8) | 
           (ptr[0]);
}
static inline uint32_t lend2bend32(uint32_t lend)
{
    char* ptr = (char*)&lend;
    return ((uint32_t)ptr[3] << 24) |
           ((uint32_t)ptr[2] << 16) |
           ((uint32_t)ptr[1] << 8) | 
           ((uint32_t)ptr[0]);    
}

Texture::Texture() :
    mBuffer(nullptr),
    mOffset(0),
    mWidth(0),
    mHeight(0)
{}

Texture::~Texture()
{}

bool Texture::Init(const char* data, int width, int height, int bytesPerPixel)
{
    mWidth = width;
    mHeight= height;
    
    // force 4 bytes per pixel
    mBuffer = (u32*)rsxMemalign(128, width*height*4);
    if (!mBuffer) {
        printf("Failed to alloc tex buffer\n");
        return false;
    }
    rsxAddressToOffset(mBuffer, &mOffset);
    printf("Texture::init width, height, bpp: %u, %u, %u\n", width, height, bytesPerPixel);
    
    u8* buffer = (u8*)mBuffer;
    for (int i = 0; i < width * height * 4; i += 4)
    {
        buffer[i+1] = *data++; // r
        buffer[i+2] = *data++; // g
        buffer[i+3] = *data++; // b
        if (bytesPerPixel == 4) {
            buffer[i+0] = *data++; // a
        }
        else {
            buffer[i+0] = 255;
        }
        /*printf("buffer[%u]: %d\n"
                "buffer[%u]: %d\n"
                "buffer[%u]: %d\n"
                "buffer[%u]: %d\n",
        i+0, buffer[i+0],
        i+1, buffer[i+1],
        i+2, buffer[i+2],
        i+3, buffer[i+3]);*/
    }
    
    return true;
}

bool Texture::LoadFromTGA(const char* fileName)
{
    std::ifstream inFile(fileName, std::ios::binary);
    if (!inFile.is_open()) {
        std::cout << __func__ << " failed to open targa image file: " 
                  << fileName << std::endl;
        return false;
    }

    inFile.read((char*)&Targa::header, sizeof(Targa::Header));
    Targa::header.xOrigin = lend2bend16(Targa::header.xOrigin);
    Targa::header.yOrigin = lend2bend16(Targa::header.yOrigin);
    Targa::header.width = lend2bend16(Targa::header.width);
    Targa::header.height = lend2bend16(Targa::header.height);
    if (!Targa::IsImageTypeSupported(Targa::header)) {
        std::cout << __func__ << " unsupported targa type" << std::endl;
        return false;
    }

    Targa::width = Targa::header.width;
    Targa::height = Targa::header.height;

    Targa::bitsPerPixel = Targa::header.bpp;
    Targa::bytesPerPixel = Targa::header.bpp / 8;

    if (Targa::bytesPerPixel < 3) {
        std::cout << __func__ << " unsupported bytesPerPixel: " 
                  << Targa::bytesPerPixel << std::endl;
        return false;
    }

    unsigned int imageSize = Targa::width * Targa::height * Targa::bytesPerPixel;
    Targa::imageData.resize(imageSize);

    // skip past the id if there is one
    if (Targa::header.idLength > 0) {
        inFile.ignore(Targa::header.idLength);
    }

    bool result = false;

    if (Targa::IsUncompressed(Targa::header)) {
        result = Targa::LoadUncompressed(inFile);
    } else {
        result = Targa::LoadCompressed(inFile);
    }

    if ((Targa::header.imageDesc & Targa::TOP_LEFT) == Targa::TOP_LEFT) {
        Targa::FlipImageVertically();
    }

    if (!result) {
        std::cout << __func__ << ": failed to load targa" << std::endl;
        return false;
    }
    
    result = Init(
        (const char*)Targa::imageData.data(),
        Targa::width,
        Targa::height,
        Targa::bytesPerPixel
    );
    Targa::imageData.clear();
    Targa::width = 0;
    Targa::height = 0;
    Targa::bytesPerPixel = 0;
    
    return result;
}

#if 0
namespace BMP
{

#define PACKED __attribute__((packed))

struct Header
{
    uint16_t header; // the header field
    uint32_t filesize; // size of the file in bytes
    uint32_t reserved; // 4 bytes of reserved data (depending on the image that creates it)
    uint32_t pixOffset; // 4 bytes  offset (i.e. starting address) of where the pixmap can be found
} PACKED;

// Win32 struct (40 bytes)
struct BITMAPINFOHEADER
{
    uint32_t size;
    int32_t width;
    int32_t height;
    uint16_t planes;
    uint16_t bitCount;
    uint32_t compression;
    uint32_t sizeImage;
    int32_t xPelsPerMeter;
    int32_t yPelsPerMeter;
    uint32_t clrUsed;
    uint32_t clrImportant;
} PACKED;

#undef PACKED

bool Load(const char* fileName, std::vector<uint8_t>& buffer, int& width, int& height)
{
    uint8_t* bmpPxData = nullptr;
    Header hdr;
    BITMAPINFOHEADER infoHdr;

    FILE* fp = fopen(fileName, "rb");
    if (!fp) {
        printf("Failed to open BMP File: %s\n", fileName);
        return false;
    }

    // Read in the BMP header
    if (fread((void*)&hdr, 1, sizeof(hdr), fp) != sizeof(hdr)) {
        printf("Failed to read BMP header\n");
        return false;
    }
    if (((char*)&hdr.header)[0] != 'B' ||
        ((char*)&hdr.header)[1] != 'M')
    {
        printf("File is not a windows BMP file: %c, %c\n",
           ((char*)&hdr.header)[0],
           ((char*)&hdr.header)[1]
        );
        return false;
    }
    if (fread((void*)&infoHdr, 1, sizeof(infoHdr), fp) != sizeof(infoHdr)) {
        printf("Failed to read BMP info header\n");
        return false;
    }
    
    infoHdr.size = lend2bend32(infoHdr.size);
    infoHdr.width = lend2bend32(infoHdr.width);
    infoHdr.height = lend2bend32(infoHdr.height);
    infoHdr.planes = lend2bend16(infoHdr.planes);
    infoHdr.bitCount = lend2bend16(infoHdr.bitCount);
    infoHdr.compression = lend2bend32(infoHdr.compression);
    infoHdr.sizeImage = lend2bend32(infoHdr.sizeImage);
    infoHdr.xPelsPerMeter = lend2bend32(infoHdr.xPelsPerMeter);
    infoHdr.yPelsPerMeter = lend2bend32(infoHdr.yPelsPerMeter);
    infoHdr.clrUsed = lend2bend32(infoHdr.clrUsed);
    infoHdr.clrImportant = lend2bend32(infoHdr.clrImportant);
    
    if (infoHdr.size != 40) {
        printf("Unexpected infoHdr size: %u\n", infoHdr.size);
        return false;
    }
    if (infoHdr.planes != 1) {
        printf("Unexpected num planes: %d\n", (int)infoHdr.planes);
        return false;
    }
    if (infoHdr.bitCount != 24) {
        printf("Unhandled bits per pixel: %d\n", (int)infoHdr.bitCount);
        return false;
    }
    // 0 == BI_RGB == no compression
    if (infoHdr.compression != 0) {
        printf("Unhandled BMP compression: %d\n", (int)infoHdr.compression);
        return false;
    }
    printf("infoHdr size: %u\n", (unsigned int)infoHdr.size);
    printf("infoHdr width, height: %d, %d\n", (int)infoHdr.width, (int)infoHdr.height);

    width = infoHdr.width;
    height = infoHdr.height;

    bmpPxData = new uint8_t[infoHdr.sizeImage];
    if (!bmpPxData) {
        printf("Failed to alloc bmp px buffer\n");
        return false;
    }

    fseek(fp, lend2bend32(hdr.pixOffset), SEEK_SET);
    if (fread((void*)bmpPxData, 1, infoHdr.sizeImage, fp) != infoHdr.sizeImage) {
        printf("Failed to read bmp px buffer data\n");
        return false;
    }
    fclose(fp);

    // assumes 24 bits per pixel
    int unpaddedRowWidthBytes = width * 3;
    int paddedRowWidthBytes = unpaddedRowWidthBytes;
    if (paddedRowWidthBytes % 4 != 0) {
        paddedRowWidthBytes += 4 - (paddedRowWidthBytes % 4);
    }
    // force 4 bytes per pixel
    buffer.resize(width * height * 4);
    uint8_t* outPx = buffer.data();
    for (int row = 0; row < height; ++row)
    {
        uint8_t* inPx = &bmpPxData[row * paddedRowWidthBytes];
        for (int col = 0; col < width; ++col)
        {
            uint8_t r = *inPx++;
            uint8_t g = *inPx++;
            uint8_t b = *inPx++;

            *outPx++ = b;
            *outPx++ = g;
            *outPx++ = r;
            *outPx++ = 255;
        }
    }

    delete[] bmpPxData;

    return true;
}
} // end namespace BMP
#endif

bool Texture::LoadFromBMP(const char* fileName)
{
    std::vector<uint8_t> imageData;
    int width = -1;
    int height = -1;
    if (!BMP::Load(fileName, imageData, width, height)) {
        return false;
    }
    return Init((const char*)imageData.data(), width, height, 4);
}

bool Texture::Update(const char* data, int width, int height, int bytesPerPixel)
{
    if (mWidth != width) {
        printf("Texture update unexpected width\n");
        return false;
    }
    if (mHeight != height) {
        printf("Texture update unexpected height\n");
        return false;
    }
    if (bytesPerPixel != 4) {
        printf("Texture update unexpected bytes per pixel\n");
        return false;
    }
    
    // force 4 bytes per pixel
    //mBuffer = (u32*)rsxMemalign(128, width*height*4);
    //if (!mBuffer) {
    //    printf("Failed to alloc tex buffer\n");
    //    return false;
    //}
    //rsxAddressToOffset(mBuffer, &mOffset);
    //printf("Texture::init width, height, bpp: %u, %u, %u\n", width, height, bytesPerPixel);
    
    u8* buffer = (u8*)mBuffer;
    for (int i = 0; i < width * height * 4; i += 4)
    {
        buffer[i+1] = *data++; // r
        buffer[i+2] = *data++; // g
        buffer[i+3] = *data++; // b
        //if (bytesPerPixel == 4) {
            buffer[i+0] = *data++; // a
        //}
        //else {
        //    buffer[i+0] = 255;
        //}
        /*printf("buffer[%u]: %d\n"
                "buffer[%u]: %d\n"
                "buffer[%u]: %d\n"
                "buffer[%u]: %d\n",
        i+0, buffer[i+0],
        i+1, buffer[i+1],
        i+2, buffer[i+2],
        i+3, buffer[i+3]);*/
    }
    
    return true;
}

} // namespace PS3
} // namespace GameLib

