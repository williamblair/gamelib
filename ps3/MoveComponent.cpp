#include <MoveComponent.h>
#include <stdio.h>

namespace GameLib
{
namespace PS3
{

MoveComponent::MoveComponent() :
    position(0.0f, 0.0f, 0.0f),
    target(0.0f, 0.0f, -1.0f),
    up(0.0f, 1.0f, 0.0f),
    forward(0.0f, 0.0f, -1.0f),
    right(1.0f, 0.0f, 0.0f),
    pitch(0.0f),
    yaw(0.0f)
{}

void MoveComponent::Update()
{
    // calculate right axis
    right = GameMath::Transform(
        GameMath::Rotate(GameMath::Deg2Rad(yaw), GameMath::Vec3(0.0f, 1.0f, 0.0f)),
        GameMath::Vec3(1.0f, 0.0f, 0.0f)
    );
    right = GameMath::Normalize(right);
    
    // calculate up axis
    up = GameMath::Transform(
        GameMath::Rotate(GameMath::Deg2Rad(pitch), right),
        GameMath::Vec3(0.0f, 1.0f, 0.0f)
    );
    up = GameMath::Normalize(up);
    
    // calculate forward axis
    forward = GameMath::Cross(up, right);
    forward = GameMath::Normalize(forward);
    
    // set target equal to a bit forward from the eye position
    target = position + forward;
}

} // namespace PS3
} // namespace GameLib

