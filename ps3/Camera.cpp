#include <Camera.h>

namespace GameLib
{
namespace PS3
{

FPSCamera::FPSCamera()
{
    mViewMat = GameMath::LookAt(
        mMoveComponent.position,
        mMoveComponent.target,
        mMoveComponent.up
    );
}

FPSCamera::~FPSCamera()
{}

void FPSCamera::Update()
{
    mMoveComponent.Update();
    
    mViewMat = GameMath::LookAt(
        mMoveComponent.position,
        mMoveComponent.target,
        mMoveComponent.up
    );
}

OrbitCamera::OrbitCamera() :
    mMoveComponent(nullptr),
    mPosition(GameMath::Vec3(200.0f, 0.0f, 0.0f)),
    mPositionOffs(GameMath::Vec3(0.0f, 0.0f, 0.0f)),
    mLookatOffs(GameMath::Vec3(0.0f, 0.0f, 0.0f)),
    mDistance(100.0f),
    mYaw(0.0f),
    mPitch(0.0f)
{
    Update();
}

void OrbitCamera::Update()
{
    GameMath::Vec3 targetPos(0.0f, 0.0f, 0.0f);
    if (mMoveComponent) {
        targetPos = mMoveComponent->position;
    }
    //targetPos = targetPos + mTargetOffs;
    
    // calculate right axis
    GameMath::Vec3 right = GameMath::Transform(
        GameMath::Rotate(GameMath::Deg2Rad(mYaw), GameMath::Vec3(0.0f, 1.0f, 0.0f)),
        GameMath::Vec3(1.0f, 0.0f, 0.0f));
    right = GameMath::Normalize(right);
    
    // calculate up axis
    GameMath::Vec3 up = GameMath::Transform(
        GameMath::Rotate(GameMath::Deg2Rad(mPitch), right),
        GameMath::Vec3(0.0f, 1.0f, 0.0f));
    up = GameMath::Normalize(up);
    
    // calculate forward axis
    GameMath::Vec3 forward = GameMath::Cross(up, right);
    forward = GameMath::Normalize(forward);
    
    targetPos = targetPos + (right * mLookatOffs.x);
    targetPos = targetPos + (up * mLookatOffs.y);
    targetPos = targetPos + (forward * mLookatOffs.z);
    
    // set our location backwards from the target
    mPosition = targetPos - forward * mDistance;
    mPosition = mPosition + (right * mPositionOffs.x);
    mPosition = mPosition + (up * mPositionOffs.y);
    
    // update view matrix
    mViewMat = GameMath::LookAt(mPosition, targetPos, GameMath::Vec3(0.0f, 1.0f, 0.0f));
}

} // namespace PS3
} // namespace GameLib

