#ifndef GAMELIB_PS3_TARGA_H_INCLUDED
#define GAMELIB_PS3_TARGA_H_INCLUDED

#include <cstdint>
#include <vector>
#include <fstream>
#include <sstream>

namespace GameLib
{
namespace PS3
{
namespace Targa
{

// imageDesc bitmasks,
// bit 4 is left-to-right ordering,
// bit 5 is top-to-bottom ordering
enum IMAGE_ORIENTATIONS
{
    BOTTOM_LEFT  = 0x00,    // first px is bottom left
    BOTTOM_RIGHT = 0x10,    // first px is bottom right
    TOP_LEFT     = 0x20,    // first px is top left
    TOP_RIGHT    = 0x30     // first px is top right
};

enum FILE_TYPES
{
    TFT_NO_DATA = 0,
    TFT_INDEXED = 1,
    TFT_RGB = 2,
    TFT_GRAYSCALE = 3,
    TFT_RLE_INDEXED = 9,
    TFT_RLE_RGB = 10,
    TFT_RLE_GRAYSCALE = 11
};

struct Header
{
    uint8_t idLength;
    uint8_t colorMapType;
    uint8_t imageTypeCode;
    uint8_t colorMapSpec[5];
    uint16_t xOrigin;
    uint16_t yOrigin;
    uint16_t width;
    uint16_t height;
    uint8_t bpp;        // bits per pixel
    uint8_t imageDesc;
};

extern unsigned int width;
extern unsigned int height;
extern unsigned int bitsPerPixel;
extern unsigned int bytesPerPixel;
extern std::vector<uint8_t> imageData;
extern Header header;

bool LoadUncompressed(std::ifstream& inFile);
bool LoadCompressed(std::ifstream& inFile);
bool IsImageTypeSupported(const Header& header);
bool IsCompressed(const Header& header);
bool IsUncompressed(const Header& header);
void FlipImageVertically();

} // namespace Targa
} // namespace PS3
} // namespace GameLib

#endif // GAMELIB_PS3_TARGA_H_INCLUDED

