#ifndef ZFX_MODEL_H_INCLUDED
#define ZFX_MODEL_H_INCLUDED

#include <cstdio>
#include <vector>
#include <VertexBuffer.h>
#include <Renderer.h>
#include <Texture.h>

#ifdef PC_BUILD
using namespace GameLib::PC;
#endif

class ZFXModel
{
public:

    ZFXModel(const char* fileName, FILE* pLog);
    ~ZFXModel();

    bool Render(
        bool bStatic,
        bool b3T,
        GameMath::Mat4& modelMat,
        GameMath::Mat4& viewMat,
        Renderer& render
    );

private:
    //ZFXRenderDevice* m_pDevice;
    //unsigned int m_numSkins;
    //unsigned int* m_pSkins;
    //unsigned int m_numVerts;
    //VERTEX* m_pVerts;
    std::vector<float> m_Vertices;
    std::vector<VertexBuffer> m_VertBufs;

    //VERTEX3T* m_pVerts3t;
    //unsigned int* m_pBufferId3t;

    std::vector<int> m_Indices;
    std::vector<Texture> m_Textures;

    //unsigned int* m_pCount; // indices per material
    //unsigned int* m_pBufferId; // static buffers

    FILE* m_pFile;
    FILE* m_pLog;
    bool m_bReady;

    struct TRI {
        int i0, i1, i2; // original indices
        int n0, n1, n2; // new indices
        unsigned int mat;
    };

    void ReadFile();
};

#endif // ZFX_MODEL_H_INCLUDED

