#ifndef GAMELIB_PC_MUSIC_H_INCLUDED
#define GAMELIB_PC_MUSIC_H_INCLUDED

#include <SDL2/SDL_mixer.h>

namespace GameLib
{
namespace PC
{

class Music
{
public:
    Music();
    ~Music();

    bool Init(const char* fileName);

    void Play(const bool loop);
    void Pause();
    void Resume();
    void Stop();

private:
    Mix_Music* mMusic;
};

} // namespace PC
} // namespace GameLib

#endif // GAMELIB_PC_MUSIC_H_INCLUDED

