#ifndef GAMELIB_PC_TEXTURE_H_INCLUDED
#define GAMELIB_PC_TEXTURE_H_INCLUDED

#include <GL/glew.h>

namespace GameLib
{
namespace PC
{

// forward declaration
class Renderer;
class Sprite;

class Texture
{
friend class Renderer;
friend class Sprite;

public:
    
    Texture();
    ~Texture();
    
    /**
     * @brief Init texture from raw pixel data
     * @param data the raw pixel data, either 24bit RGB or 32bit RGBA
     * @param width the width of the image in px
     * @param height the height of the image in px
     * @param bytesPerPixel either 3 for 1 byte each RGB, or 4 for 1 byte each RGBA
     * @return true on success, false on failure
     */
    bool Init(const char* data, int width, int height, int bytesPerPixel);
    
    bool LoadFromTGA(const char* fileName);
    bool LoadFromBMP(const char* fileName);

    int GetWidth() const { return mWidth; }
    int GetHeight() const { return mHeight; }

    /**
     * @brief set new texture data. must be the same size as the original data.
     * @param data the new pixel data to use for this texture
     * @param width the width of the pixel data (must be same as original)
     * @param height the height of the pixel data (must be same as original)
     * @param bytesPerPixel bytes per pixel of the data (must be same as original)
     * @return true on success, false on failure
     */
    bool Update(const char* data, int width, int height, int bytesPerPixel);

private:
    int mWidth;
    int mHeight;
    int mBytesPerPixel;
    GLuint mTexID;
};

} // namespace PC
} // namespace GameLib

#endif // GAMELIB_PC_TEXTURE_H_INCLUDED
