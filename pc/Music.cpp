#include <Music.h>
#include <stdio.h>

namespace GameLib
{
namespace PC
{

Music::Music() :
    mMusic(nullptr)
{}

Music::~Music()
{
    if (mMusic != nullptr) {
        Mix_FreeMusic(mMusic);
        mMusic = nullptr;
    }
}

bool Music::Init(const char* fileName)
{
    mMusic = Mix_LoadMUS(fileName);
    if (!mMusic) {
        printf("ERROR - failed to load music: %s\n", Mix_GetError());
        return false;
    }
    return true;
}

void Music::Play(const bool loop)
{
    int loopVal = loop ? -1 : 1;
    if (Mix_PlayMusic(mMusic, loopVal) == -1) {
        printf("ERROR - failed to play music: %s\n", Mix_GetError());
        return;
    }
}

void Music::Pause()
{
    Mix_PauseMusic();
}

void Music::Resume()
{
    Mix_ResumeMusic();
}

void Music::Stop()
{
    Mix_HaltMusic();
}

} // namespace PC
} // namespace GameLib

