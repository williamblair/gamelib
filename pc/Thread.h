#ifndef GAMELIB_PC_THREAD_H_INCLUDED
#define GAMELIB_PC_THREAD_H_INCLUDED

#include <cstdint>
#include <map>
#include <thread>

typedef uint64_t u64;

namespace GameLib
{
namespace PC
{

// Thread function should be of form
// void myFcn(void* arg)
typedef void (*ThreadFcn)(void*);

class Thread
{
public:
    Thread();
    ~Thread();

    bool Start(ThreadFcn fcn, void* arg, const char* name);
    void Join(u64* retVal);

    // call from within the ThreadFcn to get the current thread ID
    static u64 GetThreadId();
    // call from within the ThreadFcn to yield
    static void Yield();
    // must call this from within the ThreadFcn upon exit
    static void Exit(u64 retVal);

private:
    std::thread mThread;
    u64 mId;
    bool mStarted;
    bool mJoined;

    // for assigning IDs to threads; goes up by 1 each thread start
    static u64 sThreadIdCtr;

    // maps c++ thread::get_id() to our internal thread IDs
    static std::map<std::thread::id,u64> sStdThreadIds;

    // key = thread id
    // value = return value of thread
    static std::map<u64,u64> sRetVals;
};

} // namespace PC
} // namespace GameLib

#endif // GAMELIB_PC_THREAD_H_INCLUDED

