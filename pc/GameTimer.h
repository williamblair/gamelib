#ifndef GAMELIB_PC_GAME_TIMER_H_INCLUDED
#define GAMELIB_PC_GAME_TIMER_H_INCLUDED

#include <SDL2/SDL.h>

namespace GameLib
{
namespace PC
{

class GameTimer
{
public:
    GameTimer();
    ~GameTimer();

    // returns delta time in seconds
    float Update();

    // returns average frames per second and time per frame
    // over a 1 second interval
    float GetAvgFps() const { return mFps; }
    float GetAvgFrameTime() const { return mFrameTime; }

private:
    Uint32 mLastTicks;
    float mFps;
    float mFrameTime;
    float mDtCtr;
    float mFrameCtr;
};

} // namespace PC
} // namespace GameLib

#endif // GAMELIB_PC_GAME_TIMER_H_INCLUDED

