#ifndef GAMELIB_PC_GLTF_FRAME_H_INCLUDED
#define GAMELIB_PC_GLTF_FRAME_H_INCLUDED

namespace GameLib
{
namespace PC
{
namespace Gltf
{

// used to store keyframes in an animation track
template<unsigned int N>
class Frame
{
public:
    float value[N];
    float in[N]; // in and out tangents
    float out[N];
    float time;
};

typedef Frame<1> ScalarFrame;
typedef Frame<3> VectorFrame;
typedef Frame<4> QuaternionFrame;

} // namespace Gltf
} // namespace PC
} // namespace GameLib

#endif // GAMELIB_PC_GLTF_FRAME_H_INCLUDED

