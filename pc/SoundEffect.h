#ifndef GAMELIB_PC_SOUNDEFFECT_H_INCLUDED
#define GAMELIB_PC_SOUNDEFFECT_H_INCLUDED

#include <SDL2/SDL_mixer.h>

namespace GameLib
{
namespace PC
{

class SoundEffect
{
public:

    SoundEffect();
    ~SoundEffect();

    bool Init(const char* fileName);

    void Play(int channel = -1, bool loop = false);
    void Stop(int channel = -1);

private:
    Mix_Chunk* mChunk;
};

} // namespace PC
} // namespace GameLib

#endif // GAMELIB_PC_SOUNDEFFECT_H_INCLUDED

