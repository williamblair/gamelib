#include <SoundEffect.h>
#include <cstdio>

namespace GameLib
{
namespace PC
{

SoundEffect::SoundEffect() :
    mChunk(nullptr)
{}

SoundEffect::~SoundEffect()
{
    if (mChunk) {
        Mix_FreeChunk(mChunk);
    }
}

bool SoundEffect::Init(const char* fileName)
{
    mChunk = Mix_LoadWAV(fileName);
    if (!mChunk) {
        printf("ERROR - failed to load %s: %s\n", fileName, Mix_GetError());
        return false;
    }
    
    return true;
}

void SoundEffect::Play(int channel, bool loop) {
    if (Mix_PlayChannel(channel, mChunk, loop ? -1 : 0) < 0) {
        printf("ERROR - failed to play sound effect: %s\n", Mix_GetError());
    }
}

void SoundEffect::Stop(int channel) {
    Mix_HaltChannel(channel);
}

} // namespace PC
} // namespace GameLib

