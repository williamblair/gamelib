#ifndef GAMELIB_PC_IQM_MESH_H_INCLUDED
#define GAMELIB_PC_IQM_MESH_H_INCLUDED

#include <cstdint>
#include <string>
#include <vector>

#include <GameMath/GameMath.h>
#include <VertexBuffer.h>
#include <Renderer.h>
#include <AABB.h>

#include <iqm.h>
#include <util.h>

namespace GameLib
{
namespace PC
{

class IqmMesh
{
public:

    struct Anim
    {
        int startFrame;
        int endFrame;
        std::string name;
    };

    IqmMesh();
    ~IqmMesh();
    
    bool Init(const std::string& fileName);
    
    void Update(const float dt)
    {
        if (!mLoopAnim && mAnimFinished) {
            return;
        }
        animCounter += dt;
        if ((int)animCounter > mEndFrame) {
            if (mLoopAnim) {
                while ((int)animCounter > mEndFrame) { animCounter -= (mEndFrame - mStartFrame); }
                animateiqm(animCounter);
            } else {
                mAnimFinished = true;
            }
        } else {
            animateiqm(animCounter);
        }
    }
    
    void AddAnim(const Anim& anim) {
        mAnims.push_back(anim);
    }
    void SetAnim(const std::string& name, bool loop = true)
    {
        mLoopAnim = loop;
        for (Anim& anim : mAnims)
        {
            if (anim.name == name) {
                // only reset anim if not already set
                if (mStartFrame != anim.startFrame ||
                    mEndFrame != anim.endFrame)
                {
                    mStartFrame = anim.startFrame;
                    mEndFrame = anim.endFrame;
                    animCounter = mStartFrame;
                    mAnimFinished = false;
                }
                break;
            }
        }
    }
    bool AnimIsFinished() { return mAnimFinished; }
    
    void DrawVertexBuffer(GameMath::Mat4& modelMat, GameMath::Mat4& viewMat, Renderer& render, int vertBuf)
    {
        render.DrawVertexBuffer(modelMat, viewMat, mVertBufs[vertBuf]);
    }
    
    size_t GetNumVertexBuffers() const { return mVertBufs.size(); }
    AABB& GetAabb() { return mAabb; }
    
private:
    uint8_t* meshdata = NULL;
    uint8_t* animdata = NULL;
    float* inposition = NULL;
    float* innormal = NULL;
    float* intangent = NULL;
    float* intexcoord = NULL;
    uint8_t* inblendindex = NULL;
    uint8_t* inblendweight = NULL;
    uint8_t* incolor = NULL;
    float* outposition = NULL;
    float* outnormal = NULL;
    float* outtangent = NULL;
    float* outbitangent = NULL;
    int nummeshes = 0;
    int numtris = 0;
    int numverts = 0;
    int numjoints = 0;
    int numframes = 0;
    int numanims = 0;
    iqmtriangle* tris = NULL;
    iqmtriangle* adjacency = NULL;
    iqmmesh* meshes = NULL;
    iqmjoint* joints = NULL;
    iqmpose* poses = NULL;
    iqmanim* anims = NULL;
    iqmbounds* bounds = NULL;
    std::vector<GameMath::Mat4> baseframe;
    std::vector<GameMath::Mat4> inversebaseframe;
    std::vector<GameMath::Mat4> outframe;
    std::vector<GameMath::Mat4> frames;
    
    float animCounter = 0.0f;
    int mStartFrame = 0;
    int mEndFrame = 0;
    bool mLoopAnim = true;
    bool mAnimFinished = false;
    std::vector<Anim> mAnims;
    
    std::vector<VertexBuffer> mVertBufs;
    std::vector<float> frameVerts;

    AABB mAabb;
    
    bool loadiqmmeshes(const char* filename, const iqmheader& hdr, uchar* buf);
    bool loadiqmanims(const char *filename, const iqmheader &hdr, uchar *buf);
    bool loadiqm(const char *filename);
    void animateiqm(float curframe);
};

} // namespace PC
} // namespace GameLib

#endif // GAMELIB_PC_IQM_MESH_H_INCLUDED

