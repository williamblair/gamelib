#ifndef GAMELIB_PC_GLTF_TRANSFORM_H_INCLUDED
#define GAMELIB_PC_GLTF_TRANSFORM_H_INCLUDED

#include <GameMath/GameMath.h>

namespace GameLib
{
namespace PC
{
namespace Gltf
{
    
#define Vec3 GameMath::Vec3
#define Quat GameMath::Quat
#define Mat4 GameMath::Mat4

struct Transform
{
    Vec3 position;
    Quat rotation;
    Vec3 scale;
    
    inline Transform(const Vec3& p, const Quat& r, const Vec3& s) :
        position(p),
        rotation(r),
        scale(s)
    {}
    
    inline Transform() :
        position(0.0f, 0.0f, 0.0f),
        rotation(0.0f, 0.0f, 0.0f, 1.0f),
        scale(1.0f, 1.0f, 1.0f)
    {}
    
};

inline bool operator==(const Transform& a, const Transform& b) {
    if (a.position != b.position ||
        a.rotation != b.rotation ||
        a.scale != b.scale)
    {
        return false;
    }
    return true;
}

// operations apply from right to left like matrix mul
inline Transform Combine(const Transform& a, const Transform& b) {
    Transform out;
    out.scale = a.scale * b.scale;
    out.rotation = b.rotation * a.rotation;
    out.position = GameMath::Transform(a.rotation, (a.scale * b.position));
    out.position = a.position + out.position; // total op. is scale, rotate, translate
    return out;
}


inline Transform Inverse(const Transform& t) {
    Transform inv;
    inv.rotation = GameMath::Inverse(t.rotation);
    
    #define EPSILON 0.00001f
    inv.scale.x = fabsf(t.scale.x) < EPSILON ? 0.0f : 1.0f / t.scale.x;
    inv.scale.y = fabsf(t.scale.y) < EPSILON ? 0.0f : 1.0f / t.scale.y;
    inv.scale.z = fabsf(t.scale.z) < EPSILON ? 0.0f : 1.0f / t.scale.z;
    #undef EPSILON
    
    Vec3 invTrans = t.position * -1.0f;
    inv.position = GameMath::Transform(inv.rotation, (inv.scale * invTrans));
    return inv;
}

inline Transform Mix(const Transform& a, const Transform& b, float t) {
    // neighborhood check for optimal path interpolation
    Quat bRot = b.rotation;
    if (GameMath::Dot(a.rotation, bRot) < 0.0f) {
        bRot = -bRot;
    }
    
    return Transform(
        GameMath::Lerp(a.position, b.position, t),
        GameMath::NLerp(a.rotation, bRot, t),
        GameMath::Lerp(a.scale, b.scale, t)
    );
}

inline Mat4 TransformToMat4(const Transform& t) {
    // extract rotation basis of transform
    Vec3 x = GameMath::Transform(t.rotation, Vec3(1,0,0));
    Vec3 y = GameMath::Transform(t.rotation, Vec3(0,1,0));
    Vec3 z = GameMath::Transform(t.rotation, Vec3(0,0,1));
    // scale the basis vectors
    x = x * t.scale.x;
    y = y * t.scale.y;
    z = z * t.scale.z;
    // extract transform position
    Vec3 p = t.position;
    // put components into matrix
    return Mat4(
        x.x, y.x, z.x, p.x,
        x.y, y.y, z.y, p.y,
        x.z, y.z, z.z, p.z,
        0, 0, 0, 1
    );
}

inline Transform Mat4ToTransform(const Mat4& m) {
    Transform out;
    out.position = Vec3(m.r0c3, m.r1c3, m.r2c3); // pos = last col of mat
    out.rotation = GameMath::Mat4ToQuat(m);
    
    // get the rotate scale matrix, then estimate the scale from that
    Mat4 rotScaleMat(
        m.r0c0, m.r0c1, m.r0c2, 0,
        m.r1c0, m.r1c1, m.r1c2, 0,
        m.r2c0, m.r2c1, m.r2c2, 0,
        0, 0, 0, 1
    );
    Mat4 invRotMat = GameMath::QuatToMat4(GameMath::Inverse(out.rotation));
    Mat4 scaleSkewMat = rotScaleMat * invRotMat;
    // diagonal of matrix is the scale
    out.scale = Vec3(scaleSkewMat.r0c0, scaleSkewMat.r1c1, scaleSkewMat.r2c2);
    
    return out;
}

// operation order is right to left (scale, rotate, translate)
inline Vec3 TransformPoint(const Transform& a, const Vec3& b) {
    Vec3 out;
    out = GameMath::Transform(a.rotation, (a.scale * b));
    out = a.position + out;
    return out;
}
// vectors/directions cannot "move" so translation is not applied
inline Vec3 TransformVector(const Transform& a, const Vec3& b) {
    Vec3 out;
    out = GameMath::Transform(a.rotation, (a.scale * b));
    return out;
}

#undef Vec3
#undef Quat
#undef Mat4

} // namespace Gltf
} // namespace PC
} // namespace GameLib

#endif // GAMELIB_PC_GLTF_TRANSFORM_H_INCLUDED
