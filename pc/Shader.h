#ifndef GAMELIB_PC_SHADER_H_INCLUDED
#define GAMELIB_PC_SHADER_H_INCLUDED

#include <string>
#include <iostream>
#include <fstream>

#include <GL/glew.h>

#include <GameMath/GameMath.h>

namespace GameLib
{
namespace PC
{

class Shader
{
public:

    Shader();
    ~Shader();
    
    bool Init(const std::string vertexFile, const std::string shaderFile);
    
    void Use();
    
    void SetMat4(const char* name, GameMath::Mat4& mat) {
        GLint pos = glGetUniformLocation(this->mProgID, name);
        if (pos < 0) {
            printf("Failed to find mat4 uniform: %s\n", name);
            return;
        }
        
        glUniformMatrix4fv(pos, 1, GL_FALSE, (float*)&mat);
    }

private:

    std::string getShaderStr(const std::string filename);

    GLuint mProgID;
};

} // namespace PC
} // namespace GameLib

#endif

