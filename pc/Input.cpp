#include <Input.h>

namespace GameLib
{
namespace PC
{

Input::Input() :
    mQuit(false),
    mLeft(false),
    mRight(false),
    mUp(false),
    mDown(false),
    mSpace(false),
    mUpClicked(false),
    mDownClicked(false),
    mLeftClicked(false),
    mRightClicked(false),
    mEnterClicked(false),
    mShiftHeld(false),
    mMouseMoveX(0),
    mMouseMoveY(0)
{}

Input::~Input()
{}

// TODO - add joysticks
bool Input::Init()
{
    return true;
}

void Input::Update()
{
    SDL_Event e;
    
    mMouseMoveX = 0;
    mMouseMoveY = 0;

    mSpace = false;
    
    mUpClicked = false;
    mDownClicked = false;
    mLeftClicked = false;
    mRightClicked = false;
    mEnterClicked = false;
    
    while (SDL_PollEvent(&e))
    {
        switch (e.type)
        {
        case SDL_QUIT:
            mQuit = true;
            break;
        case SDL_MOUSEMOTION:
            mMouseMoveX = e.motion.xrel;
            mMouseMoveY = e.motion.yrel;
            break;
        case SDL_KEYDOWN:
            switch (e.key.keysym.sym)
            {
            case SDLK_w: mUp = true; break;
            case SDLK_a: mLeft = true; break;
            case SDLK_s: mDown = true; break;
            case SDLK_d: mRight = true; break;
            case SDLK_ESCAPE: mQuit = true; break;
            case SDLK_LSHIFT:
            case SDLK_RSHIFT: mShiftHeld = true; break;
            default:
                break;
            }
            break;
        case SDL_KEYUP:
        switch (e.key.keysym.sym)
            {
            case SDLK_w: mUp = false; mUpClicked = true; break;
            case SDLK_a: mLeft = false; mLeftClicked = true; break;
            case SDLK_s: mDown = false; mDownClicked = true; break;
            case SDLK_d: mRight = false; mRightClicked = true; break;
            case SDLK_SPACE: mSpace = true; break;
            case SDLK_RETURN: mEnterClicked = true; break;
            case SDLK_LSHIFT:
            case SDLK_RSHIFT: mShiftHeld = false; break;
            default:
                break;
            }
            break;
        default:
            break;
        }
    }
}

} // namespace PC
} // namespace GameLib

