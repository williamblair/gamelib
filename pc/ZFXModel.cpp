#include "ZFXModel.h"
#include <algorithm>

ZFXModel::ZFXModel(const char* fileName, FILE* pLog) :
    m_pFile(nullptr),
    m_pLog(pLog),
    m_bReady(false)
{
    m_pFile = fopen(fileName, "r");
    if (m_pFile) {
        ReadFile();
        fclose(m_pFile);
        m_bReady = true;
        m_pFile = nullptr;
    }
}

ZFXModel::~ZFXModel()
{
    //if (m_pIndices) { delete[] m_pIndices; }
    //if (m_pVerts) { delete[] m_pVerts; }
    //if (m_pVerts3t) { delete[] m_pVerts3t; }
    //if (m_pSkins) { delete[] m_pSkins; }
    //if (m_pCount) { delete[] m_pCount; }
    //if (m_pBufferId) { delete[] m_pBufferId; }
    //if (m_pBufferId3t) { delete[] m_pBufferId3t; }
}

bool ZFXModel::Render(
    bool bStatic,
    bool b3T,
    GameMath::Mat4& modelMat,
    GameMath::Mat4& viewMat,
    Renderer& render
)
{
    unsigned int i = 0;

    //TODO
    //m_pDevice->SetAmbientLight(1.0f,1.0f,1.0f,1.0f);

#if 0
    if (bStatic) {
        if (b3T) {
            for (i=0; i<m_numSkins; ++i) {
                if (!m_pDevice->GetVertexManager()->Render(m_pBufferId3t[i])) {
                    return false;
                }
            }
        } else {
            for (i=0; i<m_numSkins; ++i) {
                if (!m_pDevice->GetVertexManager()->Render(m_pBufferId[i])) {
                    return false;
                }
            }
        }
    } else {
        int* pIndices = m_pIndices;
        for (i=0; i<m_numSkins; pIndices += m_pCount[i], ++i) {
            if (b3T) {
                if (!m_pDevice->GetVertexManager()->Render(
                    VID_3T,
                    m_numVerts,
                    m_pCount[i],
                    m_pVerts3t,
                    pIndices,
                    m_pSkins[i]))
                {
                    return false;
                }
            } else {
                if (!m_pDevice->GetVertexManager()->Render(
                    VID_UU,
                    m_numVerts,
                    m_pCount[i],
                    m_pVerts3t,
                    pIndices,
                    m_pSkins[i]))
                {
                    return false;
                }
            }
        }
    }
#endif
    if (m_VertBufs.size() != m_Textures.size()) {
        fprintf(m_pLog, "VertBufs size != m_Textures size\n");
        return false;
    }
    for (size_t i=0; i<m_VertBufs.size(); ++i) {
        render.SetTexture(m_Textures[i]);
        render.DrawVertexBuffer(modelMat, viewMat, m_VertBufs[i]);
    }
    return true;
}

#define SEEK(str, key) while (instr(str,key)==-1) fgets(str,80,m_pFile);
#define NEXT(str) fgets(str, 80, m_pFile);
static int instr(const char *string, const char *substring);
struct ZFXColor { float r,g,b,a; };
void ZFXModel::ReadFile()
{
    ZFXColor cA, cD, cE, cS;
    unsigned int i, numFaces, numTris = 0;
    float fPower = 0.0f;
    char Line[80];
    char Texture[80];
    TRI* pTris = nullptr;
    unsigned int m_numSkins = 0;

    // READ SKINS
    SEEK(Line, "BEGIN_SKINS")
    NEXT(Line);
    sscanf(Line, "%d;", &m_numSkins);
    //m_pSkins = new unsigned int[m_numSkins];
    m_Textures.resize(m_numSkins);
    for (i=0; i<m_numSkins; ++i) {
        NEXT(Line)
        NEXT(Line) // skip opening bracket
        sscanf(Line, "%f, %f, %f, %f;", &cA.r, &cA.g, &cA.b, &cA.a);
        NEXT(Line)
        sscanf(Line, "%f, %f, %f, %f;", &cD.r, &cD.g, &cD.b, &cD.a);
        NEXT(Line)
        sscanf(Line, "%f, %f, %f, %f;", &cE.r, &cE.g, &cE.b, &cE.a);
        NEXT(Line)
        sscanf(Line, "%f, %f, %f, %f, %f;", &cS.r, &cS.g, &cS.b, &cS.a, &fPower);
        NEXT(Line)
        sscanf(Line, "%s", Texture);
        if (!m_Textures[i].LoadFromBMP(Texture)) {
            fprintf(m_pLog, "Failed to load texture: %s\n", Texture);
        }
    
        // add skin to skin manager
        //m_pDevice->GetSkinManager()->AddSkin(
        //    &cA,&cD,&cE,&cS,
        //    fPower,
        //    &m_pSkins[i]
        //);
        //m_pDevice->GetSkinManager()->AddTexture(
        //    m_pSkins[i],
        //    Texture,
        //    false,
        //    0,
        //    nullptr,
        //    0
        //);
        NEXT(Line) // skip closing bracket
    }

    // READ VERTICES
    rewind(m_pFile);
    SEEK(Line, "BEGIN_VERTICES")
    NEXT(Line)
    int m_numVerts = 0;
    sscanf(Line, "%d;", &m_numVerts);
    //m_pVerts = new VERTEX[m_numVerts];
    //m_pVerts3t = new VERTEX3T[m_numVerts];
    m_Vertices.resize(m_numVerts*5); // xyz uv
    for (i=0; i<m_numVerts; ++i) {
        NEXT(Line)
        float u,v;
        sscanf(Line, "%f,%f,%f,%f,%f;",
            &m_Vertices[i*5 + 0],
            &m_Vertices[i*5 + 1],
            &m_Vertices[i*5 + 2],
            &m_Vertices[i*5 + 3],
            &m_Vertices[i*5 + 4]);
        //memset((void*)&m_pVerts3t[i], 0, sizeof(VERTEX3T));
        //m_pVerts3t[i].x = m_pVerts[i].x;
        //m_pVerts3t[i].y = m_pVerts[i].y;
        //m_pVerts3t[i].z = m_pVerts[i].z;
        //m_pVerts3t[i].u0 = m_pVerts[i].u;
        //m_pVerts3t[i].v0 = m_pVerts[i].v;
    }

    // READ FACES TO COUNT TRIS NEEDED
    rewind(m_pFile);
    SEEK(Line, "BEGIN_FACES")
    NEXT(Line)
    sscanf(Line, "%d;", &numFaces);
    pTris = new TRI[numFaces];
    for (i=0; i<numFaces; ++i) {
        NEXT(Line)
        sscanf(
            Line, "%d,%d,%d;%d",
            &pTris[numTris].i0,
            &pTris[numTris].i1,
            &pTris[numTris].i2,
            &pTris[numTris].mat
        );
        ++numTris;
    }
    // sort by material
    std::sort(&pTris[0], &pTris[numTris], [](TRI a, TRI b) {
        return a.mat < b.mat;
    });

    // COUNT INDICES FOR EACH MATERIAL
    unsigned int oldMat = pTris[0].mat;
    unsigned int* m_pCount = new unsigned int[m_numSkins];
    //m_pBufferId = new unsigned int[m_numSkins];
    //m_pBufferId3t = new unsigned int[m_numSkins];
    memset((void*)m_pCount, 0, sizeof(unsigned int)*m_numSkins);
    //memset((void*)m_pBufferId, 0, sizeof(unsigned int)*m_numSkins);
    //memset((void*)m_pBufferId3t, 0, sizeof(unsigned int)*m_numSkins);
    //m_pIndices = new int[numTris*3];
    //m_numIndices = numFaces*3;
    m_Indices.resize(numTris*3);
    for (i=0; i<numTris; ++i) {
        m_Indices[i*3 + 0] = pTris[i].i0;
        m_Indices[i*3 + 1] = pTris[i].i1;
        m_Indices[i*3 + 2] = pTris[i].i2;

        if (pTris[i].mat != oldMat) {
            oldMat = pTris[i].mat;
        } else {
            m_pCount[pTris[i].mat] += 3;
        }
    }

    int* pIndices = m_Indices.data();
    m_VertBufs.resize(m_numSkins);
    for (i=0; i<m_numSkins; pIndices += m_pCount[i],++i) {
        //printf("verts:\n");
        /*for (size_t ii=0; ii<m_numVerts; ++ii) {
            printf("%f,%f,%f,%f,%f,%f,%f,%f\n",
                m_pVerts[ii].x, m_pVerts[ii].y, m_pVerts[ii].z,
                m_pVerts[ii].normal[0], m_pVerts[ii].normal[1], m_pVerts[ii].normal[2],
                m_pVerts[ii].u, m_pVerts[ii].v);
        }*/
        //if (!m_pDevice->GetVertexManager()->CreateStaticBuffer(
        //    VID_UU,
        //    m_pSkins[i],
        //    m_numVerts,
        //    m_pCount[i],
        //    m_pVerts,
        //    pIndices,
        //    &m_pBufferId[i]
        //))
        //{
        //    fprintf(m_pLog, "S3d Create Static buff failed\n");
        //}
        //if (!m_pDevice->GetVertexManager()->CreateStaticBuffer(
        //    VID_3T,
        //    m_pSkins[i],
        //    m_numVerts,
        //    m_pCount[i],
        //    m_pVerts3t,
        //    pIndices,
        //    &m_pBufferId3t[i]
        //))
        //{
        //    fprintf(m_pLog, "S3d Create Static buff 3t failed\n");
        //}
        if (!m_VertBufs[i].Init(
                VertexBuffer::POS_TEXCOORD,
                m_Vertices.data(),
                m_Vertices.size(),
                pIndices,
                m_pCount[i])) {
            fprintf(m_pLog, "Failed to init vertex buffer\n");
        }
    }
    delete[] pTris;
}

// test if a string contains the substring
int instr(const char* string, const char* substring)
{
    char a,c;
    int start;
    int Lng_SubStr = strlen(substring);
    int Lng_Str = strlen(string);

    // false when substring length > string length
    if ((Lng_SubStr <= 1) || (Lng_SubStr > Lng_Str)) { return -1; }

    a = substring[0];
    start = strcspn(string, &a);

    while (start < Lng_Str)
    {
        if (string[start] != a) {
            ++start;
            continue;
        }

        int j;
        for (j=1; j<Lng_SubStr; ++j) {
            c = substring[j];
            if (string[start+j] != c) {
                break;
            }
        }
        
        if (j == Lng_SubStr) { return start; }
        ++start;
    }
    return -1;
}

