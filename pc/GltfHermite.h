#ifndef GAMELIB_PC_GLTF_HERMITE_H_INCLUDED
#define GAMELIB_PC_GLTF_HERMITE_H_INCLUDED

namespace GameLib
{
namespace PC
{
namespace Gltf
{

// similar to bezier spline, just different basis functions.
// instead of two points and interpolation, two points and slope are used
template<typename T>
T Hermite(float t, T& p1, T& s1, T& p2, T& s2) {
    return p1 * ((1.0f + 2.0f*t) * ((1.0f-t) * (1.0f-t))) +
        s1 * (t * ((1.0f-t) * (1.0f-t))) +
        p2 * ((t*t) * (3.0f - 2.0f*t)) +
        s2 * ((t*t) * (t-1.0f));
}

} // namespace Gltf
} // namespace PC
} // namespace GameLib

#endif // GAMELIB_PC_GLTF_HERMITE_H_INCLUDED

