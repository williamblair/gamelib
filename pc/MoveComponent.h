#ifndef GAMELIB_PC_MOVE_COMPONENT_H_INCLUDED
#define GAMELIB_MOVE_COMPONENT_H_INCLUDED

#include <GameMath/GameMath.h>

namespace GameLib
{
namespace PC
{

struct MoveComponent
{
    GameMath::Vec3 position;
    GameMath::Vec3 target;
    GameMath::Vec3 up;
    GameMath::Vec3 forward;
    GameMath::Vec3 right;
    
    float pitch;
    float yaw;
    
    MoveComponent();
    
    void Update();
    
    void MoveForward(const float amount) {
        position += forward * amount;
    }
    void MoveRight(const float amount) {
        position += right * amount;
    }
    
    void AddPitch(const float amount) {
        pitch += amount;
        pitch = GameMath::Clamp(pitch, -80.0f, 80.0f);
    }
    void AddYaw(const float amount) {
        yaw += amount;
        while (yaw < 0.0f) { yaw += 360.0f; }
        while (yaw > 360.0f) { yaw -= 360.0f; }
    }
};

} // namespace PC
} // namespace GameLib

#endif // GAMELIB_PC_MOVE_COMPONENT_H_INCLUDED

