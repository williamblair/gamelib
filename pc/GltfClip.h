#ifndef GAMELIB_PC_GLTF_CLIP_H_INCLUDED
#define GAMELIB_PC_GLTF_CLIP_H_INCLUDED

#include <vector>
#include <string>
#include <cstdio>

#include <GltfTransformTrack.h>
#include <GltfPose.h>

namespace GameLib
{
namespace PC
{
namespace Gltf
{

class Clip
{
public:

    Clip();

    // get and set joint id for that transform track index
    unsigned int GetIdAtIndex(unsigned int index) const { return tracks[index].GetId(); }
    void SetIdAtIndex(unsigned int index, unsigned int id) {
        tracks[index].SetId(id);
    }
    unsigned int GetSize() const { return (unsigned int)tracks.size(); }

    // fills in outPose and returns the adjusted time for that pose
    float Sample(Pose& outPose, float inTime);

    // get the transform track at the index; if it doesn't exist, a default is returned
    TransformTrack& operator[](unsigned int index);

    // sets the start/end time of the clip based on the internal tracks.
    // finds the min/max start start/end time within the tracks and uses those
    void RecalculateDuration();

    std::string& GetName() { return name; }
    void SetName(std::string& newName) { name = newName; }

    float GetDuration() const { return endTime - startTime; }
    float GetStartTime() const { return startTime; }
    float GetEndTime() const { return endTime; }
    bool GetLooping() const { return looping; }

    void SetLooping(bool inLooping) { looping = inLooping; }

private:

    std::vector<TransformTrack> tracks;
    std::string name;

    float startTime;
    float endTime;
    bool looping;

    float AdjustTimeToFitRange(float inTime);
};

} // namespace Gltf
} // namespace PC
} // namespace GameLib

#endif // GAMELIB_PC_GLTF_CLIP_H_INCLUDED

