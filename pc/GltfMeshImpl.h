#ifndef GAMELIB_PC_GLTF_MESH_IMPL_H_INCLUDED
#define GAMELIB_PC_GLTF_MESH_IMPL_H_INCLUDED

#include <vector>

#include <GameMath/GameMath.h>
#include <VertexBuffer.h>
#include <GltfSkeleton.h>
#include <GltfPose.h>
#include <AABB.h>

namespace GameLib
{
namespace PC
{
namespace Gltf
{

#define Vec2 GameMath::Vec2
#define Vec3 GameMath::Vec3
#define Vec4 GameMath::Vec4
#define Mat4 GameMath::Mat4

class Mesh
{
public:

    struct iVec4
    {
        union
        {
            struct {
                int x,y,z,w;
            };
            int v[4];
        };

        iVec4() :
            x(0),
            y(0),
            z(0),
            w(0)
        {}

        iVec4(int x, int y, int z, int w) :
            x(x),
            y(y),
            z(z),
            w(w)
        {}

        iVec4(int* fv) :
            x(fv[0]),
            y(fv[1]),
            z(fv[2]),
            w(fv[3])
        {}
    };

    Mesh();

    std::vector<Vec3>& GetPositions() { return positions; }
    std::vector<Vec3>& GetNormals() { return normals; }
    std::vector<Vec2>& GetTexCoords() { return texCoords; }
    std::vector<Vec4>& GetWeights() { return weights; }
    std::vector<iVec4>& GetInfluences() { return influences; }
    std::vector<int>& GetIndices() { return indices; }

    // applies CPU skinning
    void CPUSkin(Skeleton& skeleton, Pose& pose);
    void UpdateOpenGLBuffers();

    VertexBuffer& GetVertexBuffer() { return vertBuf; }

    AABB& GetAabb() { return mAabb; }

private:

    std::vector<Vec3> positions;
    std::vector<Vec3> normals;
    std::vector<Vec2> texCoords;
    std::vector<Vec4> weights;
    std::vector<iVec4> influences;
    std::vector<int> indices;

    VertexBuffer vertBuf;
    bool vertBufInitted;

    std::vector<Vec3> skinnedPositions;
    std::vector<Vec3> skinnedNormals;
    std::vector<Mat4> posePalette;
    std::vector<float> combinedVertices;

    AABB mAabb;
};

#undef Vec2
#undef Vec3
#undef Vec4
#undef Mat4

} // namespace Gltf
} // namespace PC
} // namespace GameLib

#endif // GAMELIB_PC_GLTF_MESH_IMPL_H_INCLUDED

