#include <Sprite.h>
#include <cstdio>

namespace GameLib
{
namespace PC
{

Sprite::Sprite() :
    mTexture(nullptr),
    mX(0), mY(0),
    mU(0), mV(0),
    mW(1.0f), mH(1.0f),
    mWidth(1.0f), mHeight(1.0f),
    mRotDegrees(0.0f)
{}

Sprite::~Sprite()
{}

bool Sprite::Init(Texture* tex)
{
    mTexture = tex;
    if (!mTexture) {
        printf("Sprite init mTex is null\n");
        return false;
    }

    mWidth = 1.0f;
    mHeight = 1.0f;
    mU = mV = 0.0f;
    mW = 1.0f;
    mH = 1.0f;

    // top left corner
    mX = -1.0f;
    mY = 1.0f;
    
    mRotDegrees = 0.0f;

    return true;
}

} // namespace PC
} // namespace GameLib


