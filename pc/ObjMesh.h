#ifndef GAMELIB_PC_OBJ_MESH_H_INCLUDED
#define GAMELIB_PC_OBJ_MESH_H_INCLUDED

#include <GameMath/GameMath.h>
#include <Renderer.h>
#include <VertexBuffer.h>

namespace GameLib
{
namespace PC
{

class ObjMesh
{
public:

    ObjMesh();
    ~ObjMesh();
    
    bool Init(const char* fileName);

    void Draw(
        GameMath::Mat4& modelMat,
        GameMath::Mat4& viewMat,
        Renderer& render)
    {
        render.DrawVertexBuffer(modelMat, viewMat, mVertBuf);
    }

private:
    VertexBuffer mVertBuf;
};

} // namespace PC
} // namespace GameLib

#endif // PC_OBJ_MESH_H_INCLUDED
