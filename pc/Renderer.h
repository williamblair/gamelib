#ifndef GAMELIB_PC_RENDERER_H_INCLUDED
#define GAMELIB_PC_RENDERER_H_INCLUDED

#include <GL/glew.h>
#include <SDL2/SDL.h>

#include <VertexBuffer.h>
#include <Shader.h>
#include <Texture.h>
#include <AABB.h>
#include <Sprite.h>

namespace GameLib
{
namespace PC
{

class Renderer
{
public:

    Renderer();
    ~Renderer();

    bool Init(int width, int height, bool fullScreen, const char* title);

    void SetBGColor(uint8_t r, uint8_t b, uint8_t g);

    void Clear();
    void Update();
    
    void DrawVertexBuffer(
        GameMath::Mat4& modelMat,
        GameMath::Mat4& viewMat,
        const VertexBuffer& vb
    );
    void DrawAabb(AABB& aabb, GameMath::Mat4& viewMat);
    void DrawSprite(Sprite& sprite);
    
    void SetTexture(Texture& tex) {
        glBindTexture(GL_TEXTURE_2D, tex.mTexID);
    }

    int GetWidth() const { return mWidth; }
    int GetHeight() const { return mHeight; }

    GameMath::Mat4& GetProjMat() { return mProjMat; }

private:
    
    SDL_Window* mWindow;
    SDL_GLContext mContext;
    int mWidth;
    int mHeight;
    GameMath::Vec3 mBGColor;
    
    static bool sShadersLoaded;
    static Shader sPosColorShader;
    static Shader sPosTexcoordShader;
    Shader* mCurShader;

    // for debug drawing AABBs
    static VertexBuffer sAabbVertBuf;
    static bool InitAabbVertBuf();

    // For drawing 2d sprites onscreen
    static VertexBuffer sSpriteVertBuf;
    static bool InitSpriteVertBuf();

    bool mLimitFps;
    Uint32 mLastTicks;
    
    GameMath::Mat4 mProjMat;
};

} // namespace PC
} // namespace GameLib

#endif // GAMELIB_PC_RENDERER_H_INCLUDED

