#include <IqmMesh.h>
#include <Renderer.h>
#include <stdio.h>
#include <cfloat>

namespace GameLib
{
namespace PC
{

static inline void printMat4(FILE* dbgFile, const GameMath::Mat4& m)
{
    float* p = (float*)&m;
    fprintf(dbgFile,
        "%f, %f, %f, %f\n"
        "%f, %f, %f, %f\n"
        "%f, %f, %f, %f\n"
        "%f, %f, %f, %f\n",
        p[0], p[4], p[8], p[12],
        p[1], p[5], p[9], p[13],
        p[2], p[6], p[10], p[14],
        p[3], p[7], p[11], p[15]);
}

IqmMesh::IqmMesh()
{}

IqmMesh::~IqmMesh()
{
    //if (textures)
    //{
    //    glDeleteTextures(nummeshes, textures);
    //    delete[] textures;
    //}
    if (outposition) delete[] outposition;
    if (outnormal) delete[] outnormal;
    if (outtangent) delete[] outtangent;
    if (outbitangent) delete[] outbitangent;
    //if (baseframe) delete[] baseframe;
    //if (inversebaseframe) delete[] inversebaseframe;
    //if (outframe) delete[] outframe;
    //if (frames) delete[] frames;
}

bool IqmMesh::Init(const std::string& fileName)
{
    bool res = loadiqm(fileName.c_str());
    return res;
}

bool IqmMesh::loadiqmmeshes(const char *filename, const iqmheader &hdr, uchar *buf)
{
    if (meshdata) return false;

    lilswap((uint *)&buf[hdr.ofs_vertexarrays], hdr.num_vertexarrays*sizeof(iqmvertexarray)/sizeof(uint));
    lilswap((uint *)&buf[hdr.ofs_triangles], hdr.num_triangles*sizeof(iqmtriangle)/sizeof(uint));
    lilswap((uint *)&buf[hdr.ofs_meshes], hdr.num_meshes*sizeof(iqmmesh)/sizeof(uint));
    lilswap((uint *)&buf[hdr.ofs_joints], hdr.num_joints*sizeof(iqmjoint)/sizeof(uint));
    if (hdr.ofs_adjacency) lilswap((uint *)&buf[hdr.ofs_adjacency], hdr.num_triangles*sizeof(iqmtriangle)/sizeof(uint));

    meshdata = buf;
    nummeshes = hdr.num_meshes;
    numtris = hdr.num_triangles;
    numverts = hdr.num_vertexes;
    numjoints = hdr.num_joints;
    outposition = new float[3*numverts];
    outnormal = new float[3*numverts];
    outtangent = new float[3*numverts];
    outbitangent = new float[3*numverts];
    outframe.resize(hdr.num_joints);
    //textures = new GLuint[nummeshes];
    //memset(textures, 0, nummeshes*sizeof(GLuint));

    const char *str = hdr.ofs_text ? (char *)&buf[hdr.ofs_text] : "";
    iqmvertexarray *vas = (iqmvertexarray *)&buf[hdr.ofs_vertexarrays];
    for(int i = 0; i < (int)hdr.num_vertexarrays; i++)
    {
        iqmvertexarray &va = vas[i];
        switch(va.type)
        {
        case IQM_POSITION: if(va.format != IQM_FLOAT || va.size != 3) return false; inposition = (float *)&buf[va.offset]; lilswap(inposition, 3*hdr.num_vertexes); break;
        case IQM_NORMAL: if(va.format != IQM_FLOAT || va.size != 3) return false; innormal = (float *)&buf[va.offset]; lilswap(innormal, 3*hdr.num_vertexes); break;
        case IQM_TANGENT: if(va.format != IQM_FLOAT || va.size != 4) return false; intangent = (float *)&buf[va.offset]; lilswap(intangent, 4*hdr.num_vertexes); break;
        case IQM_TEXCOORD: if(va.format != IQM_FLOAT || va.size != 2) return false; intexcoord = (float *)&buf[va.offset]; lilswap(intexcoord, 2*hdr.num_vertexes); break;
        case IQM_BLENDINDEXES: if(va.format != IQM_UBYTE || va.size != 4) return false; inblendindex = (uchar *)&buf[va.offset]; break;
        case IQM_BLENDWEIGHTS: if(va.format != IQM_UBYTE || va.size != 4) return false; inblendweight = (uchar *)&buf[va.offset]; break;
        case IQM_COLOR: if(va.format != IQM_UBYTE || va.size != 4) return false; incolor = (uchar *)&buf[va.offset]; break;
        }
    }
    tris = (iqmtriangle *)&buf[hdr.ofs_triangles];
    meshes = (iqmmesh *)&buf[hdr.ofs_meshes];
    joints = (iqmjoint *)&buf[hdr.ofs_joints];
    if(hdr.ofs_adjacency) adjacency = (iqmtriangle *)&buf[hdr.ofs_adjacency];

    baseframe.resize(hdr.num_joints);
    inversebaseframe.resize(hdr.num_joints);
    for (int i = 0; i < (int)hdr.num_joints; i++)
    {
        iqmjoint &j = joints[i];
        GameMath::Quat rot(j.rotate[0], j.rotate[1], j.rotate[2], j.rotate[3]);
        rot = GameMath::Normalize(rot);
        baseframe[i] = GameMath::Translate(j.translate[0], j.translate[1], j.translate[2]) *
            GameMath::QuatToMat4(rot) *
            GameMath::Scale(j.scale[0], j.scale[1], j.scale[2]);
            
        inversebaseframe[i] = GameMath::Inverse(baseframe[i]);
        if (j.parent >= 0) 
        {
            baseframe[i] = baseframe[j.parent] * baseframe[i];
            inversebaseframe[i] = inversebaseframe[i] * inversebaseframe[j.parent];
        }
    }

    for (int i = 0; i < (int)hdr.num_meshes; i++)
    {
        iqmmesh &m = meshes[i];
        printf("%s: loaded mesh: %s\n", filename, &str[m.name]);
        //textures[i] = loadtexture(&str[m.material], 0);
        //if(textures[i]) printf("%s: loaded material: %s\n", filename, &str[m.material]);
    }

    // for bounding box
    // TODO - use iqmbounds instead
    GameMath::Vec3 mins(FLT_MAX, FLT_MAX, FLT_MAX);
    GameMath::Vec3 maxs(-FLT_MAX, -FLT_MAX, -FLT_MAX);
    
    mVertBufs.resize(hdr.num_meshes);
    int meshIndex = 0;
    for (VertexBuffer& vb : mVertBufs)
    {
        std::vector<float> initVertData;
        std::vector<int> initIndices;
        
        for (int i = 0; i < numverts; ++i)
        {
            initVertData.push_back(inposition[i*3 + 0]);
            initVertData.push_back(inposition[i*3 + 1]);
            initVertData.push_back(inposition[i*3 + 2]);

            if (inposition[i*3 + 0] < mins.x) mins.x = inposition[i*3 + 0];
            if (inposition[i*3 + 1] < mins.y) mins.y = inposition[i*3 + 1];
            if (inposition[i*3 + 2] < mins.z) mins.z = inposition[i*3 + 2];
            
            if (inposition[i*3 + 0] > maxs.x) maxs.x = inposition[i*3 + 0];
            if (inposition[i*3 + 1] > maxs.y) maxs.y = inposition[i*3 + 1];
            if (inposition[i*3 + 2] > maxs.z) maxs.z = inposition[i*3 + 2];
            
            // TODO - normals
            //initVertData.push_back(innormal[i*3 + 0]);
            //initVertData.push_back(innormal[i*3 + 1]);
            //initVertData.push_back(innormal[i*3 + 2]);
            
            initVertData.push_back(intexcoord[i*2 + 0]);
            initVertData.push_back(1.0f - intexcoord[i*2 + 1]);
        }
        
        iqmmesh& mesh = meshes[meshIndex++];
        for (unsigned int i = 0; i < mesh.num_triangles; ++i)
        {
            initIndices.push_back(tris[mesh.first_triangle + i].vertex[0]);
            initIndices.push_back(tris[mesh.first_triangle + i].vertex[1]);
            initIndices.push_back(tris[mesh.first_triangle + i].vertex[2]);
        }
        vb.Init(
            VertexBuffer::POS_TEXCOORD,
            initVertData.data(),
            initVertData.size(),
            initIndices.data(),
            initIndices.size()
        );
    }

    mAabb.min = mins;
    mAabb.max = maxs;
    mAabb.pos = GameMath::Vec3(0.0f, 0.0f, 0.0f);
    
    return true;
}

bool IqmMesh::loadiqmanims(const char *filename, const iqmheader &hdr, uchar *buf)
{
    if ((int)hdr.num_poses != numjoints) return false;

    if (animdata)
    {
        if (animdata != meshdata) delete[] animdata;
        frames.clear();
        animdata = NULL;
        anims = NULL;
        numframes = 0;
        numanims = 0;
    }        

    lilswap((uint *)&buf[hdr.ofs_poses], hdr.num_poses*sizeof(iqmpose)/sizeof(uint));
    lilswap((uint *)&buf[hdr.ofs_anims], hdr.num_anims*sizeof(iqmanim)/sizeof(uint));
    lilswap((ushort *)&buf[hdr.ofs_frames], hdr.num_frames*hdr.num_framechannels);
    if(hdr.ofs_bounds) lilswap((uint *)&buf[hdr.ofs_bounds], hdr.num_frames*sizeof(iqmbounds)/sizeof(uint));
    
    animdata = buf;
    numanims = hdr.num_anims;
    numframes = hdr.num_frames;

    const char *str = hdr.ofs_text ? (char *)&buf[hdr.ofs_text] : "";
    anims = (iqmanim *)&buf[hdr.ofs_anims];
    poses = (iqmpose *)&buf[hdr.ofs_poses];
    frames.resize(hdr.num_frames * hdr.num_poses);
    ushort *framedata = (ushort *)&buf[hdr.ofs_frames];
    if(hdr.ofs_bounds) bounds = (iqmbounds *)&buf[hdr.ofs_bounds];

    for (int i = 0; i < (int)hdr.num_frames; i++)
    {
        for (int j = 0; j < (int)hdr.num_poses; j++)
        {
            iqmpose &p = poses[j];
            GameMath::Quat rotate;
            GameMath::Vec3 translate, scale;
            translate.x = p.channeloffset[0]; if(p.mask&0x01) translate.x += *framedata++ * p.channelscale[0];
            translate.y = p.channeloffset[1]; if(p.mask&0x02) translate.y += *framedata++ * p.channelscale[1];
            translate.z = p.channeloffset[2]; if(p.mask&0x04) translate.z += *framedata++ * p.channelscale[2];
            rotate.x = p.channeloffset[3]; if(p.mask&0x08) rotate.x += *framedata++ * p.channelscale[3];
            rotate.y = p.channeloffset[4]; if(p.mask&0x10) rotate.y += *framedata++ * p.channelscale[4];
            rotate.z = p.channeloffset[5]; if(p.mask&0x20) rotate.z += *framedata++ * p.channelscale[5];
            rotate.w = p.channeloffset[6]; if(p.mask&0x40) rotate.w += *framedata++ * p.channelscale[6];
            scale.x = p.channeloffset[7]; if(p.mask&0x80) scale.x += *framedata++ * p.channelscale[7];
            scale.y = p.channeloffset[8]; if(p.mask&0x100) scale.y += *framedata++ * p.channelscale[8];
            scale.z = p.channeloffset[9]; if(p.mask&0x200) scale.z += *framedata++ * p.channelscale[9];
            GameMath::Mat4 m = GameMath::Translate(translate.x, translate.y, translate.z) *
                GameMath::QuatToMat4(GameMath::Normalize(rotate)) *
                GameMath::Scale(scale.x, scale.y, scale.z);
                
            // Concatenate each pose with the inverse base pose to avoid doing this at animation time.
            // If the joint has a parent, then it needs to be pre-concatenated with its parent's base pose.
            // Thus it all negates at animation time like so: 
            //   (parentPose * parentInverseBasePose) * (parentBasePose * childPose * childInverseBasePose) =>
            //   parentPose * (parentInverseBasePose * parentBasePose) * childPose * childInverseBasePose =>
            //   parentPose * childPose * childInverseBasePose
            if (p.parent >= 0) frames[i*hdr.num_poses + j] = baseframe[p.parent] * m * inversebaseframe[j];
            else frames[i*hdr.num_poses + j] = m * inversebaseframe[j];
        }
    }
 
    mAnims.resize(hdr.num_anims);
    for (int i = 0; i < (int)hdr.num_anims; i++)
    {
        Anim& res = mAnims[i];
        iqmanim &a = anims[i];
        printf("%s: loaded anim: %s\n", filename, &str[a.name]);
        res.startFrame = a.first_frame;
        res.endFrame = a.first_frame + a.num_frames - 1;
        res.name = std::string(&str[a.name]);
    }
    
    return true;
}

bool IqmMesh::loadiqm(const char *filename)
{
    FILE *f = fopen(filename, "rb");
    if (!f) return false;

    uchar *buf = NULL;
    iqmheader hdr;
    if (fread(&hdr, 1, sizeof(hdr), f) != sizeof(hdr) || memcmp(hdr.magic, IQM_MAGIC, sizeof(hdr.magic)))
        goto error;
    lilswap(&hdr.version, (sizeof(hdr) - sizeof(hdr.magic))/sizeof(uint));
    if(hdr.version != IQM_VERSION)
        goto error;
    if(hdr.filesize > (16<<20)) 
        goto error; // sanity check... don't load files bigger than 16 MB
    buf = new uchar[hdr.filesize];
    if(fread(buf + sizeof(hdr), 1, hdr.filesize - sizeof(hdr), f) != hdr.filesize - sizeof(hdr))
        goto error;

    if(hdr.num_meshes > 0 && !loadiqmmeshes(filename, hdr, buf)) goto error;
    if(hdr.num_anims > 0 && !loadiqmanims(filename, hdr, buf)) goto error;
    if (hdr.num_anims > 0) { mStartFrame = 0; mEndFrame = numframes - 1; }
 
    fclose(f);
    return true;

error:
    printf("%s: error while loading\n", filename);
    if(buf != meshdata && buf != animdata) delete[] buf;
    fclose(f);
    return false;
}

// Note that this animates all attributes (position, normal, tangent, bitangent)
// for expository purposes, even though this demo does not use all of them for rendering.
void IqmMesh::animateiqm(float curframe)
{
    if (!numframes) return;

    int frame1 = (int)curframe,
        frame2 = frame1 + 1;
    float frameoffset = curframe - frame1;
    //frame1 %= numframes;
    //frame2 %= numframes;
    if (frame1 > mEndFrame) { frame1 = mStartFrame; frame2 = frame1 + 1; }
    if (frame2 > mEndFrame) { frame2 = mStartFrame; }
    
    GameMath::Mat4* mat1 = &frames[frame1 * numjoints];
    GameMath::Mat4* mat2 = &frames[frame2 * numjoints];
    // Interpolate matrixes between the two closest frames and concatenate with parent matrix if necessary.
    // Concatenate the result with the inverse of the base pose.
    // You would normally do animation blending and inter-frame blending here in a 3D engine.
    for(int i = 0; i < numjoints; i++)
    {
        GameMath::Mat4 mat = mat1[i]*(1 - frameoffset) + mat2[i]*frameoffset;

        if (joints[i].parent >= 0) outframe[i] = outframe[joints[i].parent] * mat;
        else outframe[i] = mat;
    }
    // The actual vertex generation based on the matrixes follows...
    const GameMath::Vec3* srcpos = (const GameMath::Vec3*)inposition;
    const GameMath::Vec3* srcnorm = (const GameMath::Vec3*)innormal;
    const GameMath::Vec4* srctan = (const GameMath::Vec4*)intangent;
    GameMath::Vec3* dstpos = (GameMath::Vec3*)outposition;
    GameMath::Vec3* dstnorm = (GameMath::Vec3*)outnormal;
    GameMath::Vec3* dsttan = (GameMath::Vec3*)outtangent;
    GameMath::Vec3* dstbitan = (GameMath::Vec3*)outbitangent;
    const uchar* index = inblendindex;
    const uchar* weight = inblendweight;
    for(int i = 0; i < numverts; i++)
    {
        // Blend matrixes for this vertex according to its blend weights. 
        // the first index/weight is always present, and the weights are
        // guaranteed to add up to 255. So if only the first weight is
        // presented, you could optimize this case by skipping any weight
        // multiplies and intermediate storage of a blended matrix. 
        // There are only at most 4 weights per vertex, and they are in 
        // sorted order from highest weight to lowest weight. Weights with 
        // 0 values, which are always at the end, are unused.
        GameMath::Mat4 mat = outframe[index[0]] * ((float)weight[0]/255.0f);
        for (int j = 1; j < 4 && weight[j]; j++) {
            mat += outframe[index[j]] * ((float)weight[j]/255.0f);
        }

        // Transform attributes by the blended matrix.
        // Position uses the full 3x4 transformation matrix.
        // Normals and tangents only use the 3x3 rotation part 
        // of the transformation matrix.
        *dstpos = GameMath::Transform(mat, *srcpos);

        // Note that if the matrix includes non-uniform scaling, normal vectors
        // must be transformed by the inverse-transpose of the matrix to have the
        // correct relative scale. Note that invert(mat) = adjoint(mat)/determinant(mat),
        // and since the absolute scale is not important for a vector that will later
        // be renormalized, the adjoint-transpose matrix will work fine, which can be
        // cheaply generated by 3 cross-products.
        //
        // If you don't need to use joint scaling in your models, you can simply use the
        // upper 3x3 part of the position matrix instead of the adjoint-transpose shown 
        // here.
        GameMath::Mat4 matnorm = GameMath::Transpose(GameMath::Inverse(mat));

        *dstnorm = GameMath::Transform(matnorm, *srcnorm);
        // Note that input tangent data has 4 coordinates, 
        // so only transform the first 3 as the tangent vector.
        *dsttan = GameMath::Transform(matnorm, GameMath::Vec3(srctan->x, srctan->y, srctan->z));
        // Note that bitangent = cross(normal, tangent) * sign, 
        // where the sign is stored in the 4th coordinate of the input tangent data.
        *dstbitan = GameMath::Cross(*dstnorm, *dsttan) * srctan->w;

        srcpos++;
        srcnorm++;
        srctan++;
        dstpos++;
        dstnorm++;
        dsttan++;
        dstbitan++;

        index += 4;
        weight += 4;
    }
    
    frameVerts.resize(numverts * 5);
    auto outPtr = frameVerts.begin();
    for (int i = 0; i < numverts; ++i)
    {
        *outPtr++ = outposition[i*3 + 0];
        *outPtr++ = outposition[i*3 + 1];
        *outPtr++ = outposition[i*3 + 2];
        //*outPtr++ = inposition[i*3 + 0];
        //*outPtr++ = inposition[i*3 + 1];
        //*outPtr++ = inposition[i*3 + 2];
        
        // TODO - normals
        //*outPtr++ = outnormal[i*3 + 0];
        //*outPtr++ = outnormal[i*3 + 1];
        //*outPtr++ = outnormal[i*3 + 2];
        
        // tex coords don't change
        *outPtr++ = intexcoord[i*2 + 0];
        *outPtr++ = 1.0f - intexcoord[i*2 + 1];
    }
    for (VertexBuffer& vb : mVertBufs)
    {
        vb.UpdateVertices(frameVerts.data(), frameVerts.size());
    }
}

} // namespace PC
} // namespace GameLib

