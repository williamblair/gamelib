#ifndef GAMELIB_PC_GLTF_INTERPOLATION_H_INCLUDED
#define GAMELIB_PC_GLTF_INTERPOLATION_H_INCLUDED

namespace GameLib
{
namespace PC
{
namespace Gltf
{

enum class Interpolation
{
    Constant,
    Linear,
    Cubic
};

} // namespace Gltf
} // namespace PC
} // namespace GameLib

#endif // GAMELIB_PC_GLTF_INTERPOLATION_H_INCLUDED

