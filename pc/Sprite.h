#ifndef GAMELIB_PC_SPRITE_H_INCLUDED
#define GAMELIB_PC_SPRITE_H_INCLUDED

#include <Texture.h>

namespace GameLib
{
namespace PC
{

class Sprite
{
friend class Renderer;

public:

    Sprite();
    ~Sprite();

    /**
     * @brief store texture pointer and initialize variables
     * @param tex texture to render when drawn
     * @return true on success, false on failure
     */
    bool Init(Texture* tex);

    /**
     * @brief set normalized screen draw location top left (-1..1)
     * @param x draw x position in range -1..1
     * @param y draw y position in range -1..1
     */
    void SetXY(float x, float y) {
        mX = x;
        mY = y;
    }

    /**
     * @brief Set draw size (percent size on screen, normalized 0..1)
     * @param w draw width screen percent
     * @param h draw height screen percent
     */
    void SetWH(float w, float h) {
        mWidth = w;
        mHeight = h;
    }

    /**
     * @brief Set clip top left x/y and size (normalized 0..1)
     *      top left = 0.0f,0.0f; bottom right = 1.0f,1.0f
     * @param u left clip texture coord
     * @param v top clip texture coord
     * @param w width of clip rectangle
     * @param h height of clip rectangle
     */
    void SetUVWH(float u, float v, float w, float h) {
        mU = u; mV = v;
        mW = w; mH = h;
    }

    /**
     * @brief Set the texture to render
     */
    void SetTexture(Texture* tex) { mTexture = tex; }
    
    /** @brief set drawing rotation in degrees */
    void SetRotation(const float rotDeg) { mRotDegrees = rotDeg; }
    /** @brief get the current sprite drawing rotation */
    float GetRotation() const { return mRotDegrees; }

private:
    Texture* mTexture;
    float mX; // normalized screen location x/y (-1..1)
    float mY;
    float mU; // clip top left x/y/width/height
    float mV;
    float mW;
    float mH;
    float mWidth; // drawing width/height
    float mHeight;
    float mRotDegrees; // drawing rotation
};

} // namespace PC
} // namespace GameLib

#endif // GAMELIB_PC_SPRITE_H_INCLUDED

