#ifndef GAMELIB_PC_THIRD_PERSON_PLAYER_H_INCLUDED
#define GAMELIB_PC_THIRD_PERSON_PLAYER_H_INCLUDED

#include <stdio.h>

#include <Renderer.h>
#include <Camera.h>
#include <Texture.h>
#include <IqmMesh.h>
#include <AABB.h>

namespace GameLib
{
namespace PC
{

class ThirdPersonPlayer
{
public:

    enum MoveState
    {
        MOVESTATE_IDLE,
        MOVESTATE_MOVING
    };

    ThirdPersonPlayer();
    ~ThirdPersonPlayer();
    
    void SetModelScale(const GameMath::Vec3& scale) { mModelScale = scale; }
    // Model rotation offset (for if it's facing by default a different direction)
    void SetDrawYawOffset(const float yawDegrees) { mDrawYawOffs = yawDegrees; }
    void SetDrawPitchOffset(const float pitchDegrees) { mDrawPitchOffs = pitchDegrees; }
    void SetPosition(const GameMath::Vec3& position) { mPosition = position; mMoveComp.position = position; }
    void SetCameraDistance(const float distance) { mCamera.SetDistance(distance); }
    void SetCameraPositionOffset(const GameMath::Vec3& offset) { mCamera.SetPositionOffset(offset); }
    void SetCameraLookatOffset(const GameMath::Vec3& offset) { mCamera.SetLookatOffset(offset); }

    GameMath::Mat4& GetViewMat() { return mCamera.GetViewMat(); }
    GameMath::Mat4& GetModelMat() { return mModelMat; }
    OrbitCamera& GetCamera() { return mCamera; }
    GameMath::Vec3& GetPosition() { return mPosition; }
    AABB& GetAabb() { return mAabb; }

    MoveState GetMoveState() const { return mMoveState; }

    void Update() {
        mMoveComp.Update();
        mCamera.Update();
        UpdateModelMat();
        mMoveState = MOVESTATE_IDLE;
    }

    // Move forward based on x/y values of analog stick
    void Move(const float x, const float y, const float amount);

private:
    GameMath::Mat4 mModelMat;
    GameMath::Vec3 mPosition;
    GameMath::Vec3 mModelScale;
    AABB mAabb;
    MoveComponent mMoveComp;
    OrbitCamera mCamera;
    float mDrawYawOffs;
    float mDrawPitchOffs;
    MoveState mMoveState;
    
    void UpdateModelMat()
    {
        mModelMat =
        GameMath::Translate(
            mMoveComp.position.x,
            mMoveComp.position.y,
            mMoveComp.position.z
        ) *
        GameMath::Rotate(GameMath::Deg2Rad(mMoveComp.yaw + mDrawYawOffs),
            GameMath::Vec3(0.0f, 1.0f, 0.0f)) *
        GameMath::Rotate(GameMath::Deg2Rad(mDrawPitchOffs),
            GameMath::Vec3(1.0f, 0.0f, 0.0f)) *
        GameMath::Scale(mModelScale.x, mModelScale.y, mModelScale.z);
    }
};

} // namespace PC
} // namespace GameLib

#endif // GAMELIB_PC_THIRD_PERSON_PLAYER_H_INCLUDED

