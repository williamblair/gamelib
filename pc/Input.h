#ifndef GAMELIB_PC_INPUT_H_INCLUDED
#define GAMELIB_PC_INPUT_H_INCLUDED

#include <SDL2/SDL.h>

namespace GameLib
{
namespace PC
{

class Input
{
public:
    
    Input();
    ~Input();

    bool Init();

    // Should be called once per frame
    void Update();

    bool Quit() const { return mQuit; }
    bool Left() const { return mLeft; }
    bool Right() const { return mRight; }
    bool Up() const { return mUp; }
    bool Down() const { return mDown; }
    bool Space() const { return mSpace; }
    
    bool UpClicked() const { return mUpClicked; }
    bool DownClicked() const { return mDownClicked; }
    bool LeftClicked() const { return mLeftClicked; }
    bool RightClicked() const { return mRightClicked; }
    
    bool EnterClicked() const { return mEnterClicked; }
    bool ShiftHeld() const { return mShiftHeld; }
    
    int MouseMoveX() { return mMouseMoveX; }
    int MouseMoveY() { return mMouseMoveY; }
    
private:
    bool mQuit;
    bool mLeft;
    bool mRight;
    bool mUp;
    bool mDown;
    bool mSpace;
    
    bool mUpClicked;
    bool mDownClicked;
    bool mLeftClicked;
    bool mRightClicked;
    bool mEnterClicked;
    bool mShiftHeld;

    int mMouseMoveX;
    int mMouseMoveY;
};

} // namespace PC
} // namespace GameLib

#endif // GAMELIB_PC_INPUT_H_INCLUDED

