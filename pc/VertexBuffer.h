#ifndef GAMELIB_PC_VERTEX_BUFFER_H_INCLUDED
#define GAMELIB_PC_VERTEX_BUFFER_H_INCLUDED

#include <GL/glew.h>

namespace GameLib
{
namespace PC
{

class VertexBuffer
{

friend class Renderer;

public:

    enum Type
    {
        POS_COLOR,
        POS_TEXCOORD,
        UNINITIALIZED
    };

    VertexBuffer();
    ~VertexBuffer();
    
    bool Init(
        Type type,
        float* vertices,
        size_t verticesSize,
        int* indices,
        size_t indiesSize
    );

    bool UpdateVertices(float* vertices, size_t verticesSize);

private:
    Type mType;
    
    GLuint mVAO, mVBO, mEBO;
    
    size_t mVerticesSize;
    size_t mFloatsPerVertex;
    size_t mNumVertices;
    size_t mIndicesSize;
};

} // namespace PC
} // namespace GameLib

#endif // GAMELIB_PC_VERTEX_BUFFER_H_INCLUDED

