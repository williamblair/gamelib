#include <VertexBuffer.h>
#include <cstdio>

namespace GameLib
{
namespace PC
{

VertexBuffer::VertexBuffer() :
    mType(UNINITIALIZED),
    mVAO(0),
    mVBO(0),
    mEBO(0),
    mVerticesSize(0),
    mFloatsPerVertex(0),
    mNumVertices(0),
    mIndicesSize(0)
{}

VertexBuffer::~VertexBuffer()
{
    if (mVerticesSize != 0)
    {
        glDeleteBuffers(1, &mVBO);
    }
    if (mIndicesSize != 0)
    {
        glDeleteBuffers(1, &mEBO);
    }
}

bool VertexBuffer::Init(
    Type type,
    float* vertices,
    size_t verticesSize,
    int* indices,
    size_t indicesSize
)
{
    int attribLocation; // aPos, where we set location = 0
    int dataType;
    int shouldNormalize;
    int vertexStride;
    int floatsPerVertex;
    void* beginOffset;
    
    glGenVertexArrays(1, &mVAO);
    glGenBuffers(1, &mVBO);
    if (indices != nullptr && indicesSize > 0)
    {
        glGenBuffers(1, &mEBO);
    }
    glBindVertexArray(mVAO);
    glBindBuffer(GL_ARRAY_BUFFER, mVBO);
    glBufferData(
        GL_ARRAY_BUFFER,
        verticesSize * sizeof(float),
        vertices,
        GL_STATIC_DRAW // TODO - allow dynamic draw
    );
    if (indices != nullptr && indicesSize > 0)
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mEBO);
        glBufferData(
            GL_ELEMENT_ARRAY_BUFFER,
            indicesSize * sizeof(int),
            indices,
            GL_STATIC_DRAW
        );
    }
    
    mType = type;
    mVerticesSize = verticesSize;
    mIndicesSize = indicesSize;
    switch (type)
    {
    case POS_COLOR:
        mFloatsPerVertex = 6;
        
        // position
        attribLocation = 0; // aPos, where we set location = 0
        dataType = GL_FLOAT;
        shouldNormalize = GL_FALSE;
        floatsPerVertex = 3; // xyz
        vertexStride = 6 * sizeof(GLfloat);
        beginOffset = (void*)0;
        glVertexAttribPointer(
            attribLocation,
            floatsPerVertex,
            dataType,
            shouldNormalize,
            vertexStride,
            beginOffset
        );
        glEnableVertexAttribArray(attribLocation);
        
        // color
        attribLocation = 1; // aColor, where we set location = 1
        dataType = GL_FLOAT;
        shouldNormalize = GL_FALSE;
        floatsPerVertex = 3; // rgb
        vertexStride = 6 * sizeof(GLfloat);
        beginOffset = (void*)(3*sizeof(float)); // skip xyz
        glVertexAttribPointer(
            attribLocation,
            floatsPerVertex,
            dataType,
            shouldNormalize,
            vertexStride,
            beginOffset
        );
        glEnableVertexAttribArray(attribLocation);
        break;
    case POS_TEXCOORD:
        mFloatsPerVertex = 5;
        
        // position
        attribLocation = 0; // aPos, where we set location = 0
        dataType = GL_FLOAT;
        shouldNormalize = GL_FALSE;
        floatsPerVertex = 3; // xyz
        vertexStride = 5 * sizeof(GLfloat);
        beginOffset = (void*)0;
        glVertexAttribPointer(
            attribLocation,
            floatsPerVertex,
            dataType,
            shouldNormalize,
            vertexStride,
            beginOffset
        );
        glEnableVertexAttribArray(attribLocation);
        
        // tex coord
        attribLocation = 1; // aTexCoord, where we set location = 1
        dataType = GL_FLOAT;
        shouldNormalize = GL_FALSE;
        floatsPerVertex = 2; // uv
        vertexStride = 5 * sizeof(GLfloat);
        beginOffset = (void*)(3*sizeof(float)); // skip xyz
        glVertexAttribPointer(
            attribLocation,
            floatsPerVertex,
            dataType,
            shouldNormalize,
            vertexStride,
            beginOffset
        );
        glEnableVertexAttribArray(attribLocation);
        break;
    default:
        printf("Unhandled VertexBuffer type\n");
        return false;
        break;
    }
    
    mNumVertices = verticesSize / mFloatsPerVertex;
    
    // unbind the current buffers
    // ORDER MATTERS - the VAO must be unbinded FIRST!
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    if (indices != nullptr && indicesSize > 0)
    {
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }
    
    return true;
}

bool VertexBuffer::UpdateVertices(
    float* vertices, 
    size_t verticesSize
)
{
    glBindBuffer(GL_ARRAY_BUFFER, mVBO);
    glBufferSubData(
        GL_ARRAY_BUFFER,
        0,
        verticesSize*sizeof(float),
        vertices
    );
    
    return true;
}

} // namespace PC
} // namespace GameLib

