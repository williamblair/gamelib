#include <Thread.h>
#include <stdio.h>

namespace GameLib
{
namespace PC
{

u64 Thread::sThreadIdCtr = 0;
std::map<std::thread::id,u64> Thread::sStdThreadIds;
std::map<u64,u64> Thread::sRetVals;

Thread::Thread() :
    mId(0),
    mStarted(false),
    mJoined(false)
{}

Thread::~Thread()
{
    if (!mJoined && mStarted) {
        mThread.join();
        mJoined = true;
    }
}

bool Thread::Start(ThreadFcn fcn, void* arg, const char* name)
{
    (void)name;
    mThread = std::thread(fcn, arg);
    mId = sThreadIdCtr++;
    sStdThreadIds[mThread.get_id()] = mId;
    sRetVals[mId] = 0;
    mStarted = true;
    return true;
}

void Thread::Join(u64* retVal)
{
    auto stdId = mThread.get_id();
    mThread.join();
    *retVal = sRetVals[mId];
    sRetVals.erase(mId);
    sStdThreadIds.erase(stdId);
    mJoined = true;
}

u64 Thread::GetThreadId()
{
    auto id = std::this_thread::get_id();
    return sStdThreadIds[id];
}

void Thread::Yield()
{
    std::this_thread::yield();
}

void Thread::Exit(u64 retVal)
{
    u64 id = GetThreadId(); 
    sRetVals[id] = retVal;
}

} // namespace PS3
} // namespace GameLib

