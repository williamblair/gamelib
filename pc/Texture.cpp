#include <Texture.h>
#include <Targa.h>
#include <Bmp.h>
#include <fstream>
#include <iostream>
#include <vector>

namespace GameLib
{
namespace PC
{

Texture::Texture() :
    mWidth(0),
    mHeight(0),
    mBytesPerPixel(0),
    mTexID(0)
{}

Texture::~Texture()
{
    if (mTexID != 0)
    {
        glDeleteTextures(1, &mTexID);
    }
}

bool Texture::Init(const char* data, int width, int height, int bytesPerPixel)
{
    mWidth = width;
    mHeight = height;
    mBytesPerPixel = bytesPerPixel;
    glGenTextures(1, &mTexID);
    glBindTexture(GL_TEXTURE_2D, mTexID);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexImage2D(
        GL_TEXTURE_2D, // target
        0, // level
        (bytesPerPixel == 3) ? GL_RGB8 : GL_RGBA8, // internal format
        width, // width
        height, // height
        0, // border
        (bytesPerPixel == 3) ? GL_RGB : GL_RGBA, // format
        GL_UNSIGNED_BYTE, // type
        data // data
    );
    
    return true;
}

bool Texture::LoadFromTGA(const char* fileName)
{
    std::ifstream inFile(fileName, std::ios::binary);
    if (!inFile.is_open()) {
        std::cout << __func__ << " failed to open targa image file: " 
                  << fileName << std::endl;
        return false;
    }

    inFile.read((char*)&Targa::header, sizeof(Targa::Header));
    if (!Targa::IsImageTypeSupported(Targa::header)) {
        std::cout << __func__ << " unsupported targa type" << std::endl;
        return false;
    }

    Targa::width = Targa::header.width;
    Targa::height = Targa::header.height;

    Targa::bitsPerPixel = Targa::header.bpp;
    Targa::bytesPerPixel = Targa::header.bpp / 8;

    if (Targa::bytesPerPixel < 3) {
        std::cout << __func__ << " unsupported bytesPerPixel: " 
                  << Targa::bytesPerPixel << std::endl;
        return false;
    }

    unsigned int imageSize = Targa::width * Targa::height * Targa::bytesPerPixel;
    Targa::imageData.resize(imageSize);

    // skip past the id if there is one
    if (Targa::header.idLength > 0) {
        inFile.ignore(Targa::header.idLength);
    }

    bool result = false;

    if (Targa::IsUncompressed(Targa::header)) {
        result = Targa::LoadUncompressed(inFile);
    } else {
        result = Targa::LoadCompressed(inFile);
    }

    if ((Targa::header.imageDesc & Targa::TOP_LEFT) == Targa::TOP_LEFT) {
        Targa::FlipImageVertically();
    }

    if (!result) {
        std::cout << __func__ << ": failed to load targa" << std::endl;
        return false;
    }
    
    result = Init(
        (const char*)Targa::imageData.data(),
        Targa::width,
        Targa::height,
        Targa::bytesPerPixel
    );
    Targa::imageData.clear();
    Targa::width = 0;
    Targa::height = 0;
    Targa::bytesPerPixel = 0;
    
    return result;
}

bool Texture::LoadFromBMP(const char* fileName)
{
    std::vector<uint8_t> imageData;
    int width = -1;
    int height = -1;
    if (!BMP::Load(fileName, imageData, width, height)) {
        return false;
    }
    return Init((const char*)imageData.data(), width, height, 4);
}

bool Texture::Update(const char* data, int width, int height, int bytesPerPixel)
{
    if (width != mWidth || height != mHeight || bytesPerPixel != mBytesPerPixel)
    {
        printf("Texture::Update mismatch of new pixel data size\n");
        return false;
    }

    glTextureSubImage2D(
        mTexID, // texture
        0, // level
        0,0, // xoffset, yoffset
        width, height,
        bytesPerPixel == 3 ? GL_RGB : GL_RGBA,
        GL_UNSIGNED_BYTE,
        (void*)data
    );
    return true;
}

} // namespace PC
} // namespace GameLib

