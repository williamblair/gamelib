#include <Renderer.h>
#include <SDL2/SDL_mixer.h>
#include <cstdio>

namespace GameLib
{
namespace PC
{

bool Renderer::sShadersLoaded = false;
Shader Renderer::sPosColorShader;
Shader Renderer::sPosTexcoordShader;

VertexBuffer Renderer::sAabbVertBuf;
VertexBuffer Renderer::sSpriteVertBuf;

Renderer::Renderer() :
    mWindow(nullptr),
    mWidth(0),
    mHeight(0),
    mBGColor(0.0f, 0.0f, 0.0f),
    mCurShader(nullptr),
    mLimitFps(true),
    mLastTicks(0)
{}

Renderer::~Renderer()
{
    if (mWindow != nullptr) {
        SDL_GL_DeleteContext(mContext);
        SDL_DestroyWindow(mWindow);
        mWindow = nullptr;
        Mix_CloseAudio();
        Mix_Quit();
        SDL_Quit();
    }
}

bool Renderer::Init(int width, int height, bool fullScreen, const char* title)
{
    mWidth = width;
    mHeight = height;

    // init SDL
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_AUDIO) < 0) {
        printf("Renderer::Renderer: failed to init sdl: %s\n", SDL_GetError());
        return false;
    }
    // init SDL mixer
    {
        int flags = MIX_INIT_OGG | MIX_INIT_MP3;
        int initted = Mix_Init(flags);
        if ((initted & flags) != flags) {
            printf("Mix init failed: %s\n", Mix_GetError());
            return false;
        }
    }
    if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024) == -1) {
        printf("ERROR - Failed to open audio: %s\n", Mix_GetError());
        return false;
    }

    // create the app window
    Uint32 windowFlags = SDL_WINDOW_OPENGL;
    if (fullScreen) {
        windowFlags |= SDL_WINDOW_FULLSCREEN_DESKTOP;
    }
    mWindow = SDL_CreateWindow(title, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, windowFlags);
    if (!mWindow) {
        printf("Failed to create window: %s\n", SDL_GetError());
        return false;
    }

    // set opengl options
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    // create a render context
    mContext = SDL_GL_CreateContext(mWindow);

    // enable vsync
    SDL_GL_SetSwapInterval(1);

    // init glew
    glewInit();

    // initial time
    mLastTicks = SDL_GetTicks();
    
    // hide/capture mouse cursor
    SDL_SetRelativeMouseMode(SDL_TRUE);
    
    // Load shaders
    if (!sShadersLoaded)
    {
        sShadersLoaded = true;
        if (!sPosColorShader.Init(
            "assets/shaders/PosColor_Vertex.glsl",
            "assets/shaders/PosColor_Fragment.glsl"
        ))
        {
            printf("Renderer failed to load pos color shader\n");
            return false;
        }
        if (!sPosTexcoordShader.Init(
            "assets/shaders/PosTexcoord_Vertex.glsl",
            "assets/shaders/PosTexcoord_Fragment.glsl"
        ))
        {
            printf("Renderer failed to load pos texcoord shader\n");
            return false;
        }
    }
    mCurShader = &sPosColorShader;
    mCurShader->Use();

    mProjMat = GameMath::Perspective(
        GameMath::Deg2Rad(60.0f), // fov radians
        (float)width/(float)height, // aspect
        1.0f, // near
        500.0f // far
    );

    if (sAabbVertBuf.mNumVertices == 0) {
        if (!InitAabbVertBuf()) {
            printf("Failed to init AABB debug draw vertex buffer\n");
            return false;
        }
    }
    
    if (sSpriteVertBuf.mNumVertices == 0) {
        if (!InitSpriteVertBuf()) {
            printf("Failed to init sprite vertex buffer\n");
            return false;
        }
    }

    return true;
}

void Renderer::SetBGColor(uint8_t r, uint8_t g, uint8_t b)
{
    mBGColor = GameMath::Vec3(
        float(r) / 255.0f,
        float(g) / 255.0f,
        float(b) / 255.0f
    );
}

void Renderer::Clear()
{
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClearColor(mBGColor.x, mBGColor.y, mBGColor.z, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Renderer::Update()
{
    if (mLimitFps) {
        Uint32 curTicks = SDL_GetTicks();
        Uint32 ticksPassed = curTicks - mLastTicks;
        const Uint32 desiredFrameTicks = (Uint32)((1.0f/60.0f)*1000.0f);
        if (ticksPassed < desiredFrameTicks) {
            SDL_Delay(desiredFrameTicks - ticksPassed);
        }
        mLastTicks = curTicks;
    }
    SDL_GL_SwapWindow(mWindow);
}


void Renderer::DrawVertexBuffer(
    GameMath::Mat4& modelMat,
    GameMath::Mat4& viewMat,
    const VertexBuffer& vb
)
{
    // Change shaders if necessary
    switch (vb.mType)
    {
    case VertexBuffer::POS_COLOR:
        if (mCurShader != &sPosColorShader) {
            mCurShader = &sPosColorShader;
            //printf("Set cur shader sPosColorShader\n");
            mCurShader->Use();
        }
        break;
    case VertexBuffer::POS_TEXCOORD:
        if (mCurShader != &sPosTexcoordShader) {
            mCurShader = &sPosTexcoordShader;
            //printf("Set cur shader sPosTexcoordShader\n");
            mCurShader->Use();
        }
        break;
    default:
        printf("DrawVertexBuffer: unhandled vertex buffer type\n");
        return;
    }
    
    GameMath::Mat4 mvpMat = mProjMat * viewMat * modelMat;
    mCurShader->SetMat4("uMvpMatrix", mvpMat);
    
    glBindVertexArray(vb.mVAO);
    if (vb.mIndicesSize > 0) {
        // note - inputs are ints but valid types are unsigned byte/short/int
        // https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/glDrawElements.xhtml
        glDrawElements(GL_TRIANGLES, vb.mIndicesSize, GL_UNSIGNED_INT, 0);
    } else {
        glDrawArrays(GL_TRIANGLES, 0, vb.mNumVertices);
    }
}

void Renderer::DrawAabb(AABB& aabb, GameMath::Mat4& viewMat)
{
    if (mCurShader != &sPosColorShader) {
        mCurShader = &sPosColorShader;
        mCurShader->Use();
    }
    GameMath::Mat4 modelMat =
        GameMath::Translate(aabb.pos) *
        GameMath::Scale(
            aabb.max.x - aabb.min.x,
            aabb.max.y - aabb.min.y,
            aabb.max.z - aabb.min.z
        );
    GameMath::Mat4 mvpMat = mProjMat * viewMat * modelMat;
    mCurShader->SetMat4("uMvpMatrix", mvpMat);
    
    glBindVertexArray(sAabbVertBuf.mVAO);
    glDrawElements(GL_LINES, sAabbVertBuf.mIndicesSize, GL_UNSIGNED_INT, 0);
}

void Renderer::DrawSprite(Sprite& sprite)
{
    // updated tex coords
    float leftU = sprite.mU;
    float rightU = leftU + sprite.mW;
    float topV = 1.0f - sprite.mV;
    float bottomV = 1.0f - (sprite.mV + sprite.mH);
    float drawVerts[5*4] = {
        // position         texcoord
        0.0f, 0.0f, 0.0f,   leftU, topV, // top left
        0.0f, -1.0f, 0.0f,   leftU, bottomV, // bottom left
        1.0f, -1.0f, 0.0f,   rightU, bottomV, // bottom right
        1.0f, 0.0f, 0.0f,   rightU, topV // top right
    };
    sSpriteVertBuf.UpdateVertices(drawVerts, 5*4);

    if (mCurShader != &sPosTexcoordShader) {
        mCurShader = &sPosTexcoordShader;
        mCurShader->Use();
    }

    SetTexture(*sprite.mTexture);
    
    // scale size from 0..1 to -1..1
    float normW = sprite.mWidth * 2.0f;
    float normH = sprite.mHeight * 2.0f;
    GameMath::Mat4 mvpMat =
        GameMath::Translate(sprite.mX, sprite.mY, 0.0f) *
        
        // need to offset by w/2, h/2 because we draw relative to top left corner,
        // as opposed to the center of the image
        GameMath::Translate(normW/2,-normH/2,0.0f) *
        GameMath::Rotate(GameMath::Deg2Rad(sprite.mRotDegrees),
            GameMath::Vec3(0.0f, 0.0f, 1.0f)) *
        GameMath::Translate(-normW/2,normH/2,0.0f) *
        
        GameMath::Scale(normW, normH, 1.0f);
    mCurShader->SetMat4("uMvpMatrix", mvpMat);

    glDisable(GL_DEPTH_TEST);
        glBindVertexArray(sSpriteVertBuf.mVAO);
        glDrawElements(GL_TRIANGLES, sSpriteVertBuf.mIndicesSize, GL_UNSIGNED_INT, 0);
    glEnable(GL_DEPTH_TEST);
}

bool Renderer::InitAabbVertBuf()
{
    float vertices[6*8] = {
        // pos                  color
        -0.5f,  0.0f, -0.5f,    0.0f, 0.0f, 1.0f, // 0 bottom back left
        -0.5f,  0.0f,  0.5f,    0.0f, 0.0f, 1.0f, // 1 bottom front left
         0.5f,  0.0f, -0.5f,    0.0f, 0.0f, 1.0f, // 2 bottom back right
         0.5f,  0.0f,  0.5f,    0.0f, 0.0f, 1.0f, // 3 bottom front right
        -0.5f,  1.0f, -0.5f,    0.0f, 0.0f, 1.0f, // 4 top back left
        -0.5f,  1.0f,  0.5f,    0.0f, 0.0f, 1.0f, // 5 top front left
         0.5f,  1.0f, -0.5f,    0.0f, 0.0f, 1.0f, // 6 top back right
         0.5f,  1.0f,  0.5f,    0.0f, 0.0f, 1.0f, // 7 top front right
    };
    int indices[2*12] = {
        0, 1,
        0, 2,
        0, 4,
        1, 3,
        2, 3,
        2, 6,
        3, 7,
        1, 5,
        4, 5,
        5, 7,
        4, 6,
        6, 7
    };
    return sAabbVertBuf.Init(
        VertexBuffer::POS_COLOR,
        vertices,
        6*8,
        indices,
        2*12
    );
}

bool Renderer::InitSpriteVertBuf()
{
    float vertices[5*4] = {
        // position         texcoord
        0.0f, 0.0f, 0.0f,   0.0f, 1.0f, // top left
        0.0f, -1.0f, 0.0f,   0.0f, 0.0f, // bottom left
        1.0f, -1.0f, 0.0f,   1.0f, 0.0f, // bottom right
        1.0f, 0.0f, 0.0f,   1.0f, 1.0f // top right
    };

    int indices[3*2] = {
        1, 2, 3, // bottom left, bottom right, top right
        1, 3, 0 // bottom left, top right, top left
    };

    return sSpriteVertBuf.Init(
        VertexBuffer::POS_TEXCOORD,
        vertices, 5*4,
        indices, 3*2
    );
}

} // namespace PC
} // namespace GameLib

