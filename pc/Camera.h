#ifndef GAMELIB_PC_CAMERA_H_INCLUDED
#define GAMELIB_PC_CAMERA_H_INCLUDED

#include <GameMath/GameMath.h>
#include <MoveComponent.h>

namespace GameLib
{
namespace PC
{

class Camera
{
public:
    virtual GameMath::Mat4& GetViewMat() = 0;
};

class FPSCamera : public Camera
{
public:
    FPSCamera();
    ~FPSCamera();
    
    MoveComponent& GetMoveComponent() { return mMoveComponent; }
    GameMath::Vec3& GetPosition() { return mMoveComponent.position; }
    GameMath::Mat4& GetViewMat() { return mViewMat; }
    
    void Update();
    
private:
    MoveComponent mMoveComponent;
    GameMath::Mat4 mViewMat;
};

class OrbitCamera : public Camera
{
public:
    
    OrbitCamera();
    
    GameMath::Mat4& GetViewMat() { return mViewMat; }
    
    void SetMoveComponent(MoveComponent* mc) { mMoveComponent = mc; }
    MoveComponent* GetMoveComponent() { return mMoveComponent; }

    // Added to the target position to look at
    void SetPositionOffset(GameMath::Vec3 offs) { mPositionOffs = offs; }
    void SetLookatOffset(GameMath::Vec3 offs) { mLookatOffs = offs; }
    
    void SetDistance(const float d) { mDistance = d; }
    void AddDistance(const float d) { mDistance += d; }
    float GetDistance() const { return mDistance; }
    
    void AddPitch(const float amount) {
        mPitch += amount;
        mPitch = GameMath::Clamp(mPitch, -80.0f, 80.0f);
    }
    void AddYaw(const float amount) {
        mYaw += amount;
        while (mYaw < 0.0f) { mYaw += 360.0f; }
        while (mYaw > 360.0f) { mYaw -= 360.0f; }
    }
    float GetYaw() const { return mYaw; }
    float GetPitch() const { return mPitch; }
    
    void Update();
    
private:
    GameMath::Mat4 mViewMat;
    MoveComponent* mMoveComponent; // the position we're following
    GameMath::Vec3 mPosition;
    GameMath::Vec3 mPositionOffs;
    GameMath::Vec3 mLookatOffs;
    float mDistance; // how far away from the moveComponent target
    float mYaw;
    float mPitch;
};

} // namespace PC
} // namespace GameLib

#endif // GAMELIB_PC_CAMERA_H_INCLUDED

