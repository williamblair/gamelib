#include <GameTimer.h>

namespace GameLib
{
namespace PC
{

GameTimer::GameTimer() :
    mLastTicks(0),
    mFps(0.0f),
    mFrameTime(0.0f),
    mDtCtr(0.0f),
    mFrameCtr(0.0f)
{}

GameTimer::~GameTimer()
{}

float GameTimer::Update()
{
    Uint32 curTicks = SDL_GetTicks();
    if (mLastTicks == 0) {
        mLastTicks = curTicks;
        return 0.0f;
    }

    Uint32 ticksPassed = curTicks - mLastTicks;
    float dt = ((float)ticksPassed) / 1000.0f;
    mLastTicks = curTicks;

    mFrameCtr += 1.0f;
    mDtCtr += dt;
    if (mDtCtr >= 1.0f)
    {
        mFps = mFrameCtr / mDtCtr;
        mFrameTime = 1.0f / mFps;
        
        while (mDtCtr >= 1.0f) { mDtCtr -= 1.0f; }
        mFrameCtr = 0.0f;
    }

    return dt;
}

} // namespace PC
} // namespace GameLib

