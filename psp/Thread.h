#ifndef GAMELIB_PSP_THREAD_H_INCLUDED
#define GAMELIB_PSP_THREAD_H_INCLUDED

#include <unordered_map>
#include <cstdint>
#include <pspkernel.h>

typedef uint64_t u64;

namespace GameLib
{
namespace Psp
{

// Thread function should be of form
// void myFcn(void* arg)
typedef void (*ThreadFcn)(void*);

class Thread
{
public:
    Thread();
    ~Thread();

    bool Start(ThreadFcn fcn, void* arg, const char* name);
    void Join(u64* retVal);

    // call from within the ThreadFcn to get the current thread ID
    static u64 GetThreadId();
    // call from within the ThreadFcn to yield
    static void Yield();
    // must call this from within the ThreadFcn upon exit
    static void Exit(u64 retVal);

private:
    int mThreadId;
    ThreadFcn mFcn;
    void* mArg;
    bool mStarted;
    bool mJoined;
    
    // Maps the thread to its return value
    static std::unordered_map<int,u64> sRetVals;
    
    // pspsdk requires returning an int, and takes the size
    // of the void* arg in bytes also
    static int sThreadFcnWrapper(SceSize args, void* arg);
};

} // namespace Psp
} // namespace GameLib

#endif // GAMELIB_PSP_THREAD_H_INCLUDED
