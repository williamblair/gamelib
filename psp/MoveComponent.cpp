#include <MoveComponent.h>
#include <stdio.h>

namespace GameLib
{
namespace Psp
{

MoveComponent::MoveComponent() :
    position(0.0f, 0.0f, 0.0f),
    target(0.0f, 0.0f, -1.0f),
    up(0.0f, 1.0f, 0.0f),
    forward(0.0f, 0.0f, -1.0f),
    right(1.0f, 0.0f, 0.0f),
    pitch(0.0f),
    yaw(0.0f)
{}

void MoveComponent::Update()
{
#if 0
    // calculate right axis
    right = GameMath::Transform(
        GameMath::Rotate(GameMath::Deg2Rad(yaw), GameMath::Vec3(0.0f, 1.0f, 0.0f)),
        GameMath::Vec3(1.0f, 0.0f, 0.0f)
    );
    right = GameMath::Normalize(right);
    
    // calculate up axis
    up = GameMath::Transform(
        GameMath::Rotate(GameMath::Deg2Rad(pitch), right),
        GameMath::Vec3(0.0f, 1.0f, 0.0f)
    );
    up = GameMath::Normalize(up);
    
    // calculate forward axis
    forward = GameMath::Cross(up, right);
    forward = GameMath::Normalize(forward);
    
    // set target equal to a bit forward from the eye position
    target = position + forward;
#endif
    const float yawRad = GameMath::Deg2Rad(yaw);
    //pspvfpu_use_matrices(gum_vfpucontext, 0, VMAT0 | VMAT1 | VMAT2);
    __asm__ volatile(
        // trig functions on the vfpu expect input values like
        // vsin(degrees/90) or vsin(2/PI * radians)
        "mtv %2, S000\n" // mtx0 col0 row0 = yaw radians
        "vcst.s S001, VFPU_2_PI\n" // mtx0 col0 row1 = 2/PI
        "vmul.s S000, S001, S000\n" // convert yaw radians to vfpu unit
        "vcos.s S002, S000\n" // mtx0 col0 row2 = cos(yaw)
        "vnsin.s S003, S000\n" // mtx0 col0 row3 = -sin(yaw)
        // normalize the result coordinate pair
        "vdot.p S010, C002, C002\n" // mtx0 col1 row0 = dot(cos(yaw), -sin(yaw))
        "vrsq.s S010, S010\n" // mtx0 col1 row0 = 1.0/sqrt(mtx0 col1 row0)
        "vscl.p C002, C002, S010\n" // mtx0 col0 row2-3 *= mtx0 col1 row0
        // save coordinate pair in x, z
        "mfv %0, S002\n"
        "mfv %1, S003\n"
    :"=r"(right.x), /*"=r"(right.y),*/ "=r"(right.z)
    :"r"(yawRad));
    right.y = 0.0f;
    
    // calculate up axis
    //up = GameMath::Transform(
    //    GameMath::Rotate(GameMath::Deg2Rad(pitch), right),
    //    GameMath::Vec3(0.0f, 1.0f, 0.0f)
    //);
    //up = GameMath::Normalize(up);
    const float pitchRad = GameMath::Deg2Rad(pitch);
    __asm__ volatile(
        // store tmp values into mtx 0 to create a rotation mtx
        // rotation mtx will go in mtx 1
        "mtv %9, S000\n" // mtx0 col0 row0 = pitchRad
        "vcst.s S002, VFPU_2_PI\n" // col0 row2 = 2/PI
        "vmul.s S001, S000, S002\n" // col0 row1 = pitchRad * (2/PI)
        "vcos.s S000, S001\n" // col0 row0 = cos(pitchRad*(2/PI))
        "vsin.s S001, S001\n" // col0 row1 = sin(pitchRad*(2/PI))
        "vocp.s S003, S000\n" // col0 row3 = 1.0 - cos(pitchRad*(2/PI))

        "mtv %6, S130\n" // mtx1 col3 = right xyz
        "mtv %7, S131\n"
        "mtv %8, S132\n"

        "vmul.t C020, C130, C130\n" // mtx0 col2 = xyz*xyz
        //"vscl.p C030, C011, S010\n" // mtx0 col3 row0-1 = yz * x
        "vmul.s S030, S130, S131\n" // mtx0 col3 row0 = xy
        "vmul.s S031, S130, S132\n" // mtx0 col3 row1 = xz
        "vmul.s S032, S131, S132\n" // mtx0 col3 row2 = yz

        "vscl.t R013, C020, S003\n" // mtx0 col1-3 row3 = (1-c)*xx,yy,zz
        "vadd.s S100, S000, S013\n" // set diagonal = c+(1-c)*xx,yy,zz
        "vadd.s S111, S000, S023\n"
        "vadd.s S122, S000, S033\n"

        "vscl.t R013, C030, S003\n" // mtx0 col1-3 row3 = (1-c)*xy,xz,yz
        "vscl.t C010, C130, S001\n" // mtx0 col1 row0-2 = s*xyz
        
        "vsub.s S110, S013, S012\n" // col1 row0 = (1-c)xy - sz
        "vadd.s S120, S023, S011\n" // col2 row0 = (1-c)xz + sy

        "vadd.s S101, S013, S012\n" // col0 row1 = (1-c)xy + sz
        "vsub.s S121, S033, S010\n" // col2 row1 = (1-c)yz - sx

        "vsub.s S102, S023, S011\n" // col0 row2 = (1-c)xz - sy
        "vadd.s S112, S033, S010\n" // col1 row2 = (1-c)yz + sx

        // set mult vector (0,1,0) in mtx0 col0
        "vzero.t C000\n"
        "vone.s S001\n"

        // multiply rot mtx * vector(0,1,0)
        "vmmul.t M200, M100, M000\n"

        // normalize result
        "vdot.t S210, C200, C200\n"
        "vrsq.s S210, S210\n"
        "vscl.t C200, C200, S210\n"

        // save result in up
        "mfv %0, S200\n" 
        "mfv %1, S201\n" 
        "mfv %2, S202\n"


        // calculate forward axis
        "vcrsp.t C210, C200, C130\n" // cross(up,right)
        "vdot.t S200, C210, C210\n" // normalize cross(up, right)
        "vrsq.s S200, S200\n" 
        "vscl.t C210, C210, S200\n" 

        // save result in forward
        "mfv %3, S210\n"
        "mfv %4, S211\n"
        "mfv %5, S212\n"
    :"=r"(up.x), "=r"(up.y), "=r"(up.z),
     "=r"(forward.x), "=r"(forward.y), "=r"(forward.z)
    :"r"(right.x), "r"(right.y), "r"(right.z),
     "r"(pitchRad));
    
    // calculate forward axis
    //forward = GameMath::Cross(up, right);
    //forward = GameMath::Normalize(forward);
    
    // set target equal to a bit forward from the eye position
    target = position + forward;
}

} // namespace Psp
} // namespace GameLib

