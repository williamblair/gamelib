#ifndef GAMELIB_PSP_INPUT_H_INCLUDED
#define GAMELIB_PSP_INPUT_H_INCLUDED

#include <pspctrl.h>

namespace GameLib
{
namespace Psp
{

class Input
{
public:
    
    Input();
    ~Input();

    bool Init();

    // Should be called once per frame
    void Update();

    bool Quit() { return mQuit; }
    bool Left() { return mLeft; }
    bool Right() { return mRight; }
    bool Up() { return mUp; }
    bool Down() { return mDown; }

    bool UpClicked() { return mLastUp && !mUp; }
    bool DownClicked() { return mLastDown && !mDown; }
    bool LeftClicked() { return mLastLeft && !mLeft; }
    bool RightClicked() { return mLastRight && !mRight; }

    int MouseMoveX() { return mMouseMoveX; }
    int MouseMoveY() { return mMouseMoveY; }

    bool LTrigger() { return mLTrigger; }
    bool RTrigger() { return mRTrigger; }

    bool Cross() { return mCross; }
    bool CrossClicked() { return mLastCross && !mCross; }
    
private:
    bool mQuit;
    bool mLeft;
    bool mRight;
    bool mUp;
    bool mDown;
    bool mLTrigger;
    bool mRTrigger;

    bool mCross;
    bool mLastCross;

    bool mLastUp;
    bool mLastDown;
    bool mLastLeft;
    bool mLastRight;

    int mMouseMoveX;
    int mMouseMoveY;
    
    SceCtrlData mPad;
};

} // namespace Psp
} // namespace GameLib

#endif // GAMELIB_PSP_INPUT_H_INCLUDED

