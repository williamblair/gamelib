#include <cstdio>
#include <fstream>
#include <vector>
#include <sstream>

#include <ObjMesh.h>

namespace GameLib
{
namespace Psp
{

static inline bool pspgetline(int fd, std::string& line)
{
    static char fileBuf[512];
    static size_t index = 512;
    static size_t bufRemain = 0;
    static bool endOfFile = false;
    char c;
    bool foundNewline = false;
    line = "";
    while (true)
    {
        if (bufRemain == 0) {
            if (endOfFile) {
                break;
            }

            size_t amntRead = sceIoRead(fd, fileBuf, 512);
            if (amntRead == 0) {
                break;
            }
            endOfFile = (amntRead < 512);
            bufRemain = amntRead;
            index = 0;
        }
        c = fileBuf[index++];
        --bufRemain;
        if (c == '\n') {
            foundNewline = true;
            break;
        }
        line += c;
    }
    return foundNewline;
}

ObjMesh::ObjMesh()
{}

ObjMesh::~ObjMesh()
{}

bool ObjMesh::Init(const char* fileName)
{
    std::vector<float> positions;
    std::vector<float> texcoords;
    std::vector<float> normals;
    std::vector<float> combinedVertices;
    std::vector<int> vertIndices;
    std::vector<int> texcoordIndices;
    std::vector<int> normIndices;
    std::vector<int> indices;
    
    int fd = sceIoOpen(fileName, PSP_O_RDONLY, 0777);
    if (fd < 0) {
        dbgprint("Failed to open %s\n", fileName);
        return false;
    }
    
    std::string curLine;
    while (pspgetline(fd, curLine))
    {
        dbgprint("cur line: %s\n", curLine.c_str());
        std::stringstream ss(curLine);
        std::vector<std::string> lineWords;
        
        // skip lines empty or beginning with '#'
        if (curLine.empty() ||
            curLine[0] == '#')
        {
            continue;
        }
        
        std::string type;
        ss >> type;
        
        // vertex position
        if (type == "v")
        {
            float x, y, z;
            ss >> x >> y >> z;
            positions.push_back(x);
            positions.push_back(y);
            positions.push_back(z);
            // ignore w
        }
        // vertex texture coord
        else if (type == "vt")
        {
            float uv;
            ss >> uv;
            //while (uv < 0.0f) uv += 1.0f;
            //while (uv > 1.0f) uv -= 1.0f;
            texcoords.push_back(uv);
            // optional v coord
            if (ss >> uv) {
                //while (uv < 0.0f) uv += 1.0f;
                //while (uv > 1.0f) uv -= 1.0f;
                texcoords.push_back(uv);
            } else {
                texcoords.push_back(0.0f);
            }
        }
        // vertex normal
        else if (type == "vn")
        {
            float x,y,z;
            ss >> x >> y >> z;
            // normalize
            float mag = sqrtf(x*x + y*y + z*z);
            x /= mag;
            y /= mag;
            z /= mag;
            normals.push_back(x);
            normals.push_back(y);
            normals.push_back(z);
        }
        // vertex face
        else if (type == "f")
        {
            std::string curFace;
            ss >> curFace;
            if (curFace.find('/') != std::string::npos)
            {
                char fwrdSlsh;
                int index;
                // triangles only
                for (int i=0; i<3; ++i) {
                    std::stringstream faceStrm(curFace);
                    
                    // pos index
                    faceStrm >> index;
                    faceStrm >> fwrdSlsh;
                    vertIndices.push_back(index-1);
                    
                    // texcoord index
                    faceStrm >> index;
                    faceStrm >> fwrdSlsh;
                    texcoordIndices.push_back(index-1);
                    
                    // normal index
                    faceStrm >> index;
                    normIndices.push_back(index-1);
                    
                    if (!(ss >> curFace)) {
                        break;
                    }
                }
            }
            else
            {
                // triangles only
                for (int i=0; i<3; ++i) {
                    int index;
                    std::stringstream faceStrm(curFace);
                    faceStrm >> index;
                    
                    vertIndices.push_back(index-1);
                    
                    if (!(ss >> curFace)) {
                        break;
                    }
                }
            }
        }
    }
    sceIoClose(fd);
    
    indices = vertIndices;
    
    const size_t floatsPerPos = 3;
    const size_t floatsPerTexcoord = 2;
    const size_t floatsPerNormal = 3;
    if (vertIndices.size() > 0 &&
        texcoordIndices.size() > 0 &&
        normIndices.size() > 0)
    {
        if ((vertIndices.size() != texcoordIndices.size()) ||
            (vertIndices.size() != normIndices.size()))
        {
            dbgprint("Obj indices sizes mismatch\n");
            return false;
        }
        
        // positions size is the largest
        if (((positions.size()/floatsPerPos) > (texcoords.size()/floatsPerTexcoord)) &&
            ((positions.size()/floatsPerPos) > (normals.size()/floatsPerNormal)))
        {
            std::vector<float> reorganizedTexcoords((positions.size()/floatsPerPos)*floatsPerTexcoord);
            std::vector<float> reorganizedNormals((positions.size()/floatsPerPos)*floatsPerNormal);
            
            for (size_t i=0; i<vertIndices.size(); ++i)
            {
                const int posIndex = vertIndices[i];
                const int texIndex = texcoordIndices[i];
                const int nrmIndex = normIndices[i];
                
                float u = texcoords[texIndex*floatsPerTexcoord + 0];
                float v = texcoords[texIndex*floatsPerTexcoord + 1];
                
                float nrmX = normals[nrmIndex*floatsPerNormal + 0];
                float nrmY = normals[nrmIndex*floatsPerNormal + 1];
                float nrmZ = normals[nrmIndex*floatsPerNormal + 2];
                
                reorganizedTexcoords[posIndex*floatsPerTexcoord + 0] = u;
                reorganizedTexcoords[posIndex*floatsPerTexcoord + 1] = v;
                
                reorganizedNormals[posIndex*floatsPerNormal + 0] = nrmX;
                reorganizedNormals[posIndex*floatsPerNormal + 1] = nrmY;
                reorganizedNormals[posIndex*floatsPerNormal + 2] = nrmZ;
            }
            
            texcoords = reorganizedTexcoords;
            normals = reorganizedNormals;
            indices = vertIndices;
        }
        // texcoords size is the largest
        else if (((texcoords.size()/floatsPerTexcoord) > (positions.size()/floatsPerPos)) &&
            ((texcoords.size()/floatsPerTexcoord) > (normals.size()/floatsPerNormal)))
        {
            std::vector<float> reorganizedPositions((texcoords.size()/floatsPerTexcoord)*floatsPerPos);
            std::vector<float> reorganizedNormals((texcoords.size()/floatsPerTexcoord)*floatsPerNormal);
            
            for (size_t i=0; i<texcoordIndices.size(); ++i)
            {
                const int posIndex = vertIndices[i];
                const int texIndex = texcoordIndices[i];
                const int nrmIndex = normIndices[i];
                
                float x = positions[posIndex*floatsPerPos + 0];
                float y = positions[posIndex*floatsPerPos + 1];
                float z = positions[posIndex*floatsPerPos + 2];
                
                float nrmX = normals[nrmIndex*floatsPerNormal + 0];
                float nrmY = normals[nrmIndex*floatsPerNormal + 1];
                float nrmZ = normals[nrmIndex*floatsPerNormal + 2];
                
                reorganizedPositions[texIndex*floatsPerPos + 0] = x;
                reorganizedPositions[texIndex*floatsPerPos + 1] = y;
                reorganizedPositions[texIndex*floatsPerPos + 2] = z;
                
                reorganizedNormals[texIndex*floatsPerNormal + 0] = nrmX;
                reorganizedNormals[texIndex*floatsPerNormal + 1] = nrmY;
                reorganizedNormals[texIndex*floatsPerNormal + 2] = nrmZ;
            }
            
            positions = reorganizedPositions;
            normals = reorganizedNormals;
            indices = texcoordIndices;
        }
        // normals.size() is the largest
        else
        {
            std::vector<float> reorganizedPositions((normals.size()/floatsPerNormal)*floatsPerPos);
            std::vector<float> reorganizedTexcoords((normals.size()/floatsPerNormal)*floatsPerTexcoord);
            
            for (size_t i=0; i<normIndices.size(); ++i)
            {
                const int posIndex = vertIndices[i];
                const int texIndex = texcoordIndices[i];
                const int nrmIndex = normIndices[i];
                
                float x = positions[posIndex*floatsPerPos + 0];
                float y = positions[posIndex*floatsPerPos + 1];
                float z = positions[posIndex*floatsPerPos + 2];
                
                float u = texcoords[texIndex*floatsPerTexcoord + 0];
                float v = texcoords[texIndex*floatsPerTexcoord + 1];
                
                reorganizedPositions[nrmIndex*floatsPerPos + 0] = x;
                reorganizedPositions[nrmIndex*floatsPerPos + 1] = y;
                reorganizedPositions[nrmIndex*floatsPerPos + 2] = z;
                
                reorganizedTexcoords[nrmIndex*floatsPerTexcoord + 0] = u;
                reorganizedTexcoords[nrmIndex*floatsPerTexcoord + 1] = v;
            }
            
            positions = reorganizedPositions;
            texcoords = reorganizedTexcoords;
            indices = normIndices;
        }
    }

    // assumes model has pos, norm, texcoords
    if (((texcoords.size()/floatsPerTexcoord) != (positions.size()/floatsPerPos)) ||
        ((normals.size()/floatsPerNormal) != (positions.size()/floatsPerPos)))
    {
        printf("Obj positions,texcoords,normals sizes mismatch\n");
        return false;
    }
    const size_t numVertices = positions.size()/floatsPerPos;
    for (size_t i=0; i<numVertices; ++i)
    {
        const size_t posIndex = i*floatsPerPos;
        const size_t texIndex = i*floatsPerTexcoord;
        const size_t nrmIndex = i*floatsPerNormal;
        
        // TODO - support normals in VertexBuffer
        combinedVertices.push_back(positions[posIndex + 0]);
        combinedVertices.push_back(positions[posIndex + 1]);
        combinedVertices.push_back(positions[posIndex + 2]);
        //combinedVertices.push_back(normals[nrmIndex + 0]);
        //combinedVertices.push_back(normals[nrmIndex + 1]);
        //combinedVertices.push_back(normals[nrmIndex + 2]);
        combinedVertices.push_back(texcoords[texIndex + 0]);
        combinedVertices.push_back(texcoords[texIndex + 1]);
    }

    
    return mVertBuf.Init(
        VertexBuffer::POS_TEXCOORD,
        combinedVertices.data(),
        combinedVertices.size(),
        (indices.size() > 0) ? indices.data() : nullptr,
        indices.size()
    );
}

} // namespace Psp
} // namespace GameLib

