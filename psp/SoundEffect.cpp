#include <SoundEffect.h>

#include <unistd.h>
#include <string.h>

#include <pspctrl.h>
#include <pspdebug.h>
#include <pspdisplay.h>
#include <psptypes.h>
#include <pspmoduleinfo.h>
#include <pspthreadman.h>
#include <pspkernel.h>
#include <pspgu.h>
#include <pspgum.h>
#include <pspaudiolib.h>
#include <pspaudio.h>
#include <psputility.h>

#include <bjdebug.h>

namespace GameLib
{
namespace Psp
{

// This function gets called by pspaudiolib every time the
// audio buffer needs to be filled. The sample format is
// 16-bit, stereo.
void SoundEffect::audioCallback(void* buf, unsigned int length, void* userdata)
{
    SoundEffect* mus = (SoundEffect*)userdata;
    Music::WAVHeader& wavHdr = mus->mWavHdr;
    Music::WAVStreamData& wavStream = mus->mWavStream;

    if (mus->mLocked) {
        return;
    }
    mus->mLocked = true;

    if (wavStream.ended && !wavStream.looping) {
        // hopefully this causes this function to stop being called...
        pspAudioSetChannelCallback(
            mus->mChannel,
            nullptr,
            nullptr
        );
        mus->mLocked = false;
        return;
    }

    // length = number of samples = 32bits/4bytes (left, right channel)
    int outBufBytes = length*4;
    int numBytesRemain = outBufBytes;
    unsigned char* outBuf = (unsigned char*)buf;
    while (numBytesRemain > 0) {

        // copy buffered samples into output
        int amountBytesCopy =
            (wavStream.bufSizeRemain >= numBytesRemain) ?
            numBytesRemain :
            wavStream.bufSizeRemain;
        // assumes WAV file data is expected format, 16bit stereo, with
        // matching sample rate
        memcpy(outBuf, wavStream.bufPtr, amountBytesCopy);

        outBuf += amountBytesCopy;
        numBytesRemain -= amountBytesCopy;
        wavStream.bufPtr += amountBytesCopy;
        wavStream.bufSizeRemain -= amountBytesCopy;

        // get more WAV file samples if buffer is empty
        if (wavStream.bufSizeRemain == 0) {

            wavStream.bufPtr = wavStream.buffer;
            wavStream.bufSizeRemain = sizeof(wavStream.buffer);

            int numFileReadRemain = sizeof(wavStream.buffer);
            unsigned char* fileReadPtr = wavStream.buffer;

            while (numFileReadRemain > 0) {

                int numToRead =
                    (wavStream.fileBytesRemain >= numFileReadRemain) ?
                    numFileReadRemain :
                    wavStream.fileBytesRemain;
                int numRead = sceIoRead(wavStream.wavFd, fileReadPtr, numToRead);
                if (numRead <= 0) {
                    if (wavStream.looping) {
                        dbgprint("resetting wav file to data past header\n");

                        // go back to beginning of WAV file past header
                        sceIoLseek32(wavStream.wavFd, sizeof(Music::WAVHeader), PSP_SEEK_SET);
                        wavStream.fileBytesRemain = wavStream.fileSizeBytes;

                        // re-read
                        numRead = sceIoRead(wavStream.wavFd, fileReadPtr, numToRead);
                    } else {
                        wavStream.ended = true;
                        mus->mLocked = false;
                        return;
                    }
                }

                fileReadPtr += numRead;
                numFileReadRemain -= numRead;
                wavStream.fileBytesRemain -= numRead;

                // If we reached the end of the file...
                if (wavStream.fileBytesRemain == 0) {
                    if (wavStream.looping) {
                        dbgprint("resetting wav file to data past header\n");
                        // go back to beginning of WAV file past header
                        sceIoLseek32(wavStream.wavFd, sizeof(Music::WAVHeader), PSP_SEEK_SET);
                        wavStream.fileBytesRemain = wavStream.fileSizeBytes;
                    } else {
                        wavStream.ended = true;
                        mus->mLocked = false;
                        return;
                    }
                }
            }
        }
    }
    mus->mLocked = false;
}

SoundEffect::SoundEffect() :
    mLooping(false),
    mChannel(1)
{}

SoundEffect::~SoundEffect()
{
}

bool SoundEffect::Init(const char* fileName)
{
    mLooping = true;
    mChannel = 1;
    
    Music::WAVHeader& hdr = mWavHdr;
    Music::WAVStreamData& strm = mWavStream;

    // open the WAV file
    strm.wavFd = sceIoOpen(fileName, PSP_O_RDONLY, 0777);
    if (strm.wavFd < 0) {
        dbgprint("Failed to open %s\n", fileName);
        return false;
    }

    // read the WAV header
    int numRead = sceIoRead(strm.wavFd, (char*)&hdr, sizeof(Music::WAVHeader));
    if (numRead != sizeof(Music::WAVHeader)) {
        dbgprint("Failed to read WAV header\n");
        return false;
    }

    // Verify expected header contents
    dbgprint("hdr RIFF: %c,%c,%c,%c\n",
        hdr.riff[0],
        hdr.riff[1],
        hdr.riff[2],
        hdr.riff[3]);
    dbgprint("hdr fileSize: %ld\n", hdr.fileSize);
    dbgprint("hdr WAVE: %c,%c,%c,%c\n",
        hdr.wave[0],
        hdr.wave[1],
        hdr.wave[2],
        hdr.wave[3]);
    dbgprint("hdr fmt: %c,%c,%c,%c\n",
        hdr.fmt[0],
        hdr.fmt[1],
        hdr.fmt[2],
        hdr.fmt[3]);
    dbgprint("hdr fmtLen: %ld\n", hdr.fmtLen);
    dbgprint("hdr fmtType: %d\n", hdr.fmtType);
    dbgprint("hdr numChannels: %d\n", hdr.numChannels);
    dbgprint("hdr sampleRate: %ld\n", hdr.sampleRate);
    dbgprint("hdr dataRateBytes: %ld\n", hdr.dataRateBytes);
    dbgprint("hdr sampleSizeBytes: %d\n", hdr.sampleSizeBytes);
    dbgprint("hdr bitsPerSample: %d\n", hdr.bitsPerSample);
    dbgprint("hdr DATA: %c,%c,%c,%c\n",
        hdr.data[0],
        hdr.data[1],
        hdr.data[2],
        hdr.data[3]);
    dbgprint("hdr dataSize: %ld\n", hdr.dataSize);

    if (hdr.sampleRate != 44100) {
        dbgprint("Unexpected WAV sampleRate\n");
        return false;
    }
    if (hdr.sampleSizeBytes != 4) {
        dbgprint("Unexpected sampleSizeBytes\n");
        return false;
    }
    if (hdr.bitsPerSample != 16) {
        dbgprint("Unexpected bitsPerSample\n");
        return false;
    }

    // initialize streaming data
    strm.fileSizeBytes = hdr.dataSize; // bytes of the data section
    strm.fileBytesRemain = hdr.dataSize; // how many bytes left to be read from the file
    strm.bufPtr = strm.buffer; // beginning of the samples buffer
    strm.bufSizeRemain = sizeof(strm.buffer); // sample data left in buffer in bytes
    strm.looping = false;
    strm.ended = false;

    // read initial samples into the buffer
    numRead = sceIoRead(strm.wavFd, strm.bufPtr, sizeof(strm.buffer));
    if (numRead != sizeof(strm.buffer)) {
        dbgprint("Failed to read initial samples\n");
        return false;
    }

    // initialize audio if not done already
    if (!Music::sAudioInitted) {
        pspAudioInit();
        Music::sAudioInitted = true;
    }

    return true;
}

void SoundEffect::Play(int channel, bool loop) {
    Music::WAVStreamData& strm = mWavStream;
    Music::WAVHeader& hdr = mWavHdr;

    while (mLocked) {
        usleep(5); // sleep 5 microseconds
    }
    mLocked = true;
    
    // TODO - different channels
    if (channel == -1) { channel = 1; }
    pspAudioSetChannelCallback(
        channel,
        nullptr,
        nullptr
    );
    
    // initialize streaming data to beginning
    strm.fileSizeBytes = hdr.dataSize; // bytes of the data section
    strm.fileBytesRemain = hdr.dataSize; // how many bytes left to be read from the file
    strm.bufPtr = strm.buffer; // beginning of the samples buffer
    strm.bufSizeRemain = sizeof(strm.buffer); // sample data left in buffer in bytes
    strm.looping = false;
    strm.ended = false;

    // Go to beginning of file
    sceIoLseek32(strm.wavFd, sizeof(Music::WAVHeader), PSP_SEEK_SET);
    // Read initial samples into buffer
    int numRead = sceIoRead(strm.wavFd, strm.bufPtr, sizeof(strm.buffer));
    if (numRead != sizeof(strm.buffer)) {
        dbgprint("Failed to read initial samples\n");
        return;
    }
    mChannel = channel;
    mWavStream.looping = loop;
    mLocked = false;
    pspAudioSetChannelCallback(
        channel,
        audioCallback,
        (void*)this
    );
}

void SoundEffect::Stop(int channel) {
    // TODO - different channels
    if (channel == -1) { channel = 1; }
    pspAudioSetChannelCallback(
        channel,
        nullptr,
        nullptr
    );
}

} // namespace PC
} // namespace GameLib

