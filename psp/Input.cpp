#include <Input.h>

namespace GameLib
{
namespace Psp
{

Input::Input() :
    mQuit(false),
    mLeft(false),
    mRight(false),
    mUp(false),
    mDown(false),
    mLTrigger(false),
    mRTrigger(false),
    mCross(false),
    mLastCross(false),
    mLastUp(false),
    mLastDown(false),
    mLastLeft(false),
    mLastRight(false),
    mMouseMoveX(0),
    mMouseMoveY(0)
{}

Input::~Input()
{}

// TODO - add joysticks
bool Input::Init()
{
    return true;
}

void Input::Update()
{
    sceCtrlReadBufferPositive(&mPad, 1);
    
    mMouseMoveX = 0;
    mMouseMoveY = 0;
    
    mLastUp = mUp;
    mLastDown = mDown;
    mLastLeft = mLeft;
    mLastRight = mRight;

    mUp = mDown = mLeft = mRight = false;
    mLTrigger = mRTrigger = false;

    mLastCross = mCross;
    mCross = false;
    
    if (mPad.Buttons != 0) {
        if (mPad.Buttons & PSP_CTRL_SELECT) {
            mQuit = true;
        }
        if (mPad.Buttons & PSP_CTRL_UP) { mUp = true; }
        if (mPad.Buttons & PSP_CTRL_DOWN) { mDown = true; }
        if (mPad.Buttons & PSP_CTRL_LEFT) { mLeft = true; }
        if (mPad.Buttons & PSP_CTRL_RIGHT) { mRight = true; }
        if (mPad.Buttons & PSP_CTRL_LTRIGGER) { mLTrigger = true; }
        if (mPad.Buttons & PSP_CTRL_RTRIGGER) { mRTrigger = true; }
        if (mPad.Buttons & PSP_CTRL_CROSS) { mCross = true; }
    }
}

} // namespace Psp
} // namespace GameLib

