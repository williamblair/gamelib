#ifndef GAMELIB_PSP_RENDERER_H_INCLUDED
#define GAMELIB_PSP_RENDERER_H_INCLUDED

#include <pspctrl.h>
#include <pspdebug.h>
#include <pspdisplay.h>
#include <psptypes.h>
#include <pspmoduleinfo.h>
#include <pspthreadman.h>
#include <pspkernel.h>
#include <pspgu.h>
#include <pspgum.h>

#include <malloc.h>
#include <stdio.h>

#include <GameMath/GameMath.h>
#include <Texture.h>
#include <VertexBuffer.h>
#include <AABB.h>
#include <Sprite.h>
#include <bjdebug.h>

// 6 vertices per sprite
#define SPRITE_NVERTICES (512*6) 

namespace GameLib
{
namespace Psp
{

class Renderer
{
public:

    Renderer();
    ~Renderer();
    
    bool Init(int width, int height, bool fullScreen, const char* title);
    
    void SetBGColor(uint8_t r, uint8_t g, uint8_t b);
    
    void Clear();
    void Update();
    
    void DrawVertexBuffer(
        GameMath::Mat4& modelMat,
        GameMath::Mat4& viewMat,
        const VertexBuffer& vb
    );
    void DrawAabb(AABB& aab, GameMath::Mat4& viewMat);
    void DrawSprite(Sprite& sprite);

    void SetTexture(Texture& tex) { mCurTex = &tex; }

    int GetWidth() const { return mWidth; }
    int GetHeight() const { return mHeight; }

private:
    
    int mWidth;
    int mHeight;
    uint32_t mBgColor;
    
    GameMath::Mat4 mProjMat;
    enum class ProjMatType
    {
        IDENTITIY,
        PERSPECTIVE
    };
    ProjMatType mCurProjMat;

    Texture* mCurTex;
    Texture* mCurLoadedTex;

    // for debug drawing AABBs
    static VertexBuffer sAabbVertBuf;
    static bool InitAabbVertBuf();

    // for debug drawing 2d sprites onscreen
    int mCurSpriteVertBuf;
    //static VertexBuffer sSpriteVertBufs[SPRITE_NVERTBUFS];
    static VertexBuffer::PosTexVertex* sSpriteVertBufs;
    static bool InitSpriteVertBufs();
};
    
} // namespace Psp
} // namespace GameLib

#endif // GAMELIB_PSP_RENDERER_H_INCLUDED
