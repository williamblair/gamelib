#include <Md2Mesh.h>
#include <bjdebug.h>

namespace GameLib
{
namespace Psp
{

Md2Mesh::Md2Mesh() :
    mStartFrame(0),
    mEndFrame(0),
    mCurrentFrame(0),
    mNextFrame(1),
    mInterpolation(0.0f),
    mLoopAnim(true)
{
    mCurrentAnimName[0] = '\0';
}
Md2Mesh::~Md2Mesh()
{
    if (mSkins) free(mSkins);
    if (mTexCoords) delete[] mTexCoords;
    if (mMd2TexCoords) free(mMd2TexCoords);
    if (mTriangles) free(mTriangles);
    if (mKeyFrames) free(mKeyFrames);
    for (size_t i = 0; i < mKeyFramesSize; ++i)
    {
        delete[] mKeyFrames[i].vertices;
        if (mKeyFrames[i].md2Vertices) free(mKeyFrames[i].md2Vertices);
    }
    if (mRadii) free(mRadii);
}
    
bool Md2Mesh::Init(const std::string& fileName)
{
    int fd = sceIoOpen(fileName.c_str(), PSP_O_RDONLY, 0777);
    if (fd < 0) {
        dbgprint("Failed to open Md2 file: %s\n", fileName.c_str());
        return false;
    }
    
    Header header;
    sceIoRead(fd, (char*)&header, sizeof(Header));
    
    // Verify file header correctness
    if (header.magic[0] != 'I' ||
        header.magic[1] != 'D' ||
        header.magic[2] != 'P' ||
        header.magic[3] != '2')
    {
        printf("Invalid header: magic != IDP2\n");
        return false;
    }
    
    if (header.version != 8)
    {
        printf("Invalid header: version != 8\n");
        return false;
    }
    
    // reserve space for MD2 data
    mSkins = (Skin*)malloc(header.numSkins*sizeof(Skin)); mSkinsSize = header.numSkins;
    mTexCoords = new GameMath::Vec2[header.numTexCoords]; mTexCoordsSize = header.numTexCoords;
    mMd2TexCoords = (TexCoord*)malloc(header.numTexCoords*sizeof(TexCoord)); mMd2TexCoordsSize = header.numTexCoords;
    mTriangles = (Triangle*)malloc(header.numTriangles*sizeof(Triangle)); mTrianglesSize = header.numTriangles;
    mKeyFrames = (KeyFrame*)malloc(header.numFrames*sizeof(KeyFrame)); mKeyFramesSize = header.numFrames;
    
    for (size_t i = 0; i < mKeyFramesSize; ++i)
    {
        mKeyFrames[i].vertices = new GameMath::Vec3[header.numVertices];
        mKeyFrames[i].verticesSize = header.numVertices;
        mKeyFrames[i].md2Vertices = (Vertex*)malloc(header.numVertices*sizeof(Vertex));
        mKeyFrames[i].md2VerticesSize = header.numVertices;
    }
    
    dbgprint("Keyframes size setvertices: %u\n", mKeyFramesSize);
    
    // read MD2 components
#define READ_DATA(offset, vec, number, type) \
    sceIoLseek32(fd, offset, PSP_SEEK_SET); \
    sceIoRead(fd, (void*)vec, number*sizeof(type))
    
    READ_DATA(header.skinOffset, mSkins, header.numSkins, Skin);    
    READ_DATA(header.texCoordOffset, mMd2TexCoords, header.numTexCoords, TexCoord);
    READ_DATA(header.triangleOffset, mTriangles, header.numTriangles, Triangle);

#undef READ_DATA
    
    sceIoLseek32(fd, header.frameOffset, PSP_SEEK_SET);
    for (int i = 0; i < header.numFrames; ++i)
    {
        KeyFrame* f = &mKeyFrames[i];
        sceIoRead(fd, (void*)(f->scale), 3*sizeof(float));
        sceIoRead(fd, (void*)(f->translate), 3*sizeof(float));
        sceIoRead(fd, (void*)(f->name), 16*sizeof(char));
        sceIoRead(fd, (void*)(f->md2Vertices), header.numVertices*sizeof(Vertex));
    }
    
    // scale MD2 vertices into regular OpenGL vertices
    mRadii = (float*)malloc(mKeyFramesSize*sizeof(float));
    mRadiiSize = mKeyFramesSize;
    size_t mRadiiIndex = 0;
    for (size_t i = 0; i < mKeyFramesSize; ++i)
    {
        KeyFrame& frame = mKeyFrames[i];
        
        float min = 10000.0f;
        float max = -10000.0f;
        
        int k = 0;
        for (size_t j = 0; j < frame.verticesSize; ++j)
        {
            GameMath::Vec3& vertex = frame.vertices[j];
            
            vertex.x = frame.scale[0] * frame.md2Vertices[k].v[0] + frame.translate[0];
            vertex.z = frame.scale[1] * frame.md2Vertices[k].v[1] + frame.translate[1];
            vertex.y = frame.scale[2] * frame.md2Vertices[k].v[2] + frame.translate[2];
             
            ++k;
            
            if (vertex.y < min) min = vertex.y;
            if (vertex.y > max) max = vertex.y;
        }
        
        float frameRadius = (max - min) / 2.0f;
        mRadii[mRadiiIndex++] = frameRadius;
    }
        
    // scale tex coords into regular OpenGL tex coords
    int i = 0;
    for (size_t j = 0; j < mTexCoordsSize; ++j)
    {
        GameMath::Vec2& texCoord = mTexCoords[j];
        texCoord.x = float(mMd2TexCoords[i].s) / float(header.skinWidth); // s
        texCoord.y = 1.0f - (float(mMd2TexCoords[i].t) / float(header.skinHeight)); // t
        ++i;
    }
    
    sceIoClose(fd);
    
    reorganizeVertices();
    
    // 3 pos, 2 tex coord
    mInterpolatedFrame.resize(mKeyFrames[0].verticesSize*5);
    for (int i = 0; i < mInterpolatedFrame.size()/5; ++i)
    {
        float* vert = &mInterpolatedFrame[i*5];
        vert[0] = mKeyFrames[0].vertices[i].x;
        vert[1] = mKeyFrames[0].vertices[i].y;
        vert[2] = mKeyFrames[0].vertices[i].z;
        
        vert[3] = mTexCoords[i].x;
        vert[4] = mTexCoords[i].y;
    }
    strcpy(mCurrentAnimName, mKeyFrames[0].name);
    dbgprint("Interpolated frame vertices size: %u\n",
        mInterpolatedFrame.size());
        
    genBuffers();
    genAnimations();
    
    // free unused data now
    free(mMd2TexCoords); mMd2TexCoords = nullptr;
    free(mSkins); mSkins = nullptr;
    free(mTriangles); mTriangles = nullptr;
    for (size_t i = 0; i < mKeyFramesSize; ++i)
    {
        free(mKeyFrames[i].md2Vertices);
        mKeyFrames[i].md2Vertices = nullptr;
    }

    return true;
}

void Md2Mesh::Update(const float dt)
{
    const float FRAMES_PER_SECOND = 8.0f;
    mInterpolation += dt * FRAMES_PER_SECOND;
    if (mInterpolation >= 1.0f)
    {
        mCurrentFrame = mNextFrame++;
        if (mNextFrame >= mEndFrame)
        {
            mLoopAnim = true;
            if (mLoopAnim)
            {
                mNextFrame = mStartFrame;
            }
            else
            {
                mNextFrame = mEndFrame;
                mStartFrame = mEndFrame;
            }

        }
        
        mInterpolation = 0.0f;
    }
    
    float t = mInterpolation;
    if (mStartFrame == mEndFrame)
    {
        t = 0.0f;
    }
    int i = 0;
    VertexBuffer::PosTexVertex* ptv = mVertexBuf.GetPosTexVertices();
    for (size_t j = 0; j < mInterpolatedFrame.size()/5; ++j)
    {
        float* vert = &mInterpolatedFrame[j*5];
        GameMath::Vec3& vertex = *((GameMath::Vec3*)&ptv[j].x);
        GameMath::Vec2& texCoord = *((GameMath::Vec2*)&ptv[j].u);
        GameMath::Vec3& curKeyFrame = mKeyFrames[mCurrentFrame].vertices[i];
        GameMath::Vec3& nextKeyFrame = mKeyFrames[mNextFrame].vertices[i];

        __asm__ volatile(
            "mtv %3, S000\n" // mtx0 col0 = cur keyframe
            "mtv %4, S001\n"
            "mtv %5, S002\n"

            "mtv %6, S010\n" // mtx1 col1 = next keyframe
            "mtv %7, S011\n"
            "mtv %8, S012\n"

            "mtv %9, S003\n" // mtx0 col0 row3 = t
            "vocp.s S013, S003\n" // mtx0 col1 row3 = 1-t

            "vscl.t C000, C000, S013\n" // col0 *= 1-t
            "vscl.t C010, C010, S003\n" // col1 *= t

            "vadd.t C020, C000, C010\n" // col2 = col0 + col1

            "mfv %0, S020\n" // vertex = col2
            "mfv %1, S021\n"
            "mfv %2, S022\n"
        :"=r"(vertex.x), "=r"(vertex.y), "=r"(vertex.z)
        :"r"(curKeyFrame.x), "r"(curKeyFrame.y), "r"(curKeyFrame.z),
         "r"(nextKeyFrame.x), "r"(nextKeyFrame.y), "r"(nextKeyFrame.z),
         "r"(t));
        
        texCoord = mTexCoords[j];
        
        ++i;
    }
}

void Md2Mesh::Draw(
    GameMath::Mat4& modelMat,
    GameMath::Mat4& viewMat,
    Renderer& render)
{
    render.DrawVertexBuffer(
        modelMat,
        viewMat,
        mVertexBuf);
}

// Because some vertices are reused, different texture indices
// by default can refer to the same vertex indices, so we copy these
// vertices so there are no conflictions
void Md2Mesh::reorganizeVertices()
{
    GameMath::Vec3* tmpVertices = new GameMath::Vec3[mTrianglesSize*3];
    GameMath::Vec2* tmpTexCoords = new GameMath::Vec2[mTrianglesSize*3];
    size_t tmpVerticesSize = mTrianglesSize * 3;
    size_t tmpTexCoordsSize = mTrianglesSize * 3;
    
    bool texCoordsDone = false;
    
    for (size_t frameIndex = 0; frameIndex < mKeyFramesSize; ++frameIndex)
    {
        KeyFrame& frame = mKeyFrames[frameIndex];
        
        size_t tmpVertIndex = 0;
        size_t tmpTexIndex = 0;
        
        for (uint32_t i = 0; i < mTrianglesSize; ++i)
        {
            tmpVertices[tmpVertIndex++] = frame.vertices[mTriangles[i].vertIndex[0]];
            tmpVertices[tmpVertIndex++] = frame.vertices[mTriangles[i].vertIndex[1]];
            tmpVertices[tmpVertIndex++] = frame.vertices[mTriangles[i].vertIndex[2]];
            if (!texCoordsDone)
            {
                tmpTexCoords[tmpTexIndex++] = mTexCoords[mTriangles[i].texCoordIndex[0]];
                tmpTexCoords[tmpTexIndex++] = mTexCoords[mTriangles[i].texCoordIndex[1]];
                tmpTexCoords[tmpTexIndex++] = mTexCoords[mTriangles[i].texCoordIndex[2]];
            }
        }
        
        // tex coords shared between frames; only copy once
        texCoordsDone = true;
        
        // copy the reassigned vertex data
        if (frame.verticesSize < tmpVerticesSize) {
            delete[] frame.vertices;
            frame.vertices = new GameMath::Vec3[tmpVerticesSize];
            frame.verticesSize = tmpVerticesSize;
        }
        memcpy(
            (void*)frame.vertices,
            (void*)tmpVertices,
            tmpVerticesSize*sizeof(GameMath::Vec3)
        );
    }
    
    // copy the reassigned tex coord
    if (mTexCoordsSize < tmpTexCoordsSize) {
        delete[] mTexCoords;
        mTexCoords = new GameMath::Vec2[tmpTexCoordsSize];
        mTexCoordsSize = tmpTexCoordsSize;
    }
    memcpy(
        (void*)mTexCoords,
        (void*)tmpTexCoords,
        tmpTexCoordsSize*sizeof(GameMath::Vec2)
    );
    
    delete[] tmpVertices;
    delete[] tmpTexCoords;
}

// remove any folder prefixes from the model texture file names
void Md2Mesh::stripTextureNames()
{
#if 0
    for (Skin& skin : mSkins)
    {
        std::string texture = skin.name;
        
        size_t fileNameStart = texture.find_last_of("/") + 1;
        size_t lastDot = texture.find_last_of(".");
        
        std::string textureName = texture.substr(fileNameStart, lastDot - fileNameStart);
        texNames.push_back(textureName);
    }
#endif
}

void Md2Mesh::genBuffers()
{
    mVertexBuf.Init(
        VertexBuffer::POS_TEXCOORD,
        mInterpolatedFrame.data(),
        mInterpolatedFrame.size(),
        nullptr,
        0
    );
}

void Md2Mesh::genAnimations()
{
    mAnimationsSize = countNumAnims();
    mAnimations = (Anim*)malloc(mAnimationsSize*sizeof(Anim));
    char curAnimName[16];
    memset((void*)curAnimName, 0, sizeof(curAnimName));
    size_t animIndex = 0;
    size_t c = 0;
    mAnimations[0].startFrame = 0;
    mAnimations[0].endFrame = 0;
    while (!(mKeyFrames[0].name[c] >= '0' && mKeyFrames[0].name[c] <= '9'))
    {
        mAnimations[0].name[c] = mKeyFrames[0].name[c];
        curAnimName[c] = mKeyFrames[0].name[c];
        ++c;
    }
    mAnimations[0].name[c] = '\0';
    curAnimName[c] = '\0';
    c = 0;
    for (size_t i = 1; i < mKeyFramesSize; ++i)
    {
        char frameAnimName[16];
        while (!(mKeyFrames[i].name[c] >= '0' && mKeyFrames[i].name[c] <= '9'))
        {
            frameAnimName[c] = mKeyFrames[i].name[c];
            ++c;
        }
        frameAnimName[c] = '\0';
        c = 0;
        if (strcmp(curAnimName, frameAnimName) != 0) {
            strcpy(curAnimName, frameAnimName);
            mAnimations[animIndex].endFrame = i - 1;
            animIndex++;
            mAnimations[animIndex].startFrame = i;
            strcpy(mAnimations[animIndex].name, curAnimName);
        }
    }
    mAnimations[mAnimationsSize-1].endFrame = mKeyFramesSize - 1;

    for (size_t i = 0; i < mAnimationsSize; ++i)
    {
        dbgprint("Anim name, start, stop: %s, %ld, %ld\n",
            mAnimations[i].name, 
            mAnimations[i].startFrame, mAnimations[i].endFrame
        );
    }
    
    SetAnim(mAnimations[0].name);
}

} // namespace Psp
} // namespace GameLib

