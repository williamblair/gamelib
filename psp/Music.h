#ifndef GAMELIB_PSP_MUSIC_H_INCLUDED
#define GAMELIB_PSP_MUSIC_H_INCLUDED

#include <pspctrl.h>
#include <pspdebug.h>
#include <pspdisplay.h>
#include <psptypes.h>
#include <pspmoduleinfo.h>
#include <pspthreadman.h>
#include <pspkernel.h>
#include <pspgu.h>
#include <pspgum.h>
#include <pspaudiolib.h>
#include <pspaudio.h>
#include <psputility.h>

namespace GameLib
{
namespace Psp
{

class Music
{

friend class SoundEffect;

public:

    struct WAVHeader {
        char riff[4]; // should equal "RIFF"
        int32_t fileSize;
        char wave[4]; // should equal "WAVE"
        char fmt[4]; // should have trailing null, "fmt "
        int32_t fmtLen;
        int16_t fmtType; // 1 == PCM
        int16_t numChannels;
        int32_t sampleRate; // typically 44100
        int32_t dataRateBytes; // (sample rate * bitsPerSample * channels) / 8
        int16_t sampleSizeBytes; // (bitsPerSample * channels) / 8
        int16_t bitsPerSample;
        char data[4]; // "data" chunk header
        int32_t dataSize; // size of the data section after this header
    };

    struct WAVStreamData {
        int wavFd;
        int fileSizeBytes; // size of the data portion of the WAV file
        int fileBytesRemain; // remaining bytes of the file data portion
        unsigned char* bufPtr; // pointer into below buffer
        unsigned int bufSizeRemain; // end of buffer - bufPtr
        unsigned char buffer[2048*4]; // save 2048 samples at once
        bool looping; // wether or not to loop back to beginning when reach end of file
        bool ended; // true if reached the end of the file and not looping
    };

    Music();
    ~Music();

    // Must be a .wav file, 16bit stereo, 44100 sample rate
    bool Init(const char* fileName);

    void Play(const bool loop);
    void Stop();

private:
    bool mLooping;
    bool mPaused;
    static bool sAudioInitted;
    WAVHeader mWavHdr;
    WAVStreamData mWavStream;

    static void audioCallback(void* buf, unsigned int length, void* userdata);
};

} // namespace Psp
} // namespace GameLib

#endif // GAMELIB_PSP_MUSIC_H_INCLUDED

