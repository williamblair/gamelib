#ifndef GAMELIB_PSP_TEXTURE_H_INCLUDED
#define GAMELIB_PSP_TEXTURE_H_INCLUDED

#include <pspkernel.h>
#include <psptypes.h>
#include <bjdebug.h>

namespace GameLib
{
namespace Psp
{

class Texture
{
friend class Renderer;
friend class Sprite;

public:
    
    Texture();
    ~Texture();
    
    bool Init(const char* data, int width, int height, int bytesPerPixel);
    
    bool LoadFromTGA(const char* fileName);
    bool LoadFromBMP(const char* fileName);

    int GetWidth() const { return mWidth; }
    int GetHeight() const { return mHeight; }

    /**
     * @brief set new texture data. must be the same size as the original data.
     * @param data the new pixel data to use for this texture
     * @param width the width of the pixel data (must be same as original)
     * @param height the height of the pixel data (must be same as original)
     * @param bytesPerPixel bytes per pixel of the data (must be same as original)
     * @return true on success, false on failure
     */
    bool Update(const char* data, int width, int height, int bytesPerPixel);

private:
    int mWidth;
    int mHeight;
    int mBytesPerPixel;
    uint8_t* mData; // pixel data
};

} // namespace Psp
} // namespace GameLib

#endif // GAMELIB_PSP_TEXTURE_H_INCLUDED

