#include <vector>
#include <cstdio>

#include <TileFloor.h>

namespace GameLib
{
namespace Psp
{

#define TILESIZE 1.0f

TileFloor::TileFloor() :
    mPosition(0.0f, 0.0f, 0.0f),
    mScale(1.0f, 1.0f, 1.0f),
    mTexture(nullptr)
{
}

TileFloor::~TileFloor()
{
}

bool TileFloor::Init()
{
    mPosition = GameMath::Vec3(0.0f, 0.0f, 0.0f);
    mScale = GameMath::Vec3(1.0f, 1.0f, 1.0f);
    mTexture = nullptr;
    std::vector<float> vertices;
    float x = 0.0f;
    for (int xCtr = 0; xCtr < 8; ++xCtr)
    {
        float z = 0.0f;
        for (int zCtr = 0; zCtr < 8; ++zCtr)
        {
            vertices.push_back(x); // bottom left
            vertices.push_back(0.0f);
            vertices.push_back(z + TILESIZE);
            vertices.push_back(0.0f);
            vertices.push_back(0.0f);

            vertices.push_back(x + TILESIZE); // bottom right
            vertices.push_back(0.0f);
            vertices.push_back(z + TILESIZE);
            vertices.push_back(1.0f);
            vertices.push_back(0.0f);
            
            vertices.push_back(x + TILESIZE); // top right
            vertices.push_back(0.0f);
            vertices.push_back(z);
            vertices.push_back(1.0f);
            vertices.push_back(1.0f);

            vertices.push_back(x); // bottom left
            vertices.push_back(0.0f);
            vertices.push_back(z + TILESIZE);
            vertices.push_back(0.0f);
            vertices.push_back(0.0f);

            vertices.push_back(x + TILESIZE); // top right
            vertices.push_back(0.0f);
            vertices.push_back(z);
            vertices.push_back(1.0f);
            vertices.push_back(1.0f);

            vertices.push_back(x); // top left
            vertices.push_back(0.0f);
            vertices.push_back(z);
            vertices.push_back(0.0f);
            vertices.push_back(1.0f);

            z += TILESIZE;
        }
        x += TILESIZE;
    }

    return mVertBuf.Init(
        VertexBuffer::POS_TEXCOORD, // type
        vertices.data(), // vertices
        vertices.size(), // vertices size
        nullptr, // indices
        0 // indicesSize
    );
}

bool TileFloor::Draw(
    GameMath::Mat4& viewMat,
    Renderer& render)
{
    GameMath::Mat4 modelMat;
    
    if (mTexture == nullptr) {
        dbgprint("TileFloor draw tex is null\n");
        return false;
    }
    
    render.SetTexture(*mTexture);

    modelMat.FromST(
        mScale.x, 1.0f, mScale.z,
        mPosition.x, mPosition.y, mPosition.z
    );

    render.DrawVertexBuffer(modelMat, viewMat, mVertBuf);

    return true;
}

} // namespace Psp
} // namespace GameLib

