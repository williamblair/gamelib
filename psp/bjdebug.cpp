#include <bjdebug.h>
#include <stdarg.h>
#include <pspkernel.h>
#include <stdio.h>
#include <string.h>

static int fd = -1;
static char buf[256];

void dbgprint(const char* fmtstr, ...)
{
    va_list valist;
    va_start(valist, fmtstr);
    vsprintf(buf, fmtstr, valist);
    va_end(valist);

    printf("%s", buf);
    
    if (fd < 0) {
        fd = sceIoOpen(
            "ms0:/bjdbg.txt",
            PSP_O_CREAT | PSP_O_TRUNC | PSP_O_WRONLY,
            0777
        );
    }
    
    sceIoWrite(fd, buf, strlen(buf));
}
