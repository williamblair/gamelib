#ifndef GAMELIB_PSP_GAME_TIMER_H_INCLUDED
#define GAMELIB_PSP_GAME_TIMER_H_INCLUDED

#include <sys/time.h>

namespace GameLib
{
namespace Psp
{

class GameTimer
{
public:

    GameTimer();
    ~GameTimer();
    
    // returns delta time in seconds
    float Update();

    // returns average frames per second and time per frame
    // over a 1 second interval
    float GetAvgFps() const { return mFps; }
    float GetAvgFrameTime() const { return mFrameTime; }

private:
    struct timeval mLastTime = {0,0};
    float mFps = 0.0f;
    float mFrameTime = 0.0f; // average time per frame
    //float mDtCtr = 0.0f; // delta time counter
    float mFrameCtr = 0.0f; // number of frames over the past second
};

} // namespace Psp
} // namespace GameLib

#endif // GAMELIB_PSP_GAME_TIMER_H_INCLUDED

