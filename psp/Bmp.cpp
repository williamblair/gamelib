#include <Bmp.h>
#include <pspkernel.h>
#include <psptypes.h>
#include <bjdebug.h>

namespace GameLib
{
namespace Psp
{
namespace BMP
{

bool Load(const char* fileName, std::vector<uint8_t>& buffer, int& width, int& height)
{
    uint32_t bytesRead;
    uint32_t paddedWidth;
    uint8_t* bmpPxData = nullptr;
    Header hdr;
    BITMAPINFOHEADER infoHdr;

    int srcFd = sceIoOpen(fileName, PSP_O_RDONLY, 0777);
    if (srcFd < 0) {
        dbgprint("Failed to open BMP File: %s\n", srcFd);
        return false;
    }

    // Read in the BMP header
    if (sceIoRead(srcFd, (char*)&hdr, sizeof(hdr)) != sizeof(hdr)) {
        dbgprint("Failed to read BMP header\n");
        return false;
    }
    if (((char*)&hdr.header)[0] != 'B' ||
        ((char*)&hdr.header)[1] != 'M')
    {
        dbgprint("File is not a windows BMP file: %c, %c\n",
           ((char*)&hdr.header)[0],
           ((char*)&hdr.header)[1]
        );
        return false;
    }
    if (sceIoRead(srcFd, (char*)&infoHdr, sizeof(infoHdr)) != sizeof(infoHdr)) {
        dbgprint("Failed to read BMP info header\n");
        return false;
    }
    if (infoHdr.size != 40) {
        dbgprint("Unexpected infoHdr size: %u\n", infoHdr.size);
        return false;
    }
    if (infoHdr.planes != 1) {
        dbgprint("Unexpected num planes: %d\n", (int)infoHdr.planes);
        return false;
    }
    if (infoHdr.bitCount != 24) {
        dbgprint("Unhandled bits per pixel: %d\n", (int)infoHdr.bitCount);
        return false;
    }
    // 0 == BI_RGB == no compression
    if (infoHdr.compression != 0) {
        dbgprint("Unhandled BMP compression: %d\n", (int)infoHdr.compression);
        return false;
    }
    dbgprint("infoHdr size: %u\n", (unsigned int)infoHdr.size);
    dbgprint("infoHdr width, height: %d, %d\n", (int)infoHdr.width, (int)infoHdr.height);

    width = infoHdr.width;
    height = infoHdr.height;

    bmpPxData = new uint8_t[infoHdr.sizeImage];
    if (!bmpPxData) {
        dbgprint("Failed to alloc bmp px buffer\n");
        return false;
    }

    sceIoLseek32(srcFd, hdr.pixOffset, PSP_SEEK_SET);
    if (sceIoRead(srcFd, bmpPxData, infoHdr.sizeImage) != infoHdr.sizeImage) {
        dbgprint("Failed to read bmp px buffer data\n");
        return false;
    }
    sceIoClose(srcFd);

    // assumes 24 bits per pixel
    int unpaddedRowWidthBytes = width * 3;
    int paddedRowWidthBytes = unpaddedRowWidthBytes;
    if (paddedRowWidthBytes % 4 != 0) {
        paddedRowWidthBytes += 4 - (paddedRowWidthBytes % 4);
    }
    // force 4 bytes per pixel
    buffer.resize(width * height * 4);
    uint8_t* outPx = buffer.data();
    for (int row = 0; row < height; ++row)
    {
        uint8_t* inPx = &bmpPxData[row * paddedRowWidthBytes];
        for (int col = 0; col < width; ++col)
        {
            uint8_t r = *inPx++;
            uint8_t g = *inPx++;
            uint8_t b = *inPx++;

            *outPx++ = b;
            *outPx++ = g;
            *outPx++ = r;
            *outPx++ = 255;
        }
    }

    delete[] bmpPxData;

    return true;
}

} // namespace BMP
} // namespace Psp
} // namespace GameLib

