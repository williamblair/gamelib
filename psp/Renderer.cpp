#include <Renderer.h>

#include <pspctrl.h>
#include <pspdebug.h>
#include <pspdisplay.h>
#include <psptypes.h>
#include <pspmoduleinfo.h>
#include <pspthreadman.h>
#include <pspkernel.h>
#include <pspgu.h>
#include <pspgum.h>

#include <malloc.h>
#include <stdio.h>

namespace GameLib
{
namespace Psp
{

int exitCallback(int arg1, int arg2, void* common) {
    sceKernelExitGame();
    return 0;
}

int callbackThread(SceSize args, void* argp) {
    int cbid;
    cbid = sceKernelCreateCallback("Exit Callback", exitCallback, NULL);
    sceKernelRegisterExitCallback(cbid);
    sceKernelSleepThreadCB();
    return 0;
}

int setupCallbacks() {
    int thid = 0;
    thid = sceKernelCreateThread(
        "update_thread",
        callbackThread,
        0x11, 0xFA0, 0, 0
    );
    if (thid >= 0) {
        sceKernelStartThread(thid, 0, 0);
    }
    return thid;
}

VertexBuffer Renderer::sAabbVertBuf;
VertexBuffer::PosTexVertex* Renderer::sSpriteVertBufs = nullptr;

Renderer::Renderer() :
    mWidth(480),
    mHeight(272),
    mBgColor(0xFF808000),
    mCurTex(nullptr),
    mCurLoadedTex(nullptr),
    mCurSpriteVertBuf(0)
{}

Renderer::~Renderer()
{}

// Used to store gpu command list
static unsigned int __attribute__((aligned(16))) list[262144];
bool Renderer::Init(int width, int height, bool fullScreen, const char* title)
{
    // unused
    (void)width;
    (void)height;
    (void)fullScreen;
    (void)title;
    
    mWidth = 480;
    mHeight = 272;
    mCurSpriteVertBuf = 0;
    
    // Setup return-to-homescreen callbacks to exit
    setupCallbacks();

    // to support vfpu usage in GameMath
    gumInit();
    pspvfpu_use_matrices(gum_vfpucontext, 0, VMAT0 | VMAT1 | VMAT2);
    
    // Initialize the GraphicalSystem
    sceGuInit();
    sceGuStart(GU_DIRECT, list);
    sceGuDrawBuffer(GU_PSM_8888, (void*)0, 512);
    sceGuDispBuffer(480, 272, (void*)0x88000, 512);
    sceGuDepthBuffer((void*)0x110000, 512);
    sceGuOffset(2048 - (480/2), 2048 - (272/2));

    // create a viewport centered at 2048,2048 width 480 and height 272
    sceGuViewport(2048, 2048, 480, 272);
    sceGuDepthRange(0xc350, 0x2710);

    sceGuScissor(0, 0, 480, 272);
    sceGuEnable(GU_SCISSOR_TEST);
    sceGuDepthFunc(GU_GEQUAL);
    //sceGuDepthFunc(GU_LEQUAL);
    sceGuEnable(GU_DEPTH_TEST);
    //sceGuFrontFace(GU_CW);
    sceGuFrontFace(GU_CCW);
    sceGuShadeModel(GU_SMOOTH);
    //sceGuEnable(GU_CULL_FACE);
    //sceGuEnable(GU_TEXTURE_2D); // needs to be not set if drawing without textures
    sceGuEnable(GU_CLIP_PLANES);
    sceGuEnable(GU_LIGHTING);
    sceGuEnable(GU_BLEND);
    sceGuBlendFunc(GU_ADD, GU_SRC_ALPHA, GU_ONE_MINUS_SRC_ALPHA, 0, 0);
    //sceGuTexWrap(GU_REPEAT, GU_REPEAT);
    //sceGuTexScale(1.0f, 1.0f);
    //sceGuTexOffset(0.0f, 0.0f);
    //sceGuTexFilter(GU_LINEAR, GU_LINEAR);
    sceGuFinish();
    // wait untill the list has finished.
    sceGuSync(0,0);
    // turn on the display
    sceGuDisplay(GU_TRUE);
    
    // Initialize projection matrix
    //mProjMat = GameMath::Perspective(
    //    GameMath::Deg2Rad(60.0f),
    //    (float)mWidth/(float)mHeight,
    //    1.0f, 500.0f
    //);
    mProjMat = GameMath::Perspective(
        GameMath::Deg2Rad(60.0f),
        (float)16.0f/(float)9.0f,
        1.0f/*2.0f*/, 1000.0f
    );
    
    // set projection matrix
    sceGumMatrixMode(GU_PROJECTION);
    sceGumLoadIdentity();
    //sceGumPerspective(45.0f, 16.0f/9.0f, 2.0f, 1000.0f);
    sceGumLoadMatrix((const ScePspFMatrix4*)&mProjMat.r0c0);
    mCurProjMat = ProjMatType::PERSPECTIVE;

    if (sAabbVertBuf.mNumVertices == 0) {
        if (!InitAabbVertBuf()) {
            dbgprint("Renderer failed to init aabb vertbuf\n");
            return false;
        }
    }

    if (sSpriteVertBufs == nullptr) {
        if (!InitSpriteVertBufs()) {
            dbgprint("Renderer failed to init sprite vertbuf\n");
            return false;
        }
    }

    mCurTex = nullptr;
    mCurLoadedTex = nullptr;
    
    return true;
}

void Renderer::SetBGColor(uint8_t r, uint8_t g, uint8_t b)
{
    mBgColor =
        0xFF000000 |
        (((uint32_t)b) << 16) |
        (((uint32_t)g) << 8) |
        (((uint32_t)r) << 0);
}

void Renderer::Clear()
{
    mCurSpriteVertBuf = 0;

    sceGuStart(GU_DIRECT, list);
    sceGuClearColor(mBgColor); // abgr
    sceGuClearDepth(0);
    sceGuClear(GU_COLOR_BUFFER_BIT | GU_DEPTH_BUFFER_BIT);
    
    // set ambient light
    sceGuAmbient(0xFFFFFFFF);

    pspvfpu_use_matrices(gum_vfpucontext, 0, VMAT0 | VMAT1 | VMAT2);
}

void Renderer::Update()
{
    sceGuFinish();
    sceGuSync(0, 0);
    sceDisplayWaitVblankStart();
    sceGuSwapBuffers();
}

void Renderer::DrawVertexBuffer(
    GameMath::Mat4& modelMat,
    GameMath::Mat4& viewMat, 
    const VertexBuffer& vb)
{
    ScePspFMatrix4 projection; // required although unused
    ScePspFMatrix4 view;
    ScePspFMatrix4 world;

    // set projection matrix
    //sceGumMatrixMode(GU_PROJECTION);
    //sceGumLoadIdentity();
    //sceGumLoadMatrix((const ScePspFMatrix4*)&mProjMat.r0c0);
    if (mCurProjMat != ProjMatType::PERSPECTIVE)
    {
        sceGumMatrixMode(GU_PROJECTION);
        sceGumLoadMatrix((const ScePspFMatrix4*)&mProjMat.r0c0);
        mCurProjMat = ProjMatType::PERSPECTIVE;
    }
    
    // set view matrix
    sceGumMatrixMode(GU_VIEW);
    sceGumLoadMatrix((const ScePspFMatrix4*)&viewMat.r0c0);
    
    // set model matrix
    sceGumMatrixMode(GU_MODEL);
    sceGumLoadMatrix((const ScePspFMatrix4*)&modelMat.r0c0);
    
    switch (vb.mType)
    {
    case VertexBuffer::POS_COLOR:
        sceGuDisable(GU_TEXTURE_2D); // needs to be not set if drawing without textures
        if (vb.mIndicesSize > 0) {
            sceGumDrawArray(
                GU_TRIANGLES,
                GU_INDEX_16BIT | GU_COLOR_8888 | GU_VERTEX_32BITF | GU_TRANSFORM_3D,
                vb.mIndicesSize,
                vb.mIndices,
                vb.mPosColorVertices
            );
        } else {
            sceGumDrawArray(
                GU_TRIANGLES,
                GU_COLOR_8888 | GU_VERTEX_32BITF | GU_TRANSFORM_3D,
                vb.mNumVertices,
                0,
                vb.mPosColorVertices
            );
        }
        break;
    case VertexBuffer::POS_TEXCOORD:
        // only draw if we have a texture
        if (mCurTex != nullptr) {
            if (mCurLoadedTex != mCurTex) {
                sceGuEnable(GU_TEXTURE_2D); // needs to be not set if drawing without textures
                sceGuTexMode(
                    GU_PSM_8888, // texture format - truecolor 32bit
                    0, // maxmips - number of mipmaps to use (0-8)
                    0, // a2 - unknown, set to 0
                    0 // swizzle - set to GU_TRUE to swizzle texture reads
                );
                sceGuTexImage(
                    0, // mipmap level
                    mCurTex->mWidth, // texture width (must be power of 2)
                    mCurTex->mHeight, // texture height (must be power of 2)
                    mCurTex->mWidth, // texture buffer width (block aligned)
                    mCurTex->mData // texture buffer pointer (16 byte aligned)
                );
                sceGuTexFunc(GU_TFX_MODULATE, GU_TCC_RGBA);
                sceGuTexFilter(GU_NEAREST, GU_NEAREST);
                sceGuTexWrap(GU_CLAMP, GU_CLAMP);
                sceGuTexScale(1, 1);
                sceGuTexOffset(0, 0);
                sceGuAmbientColor(0xFFFFFFFF);

                mCurLoadedTex = mCurTex;
            }

            if (vb.mIndicesSize > 0) {
                sceGumDrawArray(
                    GU_TRIANGLES,
                    GU_INDEX_16BIT | GU_TEXTURE_32BITF | GU_COLOR_8888 | GU_VERTEX_32BITF | GU_TRANSFORM_3D,
                    vb.mIndicesSize,
                    vb.mIndices,
                    vb.mPosTexVertices
                );
            } else {
                sceGumDrawArray(
                    GU_TRIANGLES,
                    GU_TEXTURE_32BITF | GU_COLOR_8888 | GU_VERTEX_32BITF | GU_TRANSFORM_3D,
                    vb.mNumVertices,
                    0,
                    vb.mPosTexVertices
                );
            }
        } else {
            dbgprint("DrawVertexBuffer mCurTex == nullptr\n");
        }
        break;
    default:
        dbgprint("Renderer unhandled drawvertexbuffer type\n");
        return;
    }
}

void Renderer::DrawAabb(AABB& aabb, GameMath::Mat4& viewMat)
{
    ScePspFMatrix4 projection; // required although unused
    ScePspFMatrix4 view;
    ScePspFMatrix4 world;
    GameMath::Mat4 modelMat;
    GameMath::Vec3 scl(aabb.max - aabb.min);

    modelMat.FromST(scl, aabb.pos);

    // set projection matrix
    if (mCurProjMat != ProjMatType::PERSPECTIVE)
    {
        sceGumMatrixMode(GU_PROJECTION);
        sceGumLoadMatrix((const ScePspFMatrix4*)&mProjMat.r0c0);
        mCurProjMat = ProjMatType::PERSPECTIVE;
    }
    
    // set view matrix
    sceGumMatrixMode(GU_VIEW);
    sceGumLoadMatrix((const ScePspFMatrix4*)&viewMat.r0c0);
    
    // set model matrix
    sceGumMatrixMode(GU_MODEL);
    sceGumLoadMatrix((const ScePspFMatrix4*)&modelMat.r0c0);

    VertexBuffer& vb = sAabbVertBuf;
    
    sceGuDisable(GU_TEXTURE_2D); // needs to be not set if drawing without textures
    sceGumDrawArray(
        GU_LINES,
        GU_INDEX_16BIT | GU_COLOR_8888 | GU_VERTEX_32BITF | GU_TRANSFORM_3D,
        vb.mIndicesSize,
        vb.mIndices,
        vb.mPosColorVertices
    );
}

void Renderer::DrawSprite(Sprite& sprite)
{
    ScePspFMatrix4 projection; // required although unused
    ScePspFMatrix4 view;
    ScePspFMatrix4 world;
    GameMath::Mat4 mvpMat;

    // Get the current vertices buffer for 2d sprite drawing
    VertexBuffer::PosTexVertex* ptv = &sSpriteVertBufs[mCurSpriteVertBuf];
    mCurSpriteVertBuf += 6; // 2 triangles
    if (mCurSpriteVertBuf >= SPRITE_NVERTICES) {
        dbgprint("ERROR - mCurSpriteVertBuf exceeded max for frame\n");
        return;
    }

    // updated tex coords
    float leftU = sprite.mU;
    float rightU = leftU + sprite.mW;
    float topV = 1.0f - sprite.mV;
    float bottomV = 1.0f - (sprite.mV + sprite.mH);

    ptv[0].u = leftU; ptv[0].v = bottomV; // bottom left
    ptv[1].u = rightU; ptv[1].v = bottomV; // bottom right
    ptv[2].u = rightU; ptv[2].v = topV; // top right

    ptv[3].u = leftU; ptv[3].v = bottomV; // bottom left
    ptv[4].u = rightU; ptv[4].v = topV; // top right
    ptv[5].u = leftU; ptv[5].v = topV; // top left

    // sets mCurTex = sprite.mTexture
    SetTexture(*sprite.mTexture);
    
    // scale size from 0..1 to -1..1
    float normW = sprite.mWidth * 2.0f;
    float normH = sprite.mHeight * 2.0f;
    //mvpMat.FromST(
    //    normW, normH, 1.0f,
    //    sprite.mX, sprite.mY, 0.0f
    //);
    mvpMat = 
        GameMath::Translate(sprite.mX, sprite.mY, 0.0f) *
        
        // need to offset by w/2, h/2 because we draw relative to top left corner,
        // as opposed to the center of the image
        GameMath::Translate(normW/2,-normH/2,0.0f) *
        GameMath::Rotate(GameMath::Deg2Rad(sprite.mRotDegrees),
            GameMath::Vec3(0.0f, 0.0f, 1.0f)) *
        GameMath::Translate(-normW/2,normH/2,0.0f) *
        
        GameMath::Scale(normW, normH, 1.0f);


    // set projection matrix = identity
    if (mCurProjMat != ProjMatType::IDENTITIY)
    {
        sceGumMatrixMode(GU_PROJECTION);
        sceGumLoadIdentity();
        mCurProjMat = ProjMatType::IDENTITIY;
    }
    
    // set view matrix = identity
    sceGumMatrixMode(GU_VIEW);
    sceGumLoadIdentity();
    
    // set model matrix = 2d translation/scale
    sceGumMatrixMode(GU_MODEL);
    sceGumLoadMatrix((const ScePspFMatrix4*)&mvpMat.r0c0);

    sceGuDisable(GU_DEPTH_TEST);
        if (mCurLoadedTex != mCurTex) {
            sceGuEnable(GU_TEXTURE_2D); // needs to be not set if drawing without textures
            sceGuTexMode(
                GU_PSM_8888, // texture format - truecolor 32bit
                0, // maxmips - number of mipmaps to use (0-8)
                0, // a2 - unknown, set to 0
                0 // swizzle - set to GU_TRUE to swizzle texture reads
            );
            sceGuTexImage(
                0, // mipmap level
                mCurTex->mWidth, // texture width (must be power of 2)
                mCurTex->mHeight, // texture height (must be power of 2)
                mCurTex->mWidth, // texture buffer width (block aligned)
                mCurTex->mData // texture buffer pointer (16 byte aligned)
            );
            sceGuTexFunc(GU_TFX_MODULATE, GU_TCC_RGBA);
            sceGuTexFilter(GU_NEAREST, GU_NEAREST);
            sceGuTexWrap(GU_CLAMP, GU_CLAMP);
            sceGuTexScale(1, 1);
            sceGuTexOffset(0, 0);
            sceGuAmbientColor(0xFFFFFFFF);
            
            mCurLoadedTex = mCurTex;
        }

        sceGumDrawArray(
            GU_TRIANGLES,
            GU_TEXTURE_32BITF | GU_COLOR_8888 | GU_VERTEX_32BITF | GU_TRANSFORM_3D,
            6, // 2 triangles
            0,
            ptv
        );
    sceGuEnable(GU_DEPTH_TEST);
}

bool Renderer::InitAabbVertBuf()
{
    float vertices[6*8] = {
        // pos                  color
        -0.5f,  0.0f, -0.5f,    0.0f, 0.0f, 1.0f, // 0 bottom back left
        -0.5f,  0.0f,  0.5f,    0.0f, 0.0f, 1.0f, // 1 bottom front left
         0.5f,  0.0f, -0.5f,    0.0f, 0.0f, 1.0f, // 2 bottom back right
         0.5f,  0.0f,  0.5f,    0.0f, 0.0f, 1.0f, // 3 bottom front right
        -0.5f,  1.0f, -0.5f,    0.0f, 0.0f, 1.0f, // 4 top back left
        -0.5f,  1.0f,  0.5f,    0.0f, 0.0f, 1.0f, // 5 top front left
         0.5f,  1.0f, -0.5f,    0.0f, 0.0f, 1.0f, // 6 top back right
         0.5f,  1.0f,  0.5f,    0.0f, 0.0f, 1.0f, // 7 top front right
    };
    int indices[2*12] = {
        0, 1,
        0, 2,
        0, 4,
        1, 3,
        2, 3,
        2, 6,
        3, 7,
        1, 5,
        4, 5,
        5, 7,
        4, 6,
        6, 7
    };
    return sAabbVertBuf.Init(
        VertexBuffer::POS_COLOR,
        vertices,
        6*8,
        indices,
        2*12
    );
}

bool Renderer::InitSpriteVertBufs()
{
    sSpriteVertBufs = (VertexBuffer::PosTexVertex*)memalign(
        16,
        SPRITE_NVERTICES*sizeof(VertexBuffer::PosTexVertex)
    );
    if (!sSpriteVertBufs) {
        dbgprint("Failed to alloc sprite vert bufs\n");
        return false;
    }

    float vertices[5*6] = {
        // position         texcoord
        0.0f, -1.0f, 0.0f,   0.0f, 0.0f, // bottom left
        1.0f, -1.0f, 0.0f,   1.0f, 0.0f, // bottom right
        1.0f, 0.0f, 0.0f,   1.0f, 1.0f, // top right

        0.0f, -1.0f, 0.0f,   0.0f, 0.0f, // bottom left
        1.0f, 0.0f, 0.0f,   1.0f, 1.0f, // top right
        0.0f, 0.0f, 0.0f,   0.0f, 1.0f // top left
    };

    int i = 0;
    while (i<SPRITE_NVERTICES) {
        for (int j=0; j<6; ++j, ++i) {
            VertexBuffer::PosTexVertex* ptv = &sSpriteVertBufs[i];
            float* v = &vertices[j*5]; // 5 floats per input vertex
            ptv->u = v[3];
            ptv->v = v[4];
            ptv->r = 255;
            ptv->g = 255;
            ptv->b = 255;
            ptv->a = 255;
            ptv->x = v[0];
            ptv->y = v[1];
            ptv->z = v[2];
        }
    }

    return true;
}
    
} // namespace Psp
} // namespace GameLib

