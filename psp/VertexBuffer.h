#ifndef GAMELIB_PSP_VERTEX_BUFFER_H_INCLUDED
#define GAMELIB_PSP_VERTEX_BUFFER_H_INCLUDED

#include <malloc.h>
#include <stdint.h>
#include <bjdebug.h>

namespace GameLib
{
namespace Psp
{
    
class VertexBuffer
{
friend class Renderer;

public:
    
    struct PosColorVertex {
        unsigned char r, g, b, a;
        float x, y, z;
    };
    struct PosTexVertex {
        float u, v;
        unsigned char r, g, b, a;
        float x, y, z;
    };

    enum Type
    {
        POS_COLOR,
        POS_TEXCOORD,
        UNINITIALIZED
    };

    VertexBuffer();
    ~VertexBuffer();
    
    bool Init(
        Type type,
        float* vertices, size_t verticesSize,
        int* indices, size_t indicesSize
    );
    
    bool UpdateVertices(float* vertices, size_t verticesSize);

    PosTexVertex* GetPosTexVertices() const { return mPosTexVertices; }

private:
    
    
    Type mType;
    size_t mFloatsPerVertex;
    //int* mIndices;
    unsigned short* mIndices;
    size_t mIndicesSize;
    size_t mVerticesSize; // number of floats
    size_t mNumVertices; // number of elements in vertices array below
    PosColorVertex* mPosColorVertices;
    PosTexVertex* mPosTexVertices;
};
    
} // namespace Psp
} // namespace GameLib

#endif // GAMELIB_PSP_VERTEX_BUFFER_H_INCLUDED
