#include <Thread.h>
#include <bjdebug.h>

namespace GameLib
{
namespace Psp
{

std::unordered_map<int,u64> Thread::sRetVals;

int Thread::sThreadFcnWrapper(SceSize args, void* arg)
{
    Thread* thr = (Thread*)arg;
    thr->mFcn(thr->mArg);
    return 0;
}

Thread::Thread() :
    mArg(nullptr),
    mStarted(false),
    mJoined(false)
{}

Thread::~Thread()
{
    if (!mJoined && mStarted) {
        Join(nullptr);
    }
}

bool Thread::Start(ThreadFcn fcn, void* arg, const char* name)
{
    mFcn = fcn;
    mArg = arg;
    mThreadId = sceKernelCreateThread(name, sThreadFcnWrapper, 0x18, 0x1000, THREAD_ATTR_USER, 0);
    if (mThreadId < 0) {
        dbgprint("Failed to create thread\n");
        return false;
    }
    dbgprint("sceKernelCreateThread id: %d\n", mThreadId);
    sceKernelStartThread(mThreadId, sizeof(Thread), (void*)this);
    mStarted = true;
    return true;
}

void Thread::Join(u64* retVal)
{
    sceKernelWaitThreadEnd(mThreadId, nullptr);
    sceKernelDeleteThread(mThreadId);
    if (retVal != nullptr &&
        (sRetVals.find(mThreadId) != sRetVals.end())) {
        *retVal = sRetVals[mThreadId];
        sRetVals.erase(mThreadId);
    }
    mThreadId = -1;
    mJoined = true;
}

u64 Thread::GetThreadId()
{
    return (u64)sceKernelGetThreadId();
}

void Thread::Yield()
{
    // delays by 5 microseconds
    //sceKernelDelayThread(5);
    // TODO
}

void Thread::Exit(u64 retVal)
{
    int thid = sceKernelGetThreadId();
    dbgprint("Exit thread id: %d\n", thid);
    // TODO
    // This causes an invalid memory access...
    //sRetVals[thid] = retVal;
}

} // namespace Psp
} // namespace GameLib

