#include <TextureFont.h>
#include <Renderer.h>
#include <string.h>
#include <stdio.h>

namespace GameLib
{
namespace Psp
{

TextureFont::TextureFont() :
    mTexture(nullptr),
    mCharWidth(0),
    mCharHeight(0),
    mCharsPerRow(0)
{}

TextureFont::~TextureFont()
{}

bool TextureFont::Init(Texture* tex, float charWidth, float charHeight)
{
    mTexture = tex;
    mCharWidth = charWidth;
    mCharHeight = charHeight;

    // allow for imperfect sizes
    /*if (tex->GetWidth() % charWidth != 0) {
        printf("ERROR - texture width not divisible by char width\n");
        return false;
    }
    if (tex->GetHeight() % charHeight != 0) {
        printf("ERROR - texture height not divisible by char height\n");
        return false;
    }*/

    mCharsPerRow = tex->GetWidth() / charWidth;

    return mSprite.Init(mTexture);
}

void TextureFont::Draw(const char* msg, float x, float y, Renderer& render)
{
    // clip UVs 0..1
    const float uWidthNormalized = (float)mCharWidth / (float)mTexture->GetWidth();
    const float vHeightNormalized = (float)mCharHeight / (float)mTexture->GetHeight();

    // width height % of screen 0..1
    const float scrWidthNormalized = (float)mCharWidth / (float)render.GetWidth();
    const float scrHeightNormalized = (float)mCharHeight / (float)render.GetHeight();
    mSprite.SetWH(
        scrWidthNormalized,
        scrHeightNormalized
    );

    const int msgLen = strlen(msg);
    for (int i=0; i<msgLen; ++i)
    {
        char letter = msg[i];
        // convert to uppercase if necessary
        if (letter >= 'a' && letter <= 'z') {
            letter += 'A' - 'a';
        }
        // Skip if no need to draw anything
        if (letter == ' ') {
            x += scrWidthNormalized * 2.0f;
            continue;
        }

        // TODO - assumes starts at ! char; handle otherwise
        int glyphIndex = letter - '!';
        float v = glyphIndex / mCharsPerRow;
        float u = glyphIndex % mCharsPerRow;
        v *= vHeightNormalized;
        u *= uWidthNormalized;
        mSprite.SetUVWH(u, v, uWidthNormalized, vHeightNormalized);
        mSprite.SetXY(x, y);

        // scale from 0..1 to -1..1
        x += scrWidthNormalized * 2.0f;

        render.DrawSprite(mSprite);
    }
}

} // namespace Psp
} // namespace GameLib

