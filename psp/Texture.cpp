#include <Texture.h>
#include <Targa.h>
#include <Bmp.h>
#include <iostream>
#include <vector>
#include <cstring>
#include <malloc.h>

namespace GameLib
{
namespace Psp
{

Texture::Texture() :
    mWidth(0),
    mHeight(0),
    mBytesPerPixel(0),
    mData(nullptr)
{}

Texture::~Texture()
{
    if (mData) { free(mData); mData = nullptr; }
}

bool Texture::Init(const char* data, int width, int height, int bytesPerPixel)
{
    mWidth = width;
    mHeight = height;
    mBytesPerPixel = 4; // force 4 bytes per pixel
    mData = (uint8_t*)memalign(16, mWidth*mHeight*mBytesPerPixel);
    if (!mData) {
        dbgprint("Failed to alloc image data\n");
        return false;
    }
    for (int i=0; i<mWidth*mHeight*mBytesPerPixel; i+=mBytesPerPixel)
    {
        mData[i+0] = *data++; // r
        mData[i+1] = *data++; // g
        mData[i+2] = *data++; // b
        if (bytesPerPixel == 4) {
            mData[i+3] = *data++; // a
        } else {
            mData[i+3] = 255;
        }
    }

    return true;
}

bool Texture::LoadFromTGA(const char* fileName)
{
    int inFileFd = sceIoOpen(fileName, PSP_O_RDONLY, 0777);
    if (inFileFd < 0) {
        dbgprint("Failed to open image file: %s\n", fileName);
        return false;
    }

    sceIoRead(inFileFd, (char*)&Targa::header, sizeof(Targa::Header));
    if (!Targa::IsImageTypeSupported(Targa::header)) {
        dbgprint("unsupported targa type\n");
        return false;
    }

    Targa::width = Targa::header.width;
    Targa::height = Targa::header.height;

    Targa::bitsPerPixel = Targa::header.bpp;
    Targa::bytesPerPixel = Targa::header.bpp / 8;

    if (Targa::bytesPerPixel < 3) {
        dbgprint("Unsupported bytesPerPixel: %u\n", Targa::bytesPerPixel);
        return false;
    }

    unsigned int imageSize = Targa::width * Targa::height * Targa::bytesPerPixel;
    Targa::imageData.resize(imageSize);

    // skip past the id if there is one
    if (Targa::header.idLength > 0) {
        sceIoLseek32(inFileFd, Targa::header.idLength, PSP_SEEK_CUR);
    }

    bool result = false;

    if (Targa::IsUncompressed(Targa::header)) {
        result = Targa::LoadUncompressed(inFileFd);
    } else {
        result = Targa::LoadCompressed(inFileFd);
    }

    if ((Targa::header.imageDesc & Targa::TOP_LEFT) == Targa::TOP_LEFT) {
        Targa::FlipImageVertically();
    }

    if (!result) {
        dbgprint("Failed to load targa\n");
        return false;
    }
    
    result = Init(
        (const char*)Targa::imageData.data(),
        Targa::width,
        Targa::height,
        Targa::bytesPerPixel
    );
    Targa::imageData.clear(); // free vector memory
    Targa::width = 0;
    Targa::height = 0;
    Targa::bytesPerPixel = 0;

    return result;
}

bool Texture::LoadFromBMP(const char* fileName)
{
    std::vector<uint8_t> imageData;
    int width = -1;
    int height = -1;
    if (!BMP::Load(fileName, imageData, width, height)) {
        return false;
    }
    return Init((const char*)imageData.data(), width, height, 4);
}

bool Texture::Update(
    const char* data,
    int width, int height,
    int bytesPerPixel)
{
    if (width != mWidth ||
        height != mHeight ||
        bytesPerPixel != mBytesPerPixel)
    {
        dbgprint("Texture::Update params mismatch\n");
        return false;
    }
    memcpy((void*)mData, (void*)data, width*height*bytesPerPixel);
    return true;
}

} // namespace Psp
} // namespace GameLib

