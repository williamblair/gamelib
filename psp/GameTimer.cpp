#include <GameTimer.h>

namespace GameLib
{
namespace Psp
{

GameTimer::GameTimer()
{}

GameTimer::~GameTimer()
{}

static inline float diffSeconds(timeval* cur, timeval* prev)
{
    return (float)(cur->tv_sec - prev->tv_sec) +
        ((float)(cur->tv_usec - prev->tv_usec)) / 1000000.0f;
}

static float dtCtr = -1.0f;
float GameTimer::Update()
{
    struct timeval curTime;
    gettimeofday(&curTime, 0);
    
    
    float dt = diffSeconds(&curTime, &mLastTime);
    
    mLastTime.tv_sec = curTime.tv_sec;
    mLastTime.tv_usec = curTime.tv_usec;

    mFrameCtr += 1.0f;
    //mDtCtr += dt;
    if (dtCtr < 0.0f) dtCtr = 0.0f;
    else dtCtr += dt;
    if (dtCtr >= 1.0f)
    {
        mFps = mFrameCtr / dtCtr;
        mFrameTime = 1.0f / mFps;

        while (dtCtr >= 1.0f) { dtCtr -= 1.0f; }
        mFrameCtr = 0.0f;
    }
    
    return dt;
}

} // namespace Psp
} // namespace GameLib

