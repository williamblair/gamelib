#ifndef BJ_DEBUG_H_INCLUDED
#define BJ_DEBUG_H_INCLUDED

void dbgprint(const char* fmtstr, ...);

#endif // BJ_DEBUG_H_INCLUDED
