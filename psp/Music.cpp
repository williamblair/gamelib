#include <Music.h>
#include <bjdebug.h>
#include <stdio.h>
#include <string.h>

namespace GameLib
{
namespace Psp
{

// This function gets called by pspaudiolib every time the
// audio buffer needs to be filled. The sample format is
// 16-bit, stereo.
void Music::audioCallback(void* buf, unsigned int length, void* userdata)
{
    Music* mus = (Music*)userdata;
    WAVHeader& wavHdr = mus->mWavHdr;
    WAVStreamData& wavStream = mus->mWavStream;

    if (mus->mPaused || (wavStream.ended && !wavStream.looping)) {
        return;
    }

    // length = number of samples = 32bits/4bytes (left, right channel)
    int outBufBytes = length*4;
    int numBytesRemain = outBufBytes;
    unsigned char* outBuf = (unsigned char*)buf;
    while (numBytesRemain > 0) {

        // copy buffered samples into output
        int amountBytesCopy =
            (wavStream.bufSizeRemain >= numBytesRemain) ?
            numBytesRemain :
            wavStream.bufSizeRemain;
        // assumes WAV file data is expected format, 16bit stereo, with
        // matching sample rate
        memcpy(outBuf, wavStream.bufPtr, amountBytesCopy);

        outBuf += amountBytesCopy;
        numBytesRemain -= amountBytesCopy;
        wavStream.bufPtr += amountBytesCopy;
        wavStream.bufSizeRemain -= amountBytesCopy;

        // get more WAV file samples if buffer is empty
        if (wavStream.bufSizeRemain == 0) {

            wavStream.bufPtr = wavStream.buffer;
            wavStream.bufSizeRemain = sizeof(wavStream.buffer);

            int numFileReadRemain = sizeof(wavStream.buffer);
            unsigned char* fileReadPtr = wavStream.buffer;

            while (numFileReadRemain > 0) {

                int numToRead =
                    (wavStream.fileBytesRemain >= numFileReadRemain) ?
                    numFileReadRemain :
                    wavStream.fileBytesRemain;
                int numRead = sceIoRead(wavStream.wavFd, fileReadPtr, numToRead);
                if (numRead <= 0) {
                    if (wavStream.looping) {
                        dbgprint("resetting wav file to data past header\n");

                        // go back to beginning of WAV file past header
                        sceIoLseek32(wavStream.wavFd, sizeof(WAVHeader), PSP_SEEK_SET);
                        wavStream.fileBytesRemain = wavStream.fileSizeBytes;

                        // re-read
                        numRead = sceIoRead(wavStream.wavFd, fileReadPtr, numToRead);
                    } else {
                        wavStream.ended = true;
                        return;
                    }
                }

                fileReadPtr += numRead;
                numFileReadRemain -= numRead;
                wavStream.fileBytesRemain -= numRead;

                // If we reached the end of the file...
                if (wavStream.fileBytesRemain == 0) {
                    if (wavStream.looping) {
                        dbgprint("resetting wav file to data past header\n");
                        // go back to beginning of WAV file past header
                        sceIoLseek32(wavStream.wavFd, sizeof(WAVHeader), PSP_SEEK_SET);
                        wavStream.fileBytesRemain = wavStream.fileSizeBytes;
                    } else {
                        wavStream.ended = true;
                        return;
                    }
                }
            }
        }
    }
}

bool Music::sAudioInitted = false;

Music::Music() :
    mLooping(true),
    mPaused(false)
{}

Music::~Music()
{
}

bool Music::Init(const char* fileName)
{
    mLooping = true;
    mPaused = false;
    
    WAVHeader& hdr = mWavHdr;
    WAVStreamData& strm = mWavStream;

    // open the WAV file
    strm.wavFd = sceIoOpen(fileName, PSP_O_RDONLY, 0777);
    if (strm.wavFd < 0) {
        dbgprint("Failed to open %s\n", fileName);
        return false;
    }

    // read the WAV header
    int numRead = sceIoRead(strm.wavFd, (char*)&hdr, sizeof(WAVHeader));
    if (numRead != sizeof(WAVHeader)) {
        dbgprint("Failed to read WAV header\n");
        return false;
    }

    // Verify expected header contents
    dbgprint("hdr RIFF: %c,%c,%c,%c\n",
        hdr.riff[0],
        hdr.riff[1],
        hdr.riff[2],
        hdr.riff[3]);
    dbgprint("hdr fileSize: %ld\n", hdr.fileSize);
    dbgprint("hdr WAVE: %c,%c,%c,%c\n",
        hdr.wave[0],
        hdr.wave[1],
        hdr.wave[2],
        hdr.wave[3]);
    dbgprint("hdr fmt: %c,%c,%c,%c\n",
        hdr.fmt[0],
        hdr.fmt[1],
        hdr.fmt[2],
        hdr.fmt[3]);
    dbgprint("hdr fmtLen: %ld\n", hdr.fmtLen);
    dbgprint("hdr fmtType: %d\n", hdr.fmtType);
    dbgprint("hdr numChannels: %d\n", hdr.numChannels);
    dbgprint("hdr sampleRate: %ld\n", hdr.sampleRate);
    dbgprint("hdr dataRateBytes: %ld\n", hdr.dataRateBytes);
    dbgprint("hdr sampleSizeBytes: %d\n", hdr.sampleSizeBytes);
    dbgprint("hdr bitsPerSample: %d\n", hdr.bitsPerSample);
    dbgprint("hdr DATA: %c,%c,%c,%c\n",
        hdr.data[0],
        hdr.data[1],
        hdr.data[2],
        hdr.data[3]);
    dbgprint("hdr dataSize: %ld\n", hdr.dataSize);

    if (hdr.sampleRate != 44100) {
        dbgprint("Unexpected WAV sampleRate\n");
        return false;
    }
    if (hdr.sampleSizeBytes != 4) {
        dbgprint("Unexpected sampleSizeBytes\n");
        return false;
    }
    if (hdr.bitsPerSample != 16) {
        dbgprint("Unexpected bitsPerSample\n");
        return false;
    }

    // initialize streaming data
    strm.fileSizeBytes = hdr.dataSize; // bytes of the data section
    strm.fileBytesRemain = hdr.dataSize; // how many bytes left to be read from the file
    strm.bufPtr = strm.buffer; // beginning of the samples buffer
    strm.bufSizeRemain = sizeof(strm.buffer); // sample data left in buffer in bytes
    strm.looping = false;
    strm.ended = false;

    // read initial samples into the buffer
    numRead = sceIoRead(strm.wavFd, strm.bufPtr, sizeof(strm.buffer));
    if (numRead != sizeof(strm.buffer)) {
        dbgprint("Failed to read initial samples\n");
        return false;
    }

    // initialize audio if not done already
    if (!sAudioInitted) {
        pspAudioInit();
        sAudioInitted = true;
    }

    return true;
}

void Music::Play(const bool loop)
{
    mWavStream.looping = loop;

    // TODO - different channels
    pspAudioSetChannelCallback(
        0, // channel
        audioCallback,
        (void*)this
    );
}

void Music::Stop()
{
    // TODO - different channels
    pspAudioSetChannelCallback(
        0, // channel
        nullptr, // pspAudioCallback_t
        nullptr
    );
}

} // namespace Psp
} // namespace GameLib

