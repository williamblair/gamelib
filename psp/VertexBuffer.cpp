#include <VertexBuffer.h>
#include <string.h>
#include <stdio.h>

namespace GameLib
{
namespace Psp
{

VertexBuffer::VertexBuffer() :
    mType(UNINITIALIZED),
    mFloatsPerVertex(0),
    mVerticesSize(0),
    mPosColorVertices(nullptr)
{}

VertexBuffer::~VertexBuffer()
{}

bool VertexBuffer::Init(
    Type type,
    float* vertices, size_t verticesSize,
    int* indices, size_t indicesSize)
{
    mType = type;
    
    dbgprint("before vertbuf alloc indices\n");
    if (indices && indicesSize > 0) {
        //mIndices = new int[indicesSize];
        //mIndices = new uint16_t[indicesSize];
    
        dbgprint("vertbuf memalign indices\n");
        mIndices = (unsigned short*)memalign(16, indicesSize * sizeof(unsigned short));
        if (!mIndices) {
            dbgprint("Failed to alloc indices\n");
            return false;
        }
        //memcpy((void*)mIndices, (void*)indices, indicesSize*sizeof(int));
        for (size_t i=0; i<indicesSize; ++i) {
            mIndices[i] = (unsigned short)indices[i];
        }
        mIndicesSize = indicesSize;
    }
    
    switch (mType)
    {
    case POS_COLOR:
    {
        mFloatsPerVertex = 6; // xyz rgb
        mNumVertices = verticesSize / mFloatsPerVertex;
        mVerticesSize = verticesSize;
        dbgprint("vertbuf memalign vertices\n");
        mPosColorVertices = (PosColorVertex*)memalign(
            16,
            mNumVertices * sizeof(PosColorVertex)
        );
        if (!mPosColorVertices) {
            dbgprint("Failed to alloc pos color vertices\n");
            return false;
        }
        
        dbgprint("vertbuf copying vertices\n");
        //if (indices == nullptr || indicesSize == 0) {
        {
            float* vertPtr = vertices;
            for (size_t i=0; i<mNumVertices; ++i) {
                mPosColorVertices[i].x = vertPtr[0];
                mPosColorVertices[i].y = vertPtr[1];
                mPosColorVertices[i].z = vertPtr[2];
                mPosColorVertices[i].r = (unsigned char)(vertPtr[3] * 255.0f);
                mPosColorVertices[i].g = (unsigned char)(vertPtr[4] * 255.0f);
                mPosColorVertices[i].b = (unsigned char)(vertPtr[5] * 255.0f);
                mPosColorVertices[i].a = 255;
                
                vertPtr += mFloatsPerVertex;
            }
        }
        break;
    }
    case POS_TEXCOORD:
    {
        mFloatsPerVertex = 5; // xyz uv
        mNumVertices = verticesSize / mFloatsPerVertex;
        mVerticesSize = verticesSize;
        mPosTexVertices = (PosTexVertex*)memalign(
            16,
            mNumVertices * sizeof(PosTexVertex)
        );
        if (!mPosTexVertices) {
            dbgprint("Failed to alloc pos tex vertices\n");
            return false;
        }
        
        
        {
            float* vertPtr = vertices;
            for (size_t i=0; i<mNumVertices; ++i) {
                mPosTexVertices[i].x = vertPtr[0];
                mPosTexVertices[i].y = vertPtr[1];
                mPosTexVertices[i].z = vertPtr[2];
                mPosTexVertices[i].u = vertPtr[3];
                mPosTexVertices[i].v = vertPtr[4];
                mPosTexVertices[i].r = 255;
                mPosTexVertices[i].g = 255;
                mPosTexVertices[i].b = 255;
                mPosTexVertices[i].a = 255;
                
                vertPtr += mFloatsPerVertex;
            }
        }
        break;
    }
    default:
        dbgprint("Unhandled vertex buffer type\n");
        return false;
    }
    
    return true;
}

bool VertexBuffer::UpdateVertices(float* vertices, size_t verticesSize)
{
    /*if (mIndices && mIndicesSize > 0) {
        switch (mType)
        {
        case POS_COLOR:
            for (size_t i=0; i<mIndicesSize; ++i) {
                float* vert = &vertices[mIndices[i]*mFloatsPerVertex];
                mPosColorVertices[i].x = vert[0];
                mPosColorVertices[i].y = vert[1];
                mPosColorVertices[i].z = vert[2];
                //mPosTexVertices[i].u = vert[3];
                //mPosTexVertices[i].v = vert[4];
                mPosColorVertices[i].r = (unsigned char)(vert[3]*255.0f);
                mPosColorVertices[i].g = (unsigned char)(vert[4]*255.0f);
                mPosColorVertices[i].b = (unsigned char)(vert[5]*255.0f);
                //mPosTexVertices[i].a = 255;
            }
            break;
        case POS_TEXCOORD:
            for (size_t i=0; i<mIndicesSize; ++i) {
                float* vert = &vertices[mIndices[i]*mFloatsPerVertex];
                mPosTexVertices[i].x = vert[0];
                mPosTexVertices[i].y = vert[1];
                mPosTexVertices[i].z = vert[2];
                mPosTexVertices[i].u = vert[3];
                mPosTexVertices[i].v = vert[4];
                //mPosTexVertices[i].r = 255;
                //mPosTexVertices[i].g = 255;
                //mPosTexVertices[i].b = 255;
                //mPosTexVertices[i].a = 255;
            }
            break;
        default:
            dbgprint("Unhandled update vertices mType\n");
            return false;
        }
        
        return true;
    }*/
    
    if (verticesSize != mVerticesSize) {
        dbgprint("ERROR - update vertices size mismatch\n");
        return false;
    }
    
    switch (mType)
    {
    case POS_COLOR:
    {
        float* vertPtr = vertices;
        for (size_t i=0; i<mNumVertices; ++i) {
            mPosColorVertices[i].x = vertPtr[0];
            mPosColorVertices[i].y = vertPtr[1];
            mPosColorVertices[i].z = vertPtr[2];
            mPosColorVertices[i].r = (unsigned char)(vertPtr[3] * 255.0f);
            mPosColorVertices[i].g = (unsigned char)(vertPtr[4] * 255.0f);
            mPosColorVertices[i].b = (unsigned char)(vertPtr[5] * 255.0f);
            mPosColorVertices[i].a = 255;
            
            vertPtr += mFloatsPerVertex;
        }
        break;
    }
    case POS_TEXCOORD:
    {
        float* vertPtr = vertices;
        for (size_t i=0; i<mNumVertices; ++i) {
            mPosTexVertices[i].x = vertPtr[0];
            mPosTexVertices[i].y = vertPtr[1];
            mPosTexVertices[i].z = vertPtr[2];
            mPosTexVertices[i].u = vertPtr[3];
            mPosTexVertices[i].v = vertPtr[4];
            //mPosColorVertices[i].r = (unsigned char)(vertPtr[3] * 255.0f);
            //mPosColorVertices[i].g = (unsigned char)(vertPtr[4] * 255.0f);
            //mPosColorVertices[i].b = (unsigned char)(vertPtr[5] * 255.0f);
            //mPosColorVertices[i].a = 255;
            
            vertPtr += mFloatsPerVertex;
        }
        break;
    }
    default:
        dbgprint("Unhandled update vertices type\n");
        return false;
    }
    
    return true;
}

} // namespace Psp
} // namespace GameLib

