#ifndef GAMELIB_PSP_SOUNDEFFECT_H_INCLUDED
#define GAMELIB_PSP_SOUNDEFFECT_H_INCLUDED

#include <Music.h>

namespace GameLib
{
namespace Psp
{

class SoundEffect
{
public:

    SoundEffect();
    ~SoundEffect();

    // Must be a .wav file, 16bit stereo, 44100 sample rate
    bool Init(const char* fileName);

    void Play(int channel = -1, bool loop = false);
    void Stop(int channel = -1);

private:
    bool mLooping;

    // true while callback function is in use
    // used like a mutex
    bool mLocked;

    Music::WAVHeader mWavHdr;
    Music::WAVStreamData mWavStream;

    int mChannel;
    static void audioCallback(void* buf, unsigned int length, void* userdata);
};

} // namespace Psp
} // namespace GameLib

#endif // GAMELIB_PSP_SOUNDEFFECT_H_INCLUDED

