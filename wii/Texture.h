#ifndef GAMELIB_WII_TEXTURE_H_INCLUDED
#define GAMELIB_WII_TEXTURE_H_INCLUDED

#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <gccore.h>
#include <wiiuse/wpad.h>

namespace GameLib
{
namespace Wii
{

class Texture
{

friend class Renderer;
friend class Sprite;

public:

    Texture();
    ~Texture();

    /**
     * @brief Init texture from raw pixel data
     * @param data the raw pixel data, must be 32bit RGBA
     * @param width the width of the image in px
     * @param height the height of the image in px
     * @param bytesPerPixel; must be 4 for 1 byte each RGBA
     * @return true on success, false on failure
     */
    bool Init(const char* data, int width, int height, int bytesPerPixel);

    /**
     * @brief Read TGA image from file and initialize from it
     * @param fileName the tga image file location
     * @return true on success, false on failure
     */
    bool LoadFromTGA(const char* fileName);

    /**
     * @brief Read BMP image from file and initialize from it
     * @param fileName the bmp image file location
     * @return true on success, false on failure
     */
    bool LoadFromBMP(const char* fileName);
    
    int GetWidth() const { return mWidth; }
    int GetHeight() const { return mHeight; }

    /**
     * @brief set new texture data. must be the same size as the original data.
     * @param data the new pixel data to use for this texture
     * @param width the width of the pixel data (must be same as original)
     * @param height the height of the pixel data (must be same as original)
     * @param bytesPerPixel bytes per pixel of the data (must be same as original)
     * @return true on success, false on failure
     */
    bool Update(const char* data, int width, int height, int bytesPerPixel);
    
private:
    uint8_t* mTexData;
    GXTexObj mTexture;
    int mWidth;
    int mHeight;

    bool Init16Bit(const char* data, int width, int height, int bytesPerPixel);
    bool Init32Bit(const char* data, int width, int height, int bytesPerPixel);
};

} // namespace Wii
} // namespace GameLib

#endif // GAMELIB_WII_TEXTURE_H_INCLUDED

