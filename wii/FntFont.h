#ifndef GAMELIB_WII_FNT_FONT_H_INCLUDED
#define GAMELIB_WII_FNT_FONT_H_INCLUDED

#include <map>

#include <Texture.h>
#include <Sprite.h>
#include <Renderer.h>

namespace GameLib
{
namespace Wii
{

/**
 * @brief Font drawing from fnt format from 
 *  <a>https://angelcode.com/products/bmfont/</a>
 */
class FntFont
{
public:
    
    FntFont();
    ~FntFont();
    
    /**
     * @brief Load font file and initialize
     * @param fileName the location of the .fnt file describing texture
     * @param texFileName texture file to use
     * @param r font color red
     * @param g font color green
     * @param b font color blue
     * @return true on success, false on failure
     */
    bool Init(
        const char* fileName,
        const char* texFileName,
        uint8_t r, uint8_t g, uint8_t b
    );
    
    /**
     * @brief how much to scale the font size when drawing; default = 1
     */
    void SetDrawScale(float scale) { mDrawScale = scale; }
    
    /**
     * @brief Draw string to the screen with the top left at x,y
     * @param x normalized screen position x (-1..1)
     * @param y normalized screen position y (-1..1)
     */
    void Draw(const char* msg, float x, float y, Renderer& render);
    
    /** @brief returns how many pixels tall each drawn line is */
    int GetLineHeight() const { return mLineHeight; }
    
private:
    Texture mTexture;
    Sprite mSprite;
    int mLineHeight;
    float mDrawScale;
    struct CharInfo
    {
        char id;
        int x, y;
        int w, h;
        int xoffset, yoffset;
        int xadvance;
        int page;
        int channel;
        // normalized position and size within texture for uv coords
        float u, v;
        float uWidth, vHeight;
        
        CharInfo() :
            id(0),
            x(0), y(0),
            w(0), h(0),
            xoffset(0), yoffset(0),
            xadvance(0),
            page(0),
            channel(0),
            u(0.0f), v(0.0f),
            uWidth(0.0f), vHeight(0.0f)
        {}
        CharInfo(const CharInfo& other) :
            id(other.id),
            x(other.x), y(other.y),
            w(other.w), h(other.h),
            xoffset(other.xoffset), yoffset(other.yoffset),
            xadvance(other.xadvance),
            page(other.page),
            channel(other.channel),
            u(other.u), v(other.v),
            uWidth(other.uWidth), vHeight(other.vHeight)
        {}
        CharInfo& operator=(const CharInfo& other)
        {
            id = other.id;
            x = other.x; y = other.y;
            w = other.w; h = other.h;
            xoffset = other.xoffset;
            yoffset = other.yoffset;
            xadvance = other.xadvance;
            page = other.page;
            channel = other.channel;
            u = other.u; v = other.v;
            uWidth = other.uWidth;
            vHeight = other.vHeight;
            return *this;
        }
    };
    std::map<int,CharInfo> mCharInfo;
    //char mMinChar;
    //std::vector<CharInfo> mCharInfo;
    
    bool LoadFntFile(const char* fileName);
    bool LoadTexture(const char* texFileName,
        uint8_t r, uint8_t g, uint8_t b);
    bool LoadFromTga(const char* texFileName,
        uint8_t r, uint8_t g, uint8_t b);
    bool LoadFromBMP(const char* texFileName,
        uint8_t r, uint8_t g, uint8_t b);
};

} // namespace Wii
} // namespace GameLib

#endif // GAMELIB_WII_FNT_FONT_H_INCLUDED
