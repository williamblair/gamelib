#include <VertexBuffer.h>
#include <string.h>
#include <stdio.h>

namespace GameLib
{
namespace Wii
{

VertexBuffer::VertexBuffer() :
    mType(UNINITIALIZED),
    mVertices(nullptr),
    mVerticesSize(0),
    mIndices(nullptr),
    mIndicesSize(0),
    mFloatsPerVertex(0)
{
}

VertexBuffer::~VertexBuffer()
{
    if (mVertices != nullptr) delete[] mVertices;
    if (mIndices != nullptr) delete[] mIndices;
}

bool VertexBuffer::Init(
    Type type,
    float* vertices,
    size_t verticesSize,
    int* indices,
    size_t indicesSize)
{
    mVertices = new float[verticesSize];
    if (indicesSize > 0) { mIndices = new int[indicesSize]; }
    mVerticesSize = verticesSize;
    mIndicesSize = indicesSize;
    mType = type;
    switch (type)
    {
    case POS_COLOR: mFloatsPerVertex = 6; break; // xyz rgb
    case POS_TEXCOORD: mFloatsPerVertex = 5; break; // xyz uv
    default:
        printf("ERROR - unhandled type in vertbuf init\n");
        return false;
        break;
    }
    
    memcpy((void*)mVertices, (void*)vertices, verticesSize*sizeof(float));
    if (indicesSize > 0) {
        memcpy((void*)mIndices, (void*)indices, indicesSize*sizeof(int));
    }

    // TODO - probably error checking
    return true;
}

bool VertexBuffer::UpdateVertices(
    float* vertices,
    size_t verticesSize)
{
    
    if (verticesSize != mVerticesSize) {
        printf("ERROR - vert buf update sizes dont match\n");
        return false;
    }
    memcpy((void*)mVertices, (void*)vertices, verticesSize*sizeof(float));
    return true;
}

} // namespace Wii
} // namespace GameLib

