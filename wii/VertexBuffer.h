#ifndef GAMELIB_WII_VERTEX_BUFFER_H_INCLUDED
#define GAMELIB_WII_VERTEX_BUFFER_H_INCLUDED

#include <cstddef>

namespace GameLib
{
namespace Wii
{

class VertexBuffer
{

friend class Renderer;

public:
    
    enum Type
    {
        POS_COLOR,
        POS_TEXCOORD,
        UNINITIALIZED
    };
    
    VertexBuffer();
    ~VertexBuffer();
    
    bool Init(
        Type type,
        float* vertices,
        size_t verticesSize,
        int* indices,
        size_t indicesSize
    );
    
    bool UpdateVertices(float* vertices, size_t verticesSize);

private:
    Type mType;
    float* mVertices;
    size_t mVerticesSize;
    int* mIndices;
    size_t mIndicesSize;
    size_t mFloatsPerVertex;
};

} // namespace Wii
} // namespace GameLib

#endif // GAMELIB_WII_VERTEX_BUFFER_H_INCLUDED

