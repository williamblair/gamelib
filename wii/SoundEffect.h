#ifndef GAMELIB_WII_SOUNDEFFECT_H_INCLUDED
#define GAMELIB_WII_SOUNDEFFECT_H_INCLUDED

namespace GameLib
{
namespace Wii
{

class SoundEffect
{
public:

    SoundEffect();
    ~SoundEffect();

    bool Init(const char* fileName);

    void Play(int channel = -1, bool loop = false);
    void Stop(int channel = -1);

private:
    unsigned char* mBuffer;
    int mBufferLen;
    int mVoice;
};

} // namespace Wii
} // namespace GameLib

#endif // GAMELIB_WII_SOUNDEFFECT_H_INCLUDED

