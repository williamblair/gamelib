#ifndef GAMELIB_WII_INPUT_H_INCLUDED
#define GAMELIB_WII_INPUT_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <gccore.h>
#include <wiiuse/wpad.h>

namespace GameLib
{
namespace Wii
{

class Input
{
public:
    
    Input();
    ~Input();

    bool Init();
    
    // Should be called once per frame
    void Update();
    
    bool ConfirmClicked() const { return mConfirmClicked; }
    
    // possibly TODO - allow exit/quit
    bool Quit() const { return false; }
    bool Up() const { return mUp; }
    bool Down() const { return mDown; }
    bool Left() const { return mLeft; }
    bool Right() const { return mRight; }
    
    bool UpClicked() const { return mUpClicked; }
    bool DownClicked() const { return mDownClicked; }
    bool LeftClicked() const { return mLeftClicked; }
    bool RightClicked() const { return mRightClicked; }
    
    float WiimoteX() const { return mWiimoteX; }
    float WiimoteY() const { return mWiimoteY; }

    // for PC compatibility
    int MouseMoveX() const { return (int)(mWiimoteX - mLastWiimoteX); }
    int MouseMoveY() const { return (int)(mWiimoteY - mLastWiimoteY); }
    
private:
    bool mConfirmClicked;
    bool mUp;
    bool mDown;
    bool mLeft;
    bool mRight;
    
    bool mUpClicked;
    bool mDownClicked;
    bool mLeftClicked;
    bool mRightClicked;
    
    u32 mDownBtns;
    
    float mWiimoteX;
    float mWiimoteY;
    float mLastWiimoteX;
    float mLastWiimoteY;
    
    ir_t mIr;
};

} // namespace Wii
} // namespace GameLib

#endif // GAMELIB_WII_INPUT_H_INCLUDED

