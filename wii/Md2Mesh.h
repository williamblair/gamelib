#ifndef GAMELIB_WII_MD2_MESH_H_INCLUDED
#define GAMELIB_WII_MD2_MESH_H_INCLUDED

#include <vector>
#include <string>
#include <cstring>
#include <cstdint>

#include <Renderer.h>
#include <VertexBuffer.h>
#include <GameMath/GameMath.h>

namespace GameLib
{
namespace Wii
{

class Md2Mesh
{
public:
    
    struct Anim
    {
        int startFrame;
        int endFrame;
        char name[16];
    };
    
    Md2Mesh();
    ~Md2Mesh();
    
    bool Init(const std::string& fileName);
    
    void Update(const float dt);
    
    void SetAnim(const std::string& name)
    {
        for (size_t i = 0; i < mAnimationsSize; ++i) {
            if (strcmp(name.c_str(), mAnimations[i].name) == 0) {
                // only reset anim if not already set
                if (strcmp(mCurrentAnimName, name.c_str()) != 0) {
                    mCurrentFrame = mAnimations[i].startFrame;
                    mEndFrame = mAnimations[i].endFrame;
                    mNextFrame =  mCurrentFrame;
                    
                    mInterpolation = 0.0f;
                }
            }
        }
    }
    
    void Draw(
        GameMath::Mat4& modelMat,
        GameMath::Mat4& viewMat,
        Renderer& render
    );
    
private:

    struct Header
    {
        uint8_t magic[4];       // IDP2, 8
        int32_t version;
        
        int32_t skinWidth;      // tex width, height
        int32_t skinHeight;
        
        int32_t frameSize;      // size of 1 keyframe in bytes
        
        int32_t numSkins;       // number of textures
        int32_t numVertices;
        int32_t numTexCoords;
        int32_t numTriangles;
        int32_t numGLCmds;      // number of opengl draw commands
        int32_t numFrames;      // number of keyframes
        
        int32_t skinOffset;     // offset to skin names (64 bytes each)
        int32_t texCoordOffset; // offset to texture coords
        int32_t triangleOffset; // offset to mTriangles
        int32_t frameOffset;    // offset to keyframes
        int32_t GLCmdOffset;    // offset to opengl draw commands
        int32_t eofOffset;      // offset to end of file
    };
    
    struct Vertex
    {
        uint8_t v[3];   // compressed vertices; uncompress = scale*v + translate
        uint8_t lightNormIndex;
    };
    
    struct TexCoord
    {
        int16_t s;
        int16_t t;
    };
    
    struct KeyFrame
    {
        float scale[3];     // use to multiply and add Vertex::v
        float translate[3];
        char name[16];
        Vertex* md2Vertices;
        size_t md2VerticesSize;
        GameMath::Vec3* vertices; // converted result vertices
        size_t verticesSize;
    };
    
    struct Triangle
    {
        int16_t vertIndex[3];
        int16_t texCoordIndex[3];
    };
    
    struct Skin
    {
        char name[64]; // filename of the texture
    };

    Skin* mSkins;
    size_t mSkinsSize;
    TexCoord* mMd2TexCoords;
    size_t mMd2TexCoordsSize;
    Triangle* mTriangles;
    size_t mTrianglesSize;
    
    GameMath::Vec2* mTexCoords; // converted tex coords
    size_t mTexCoordsSize;
    KeyFrame* mKeyFrames;
    size_t mKeyFramesSize;

    std::vector<float> mInterpolatedFrame;
    char mCurrentAnimName[256]; // 256 arbitrarily chosen
    
    int mStartFrame;
    int mEndFrame;
    int mCurrentFrame;
    int mNextFrame;
    float mInterpolation;
    bool mLoopAnim;
    
    VertexBuffer mVertexBuf;
    
    float* mRadii; // radius for each frame
    size_t mRadiiSize;

    Anim* mAnimations;
    size_t mAnimationsSize;
    
    void reorganizeVertices(); // recreate vertices so that we don't need to use indices
    void stripTextureNames(); // remove folder prefixes from texture names
    void genBuffers(); // create opengl buffers
    void genAnimations();
    
    inline size_t countNumAnims()
    {
        //printf("Printing anim names\n");
        char curAnimName[16];
        size_t animCount = 1;
        size_t c = 0;
        while (!(mKeyFrames[0].name[c] >= '0' && mKeyFrames[0].name[c] <= '9'))
        {
            curAnimName[c] = mKeyFrames[0].name[c];
            c++;
        }
        curAnimName[c] = '\0';
        c = 0;
        for (size_t i = 1; i < mKeyFramesSize; ++i)
        {
            char frameAnimName[16];
            while (!(mKeyFrames[i].name[c] >= '0' && mKeyFrames[i].name[c] <= '9'))
            {
                frameAnimName[c] = mKeyFrames[i].name[c];
                c++;
            }
            frameAnimName[c] = '\0';
            c = 0;
            if (strcmp(frameAnimName, curAnimName) != 0) {
                ++animCount;
                strcpy(curAnimName, frameAnimName);
            }
        }

        return animCount;
    }
    
    inline int32_t lend2bend32(int32_t val)
    {
        int32_t res;
        int8_t* out = (int8_t*)&res;
        int8_t* in = (int8_t*)&val;
        out[0] = in[3];
        out[1] = in[2];
        out[2] = in[1];
        out[3] = in[0];
        return res;
    }
    inline float lend2bend32(float val)
    {
        float res;
        int8_t* out = (int8_t*)&res;
        int8_t* in = (int8_t*)&val;
        out[0] = in[3];
        out[1] = in[2];
        out[2] = in[1];
        out[3] = in[0];
        return res;
    }
    inline int32_t lend2bend16(int16_t val)
    {
        int16_t res;
        int8_t* out = (int8_t*)&res;
        int8_t* in = (int8_t*)&val;
        out[0] = in[1];
        out[1] = in[0];
        return res;
    }
};

} // namespace Wii
} // namespace GameLib

#endif // GAMELIB_WII_MD2_MESH_H_INCLUDED

