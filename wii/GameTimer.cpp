#include <GameTimer.h>

namespace GameLib
{
namespace Wii
{

GameTimer::GameTimer()
{}

GameTimer::~GameTimer()
{}

static inline float diffSeconds(timeval* cur, timeval* prev)
{
    return (float)(cur->tv_sec - prev->tv_sec) +
        ((float)(cur->tv_usec - prev->tv_usec)) / 1000000.0f;
}

float GameTimer::Update()
{
    struct timeval curTime;
    gettimeofday(&curTime, 0);
    
    mFrameCtr += 1.0f;
    
    float dt = diffSeconds(&curTime, &mLastTime);
    
    if (mDtCtr < 0.0f) mDtCtr = 0.0f;
    else mDtCtr += dt;

    if (mDtCtr >= 1.0f)
    {
        mFps = mFrameCtr / mDtCtr;
        mFrameTime = 1.0f / mFps;

        while (mDtCtr >= 1.0f) { mDtCtr -= 1.0f; }
        mFrameCtr = 0.0f;
    }
    
    
    mLastTime.tv_sec = curTime.tv_sec;
    mLastTime.tv_usec = curTime.tv_usec;
    
    return dt;
}

} // namespace Wii
} // namespace GameLib

