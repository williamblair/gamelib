#ifndef GAMELIB_WII_SPRITE_H_INCLUDED
#define GAMELIB_WII_SPRITE_H_INCLUDED

#include <Texture.h>

namespace GameLib
{
namespace Wii
{

class Sprite
{
friend class Renderer;

public:

    Sprite();
    ~Sprite();

    bool Init(Texture* tex);

    // Set normalized screen location top left (-1..1)
    void SetXY(float x, float y) {
        mX = x;
        mY = y;
    }

    // Set draw size (percent size on screen, normalized 0..1)
    void SetWH(float w, float h) {
        mWidth = w;
        mHeight = h;
    }

    // Set clip top left x/y and size (normalized 0..1)
    // top left = 0.0f,0.0f
    // bottom right = 1.0f,1.0f
    void SetUVWH(float u, float v, float w, float h) {
        mU = u; mV = v;
        mW = w; mH = h;
    }

    /** @brief set drawing rotation in degrees */
    void SetRotation(const float rotDeg) { mRotDegrees = rotDeg; }
    /** @brief get the current sprite drawing rotation */
    float GetRotation() const { return mRotDegrees; }

private:
    Texture* mTexture;
    float mX; // normalized screen location x/y (-1..1)
    float mY;
    float mU; // clip top left x/y/width/height
    float mV;
    float mW;
    float mH;
    float mWidth; // drawing width/height
    float mHeight;
    float mRotDegrees;
};

} // namespace Wii
} // namespace GameLib

#endif // GAMELIB_WII_SPRITE_H_INCLUDED

