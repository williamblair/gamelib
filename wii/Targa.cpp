#include <Targa.h>

namespace GameLib
{
namespace Wii
{
namespace Targa
{

Header header;
unsigned int width;
unsigned int height;
unsigned int bitsPerPixel;
unsigned int bytesPerPixel;
std::vector<uint8_t> imageData;

bool LoadUncompressed(std::ifstream& inFile)
{
    unsigned int imageSize = imageData.size();
    inFile.read((char*)imageData.data(), imageSize);

    // swap R and B components (BGR --> RGB)
    for (unsigned int swap = 0; swap < imageSize; swap += bytesPerPixel)
    {
        uint8_t cswap = imageData[swap];
        imageData[swap] = imageData[swap+2];
        imageData[swap+2] = cswap;
    }

    return true;
}
bool LoadCompressed(std::ifstream& inFile)
{
    unsigned int pixelCount = height * width;
    unsigned int curPixel = 0;
    unsigned int curByte = 0;

    std::vector<uint8_t> colorBuffer(bytesPerPixel);
    std::vector<uint8_t> fileBuf;
    
    std::streampos fsize = 0;
    std::streampos origPos;
    fsize = inFile.tellg();
    origPos = fsize;
    inFile.seekg(0, std::ios::end);
    fsize = inFile.tellg() - fsize;
    inFile.seekg(origPos, std::ios::beg);
    
    fileBuf.resize(fsize);
    inFile.read((char*)fileBuf.data(), fileBuf.size());
    uint8_t* filePtr = fileBuf.data();

    do
    {
        uint8_t chunkHeader = 0;
        chunkHeader = *filePtr++;

        if (chunkHeader < 128)
        {
            chunkHeader++;

            for (short counter = 0; counter < chunkHeader; counter++)
            {
                // read a color chunk
                for (size_t i=0; i<bytesPerPixel; ++i) {
                    colorBuffer[i] = *filePtr++;
                }

                imageData[curByte+0] = colorBuffer[2];
                imageData[curByte+1] = colorBuffer[1];
                imageData[curByte+2] = colorBuffer[0];

                if (bytesPerPixel == 4) {
                    imageData[curByte + 3] = colorBuffer[3];
                }

                curByte += bytesPerPixel;
                curPixel++;

                // mismatch between sizes
                if (curPixel > pixelCount) {
                    return false;
                }
            }
        }
        // chunkHeader >= 128
        else
        {
            chunkHeader -= 127;

            // read a color chunk
            for (size_t i=0; i<bytesPerPixel; ++i) {
                colorBuffer[i] = *filePtr++;
            }

            for (short counter = 0; counter < chunkHeader; counter++)
            {
                imageData[curByte+0] = colorBuffer[2];
                imageData[curByte+1] = colorBuffer[1];
                imageData[curByte+2] = colorBuffer[0];
                
                if (bytesPerPixel == 4) {
                    imageData[curByte + 3] = colorBuffer[3];
                }

                curByte += bytesPerPixel;
                curPixel++;

                // size mismatch
                if (curPixel > pixelCount) {
                    return false;
                }
            }
        }

    } while (curPixel < pixelCount);

    return true;
}

bool IsImageTypeSupported(const Header& header)
{
    // only support color images currently
    return ((header.imageTypeCode == TFT_RGB ||
            header.imageTypeCode == TFT_RLE_RGB) &&
            header.colorMapType == 0);
}
bool IsCompressed(const Header& header)
{
    return (header.imageTypeCode == TFT_RLE_RGB ||
        header.imageTypeCode == TFT_RLE_GRAYSCALE);
}
bool IsUncompressed(const Header& header)
{
    return (header.imageTypeCode == TFT_RGB ||
        header.imageTypeCode == TFT_GRAYSCALE);
}

void FlipImageVertically()
{
    std::vector<uint8_t> flippedData(imageData.size());
    size_t flippedIndex = 0;
    int step = bytesPerPixel;
    for (int row = height - 1; row >= 0; --row) {
        uint8_t* rowData = &imageData[row * (width * step)];
        for (unsigned int col = 0; col < width*step; ++col) {
            flippedData[flippedIndex] = *rowData;
            flippedIndex++;
            rowData++;
        }
    }

    imageData.assign(flippedData.begin(), flippedData.end());
}

} // namespace Targa
} // namespace Wii
} // namespace GameLib


