#ifndef GAMELIB_WII_BMP_H_INCLUDED
#define GAMELIB_WII_BMP_H_INCLUDED

#include <vector>
#include <cstdint>

namespace GameLib
{
namespace Wii
{
namespace BMP
{

#define PACKED __attribute__((packed))

struct Header
{
    uint16_t header; // the header field
    uint32_t filesize; // size of the file in bytes
    uint32_t reserved; // 4 bytes of reserved data (depending on the image that creates it)
    uint32_t pixOffset; // 4 bytes  offset (i.e. starting address) of where the pixmap can be found
} PACKED;

// Win32 struct (40 bytes)
struct BITMAPINFOHEADER
{
    uint32_t size;
    int32_t width;
    int32_t height;
    uint16_t planes;
    uint16_t bitCount;
    uint32_t compression;
    uint32_t sizeImage;
    int32_t xPelsPerMeter;
    int32_t yPelsPerMeter;
    uint32_t clrUsed;
    uint32_t clrImportant;
} PACKED;

#undef PACKED

/**
 * @brief puts pixel data from the given BMP file into buffer at 4bpp
 * @param fileName the BMP file to load
 * @param buffer the result to store the image pixels in
 * @param with stores the result image width
 * @param height stores the result image height
 * @return true on success, false on failure
 */
bool Load(
    const char* fileName,
    std::vector<uint8_t>& buffer,
    int& width, int& height
);

} // namespace BMP
} // namespace Wii
} // namespace GameLib

#endif // GAMELIB_WII_BMP_H_INCLUDED

