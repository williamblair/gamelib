#include <Texture.h>
#include <bjdebug.h>
#include <Bmp.h>
#include <Targa.h>

namespace GameLib
{
namespace Wii
{

// little endian to big endian
static inline uint16_t lend2bend16(uint16_t lend)
{
    char* ptr = (char*)&lend;
    return (ptr[1] << 8) | 
           (ptr[0]);
}
static inline uint32_t lend2bend32(uint32_t lend)
{
    char* ptr = (char*)&lend;
    return ((uint32_t)ptr[3] << 24) |
           ((uint32_t)ptr[2] << 16) |
           ((uint32_t)ptr[1] << 8) | 
           ((uint32_t)ptr[0]);    
}

Texture::Texture() :
    mTexData(nullptr),
    mWidth(0),
    mHeight(0)
{}

Texture::~Texture()
{
    if (mTexData != nullptr) {
        free(mTexData);
        mTexData = nullptr;
    }
}

bool Texture::Init16Bit(const char* data, int width, int height, int bytesPerPixel)
{
    if (bytesPerPixel != 4) {
        dbgprint("Unsupported bytes per pixel (must be 4): %d\n", bytesPerPixel);
        return false;
    }
    mWidth = width;
    mHeight = height;

    // Texture data must be stored as pixel blocks, see below:
    //      https://wiki.tockdom.com/wiki/Image_Formats
    // also https://wiki.tockdom.com/wiki/TPL_(File_Format),
    //      https://wiki.tockdom.com/wiki/TEX0_(File_Format)
    // below values are specific to RGB565
    const size_t blkW = 4; // 4 px wide blocks
    const size_t blkH = 4; // 4 px tall blocks
    const size_t numBlocksX = (width / blkW) + ((width % blkW) > 0);
    const size_t numBlocksY = (height / blkH) + ((height % blkH) > 0);
    const size_t outBytesPerPixel = 2; // RGB565
    mTexData = (uint8_t*)memalign(32, numBlocksX*blkW * numBlocksY*blkH * outBytesPerPixel);
    if (!mTexData) {
        dbgprint("Failed to alloc mTexData\n");
        return false;
    }
    uint16_t* outPx = (uint16_t*)mTexData;
    for (size_t blkY = 0; blkY < numBlocksY; ++blkY)
    {
    for (size_t blkX = 0; blkX < numBlocksX; ++blkX)
    {
        size_t blkImgWidth = (width - blkX*blkW);
        size_t blkImgHeight = (height - blkY*blkH);
        
        if (blkImgWidth > blkW) { blkImgWidth = blkW; }
        if (blkImgHeight > blkH) { blkImgHeight = blkH; }        

        size_t numPxPadRows = blkH - blkImgHeight;
        size_t numPxPadCols = blkW - blkImgWidth;

        for (size_t row = 0; row < blkImgHeight; ++row)
        {
            const size_t outPxX = blkX*blkW;
            const size_t outPxY = (blkY*blkH)+row;
            uint8_t* inPxData = (uint8_t*)data;
            uint8_t* inPx = &inPxData[outPxY*width*bytesPerPixel + outPxX*bytesPerPixel];
            for (size_t col = 0; col < blkImgWidth; ++col) {
                uint8_t r = *inPx++;
                uint8_t g = *inPx++;
                uint8_t b = *inPx++;

                // scale to 16bit r5g6b5
                uint16_t rScl = (uint16_t)((float(r)/255.0f)*31.0f);
                uint16_t gScl = (uint16_t)((float(g)/255.0f)*63.0f);
                uint16_t bScl = (uint16_t)((float(b)/255.0f)*31.0f);

                *outPx++ = ((rScl&0b11111) << 11) |
                    ((gScl&0b111111) << 5) |
                    (bScl&0b11111);
            }
            for (size_t col = 0; col < numPxPadCols; ++col) {
                *outPx++ = 0;
            }
        }
        for (size_t row = 0; row < numPxPadRows; ++row) {
            for (size_t col = 0; col < blkW; ++col) {
                *outPx++ = 0;
            }
        }
    }
    }
    GX_InitTexObj(
        &mTexture, // GXTexObj* obj
        (void*)mTexData, // void* img_ptr
        (u16)(width), // u16 wd
        (u16)(height), // u16 ht
        /*GX_TF_RGBA8*/GX_TF_RGB565, // u8 fmt
        GX_REPEAT, // u8 wrap_s
        GX_REPEAT, // u8 wrap_t
        GX_FALSE // u8 mipmap
    );
    return true;
}

bool Texture::Init32Bit(const char* data, int width, int height, int bytesPerPixel)
{
    if (bytesPerPixel != 3 && bytesPerPixel != 4) {
        dbgprint("Unsupported bytes per pixel (must be 3 or 4): %d\n", bytesPerPixel);
        return false;
    }
    mWidth = width;
    mHeight = height;

    // Texture data must be stored as pixel blocks, see below:
    //      https://wiki.tockdom.com/wiki/Image_Formats
    // also https://wiki.tockdom.com/wiki/TPL_(File_Format),
    //      https://wiki.tockdom.com/wiki/TEX0_(File_Format)
    // below values are specific to RGBA32
    const size_t blkW = 4; // 4 px wide blocks
    const size_t blkH = 4; // 4 px tall blocks
    const size_t numBlocksX = (width / blkW) + ((width % blkW) > 0);
    const size_t numBlocksY = (height / blkH) + ((height % blkH) > 0);
    const size_t outBytesPerPixel = 4; // RGBA32
    mTexData = (uint8_t*)memalign(32, numBlocksX*blkW * numBlocksY*blkH * outBytesPerPixel);
    if (!mTexData) {
        dbgprint("Failed to alloc mTexData\n");
        return false;
    }
    uint8_t* outPx = (uint8_t*)mTexData;
    std::vector<uint8_t> tmpOutPxIn(blkW*blkH*outBytesPerPixel); // non-reordered
    std::vector<uint8_t> tmpOutPxOut(blkW*blkH*outBytesPerPixel); // reordered
    for (size_t blkY = 0; blkY < numBlocksY; ++blkY)
    {
    for (size_t blkX = 0; blkX < numBlocksX; ++blkX)
    {
        size_t blkImgWidth = (width - blkX*blkW);
        size_t blkImgHeight = (height - blkY*blkH);
        
        if (blkImgWidth > blkW) { blkImgWidth = blkW; }
        if (blkImgHeight > blkH) { blkImgHeight = blkH; }        

        memset(tmpOutPxIn.data(), 0, tmpOutPxIn.size()*sizeof(uint8_t));

        // store pixels in output block in normal RGBA order
        for (size_t row = 0; row < blkImgHeight; ++row)
        {
            const size_t outPxX = blkX*blkW;
            const size_t outPxY = (blkY*blkH)+row;
            uint8_t* inPxData = (uint8_t*)data;
            uint8_t* inPx = &inPxData[outPxY*width*bytesPerPixel + outPxX*bytesPerPixel];
            uint8_t* outTmp = &tmpOutPxIn[row*blkW*outBytesPerPixel];
            for (size_t col = 0; col < blkImgWidth; ++col) {
                uint8_t r = *inPx++;
                uint8_t g = *inPx++;
                uint8_t b = *inPx++;
                uint8_t a = (bytesPerPixel == 4) ? *inPx++ : 255;

                *outTmp++ = r;
                *outTmp++ = g;
                *outTmp++ = b;
                *outTmp++ = a;
            }
        }


        // rearrange into ARARAR...GBGBGB... order
        uint8_t* aPtr = &tmpOutPxOut[0];
        uint8_t* rPtr = &tmpOutPxOut[1];
        uint8_t* gPtr = &tmpOutPxOut[32];
        uint8_t* bPtr = &tmpOutPxOut[33];
        uint8_t* tmpPtr = tmpOutPxIn.data();
        for (size_t px = 0; px < 16; ++px) {
            uint8_t r = *tmpPtr++;
            uint8_t g = *tmpPtr++;
            uint8_t b = *tmpPtr++;
            uint8_t a = *tmpPtr++;
            *aPtr = a;
            *rPtr = r;
            *gPtr = g;
            *bPtr = b;
            aPtr += 2;
            rPtr += 2;
            gPtr += 2;
            bPtr += 2;
        }

        // store the reordered pixels in the output texture
        for (size_t px = 0; px < 64; ++px) {
            *outPx++ = tmpOutPxOut[px];
        }
    }
    }
    GX_InitTexObj(
        &mTexture, // GXTexObj* obj
        (void*)mTexData, // void* img_ptr
        (u16)(width), // u16 wd
        (u16)(height), // u16 ht
        GX_TF_RGBA8/*GX_TF_RGB565*/, // u8 fmt
        GX_REPEAT, // u8 wrap_s
        GX_REPEAT, // u8 wrap_t
        GX_FALSE // u8 mipmap
    );
    return true;
}

bool Texture::Init(const char* data, int width, int height, int bytesPerPixel)
{
    //return Init16Bit(data, width, height, bytesPerPixel);
    return Init32Bit(data, width, height, bytesPerPixel);
}

bool Texture::LoadFromBMP(const char* fileName)
{
    std::vector<uint8_t> imageData;
    int width = -1;
    int height = -1;
    if (!BMP::Load(fileName, imageData, width, height)) {
        return false;
    }
    return Init((const char*)imageData.data(), width, height, 4);
}

bool Texture::LoadFromTGA(const char* fileName)
{
    std::ifstream inFile(fileName, std::ios::binary);
    if (!inFile.is_open()) {
        dbgprint("failed to open targa image file: %s\n", fileName);
        return false;
    }

    inFile.read((char*)&Targa::header, sizeof(Targa::Header));
    Targa::header.xOrigin = lend2bend16(Targa::header.xOrigin);
    Targa::header.yOrigin = lend2bend16(Targa::header.yOrigin);
    Targa::header.width = lend2bend16(Targa::header.width);
    Targa::header.height = lend2bend16(Targa::header.height);
    if (!Targa::IsImageTypeSupported(Targa::header)) {
        dbgprint("unsupported targa type\n");
        return false;
    }

    Targa::width = Targa::header.width;
    Targa::height = Targa::header.height;

    Targa::bitsPerPixel = Targa::header.bpp;
    Targa::bytesPerPixel = Targa::header.bpp / 8;

    if (Targa::bytesPerPixel < 3) {
        dbgprint("unsupported bytesPerPixel: %u\n", Targa::bytesPerPixel);
        return false;
    }

    unsigned int imageSize = Targa::width * Targa::height * Targa::bytesPerPixel;
    Targa::imageData.resize(imageSize);

    // skip past the id if there is one
    if (Targa::header.idLength > 0) {
        inFile.ignore(Targa::header.idLength);
    }

    bool result = false;

    if (Targa::IsUncompressed(Targa::header)) {
        result = Targa::LoadUncompressed(inFile);
    } else {
        result = Targa::LoadCompressed(inFile);
    }

    if ((Targa::header.imageDesc & Targa::TOP_LEFT) == Targa::TOP_LEFT) {
        Targa::FlipImageVertically();
    }

    if (!result) {
        dbgprint("failed to load targa\n");
        return false;
    }
    
    result = Init(
        (const char*)Targa::imageData.data(),
        Targa::width,
        Targa::height,
        Targa::bytesPerPixel
    );
    Targa::imageData.clear();
    Targa::width = 0;
    Targa::height = 0;
    Targa::bytesPerPixel = 0;
    
    return result;
}

static std::vector<uint8_t> tmpOutPxIn; // non-reordered
static std::vector<uint8_t> tmpOutPxOut; // reordered
bool Texture::Update(
    const char* data,
    int width, int height,
    int bytesPerPixel)
{
    const size_t blkW = 4; // 4 px wide blocks
    const size_t blkH = 4; // 4 px tall blocks
    const size_t numBlocksX = (width / blkW) + ((width % blkW) > 0);
    const size_t numBlocksY = (height / blkH) + ((height % blkH) > 0);
    const size_t outBytesPerPixel = 4; // RGBA32
    uint8_t* outPx = (uint8_t*)mTexData;
    //std::vector<uint8_t> tmpOutPxIn(blkW*blkH*outBytesPerPixel); // non-reordered
    //std::vector<uint8_t> tmpOutPxOut(blkW*blkH*outBytesPerPixel); // reordered
    tmpOutPxIn.resize(blkW*blkH*outBytesPerPixel); // non-reordered
    tmpOutPxOut.resize(blkW*blkH*outBytesPerPixel); // reordered
    for (size_t blkY = 0; blkY < numBlocksY; ++blkY)
    {
    for (size_t blkX = 0; blkX < numBlocksX; ++blkX)
    {
        size_t blkImgWidth = (width - blkX*blkW);
        size_t blkImgHeight = (height - blkY*blkH);
        
        if (blkImgWidth > blkW) { blkImgWidth = blkW; }
        if (blkImgHeight > blkH) { blkImgHeight = blkH; }        

        memset(tmpOutPxIn.data(), 0, tmpOutPxIn.size()*sizeof(uint8_t));

        // store pixels in output block in normal RGBA order
        for (size_t row = 0; row < blkImgHeight; ++row)
        {
            const size_t outPxX = blkX*blkW;
            const size_t outPxY = (blkY*blkH)+row;
            uint8_t* inPxData = (uint8_t*)data;
            uint8_t* inPx = &inPxData[outPxY*width*bytesPerPixel + outPxX*bytesPerPixel];
            uint8_t* outTmp = &tmpOutPxIn[row*blkW*outBytesPerPixel];
            for (size_t col = 0; col < blkImgWidth; ++col) {
                uint8_t r = *inPx++;
                uint8_t g = *inPx++;
                uint8_t b = *inPx++;
                uint8_t a = (bytesPerPixel == 4) ? *inPx++ : 255;

                *outTmp++ = r;
                *outTmp++ = g;
                *outTmp++ = b;
                *outTmp++ = a;
            }
        }


        // rearrange into ARARAR...GBGBGB... order
        uint8_t* aPtr = &tmpOutPxOut[0];
        uint8_t* rPtr = &tmpOutPxOut[1];
        uint8_t* gPtr = &tmpOutPxOut[32];
        uint8_t* bPtr = &tmpOutPxOut[33];
        uint8_t* tmpPtr = tmpOutPxIn.data();
        for (size_t px = 0; px < 16; ++px) {
            uint8_t r = *tmpPtr++;
            uint8_t g = *tmpPtr++;
            uint8_t b = *tmpPtr++;
            uint8_t a = *tmpPtr++;
            *aPtr = a;
            *rPtr = r;
            *gPtr = g;
            *bPtr = b;
            aPtr += 2;
            rPtr += 2;
            gPtr += 2;
            bPtr += 2;
        }

        // store the reordered pixels in the output texture
        for (size_t px = 0; px < 64; ++px) {
            *outPx++ = tmpOutPxOut[px];
        }
    }
    }

    return true;
}

} // namespace Wii
} // namespace GameLib

