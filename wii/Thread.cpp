#include <Thread.h>
#include <bjdebug.h>

namespace GameLib
{
namespace Wii
{

std::map<lwp_t,u64> Thread::sRetVals;

void* Thread::sThreadFcnWrapper(void* arg)
{
    Thread* thr = (Thread*)arg;
    thr->mFcn(thr->mArg);
    return (void*)0;
}

Thread::Thread() :
    mArg(nullptr),
    mLwp(0),
    mStarted(false),
    mJoined(false)
{}

Thread::~Thread()
{
    if (mStarted && !mJoined) {
        LWP_JoinThread(mLwp, nullptr);
    }
}

bool Thread::Start(ThreadFcn fcn, void* arg, const char* name)
{
    mFcn = fcn;
    mArg = arg;

    if (LWP_CreateThread(
            &mLwp,
            sThreadFcnWrapper,
            (void*)this,
            (void*)mStack,
            sizeof(mStack),
            sThreadPrio) < 0)
    {
        dbgprint("Failed to create lwp thread\n");
        return false;
    }

    mStarted = true;
    mJoined = false;
    return true;
}

void Thread::Join(u64* retVal)
{
    if (LWP_JoinThread(mLwp, nullptr) < 0) {
        dbgprint("Failed to join thread\n");
        return;
    }
    if (retVal) {
        *retVal = sRetVals[mLwp];
    }
    sRetVals.erase(mLwp);
    mJoined = true;
    mStarted = false;
}

u64 Thread::GetThreadId()
{
    return (u64)LWP_GetSelf();
}

void Thread::Yield()
{
    LWP_YieldThread();
}

void Thread::Exit(u64 retVal)
{
    lwp_t self = LWP_GetSelf();
    sRetVals[self] = retVal;
}

} // namespace PS3
} // namespace GameLib

