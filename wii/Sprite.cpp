#include <Sprite.h>
#include <cstdio>

namespace GameLib
{
namespace Wii
{

Sprite::Sprite() :
    mTexture(nullptr),
    mX(0), mY(0),
    mU(0), mV(0),
    mW(1.0f), mH(1.0f),
    mWidth(1.0f), mHeight(1.0f),
    mRotDegrees(0.0f)
{}

Sprite::~Sprite()
{}

bool Sprite::Init(Texture* tex)
{
    mTexture = tex;
    if (!mTexture) {
        printf("Sprite init mTex is null\n");
        return false;
    }

    mWidth = tex->mWidth;
    mHeight = tex->mHeight;
    mU = mV = 0.0f;
    mW = 1.0f;
    mH = 1.0f;

    // top left corner
    mX = -1.0f;
    mY = 1.0f;
    
    mRotDegrees = 0.0f;

    return true;
}

} // namespace Wii
} // namespace GameLib


