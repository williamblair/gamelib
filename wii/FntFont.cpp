#include <FntFont.h>
#include <Targa.h>
#include <Bmp.h>
#include <fstream>
#include <sstream>
#include <cstring>
#include <bjdebug.h>

namespace GameLib
{
namespace Wii
{

FntFont::FntFont() :
    mLineHeight(0),
    mDrawScale(1.0f)
{}

FntFont::~FntFont()
{}

bool FntFont::Init(
    const char* fileName,
    const char* texFileName,
    uint8_t r,
    uint8_t g,
    uint8_t b)
{
    if (!LoadFntFile(fileName)) {
        return false;
    }
    
    if (!LoadTexture(texFileName, r, g, b)) {
        return false;
    }
    
    return true;
}

bool FntFont::LoadFntFile(const char* fileName)
{
    std::ifstream infile(fileName);
    if (!infile.is_open()) {
        dbgprint("Failed to open %s\n", fileName);
        return false;
    }
    
    std::string curLine;
    while (std::getline(infile, curLine))
    {
        std::stringstream ss(curLine);
        std::string firstWord;
        ss >> firstWord;
        
        if (firstWord == "char")
        {
            CharInfo curInfo;
            std::string propertyStr;
            int value;
            while (ss >> propertyStr)
            {
                char property[256];
                sscanf(propertyStr.c_str(), "%[^=]=%d", property, &value);
                propertyStr = std::string(property);
                
                if (propertyStr == "id") {
                    curInfo.id = value;
                } else if (propertyStr == "x") {
                    curInfo.x = value;
                } else if (propertyStr == "y") {
                    curInfo.y = value;
                } else if (propertyStr == "width") {
                    curInfo.w = value;
                } else if (propertyStr == "height") {
                    curInfo.h = value;
                } else if (propertyStr == "xoffset") {
                    curInfo.xoffset = value;
                } else if (propertyStr == "yoffset") {
                    curInfo.yoffset = value;
                } else if (propertyStr == "xadvance") {
                    curInfo.xadvance = value;
                } else if (propertyStr == "page") {
                    curInfo.page = value;
                } else if (propertyStr == "channel") {
                    curInfo.channel = value;
                }
            }
            
            mCharInfo[curInfo.id] = curInfo;
        }
        else if (firstWord == "common")
        {
            std::string propertyStr;
            int value;
            while (ss >> propertyStr)
            {
                size_t propIndex = 0;
                char property[256];
                sscanf(propertyStr.c_str(), "%[^=]=%d", property, &value);
                propertyStr = std::string(property);
                if (propertyStr == "lineHeight") {
                    mLineHeight = value;
                }
            }
        }
    }
    
    return true;
}

bool FntFont::LoadTexture(
    const char* texFileName,
    uint8_t r, uint8_t g, uint8_t b)
{
    std::string fileNameStr(texFileName);
    std::string fileExt = "";
    fileExt += fileNameStr[fileNameStr.size()-3];
    fileExt += fileNameStr[fileNameStr.size()-2];
    fileExt += fileNameStr[fileNameStr.size()-1];
    
    if (fileExt == "tga" || fileExt == "TGA") {
        return LoadFromTga(texFileName, r,g,b);
    } else if (fileExt == "BMP" || fileExt == "bmp") {
        return LoadFromBMP(texFileName, r,g,b);
    }
    
    dbgprint("Unhandled FntFont texture type: %s\n", fileExt.c_str());
    return false;
}

static inline uint16_t lend2bend(uint16_t val)
{
    uint16_t result;
    uint8_t* inPtr = (uint8_t*)&val;
    uint8_t* outPtr = (uint8_t*)&result;
    outPtr[1] = inPtr[0];
    outPtr[0] = inPtr[1];
    return result;
}

bool FntFont::LoadFromTga(
    const char* texFileName,
    uint8_t r, uint8_t g, uint8_t b)
{
    std::ifstream inFile(texFileName, std::ios::binary);
    if (!inFile.is_open()) {
        dbgprint(" failed to open targa image file: %s\n", 
            texFileName);
        return false;
    }

    inFile.read((char*)&Targa::header, sizeof(Targa::Header));
    Targa::header.xOrigin = lend2bend(Targa::header.xOrigin);
    Targa::header.yOrigin = lend2bend(Targa::header.yOrigin);
    Targa::header.width = lend2bend(Targa::header.width);
    Targa::header.height = lend2bend(Targa::header.height);
    if (!Targa::IsImageTypeSupported(Targa::header)) {
        dbgprint(" unsupported targa type\n");
        return false;
    }

    Targa::width = Targa::header.width;
    Targa::height = Targa::header.height;

    Targa::bitsPerPixel = Targa::header.bpp;
    Targa::bytesPerPixel = Targa::header.bpp / 8;

    if (Targa::bytesPerPixel < 3) {
        dbgprint(" unsupported bytesPerPixel: %u\n",
            Targa::bytesPerPixel);
        return false;
    }

    dbgprint("Targa width, height, bytesPerPixel: %u, %u, %u\n",
        Targa::width, Targa::height, Targa::bytesPerPixel);
    unsigned int imageSize = Targa::width * Targa::height * Targa::bytesPerPixel;
    Targa::imageData.resize(imageSize);

    // skip past the id if there is one
    if (Targa::header.idLength > 0) {
        inFile.ignore(Targa::header.idLength);
    }

    bool result = false;

    if (Targa::IsUncompressed(Targa::header)) {
        dbgprint("Targa load uncompressed\n");
        result = Targa::LoadUncompressed(inFile);
    } else {
        printf("Targa load compressed\n");
        result = Targa::LoadCompressed(inFile);
    }
    
    if ((Targa::header.imageDesc & Targa::TOP_LEFT) == Targa::TOP_LEFT) {
        dbgprint("Targa flip image vertically\n");
        Targa::FlipImageVertically();
    }

    if (!result) {
        dbgprint("failed to load targa\n");
        return false;
    }
    
    // replace black with transparent and white with desired color
    std::vector<uint8_t> resultPxData(
        Targa::width * Targa::height * 4); // always 4 bpp
    uint8_t* tgaPx = Targa::imageData.data();
    uint8_t* resPx = resultPxData.data();
    dbgprint("Setting FntFont pixel colors\n");
    dbgprint("resultPxData size, targa img data size: %lu, %lu\n",
        resultPxData.size(), Targa::imageData.size());
    for (size_t i=0; i<Targa::width*Targa::height; ++i)
    {
        uint8_t tgaR = *tgaPx++;
        uint8_t tgaG = *tgaPx++;
        uint8_t tgaB = *tgaPx++;
        uint8_t tgaA = 255;
        if (Targa::bytesPerPixel == 4) {
            tgaA = *tgaPx++;
        }
        (void)tgaA;
        
        // black == transparent
        if (tgaR == 0 && tgaG == 0 && tgaB == 0) {
            *resPx++ = 0;
            *resPx++ = 0;
            *resPx++ = 0;
            *resPx++ = 0; // alpha = transparent
        } 
        // white/colored == set px color to desired font color
        else {
            *resPx++ = r;
            *resPx++ = g;
            *resPx++ = b;
            *resPx++ = 255; // alpha = opaque
        }
    }
    
    result = mTexture.Init(
        (const char*)resultPxData.data(),
        Targa::width,
        Targa::height,
        4 // bytes per pixel
    );
    Targa::imageData.clear();
    Targa::width = 0;
    Targa::height = 0;
    Targa::bytesPerPixel = 0;
    if (!result) {
        return false;
    }
    
    // update CharInfo based on texture info now that it's loaded
    for (auto& pair : mCharInfo)
    {
        CharInfo& info = pair.second;
        
        info.u = float(info.x) / float(mTexture.GetWidth());
        info.v = (float(info.y) / float(mTexture.GetHeight()));
        info.uWidth = float(info.w) / float(mTexture.GetWidth());
        info.vHeight = float(info.h) / float(mTexture.GetHeight());
    }
    
    return mSprite.Init(&mTexture);
}

bool FntFont::LoadFromBMP(const char* fileName, uint8_t r, uint8_t g, uint8_t b)
{
    std::vector<uint8_t> imageData;
    int width = -1;
    int height = -1;
    if (!BMP::Load(fileName, imageData, width, height)) {
        return false;
    }

    // replace black with transparent and white with desired color
    std::vector<uint8_t> resultPxData(width * height * 4); // always 4 bpp
    uint8_t* bmpPx = imageData.data();
    uint8_t* resPx = resultPxData.data();
    printf("Setting FntFont pixel colors\n");
    printf("resultPxData size, bmp img data size: %u, %u\n",
        resultPxData.size(), imageData.size());
    for (size_t i=0; i<(size_t)width*height; ++i)
    {
        uint8_t bmpR = *bmpPx++;
        uint8_t bmpG = *bmpPx++;
        uint8_t bmpB = *bmpPx++;
        uint8_t bmpA = *bmpPx++;
        (void)bmpA;
        
        // black == transparent
        if (bmpR == 0 && bmpG == 0 && bmpB == 0) {
            *resPx++ = 0;
            *resPx++ = 0;
            *resPx++ = 0;
            *resPx++ = 0; // alpha = transparent
        } 
        // white/colored == set px color to desired font color
        else {
            *resPx++ = r;
            *resPx++ = g;
            *resPx++ = b;
            *resPx++ = 255; // alpha = opaque
        }
    }
    
    bool result = mTexture.Init(
        (const char*)resultPxData.data(),
        width,
        height,
        4 // bytes per pixel
    );
    if (!result) {
        return false;
    }
    
    // update CharInfo based on texture info now that it's loaded
    for (auto& pair : mCharInfo)
    {
        CharInfo& info = pair.second;
        
        info.u = float(info.x) / float(mTexture.GetWidth());
        info.v = float(info.y) / float(mTexture.GetHeight());
        info.uWidth = float(info.w) / float(mTexture.GetWidth());
        info.vHeight = float(info.h) / float(mTexture.GetHeight());
    }
    
    return mSprite.Init(&mTexture);
}

void FntFont::Draw(const char* msg, float x, float y, Renderer& render)
{
    // clip UVs 0..1
    //const float uWidthNormalized = (float)mCharWidth / (float)mTexture->GetWidth();
    //const float vHeightNormalized = (float)mCharHeight / (float)mTexture->GetHeight();

    const int msgLen = strlen(msg);
    for (int i=0; i<msgLen; ++i)
    {
        char letter = msg[i];
        
        // don't draw unsupported characters
        if (mCharInfo.find(letter) == mCharInfo.end()) {
            continue;
        }
        
        CharInfo& info = mCharInfo[letter];
        
        float charWidth = info.w;
        float charHeight = info.h;
        
        // width height % of screen 0..1
        const float scrWidthNormalized = charWidth / (float)render.GetWidth();
        const float scrHeightNormalized = charHeight / (float)render.GetHeight();
        const float xAdvNormalized = (float)info.xadvance / (float)render.GetWidth();
        const float xOffsNormalized = 2.0f * mDrawScale * (float)info.xoffset / (float)render.GetWidth();
        const float yOffsNormalized = 2.0f * mDrawScale * (float)info.yoffset / (float)render.GetHeight();
        mSprite.SetWH(
            scrWidthNormalized * mDrawScale,
            scrHeightNormalized * mDrawScale
        );
        
        // Skip if no need to draw anything
        if (letter == ' ') {
            // scale from 0..1 to -1..1
            x += xAdvNormalized * 2.0f * mDrawScale;
            continue;
        }
        
        mSprite.SetUVWH(info.u, info.v, info.uWidth, info.vHeight);
        mSprite.SetXY(x + xOffsNormalized, y - yOffsNormalized);
        
        // scale from 0..1 to -1..1
        x += xAdvNormalized * 2.0f * mDrawScale;
        render.DrawSprite(mSprite);
    }
}

} // namespace Wii
} // namespace GameLib

