#include <Renderer.h>
#include <asndlib.h>

namespace GameLib
{
namespace Wii
{

#define MTXCAST(m) ((f32(*)[4])(m).v)

GXTexObj texture;

Renderer::Renderer()
{}

Renderer::~Renderer()
{}

#define DEFAULT_FIFO_SIZE (256*1024)

bool Renderer::Init(int width, int height, bool fullScreen, const char* title)
{
    // init the vi.
    VIDEO_Init();
    WPAD_Init();
    // TODO - move to platform init
    // init audio subsystem
    ASND_Init();
    
    mRMode = VIDEO_GetPreferredMode(NULL);
    
    // allocate 2 framebuffers for doublebuffering
    mFrameBuffer[0] = MEM_K0_TO_K1(SYS_AllocateFramebuffer(mRMode));
    mFrameBuffer[1] = MEM_K0_TO_K1(SYS_AllocateFramebuffer(mRMode));

    // initialize console, required for printf
    console_init(
        mFrameBuffer[0],
        20,20,
        mRMode->fbWidth, mRMode->xfbHeight,
        mRMode->fbWidth*VI_DISPLAY_PIX_SZ
    );
    
    VIDEO_Configure(mRMode);
    VIDEO_SetNextFramebuffer(mFrameBuffer[mFbIndex]);
    VIDEO_SetBlack(FALSE);
    VIDEO_Flush();
    VIDEO_WaitVSync();
    if (mRMode->viTVMode & VI_NON_INTERLACE) VIDEO_WaitVSync();
    
    // set up the fifo and then init the flipper
    void* gpFifo = NULL;
    gpFifo = memalign(32, DEFAULT_FIFO_SIZE);
    memset(gpFifo, 0, DEFAULT_FIFO_SIZE);
    
    GX_Init(gpFifo, DEFAULT_FIFO_SIZE);
    
    // clears the bg to color and clears the zbuffer
    GX_SetCopyClear(mBgColor, 0x00FFFFFF);
    
    // other gx setup
    GX_SetViewport(0,0, mRMode->fbWidth, mRMode->efbHeight, 0, 1);
    yscale = GX_GetYScaleFactor(mRMode->efbHeight, mRMode->xfbHeight);
    xfbHeight = GX_SetDispCopyYScale(yscale);
    GX_SetScissor(0,0, mRMode->fbWidth, mRMode->efbHeight);
    GX_SetDispCopySrc(0,0, mRMode->fbWidth, mRMode->efbHeight);
    GX_SetDispCopyDst(mRMode->fbWidth, mRMode->efbHeight);
    GX_SetCopyFilter(mRMode->aa, mRMode->sample_pattern, GX_TRUE, mRMode->vfilter);
    GX_SetFieldMode(mRMode->field_rendering, ((mRMode->viHeight == 2*mRMode->xfbHeight) ? GX_ENABLE : GX_DISABLE));
    
    if (mRMode->aa) {
        GX_SetPixelFmt(GX_PF_RGB565_Z16, GX_ZC_LINEAR);
    }
    else {
        GX_SetPixelFmt(GX_PF_RGB8_Z24, GX_ZC_LINEAR);
    }
    
    GX_SetCullMode(GX_CULL_NONE);
    GX_CopyDisp(mFrameBuffer[mFbIndex], GX_TRUE);
    GX_SetDispCopyGamma(GX_GM_1_0);
    
    // setup vertex descriptor
    // tells the flipper to expect direct data
    GX_ClearVtxDesc();
    GX_SetVtxDesc(GX_VA_POS, GX_DIRECT);
    GX_SetVtxDesc(GX_VA_CLR0, GX_DIRECT);
    GX_SetVtxDesc(GX_VA_TEX0, GX_DIRECT);
    
    // setup the vertex attribute table
    // describes the data
    // args: vat location 0-7, type fo data, data format, size, scale
    // so for ex. in teh first call we are sending position data with
    // 3 values X,Y,Z of size F32. scale sets the number of fractional
    // bits for non float data
    GX_SetVtxAttrFmt(GX_VTXFMT0, GX_VA_POS, GX_POS_XYZ, GX_F32, 0);
    GX_SetVtxAttrFmt(GX_VTXFMT0, GX_VA_TEX0, GX_TEX_ST, GX_F32, 0);

    // for drawing lines for bounding box drawing
    GX_SetVtxAttrFmt(GX_VTXFMT1, GX_VA_POS, GX_POS_XYZ, GX_F32, 0);
    GX_SetVtxAttrFmt(GX_VTXFMT1, GX_VA_CLR0, GX_CLR_RGBA, GX_RGB8, 0);
    
    // set number of rasterized color channels
    GX_SetNumChans(1);
    // set number of textures to generate
    GX_SetNumTexGens(1);
    //GX_SetNumTexGens(0);
    
    // set texture coordinate generation
    // args: texcoord slot 0-7, matrix type, source to generate texture coordinates from, matrix to use
    //GX_SetTexCoordGen(GX_TEXCOORD0, GX_TG_MTX2x4, GX_TG_TEX0, GX_IDENTITY);
    //GX_InvVtxCache();
    //GX_InvalidateTexAll();
    
    GX_SetTevOp(GX_TEVSTAGE0, GX_BLEND);
    GX_SetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD0, GX_TEXMAP0, GX_COLOR0A0);
    //GX_SetTevOp(GX_TEVSTAGE0, GX_PASSCLR);
    //GX_SetTevOrder(GX_TEVSTAGE0, GX_TEXCOORDNULL, GX_TEXMAP_NULL, GX_COLOR0A0);
    
    // setup projection matrix
    f32 w = mRMode->viWidth;
    f32 h = mRMode->viHeight;
    //guPerspective((f32(*)[4])mPerspMat.v, 45, (f32)w/h, 0.1f, 300.0f);
    guPerspective((f32(*)[4])mPerspMat.v, 60, (f32)w/h, 1.0f, 500.0f);
    GX_LoadProjectionMtx((f32(*)[4])mPerspMat.v, GX_PERSPECTIVE);
    
    // Set wiimote movement size and we wamt buttons, acceleromotor, and IR data
    WPAD_SetVRes(0, mRMode->fbWidth, mRMode->efbHeight);
    WPAD_SetDataFormat(WPAD_CHAN_0, WPAD_FMT_BTNS_ACC_IR);
    
    // Enable filesystem access
    if (!fatInitDefault()) {
        printf("ERROR - failed to init fat FS\n");
        return false;
    }
    return true;
}

void Renderer::DrawAabb(AABB& aabb, GameMath::Mat4& viewMat)
{
    // just view, world position is stored in aabb.pos and used below
    GameMath::Mat4 modelview;
    modelview = viewMat;

    // setup vertex descriptor
    // tells the flipper to expect direct data
    GX_ClearVtxDesc();
    GX_SetVtxDesc(GX_VA_POS, GX_DIRECT);
    GX_SetVtxDesc(GX_VA_CLR0, GX_DIRECT);
    GX_SetTevOp(GX_TEVSTAGE0, GX_PASSCLR);
    GX_SetTevOrder(GX_TEVSTAGE0, GX_TEXCOORDNULL, GX_TEXMAP_NULL, GX_COLOR0A0);
    
    GX_LoadProjectionMtx((f32(*)[4])mPerspMat.v, GX_PERSPECTIVE);
    GX_LoadPosMtxImm(MTXCAST(modelview), GX_PNMTX0);

    const int vertexCount = 2*12;
    GX_Begin(GX_LINES, GX_VTXFMT1, vertexCount);
        // world space min and max
        GameMath::Vec3 min = aabb.pos + aabb.min;
        GameMath::Vec3 max = aabb.pos + aabb.max;
        // TODO - fix color not working (draws white)
        u8 r = 255;
        u8 g = 0;
        u8 b = 0;
        
        // bottom left
        GX_Position3f32(min.x, min.y, min.z);
        GX_Color3u8(r,g,b);
        GX_Position3f32(min.x, min.y, max.z);
        GX_Color3u8(r,g,b);

        // bottom right
        GX_Position3f32(max.x, min.y, min.z);
        GX_Color3u8(r,g,b);
        GX_Position3f32(max.x, min.y, max.z);
        GX_Color3u8(r,g,b);

        // bottom front
        GX_Position3f32(min.x, min.y, max.z);
        GX_Color3u8(r,g,b);
        GX_Position3f32(max.x, min.y, max.z);
        GX_Color3u8(r,g,b);

        // bottom back
        GX_Position3f32(min.x, min.y, min.z);
        GX_Color3u8(r,g,b);
        GX_Position3f32(max.x, min.y, min.z);
        GX_Color3u8(r,g,b);

        // top left
        GX_Position3f32(min.x, max.y, min.z);
        GX_Color3u8(r,g,b);
        GX_Position3f32(min.x, max.y, max.z);
        GX_Color3u8(r,g,b);

        // top right
        GX_Position3f32(max.x, max.y, min.z);
        GX_Color3u8(r,g,b);
        GX_Position3f32(max.x, max.y, max.z);
        GX_Color3u8(r,g,b);

        // top front
        GX_Position3f32(min.x, max.y, max.z);
        GX_Color3u8(r,g,b);
        GX_Position3f32(max.x, max.y, max.z);
        GX_Color3u8(r,g,b);

        // top back
        GX_Position3f32(min.x, max.y, min.z);
        GX_Color3u8(r,g,b);
        GX_Position3f32(max.x, max.y, min.z);
        GX_Color3u8(r,g,b);

        // vertical back left
        GX_Position3f32(min.x, min.y, min.z);
        GX_Color3u8(r,g,b);
        GX_Position3f32(min.x, max.y, min.z);
        GX_Color3u8(r,g,b);

        // vertical back right
        GX_Position3f32(max.x, min.y, min.z);
        GX_Color3u8(r,g,b);
        GX_Position3f32(max.x, max.y, min.z);
        GX_Color3u8(r,g,b);

        // vertical front left
        GX_Position3f32(min.x, min.y, max.z);
        GX_Color3u8(r,g,b);
        GX_Position3f32(min.x, max.y, max.z);
        GX_Color3u8(r,g,b);

        // vertical front right
        GX_Position3f32(max.x, min.y, max.z);
        GX_Color3u8(r,g,b);
        GX_Position3f32(max.x, max.y, max.z);
        GX_Color3u8(r,g,b);
        
    GX_End();
}

void Renderer::Update()
{

    // do this stuff after drawing
    GX_DrawDone();
    
    // flip framebuffer
    mFbIndex ^= 1;
    GX_SetZMode(GX_TRUE, GX_LEQUAL, GX_TRUE);
    GX_SetBlendMode(GX_BM_BLEND, GX_BL_SRCALPHA, GX_BL_INVSRCALPHA, GX_LO_SET);
    GX_SetColorUpdate(GX_TRUE);
    GX_SetAlphaUpdate(GX_TRUE);
    GX_CopyDisp(mFrameBuffer[mFbIndex], GX_TRUE);
    
    VIDEO_SetNextFramebuffer(mFrameBuffer[mFbIndex]);
    VIDEO_Flush();
    VIDEO_WaitVSync();

    // VT Terminal escape codes
    // positions cursor on row 2, column 0
    printf("\x1b[2;0H");
}

// TODO - reorganize
void Renderer::DrawVertexBuffer(
    GameMath::Mat4& modelMat,
    GameMath::Mat4& viewMat,
    const VertexBuffer& vb
)
{
    GameMath::Mat4 modelview;
    modelview = viewMat * modelMat;

    // setup vertex descriptor
    // tells the flipper to expect direct data
    GX_ClearVtxDesc();
    GX_SetVtxDesc(GX_VA_POS, GX_DIRECT);
    if (vb.mFloatsPerVertex == 6) { // xyz rgb
        GX_SetVtxDesc(GX_VA_CLR0, GX_DIRECT);
    }
    else if (vb.mFloatsPerVertex == 5) { // xyz uv
        GX_SetVtxDesc(GX_VA_TEX0, GX_DIRECT);
    }
    
    //if (projMat == nullptr) {
        GX_LoadProjectionMtx((f32(*)[4])mPerspMat.v, GX_PERSPECTIVE);
    //}
    //else {
        // TODO - assumes this is for 2d sprite drawing
    //    GX_LoadProjectionMtx((f32(*)[4])projMat->v, GX_PERSPECTIVE);
    //}
    
    // load the modelview matrix into matrix memory
    GX_LoadPosMtxImm(MTXCAST(modelview), GX_PNMTX0);
    //GX_LoadPosMtxImm(view, GX_PNMTX0);
    
    // position only, non indexed
    if (vb.mFloatsPerVertex == 3 && vb.mIndicesSize == 0)
    {        
        int vertexCount = vb.mVerticesSize / vb.mFloatsPerVertex;
        GX_Begin(GX_TRIANGLES, GX_VTXFMT0, vertexCount);
            for (size_t i = 0; i < vb.mVerticesSize; i += vb.mFloatsPerVertex)
            {
                GX_Position3f32(vb.mVertices[i+0], vb.mVertices[i+1], vb.mVertices[i+2]);
            }
        GX_End();
        return/* true*/;
    }
    // position only, indexed
    if (vb.mFloatsPerVertex == 3 && vb.mIndicesSize > 0)
    {
        int vertexCount = vb.mIndicesSize;
        GX_Begin(GX_TRIANGLES, GX_VTXFMT0, vertexCount);
            for (size_t i = 0; i < vb.mIndicesSize; ++i)
            {
                float* vert = &vb.mVertices[vb.mIndices[i]*3];
                GX_Position3f32(vert[0], vert[1], vert[2]);
            }
        GX_End();
        return/* true*/;
    }
    // position and tex coord, non indexed
    if (vb.mFloatsPerVertex == 5 && vb.mIndicesSize == 0)
    {
        int vertexCount = vb.mVerticesSize / vb.mFloatsPerVertex;
        GX_Begin(GX_TRIANGLES, GX_VTXFMT0, vertexCount);
            for (size_t i = 0; i < vb.mVerticesSize; i += vb.mFloatsPerVertex)
            {
                GX_Position3f32(vb.mVertices[i+0], vb.mVertices[i+1], vb.mVertices[i+2]);
                GX_TexCoord2f32(vb.mVertices[i+3], vb.mVertices[i+4]);
            }
        GX_End();
        return/* true*/;
    }
    // position and tex coord, indexed
    if (vb.mFloatsPerVertex == 5 && vb.mIndicesSize > 0)
    {
        int vertexCount = vb.mIndicesSize;
        GX_Begin(GX_TRIANGLES, GX_VTXFMT0, vertexCount);
            for (size_t i = 0; i < vb.mIndicesSize; ++i)
            {
                float* vert = &vb.mVertices[vb.mIndices[i]*5];
                GX_Position3f32(vert[0], vert[1], vert[2]);
                GX_TexCoord2f32(vert[3], vert[4]);
            }
        GX_End();
        return/* true*/;
    }
    // position and color, non indexed
    if (vb.mFloatsPerVertex == 6 && vb.mIndicesSize == 0)
    {
        GX_SetVtxDesc(GX_VA_POS, GX_DIRECT);
        GX_SetVtxDesc(GX_VA_CLR0, GX_DIRECT);
        GX_SetTevOp(GX_TEVSTAGE0, GX_PASSCLR);
        GX_SetTevOrder(GX_TEVSTAGE0, GX_TEXCOORDNULL, GX_TEXMAP_NULL, GX_COLOR0A0);
        int vertexCount = vb.mVerticesSize / vb.mFloatsPerVertex;
        GX_Begin(GX_TRIANGLES, GX_VTXFMT1, vertexCount); // fmt1 = pos,color
            for (size_t i = 0; i < vb.mVerticesSize; i += vb.mFloatsPerVertex)
            {
                GX_Position3f32(vb.mVertices[i+0], vb.mVertices[i+1], vb.mVertices[i+2]);
                GX_Color3f32(vb.mVertices[i+3], vb.mVertices[i+4], vb.mVertices[i+5]);
            }
        GX_End();
        return /* true */;
    }
    if (vb.mFloatsPerVertex == 6 && vb.mIndicesSize > 0)
    {
        GX_SetVtxDesc(GX_VA_POS, GX_DIRECT);
        GX_SetVtxDesc(GX_VA_CLR0, GX_DIRECT);
        GX_SetTevOp(GX_TEVSTAGE0, GX_PASSCLR);
        GX_SetTevOrder(GX_TEVSTAGE0, GX_TEXCOORDNULL, GX_TEXMAP_NULL, GX_COLOR0A0);
        int vertexCount = vb.mIndicesSize;
        GX_Begin(GX_TRIANGLES, GX_VTXFMT1, vertexCount); // fmt1 = pos,color
            for (size_t i = 0; i < vb.mIndicesSize; ++i)
            {
                float* vert = &vb.mVertices[vb.mIndices[i]*6];
                GX_Position3f32(vert[0], vert[1], vert[2]);
                GX_Color3f32(vert[3], vert[4], vert[5]);
            }
        GX_End();
        return;
    }
    printf("Unhandled floats per vertex: %u\n", vb.mFloatsPerVertex);
}

void Renderer::DrawSprite(Sprite& sprite)
{
    // updated tex coords
    float leftU = sprite.mU;
    float rightU = leftU + sprite.mW;
    float topV = sprite.mV;
    float bottomV = (sprite.mV + sprite.mH);
    float drawVerts[5*4] = {
        // position         texcoord
        0.0f, 0.0f, 0.0f,   leftU, topV, // top left
        0.0f, -1.0f, 0.0f,   leftU, bottomV, // bottom left
        1.0f, -1.0f, 0.0f,   rightU, bottomV, // bottom right
        1.0f, 0.0f, 0.0f,   rightU, topV // top right
    };
    int indices[3*2] = {
        1, 2, 3,
        1, 3, 0
    };

    SetTexture(*sprite.mTexture);

    // scale size from 0..1 to -1..1
    float normW = sprite.mWidth * 2.0f;
    float normH = sprite.mHeight * 2.0f;
    GameMath::Mat4 identMat; // for projection = identity
    GameMath::Mat4 mvpMat =
        GameMath::Translate(sprite.mX, sprite.mY, 0.0f) *
        
        // need to offset by w/2, h/2 because we draw relative to top left corner,
        // as opposed to the center of the image
        GameMath::Translate(normW/2,-normH/2,0.0f) *
        GameMath::Rotate(GameMath::Deg2Rad(sprite.mRotDegrees),
            GameMath::Vec3(0.0f, 0.0f, 1.0f)) *
        GameMath::Translate(-normW/2,normH/2,0.0f) *
        
        GameMath::Scale(normW, normH, 1.0f);
    
    // disable depth test
    GX_SetZMode(GX_FALSE, 0,0);
    
    // setup vertex descriptor
    // tells the flipper to expect direct data
    GX_ClearVtxDesc();
    // xyz uv
    GX_SetVtxDesc(GX_VA_POS, GX_DIRECT);
    GX_SetVtxDesc(GX_VA_TEX0, GX_DIRECT);

    //GX_LoadProjectionMtx((f32(*)[4])identMat.v, GX_PERSPECTIVE);
    GX_LoadProjectionMtx((f32(*)[4])identMat.v, GX_ORTHOGRAPHIC);
    GX_LoadPosMtxImm(MTXCAST(mvpMat), GX_PNMTX0);

    // 2 triangles
    GX_Begin(GX_TRIANGLES, GX_VTXFMT0, 3*2);
        for (int i=0; i<3*2; ++i) {
            float* v = &drawVerts[indices[i]*5];
            GX_Position3f32(v[0], v[1], /*v[2]*/-1.0f);
            GX_TexCoord2f32(v[3], 1.0f - v[4]);
        }
    GX_End();

    // enable depth test
    GX_SetZMode(GX_TRUE, GX_LEQUAL, GX_TRUE);
}

} // namespace Wii
} // namespace GameLib

