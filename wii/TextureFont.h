#ifndef GAMELIB_WII_TEXTUREFONT_H_INCLUDED
#define GAMELIB_WII_TEXTUREFONT_H_INCLUDED

#include <Texture.h>
#include <Sprite.h>

namespace GameLib
{
namespace Wii
{

// forward declaration
class Renderer;

class TextureFont
{
public:

    TextureFont();
    ~TextureFont();

    // char width,height are size in pixels of 1 character
    bool Init(Texture* tex, float charWidth, float charHeight);

    // Draw string to the screen with the top left at x,y
    // x,y are normalized (-1..1)
    void Draw(const char* msg, float x, float y, Renderer& render);

private:
    Texture* mTexture;
    Sprite mSprite;
    float mCharWidth;
    float mCharHeight;
    int mCharsPerRow;
};

} // namespace Wii
} // namespace GameLib

#endif // GAMELIB_WII_TEXTUREFONT_H_INCLUDED

