#include <bjdebug.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

static FILE* fp = nullptr;
static char buf[256];

void dbgprint(const char* fmtstr, ...)
{
    va_list valist;
    va_start(valist, fmtstr);
    vsprintf(buf, fmtstr, valist);
    va_end(valist);
    
    if (fp == nullptr) {
        fp = fopen(
            "sd:/bjdbg.txt",
            "w"
        );
    }
    
    //sceIoWrite(fd, buf, strlen(buf));
    fprintf(fp, buf);
}

