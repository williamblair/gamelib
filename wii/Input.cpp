#include <Input.h>

namespace GameLib
{
namespace Wii
{

Input::Input() :
    mConfirmClicked(false),
    mUp(false),
    mDown(false),
    mLeft(false),
    mRight(false),
    mUpClicked(false),
    mDownClicked(false),
    mLeftClicked(false),
    mRightClicked(false),
    mWiimoteX(0.0f),
    mWiimoteY(0.0f),
    mLastWiimoteX(0.0f),
    mLastWiimoteY(0.0f)
{
}

Input::~Input()
{
}

bool Input::Init()
{
    return true;
}

void Input::Update()
{
    WPAD_ScanPads();
    mDownBtns = WPAD_ButtonsDown(0);
    u32 held = WPAD_ButtonsHeld(0);
    
    // IR movement
    WPAD_IR(0, &mIr);
    
    mWiimoteX = mIr.x;
    mWiimoteY = mIr.y;
    
    bool lastUp = mUp;
    bool lastDown = mDown;
    bool lastLeft = mLeft;
    bool lastRight = mRight;
    
    mConfirmClicked = false;
    mUp = mUpClicked = false;
    mDown = mDownClicked = false;
    mLeft = mLeftClicked = false;
    mRight = mRightClicked = false;
    if (mDownBtns & WPAD_BUTTON_A) { mConfirmClicked = true; }
    
    if (held & WPAD_BUTTON_UP) { mUp = true; }
    if (held & WPAD_BUTTON_DOWN) { mDown = true; }
    if (held & WPAD_BUTTON_LEFT) { mLeft = true; }
    if (held & WPAD_BUTTON_RIGHT) { mRight = true; }
    
    if (!mUp && lastUp) { mUpClicked = true; }
    if (!mDown && lastDown) { mDownClicked = true; }
    if (!mLeft && lastLeft) { mLeftClicked = true; }
    if (!mRight && lastRight) { mRightClicked = true; }

    mLastWiimoteX = mWiimoteX;
    mLastWiimoteY = mWiimoteY;
}

} // namespace Wii
} // namespace GameLib

