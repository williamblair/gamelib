#ifndef GAMELIB_WII_MUSIC_H_INCLUDED
#define GAMELIB_WII_MUSIC_H_INCLUDED

namespace GameLib
{
namespace Wii
{

class Music
{
public:

    Music();
    ~Music();

    // Must be an OGG file
    bool Init(const char* fileName);
    
    void Play(bool looping);
    void Stop();
    void Pause();
    void Resume();

private:
    unsigned char* mBuffer;
    int mBufferLen;
    int mVoice;
};

} // namespace Wii
} // namespace GameLib

#endif // GAMELIB_WII_MUSIC_H_INCLUDED

