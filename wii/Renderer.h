#ifndef GAMELIB_WII_RENDERER_H_INCLUDED
#define GAMELIB_WII_RENDERER_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <gccore.h>
#include <wiiuse/wpad.h>
#include <fat.h>

#include <VertexBuffer.h>
#include <Texture.h>
#include <AABB.h>
#include <Sprite.h>
#include <GameMath/GameMath.h>

namespace GameLib
{
namespace Wii
{

class Renderer
{
public:

    Renderer();
    ~Renderer();
    
    bool Init(int width, int height, bool fullScreen, const char* title);
    
    void SetBGColor(uint8_t r, uint8_t g, uint8_t b) {
        mBgColor.r = r;
        mBgColor.g = g;
        mBgColor.b = b;
    }
    
    void SetTexture(Texture& tex) {
        GX_SetTevOp(GX_TEVSTAGE0, GX_REPLACE);
        GX_SetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD0, GX_TEXMAP0, GX_COLOR0A0);
        GX_LoadTexObj(&tex.mTexture, GX_TEXMAP0);
    }
    //void DisableDepthTest() { GX_SetZMode(GX_FALSE, 0,0); }
    //void EnableDepthTest() { GX_SetZMode(GX_TRUE, GX_LEQUAL, GX_TRUE); }
    
    void Clear() {
        mBgColor.r = 0;
        mBgColor.g = 0x80;
        mBgColor.b = 0x80;
        // clears the bg to color and clears the zbuffer
        GX_SetCopyClear(mBgColor, 0x00FFFFFF);
        
        // do this before drawing
        GX_SetViewport(0, 0, mRMode->fbWidth, mRMode->efbHeight, 0, 1);
        
        //GX_SetTevOp(GX_TEVSTAGE0, GX_REPLACE);
        //GX_SetTevOrder(GX_TEVSTAGE0, GX_TEXCOORD0, GX_TEXMAP0, GX_COLOR0A0);
        //GX_SetTevOp(GX_TEVSTAGE0, GX_REPLACE);
        //GX_SetTevOrder(GX_TEVSTAGE0, GX_TEXCOORDNULL, GX_TEXMAP_NULL, GX_COLOR0A0);
    }
    void Update();
    
    void DrawVertexBuffer(
        GameMath::Mat4& modelMat,
        GameMath::Mat4& viewMat,
        const VertexBuffer& vb
    );

    void DrawAabb(AABB& aabb, GameMath::Mat4& viewMat);
    void DrawSprite(Sprite& sprite);
    

    GameMath::Mat4& GetProjMat() { return mPerspMat; }
    int GetWidth() const { return mRMode->viWidth; }
    int GetHeight() const { return mRMode->viHeight; }

private:
    
    u32 mFbIndex = 0;
    void* mFrameBuffer[2] = { NULL, NULL };
    GXRModeObj* mRMode;
    
    GameMath::Mat4 mPerspMat;
    
    GXColor mBgColor = { 0, 0, 0, 0xff };
    
    f32 yscale;
    u32 xfbHeight;
    
    GXTexObj texture;
    TPLFile neheTPL;
};

} // namespace Wii
} // namespace GameLib

#endif // RENDERER_H_INCLUDED

