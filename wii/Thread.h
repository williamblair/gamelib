#ifndef GAMELIB_WII_THREAD_H_INCLUDED
#define GAMELIB_WII_THREAD_H_INCLUDED

#include <map>
#include <cstdint>

#include <gccore.h>

//typedef uint64_t u64;

namespace GameLib
{
namespace Wii
{

// Thread function should be of form
// void myFcn(void* arg)
typedef void (*ThreadFcn)(void*);

class Thread
{
public:
    Thread();
    ~Thread();

    bool Start(ThreadFcn fcn, void* arg, const char* name);
    void Join(u64* retVal);

    // call from within the ThreadFcn to get the current thread ID
    static u64 GetThreadId();
    // call from within the ThreadFcn to yield
    static void Yield();
    // must call this from within the ThreadFcn upon exit
    static void Exit(u64 retVal);

private:

    ThreadFcn mFcn;
    void* mArg;
    lwp_t mLwp; // the thread handle
    uint8_t mStack[8192];

    bool mStarted;
    bool mJoined;

    static const uint8_t sThreadPrio = 80; // priority is 0..127
    static std::map<lwp_t,u64> sRetVals;
    // Wii thread function requires returning a void*
    static void* sThreadFcnWrapper(void* arg);
};

} // namespace Wii
} // namespace GameLib

#endif // GAMELIB_WII_THREAD_H_INCLUDED

