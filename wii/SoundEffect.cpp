#include <SoundEffect.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <gccore.h>
#include <oggplayer.h>
#include <asndlib.h>
#include <bjdebug.h>

namespace GameLib
{
namespace Wii
{

SoundEffect::SoundEffect() :
    mBuffer(nullptr),
    mBufferLen(0),
    mVoice(1)
{}

SoundEffect::~SoundEffect()
{
    if (mBuffer != nullptr) {
        delete[] mBuffer;
        mBuffer = nullptr;
    }
}

bool SoundEffect::Init(const char* fileName)
{
    FILE* fp = fopen(fileName, "rb");
    if (!fp) {
        dbgprint("Failed to open %s\n", fileName);
        return false;
    }
    // Get the file size
    fseek(fp, 0, SEEK_END);
    long fileSize = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    mBuffer = new unsigned char[fileSize];
    if (!mBuffer) {
        dbgprint("Failed to alloc music buffer\n");
        fclose(fp);
        return false;
    }
    mBufferLen = fileSize;
    if (fread((void*)mBuffer, 1, fileSize, fp) != (size_t)fileSize) {
        dbgprint("Failed to read music file %s\n", fileName);
        fclose(fp);
        delete[] mBuffer;
        mBuffer = nullptr;
        mBufferLen = 0;
        return false;
    }
    fclose(fp);
    return true;
}

void SoundEffect::Play(int channel, bool loop) {
    mVoice = 1; // TODO - different channels
    int ret = PlayOgg(
        mBuffer,
        mBufferLen,
        0,
        loop ? OGG_INFINITE_TIME : OGG_ONE_TIME,
        mVoice
    );
    (void)ret;
}

void SoundEffect::Stop(int channel) {
    StopOgg(mVoice);
}

} // namespace Wii
} // namespace GameLib

