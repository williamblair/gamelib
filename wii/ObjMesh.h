#ifndef GAMELIB_WII_OBJ_MESH_H_INCLUDED
#define GAMELIB_WII_OBJ_MESH_H_INCLUDED

#include <GameMath/GameMath.h>
#include <Renderer.h>
#include <VertexBuffer.h>

namespace GameLib
{
namespace Wii
{

class ObjMesh
{
public:

    ObjMesh();
    ~ObjMesh();
    
    bool Init(const char* fileName);

    void Draw(
        GameMath::Mat4& modelMat,
        GameMath::Mat4& viewMat,
        Renderer& render)
    {
        render.DrawVertexBuffer(modelMat, viewMat, mVertBuf);
    }

private:
    VertexBuffer mVertBuf;
};

} // namespace Wii
} // namespace GameLib

#endif // GAMELIB_WII_OBJ_MESH_H_INCLUDED
